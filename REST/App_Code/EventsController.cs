﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Xml;

public class EventsController : ApiController
{
    [CacheFilter(TimeDuration = 300)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/

    // GET api/<controller>
    public HttpResponseMessage Get(string filter="")
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        if (filter == "") {
            filter = "community-events-district-wide";
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            //Read XML from 25Live feed
            ////string feedURL = "http://25livepub.collegenet.com/calendars/upcoming-event-list.xml";
            string feedURL = "http://25livepub.collegenet.com/calendars/" + filter + ".rss?events=4";
            XmlDocument feedContent = new XmlDocument();
            feedContent.Load(feedURL);

            //Format XML for High Monkey use
            // string undesired = "<gd:where valueString=\"\" />";
            // string feedModified = feedContent.OuterXml.Replace(undesired, "");
            //feedModified = feedModified.Replace("<link rel=\"alternate\" type=\"text/html\" href=\"http://25livepub.collegenet.com/calendars/upcoming-event-list?eventid", "<rawlink>http://25livepub.collegenet.com/calendars/upcoming-event-list?eventid");
            //feedModified = feedModified.Replace("xml\" /><author><name>", "xml\"   /> <author > <name > ");
            //feedModified = feedModified.Replace("\" /><author><name>","</rawlink><author><name>");

            string feedModified = feedContent.OuterXml;

            //return Ok(new { feedModified });
            return new HttpResponseMessage()
            {
                Content = new StringContent(feedModified, Encoding.UTF8, "application/xml")
            };
        }
        else
        {
            return null;
        }
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
