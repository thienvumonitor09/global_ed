﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Http;
using System.Xml;

public class AthleticEventsController : ApiController
{

    [CacheFilter(TimeDuration = 90)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/


    // GET api/<controller>
    public IHttpActionResult Get()
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            DataTable dt = new DataTable();
            string selectCommand = "SELECT [Sport],[Location],[OpponentEtc],[LiveFeedURL],[LiveStatsURL],[CurrentScore],[CurrentScoreDateTime],[FinalScore],[LeagueRecord]" +
                                     ",[OverallRecord],[LeagueGame],[DoubleHeader],[ResultsURL],[Canceled],[Venue],[StartHour],[StartMinute],[StartTimeTBA],[StartAMPM]" +
                                     ",[StartDate],[EndDate],[EasternRegionGame],[NonConferenceGame] " +
                                   "FROM[KenticoCCSNew].[dbo].[athleticeventscustomtable_AthleticEvents] order by StartDate, StartHour, StartMinute, StartAMPM";
            using (var con = new SqlConnection("Data Source=CCS-SFCC-SQL;Initial Catalog=KenticoCCSNew;Integrated Security=SSPI;"))
            using (var cmd = new SqlCommand(selectCommand, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(dt);
            }

            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();

                foreach (DataColumn col in dt.Columns)
                {
                    dict[col.ColumnName] = row[col];
                }
                list.Add(dict);
            }


            /*   ---FILE CACHING - EXPERIMENTAL---
            DataContractSerializer serializer = new DataContractSerializer(list.GetType(), new[] { typeof(DBNull) });
            string xmlVersion = "";

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter writer = new XmlTextWriter(sw))
                {
                    // add formatting so the XML is easy to read in the log
                    writer.Formatting = Formatting.Indented;

                    serializer.WriteObject(writer, list);

                    writer.Flush();

                    xmlVersion = sw.ToString();
                }
            } 

            System.DateTime currentTime = System.DateTime.Now;

            string filepath = "e:\\WebSites\\external.spokane.edu_dev_5150\\REST\\cached\\data_" + currentTime.Minute + "-" + currentTime.Second + "_.xml";
            File.WriteAllText(filepath, xmlVersion); */

            return Ok(new { data = list });
        }
        else
        {
            return Ok(new { status = "Request not supported." });
        }
        //return Ok( new { blah=7, data = list });
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
