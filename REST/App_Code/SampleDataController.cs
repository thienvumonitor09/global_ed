﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

public class SampleDataController : ApiController
{
    [CacheFilter(TimeDuration = 600)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/
    // GET api/<controller>
    public IHttpActionResult Get()
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = true;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            string jsonString = @"{
  ""AreaOfStudy"": [
    {
      ""ID"": 1,
      ""Title"": ""Accounting"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Accounting"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 95,
      ""Title"": ""Addiction Studies"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Addiction-Studies"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 2,
      ""Title"": ""Administrative Assistant"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Administrative-Support/Administrative-Assistant"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 84,
      ""Title"": ""Administrative Assistant"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Administrative-Assistant"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 21,
      ""Title"": ""Administrative Office Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Administrative-Support/Administrative-Office-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 85,
      ""Title"": ""Administrative/Computer Specialist"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Administrative-Computer-Specialist"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 71,
      ""Title"": ""Agriculture"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Agriculture"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 156,
      ""Title"": ""Applied Management Bachelors"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Bachelors/Applied-Management-Bachelor-of-Applied-Science"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 33,
      ""Title"": ""Apprenticeships"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Apprenticeships"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 204,
      ""Title"": ""Arboriculture Urban Forestry"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Arboriculture-Urban-Forestry"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 34,
      ""Title"": ""Architecture"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Architecture"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 101,
      ""Title"": ""Audio Engineering"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Auto-Engineering"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 36,
      ""Title"": ""Automotive Collision and Refinishing"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Automotive/Collision-Refinishing"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 38,
      ""Title"": ""Automotive Maintenance and Light Repair"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Automotive/Maintenance-Light-Repair"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 39,
      ""Title"": ""Automotive Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Automotive/Automotive-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 40,
      ""Title"": ""Automotive Toyota T-Ten"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Automotive/Toyota-T-Ten"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 35,
      ""Title"": ""Aviation Maintenance Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Aviation-Maintenance-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 77,
      ""Title"": ""Baking"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Baking"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 52,
      ""Title"": ""Biomedical Equipment"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Biomedical-Equipment"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 88,
      ""Title"": ""Business and Software Applications"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Business/Business-Software-Applications"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 10,
      ""Title"": ""Business General"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Business/Business-General"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 89,
      ""Title"": ""Business Management"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Business/Business-Management"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 11,
      ""Title"": ""Business Occupations"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Business/Business-Occupations"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 9,
      ""Title"": ""Business Software Specialist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Business/Business-Software-Specialist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 90,
      ""Title"": ""Business Technology and Software Specialist"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Business/Business-Technology-Software-Specialist"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 8,
      ""Title"": ""Business Writing"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Business/Business-Writing"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 17,
      ""Title"": ""CAD Design and Drafting"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/STEM/CAD-Design-and-Drafting"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 3,
      ""Title"": ""Clerical Assistant"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Administrative-Support/Clerical-Assistant"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 115,
      ""Title"": ""Computer Forensics / Network Security"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/science-technology-engineering-mathematics/Computer-Science/Computer-Forensics-Network-Security"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 116,
      ""Title"": ""Computer Network Support"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/science-technology-engineering-mathematics/Computer-Science/Computer-Network-Support"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 122,
      ""Title"": ""Computer Science & Information Systems"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/science-technology-engineering-mathematics/Computer-Science/Computer-Science"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 78,
      ""Title"": ""Cosmetology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Cosmetology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 24,
      ""Title"": ""Criminal Justice"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Criminal-Justice"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 79,
      ""Title"": ""Culinary"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Culinary"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 12,
      ""Title"": ""Customer Service Representative"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Business/Business-Occupations"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 118,
      ""Title"": ""Cyber Security"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Bachelors/Cyber-Security-Bachelor-of-Applied-Science"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 53,
      ""Title"": ""Dental"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Dental"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 54,
      ""Title"": ""Diagnostic Medical Sonography"",
      ""WebsiteURL"": ""https://scc.spokane.edu/Diagnostic-Medical-Sonography"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 72,
      ""Title"": ""Diesel/Heavy Duty Equipment"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Diesel-Heavy-Duty-Equipment"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 91,
      ""Title"": ""Digital Marketing"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Digital-Marketing-Specialist"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 102,
      ""Title"": ""Digital Media Production"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Digital-Media-Production"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 96,
      ""Title"": ""Early Childhood Education"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Early-Childhood-Education"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 55,
      ""Title"": ""Echocardiography"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Echocardiography"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 97,
      ""Title"": ""Education Paraprofessional - Special Education"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Education-Paraprofessional-Special-Education"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 18,
      ""Title"": ""Electrical Maintenance and Automation"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Science,-Technology,-Mathematics,-Computing-Engi/Electronics/Electrical-Maintenance-and-Automation"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 20,
      ""Title"": ""Electrical Trainee"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Science,-Technology,-Mathematics,-Computing-Engi/Electronics/Electrical-Trainee"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 19,
      ""Title"": ""Electronics Engineering"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Science,-Technology,-Mathematics,-Computing-Engi/Electronics/Electronics-Engineering"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 56,
      ""Title"": ""Emergency Medical Technician/Paramedic (EMT)"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/EMT"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 13,
      ""Title"": ""Entrepreneurship"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Entrepreneurship"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 80,
      ""Title"": ""Esthetician"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Esthetician"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 104,
      ""Title"": ""Fine Arts"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Fine-Arts"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 26,
      ""Title"": ""Fire Science"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Fire-Science"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 5,
      ""Title"": ""Front Office Professional"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Administrative-Support/Front-Office-Professional"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 105,
      ""Title"": ""Graphic Design"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Graphics-Design"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 73,
      ""Title"": ""Greenhouse/Nursery"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Greenhouse-Nursery"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 57,
      ""Title"": ""Health Information Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Health-Information-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 110,
      ""Title"": ""Health/Fitness Technician"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Health/Health-Fitness-Technician"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 111,
      ""Title"": ""Hearing Instrument Specialist"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Health/Hearing-Instrument-Specialist"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 51,
      ""Title"": ""Heating, Ventilation, Air-Conditioning and Refrigeration"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/HVACR"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 81,
      ""Title"": ""Hotel & Restaurant Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Hotel-Restaurant-Management-(1)"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 42,
      ""Title"": ""Hydraulic and Pneumatic Automation"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Hydraulic-Pneumatic-Automation"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 119,
      ""Title"": ""Information Technology"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/science-technology-engineering-mathematics/Computer-Science/Information-Technology"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 106,
      ""Title"": ""Interior Design"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Interior-Design"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 98,
      ""Title"": ""Interpreter Training"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Interpreter-Training"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 59,
      ""Title"": ""Invasive Cardiovascular Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Invasive-Cardiovascular-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 74,
      ""Title"": ""Landscape Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Landscape-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 27,
      ""Title"": ""Legal Administration"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Legal-Administration"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 4,
      ""Title"": ""Legal Administrative Assistant"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Legal-Administrative-Assistant"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 31,
      ""Title"": ""Legal Nurse"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Legal-Nurse"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 28,
      ""Title"": ""Legal Office Software Specialist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Legal-Office-Software-Specialist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 6,
      ""Title"": ""Legal Receptionist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Legal-Receptionist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 99,
      ""Title"": ""Library and Information Services"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Library-and-Information-Services"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 43,
      ""Title"": ""Line Construction - Avista"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Line-Constructor-Avista"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 44,
      ""Title"": ""Machinist - CNC Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Machinist-CNC-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 15,
      ""Title"": ""Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 82,
      ""Title"": ""Manicurist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Food-Personal-Services/Manicurist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 14,
      ""Title"": ""Marketing"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Marketing"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 60,
      ""Title"": ""Medical Assistant"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Medical-Assistant"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 62,
      ""Title"": ""Medical Office Billing and Coding Specialist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Medical-Office-Support/Medical-Office-Billing-and-Coding-Specialist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 63,
      ""Title"": ""Medical Office Receptionist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Medical-Office-Support/Medical-Office-Receptionist"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 64,
      ""Title"": ""Medical Office Specialist"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Medical-Office-Support/Medical-Transcription"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 108,
      ""Title"": ""Music"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Music"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 75,
      ""Title"": ""Natural Resource Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Natural-Resource-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 22,
      ""Title"": ""Network Design and Administration"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Science,-Technology,-Mathematics,-Computing-Engi/Network-Design-and-Administration-(1)"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 65,
      ""Title"": ""Nursing"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Nursing"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 112,
      ""Title"": ""Occupational Therapy Assistant"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Health/Occupational-Therapy-Assistant"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 92,
      ""Title"": ""Office Assistant"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Office-Assistant"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 113,
      ""Title"": ""Orthotics and Prosthetics"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Health/Orthotics-Prosthetics-Technician"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 32,
      ""Title"": ""Paralegal"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Legal,-Public-Behavioral-Services/Legal-Careers/Paralegal"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 66,
      ""Title"": ""Pharmacy"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Pharmacy"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 109,
      ""Title"": ""Photography"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Photography"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 114,
      ""Title"": ""Physical Therapist Assistant"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Health/Physical-Therapist-Assistant"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 16,
      ""Title"": ""Project Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Business-Office-Professionals/Project-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 67,
      ""Title"": ""Radiology Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Radiology-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 68,
      ""Title"": ""Respiratory Care"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Respiratory-Care"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 93,
      ""Title"": ""Retail Management"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Retail-Management"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 205,
      ""Title"": ""Small Farm Production"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Small-Farm-Production"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 94,
      ""Title"": ""Social Media Marketing"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/Business-Marketing/Social-Media-Marketing"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 100,
      ""Title"": ""Social Services"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/education-social-behavioral-science/Social-Services-Gerontology"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 23,
      ""Title"": ""Software Development"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Science,-Technology,-Mathematics,-Computing-Engi/Software-Development-(2)"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 69,
      ""Title"": ""Surgical Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Surgical-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 103,
      ""Title"": ""Theatre"",
      ""WebsiteURL"": ""https://sfcc.spokane.edu/What-to-Study/humanities-arts-design/Theatre"",
      ""College"": ""SFCC""
    },
    {
      ""ID"": 83,
      ""Title"": ""Transfer Degrees"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/transfer-options"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 203,
      ""Title"": ""Transfer Degrees"",
      ""WebsiteURL"": null,
      ""College"": ""SFCC""
    },
    {
      ""ID"": 45,
      ""Title"": ""Truck Driver Training (CDL)"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Truck-Driver-Training-CDL"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 70,
      ""Title"": ""Vascular Technology"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Healthcare/Vascular-Technology"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 76,
      ""Title"": ""Water Resources Management"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/The-Natural-World/Water-Resources-Management"",
      ""College"": ""SCC""
    },
    {
      ""ID"": 46,
      ""Title"": ""Welding and Fabrication"",
      ""WebsiteURL"": ""https://scc.spokane.edu/What-to-Study/Hands-on-Building-Trades/Welding-Fabrication"",
      ""College"": ""SCC""
    }
  ]
}";

            var jo = JObject.Parse(jsonString);

            return Ok(new { data = jo });
        }
        else
        {
            return Ok(new { status = "Request not supported." });
        }
        //return Ok( new { blah=7, data = list });
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
