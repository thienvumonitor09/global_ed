﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

public class CompositeSportsEventsController : ApiController
{
    // GET api/<controller>
    public HttpResponseMessage Get(string filter = "", string mode = "", string max = "")
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {         

            //Handle max if present
            int imax = 0;
            if (max != "")
            {
                Int32.TryParse(max, out imax);
            }
            if (imax == 0)
            {
                imax = 1000;
            }

            //Handle filter if present
            switch (filter.ToLower())
            {
                case "menxc":
                    filter = "men's cross";
                    break;
                case "womenxc":
                    filter = "women's cross";
                    break;
                case "baseball":
                    filter = "men's cross";
                    break;
                case "menbasketball":
                    filter = "men's basket";
                    break;
                case "womenbasketball":
                    filter = "women's basket";
                    break;
                case "mengolf":
                    filter = "men's golf";
                    break;
                case "womengolf":
                    filter = "women's golf";
                    break;
                case "softball":
                    filter = "softball";
                    break;
                case "mensoccer":
                    filter = "men's soccer";
                    break;
                case "womensoccer":
                    filter = "women's soccer";
                    break;
                case "mentennis":
                    filter = "men's tennis";
                    break;
                case "womentennis":
                    filter = "women's tennis";
                    break;
                case "mentf":
                    filter = "men's track";
                    break;
                case "womentf":
                    filter = "women's track";
                    break;
                case "volleyball":
                    filter = "volleyball";
                    break;
                default:
                    if (filter.Length > 20) //Eliminate any filter values longer than 20 characters, otherwise any filter will be accepted as a category substring match
                    {
                        filter = "";
                    }
                    break;
            }

            string modeSpec = "";
            string threshDate= "2017-05-01"; //will include all events after May 1, 2017 by default

            //Handle view mode
            if (mode.ToLower() == "upcoming")
            {
                modeSpec = "&view=1"; //original idea, but the upcoming NWAC view (1) only shows events which are coming up within the next week or so, it seems - so using threshDate approach
                modeSpec = "";
                DateTime curTime = DateTime.Now;
                threshDate = curTime.Year + "-";
                if (curTime.Month < 10) {
                    threshDate += "0";
                }
                threshDate += curTime.Month.ToString();
                if (curTime.Day < 10) {
                    threshDate += "0";
                }
                threshDate += curTime.Day.ToString();
                //beginningDate = "2017-11-27";
            }
            else if (mode.ToLower() == "results")
            {
                modeSpec = "&view=2";
            }

            ///XmlDocument feedContent = new XmlDocument();
            ///feedContent.Load(feedURL);

            //Read XML from NWAC feeds
            XDocument mbkbFeedContent = XDocument.Load("http://nwacstats.org/sports/mbkb/composite?print=rss" + modeSpec);
            XDocument wbkbFeedContent = XDocument.Load("http://nwacstats.org/sports/wbkb/composite?print=rss" + modeSpec);
            XDocument msocFeedContent = XDocument.Load("http://nwacstats.org/sports/msoc/composite?print=rss" + modeSpec);
            XDocument wsocFeedContent = XDocument.Load("http://nwacstats.org/sports/wsoc/composite?print=rss" + modeSpec);
            XDocument wvballFeedContent = XDocument.Load("http://nwacstats.org/sports/wvball/composite?print=rss" + modeSpec);

            //Convert to string in order to enable replacing namespaced (colon-including) element names
            //Replace namespaced element names

            string mbkbFeedContentFixed = mbkbFeedContent.ToString().Replace("ps:score", "score");
            mbkbFeedContentFixed = mbkbFeedContentFixed.Replace("dc:date", "date");
            mbkbFeedContentFixed = mbkbFeedContentFixed.Replace("ps:opponent", "opponent");

            string wbkbFeedContentFixed = wbkbFeedContent.ToString().Replace("ps:score", "score");
            wbkbFeedContentFixed = wbkbFeedContentFixed.Replace("dc:date", "date");
            wbkbFeedContentFixed = wbkbFeedContentFixed.Replace("ps:opponent", "opponent");

            string msocFeedContentFixed = msocFeedContent.ToString().Replace("ps:score", "score");
            msocFeedContentFixed = msocFeedContentFixed.Replace("dc:date", "date");
            msocFeedContentFixed = msocFeedContentFixed.Replace("ps:opponent", "opponent");

            string wsocFeedContentFixed = wsocFeedContent.ToString().Replace("ps:score", "score");
            wsocFeedContentFixed = wsocFeedContentFixed.Replace("dc:date", "date");
            wsocFeedContentFixed = wsocFeedContentFixed.Replace("ps:opponent", "opponent");

            string wvballFeedContentFixed = wvballFeedContent.ToString().Replace("ps:score", "score");
            wvballFeedContentFixed = wvballFeedContentFixed.Replace("dc:date", "date");
            wvballFeedContentFixed = wvballFeedContentFixed.Replace("ps:opponent", "opponent");

            //Convert back to XDocument to enable Linq to XML query
            mbkbFeedContent = XDocument.Parse(mbkbFeedContentFixed);
            wbkbFeedContent = XDocument.Parse(wbkbFeedContentFixed);
            msocFeedContent = XDocument.Parse(msocFeedContentFixed);
            wsocFeedContent = XDocument.Parse(wsocFeedContentFixed);
            wvballFeedContent = XDocument.Parse(wvballFeedContentFixed);

            XDocument resultCollated = new XDocument(
                                 new XElement("Events",
                                   new XElement("mbkbSpokaneRelated",
                       (from item in mbkbFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter) && String.Compare(item.Element("date").Value, threshDate) > 0)
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax)),
                                   new XElement("wbkbSpokaneRelated",
                       (from item in wbkbFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter) && String.Compare(item.Element("date").Value, threshDate) > 0)
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax)),
                                   new XElement("msocSpokaneRelated",
                       (from item in msocFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter) && String.Compare(item.Element("date").Value, threshDate) > 0)
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax)),
                                   new XElement("wsocSpokaneRelated",
                       (from item in wsocFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter) && String.Compare(item.Element("date").Value, threshDate) > 0)
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax)),
                                   new XElement("wvballSpokaneRelated",
                       (from item in wvballFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter) && String.Compare(item.Element("date").Value, threshDate) > 0)
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax))
                                   ));

            XDocument result = new XDocument(
                                new XElement("Events",
                                  new XElement("SpokaneRelated",
                      (from item in resultCollated.Descendants("Event")
                       where (item.Element("Title").Value.ToLower().Contains("spokane") && item.Element("Category").Value.ToLower().Contains(filter))
                       orderby (string)item.Element("Date").Value
                       select new XElement("Event",
                               new XElement("Title", item.Element("Title").Value),
                               new XElement("Link", item.Element("Link").Value),
                               new XElement("Description", item.Element("Description").Value),
                               new XElement("Category", item.Element("Category").Value),
                               new XElement("Published", item.Element("Published").Value),
                               new XElement("GUID", item.Element("GUID").Value),
                               new XElement("Date", item.Element("Date").Value),
                               new XElement("Score", item.Element("Score").Value),
                               new XElement("Opponent", item.Element("Opponent").Value))).Take(imax))));



            ////string feedModified = feedContent.OuterXml;
            string feedModified = result.ToString();

            //return Ok(new { feedModified });
            return new HttpResponseMessage()
            {
                Content = new StringContent(feedModified, Encoding.UTF8, "application/xml")
            };
        }
        else
        {
            return null;
        }
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
