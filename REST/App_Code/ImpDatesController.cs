﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

public class ImpDatesController : ApiController
{
    [CacheFilter(TimeDuration = 600)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/
    // GET api/<controller>
    public HttpResponseMessage Get(string mode="scc", string filter="financial-aid-priority")
    {
        var origin = "not found";

        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = true;

        if (testMode || origin.Contains("spokane.edu/"))
        {


            //Read XML from 25Live feed
            string feedURL = "http://25livepub.collegenet.com/calendars/scc-important-dates.rss";
            if (mode.ToLower().Contains("sfcc"))
            {
                feedURL = "http://25livepub.collegenet.com/calendars/sfcc-important-dates.rss";
            }
            if (mode.ToLower().Contains("global-ed"))
            {
                feedURL = "http://25livepub.collegenet.com/calendars/global-ed.rss";
            }

            string dateRangeSpec = "?startdate=**STARTDATE**&enddate=**ENDDATE**";
            string startDate = "0401";
            string endDate = "0401";
            int maxEvents = 99;
            bool correctFormatting = false;

            DateTime currentMoment = DateTime.Now;
            int startYear = currentMoment.Year;
            int endYear = startYear + 1;

            /* DATE RANGE LOGIC PRIOR TO 07/26/2018
            startDate = "0620"; 
            endDate = "0619";
        
            if ((currentMoment.Month > 6) || (currentMoment.Month == 6 && currentMoment.Day > 19))  // Per Mark Baker 2018-06-07 - switch AY to new AY on June 20 
            {
                startDate = "0620"; // Per Mark Baker 2018-06-07 - starting 6/20, stop including any current AY items prior to 6/20
            }
            else
            {
                startYear--;
                endYear--;
                if (currentMoment.Month > 3) // Per Mark Baker 2018-06-07 - from 4/1 to 6/19, include next AY items and exclude any current AY items prior to 4/1
                {
                    endYear++;
                    startDate = "0401";
                }
            }
           */

            /* CURRENT DATE RANGE LOGIC AS OF 07/26/2018 */
            startDate = "0401";
            endDate = "0401";

            if (currentMoment.Month > 3) // Per Gretje Witt 2018-07-26 - Show all dates from April 1 to April 1, switch over on April 1 each year
            {
                endYear++;
            }
            else
            {
                startYear--;
                endYear--;
            }

            /* DATE RANGE OVERRIDE FOR 3 MONTH SHADOW MODE used with quarter start info views ADDED 2018-08-30 BN    Modified to include first day of three months back through a year from current date 2018-12-13 BN */
            if (mode.ToLower().Contains("shadow"))
            {
                startYear = currentMoment.Year;
                endYear = startYear + 1;
                if (currentMoment.Month > 3)
                {
                    startDate = "0" + (currentMoment.Month - 3) + "01";
                }
                else
                {
                    startDate = "" + (currentMoment.Month + 9) + "01";
                    startYear--;
                }
                if (currentMoment.Month < 10)
                {
                    endDate = "0" + currentMoment.Month;
                }
                else
                {
                    endDate = "" + currentMoment.Month;
                }
                if (currentMoment.Day < 10)
                {
                    endDate += "0" + currentMoment.Day;
                }
                else
                {
                    endDate += "" + currentMoment.Day;
                }
                //Handle leap year possibility
                if (endDate == "0229")
                {
                    endDate = "0228";
                }
            }

            /* DATE RANGE OVERRIDE for display of only the first upcoming new student application deadline date - https://ccspokane.freshservice.com/helpdesk/tickets/47140 */
            if (filter.ToLower().Equals("new-student-admission-application-deadline"))
            {
                startYear = currentMoment.Year;
                endYear = startYear + 1;
                if (currentMoment.Month < 10)
                {
                    startDate = "0" + currentMoment.Month;
                }
                else
                {
                    startDate = "" + currentMoment.Month;
                }
                if (currentMoment.Day < 10)
                {
                    startDate += "0" + currentMoment.Day;
                }
                else
                {
                    startDate += "" + currentMoment.Day;
                }
                endDate = startDate; // Same month and day, next year
                maxEvents = 1; // limit to only the first result                
            }

            /* DATE RANGE OVERRIDE and correctFormatting flag setting for registration begin dates */
            if (filter.ToLower().Equals("registration-begins"))
            {
                startYear = currentMoment.Year;
                endYear = startYear + 1;
                if (currentMoment.Month > 3) // similar to shadow mode above
                {
                    startDate = "0" + (currentMoment.Month - 3);
                }
                else
                {
                    startDate = "" + (currentMoment.Month + 9);
                    startYear--;
                    endYear--;
                }
                if (currentMoment.Day < 10)
                {
                    startDate += "0" + currentMoment.Day;
                }
                else
                {
                    startDate += "" + currentMoment.Day;
                }
                endDate = startDate; // Same month and day, next year
                correctFormatting = true;
                maxEvents = 9; // Limit to only the first nine results             
            }

            dateRangeSpec = dateRangeSpec.Replace("**STARTDATE**", "" + startYear + startDate);
            dateRangeSpec = dateRangeSpec.Replace("**ENDDATE**", "" + endYear + endDate);

            feedURL += dateRangeSpec;

            XDocument allFeedContent = XDocument.Load(feedURL);

            filter = filter.ToLower().Replace("-", " "); // Format filter specifier for string matching

            XDocument result = new XDocument(
                                 new XElement("ImpDates",
                       from item in allFeedContent.Descendants("item")
                       where (item.Element("title").Value.ToLower().Contains(filter))
                       orderby (string)item.Element("category").Value
                       select new XElement("Date",
                              new XElement("Title", item.Element("title").Value),
                              new XElement("Link", item.Element("link").Value),
                              new XElement("Description", item.Element("description").Value),
                              new XElement("Category", item.Element("category").Value))));


            // Remove any duplicate nodes
            var alldates = result.Descendants("Date");
            string currentNodeContents = "";
            foreach (var dt in alldates)
            {
                if (currentNodeContents.Contains("|||" + dt.Descendants("Title").FirstOrDefault().ToString() + "||" + dt.Descendants("Category").FirstOrDefault().ToString() + "|||")) // empty the node if it is an exact duplicate of another node [Actually need to capture just Title and Category rather than all nodes]
                {
                    dt.RemoveAttributes();
                    dt.RemoveNodes();
                }
                else
                {
                   currentNodeContents += "|||" + dt.Descendants("Title").FirstOrDefault().ToString() + "||" + dt.Descendants("Category").FirstOrDefault().ToString() + "|||";
                }
            }

            // Limit number of results by emptying any nodes after maxEvents count is reached
            alldates = result.Descendants("Date");
            int datecounter = 1;
            foreach (var dt in alldates)
            {
                if (datecounter > maxEvents)
                {
                    dt.RemoveAttributes();
                    dt.RemoveNodes();
                }
                if (dt.HasElements)
                {
                    datecounter++;
                }
            }



            ////string feedModified = feedContent.OuterXml;
            string feedModified = "";
            if (correctFormatting)
            {
                feedModified = result.ToString().Replace(" for", " - for").Replace("(for ", "(- for ").Replace("for Students", "for students").Replace("  <Date />\r\n", ""); // Standardize errant text and remove any empty Date elements
            }
            else
            {
                feedModified = result.ToString().Replace("  <Date />\r\n",""); // Remove any empty Date elements
            }

            //return Ok(new { feedModified });
            return new HttpResponseMessage()
            {
                Content = new StringContent(feedModified, Encoding.UTF8, "application/xml")
            };
        }
        else
        {
            return null;
        }
    }
}
