﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;

public class EmpSearchSpec
{
    public string BooleanLogic;
    public string DivMatch;
    public string DeptMatch;
    public string EmailMatch;
    public string EmailExclude;
    public string MaxResults;
}

public class MatchingEmployeesController : ApiController
{

    [CacheFilter(TimeDuration = 900)]    //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/
                                        // GET api/<controller>

    public IHttpActionResult Get(string div="All", string dept="All")
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            DataTable dt = new DataTable();
            //using (var con = new SqlConnection("Data Source=CCS-SQLDEV;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var con = new SqlConnection("Data Source=CCSSQL2\\CCSSQLINT2;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var cmd = new SqlCommand("usp_GetMatchingEmployeeDirectoryEntries", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.AddWithValue("@Search", "");
                cmd.Parameters.AddWithValue("@PageNum", 1);
                cmd.Parameters.AddWithValue("@PageSize", 10000); //large page size to send whole data set
                cmd.Parameters.AddWithValue("@DivMatch", div);
                cmd.Parameters.AddWithValue("@DeptMatch", dept);
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }

            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();

                foreach (DataColumn col in dt.Columns)
                {
                    dict[col.ColumnName] = row[col];
                }
                //dict["headers"] = origin;
                //dict["Credentials"] = "Currently not available ~ coming soon";
                if ((dict["Phone"].ToString().Contains("000-0000")) || (dict["Phone"].ToString().Contains("111-1111")))
                {
                    dict["Phone"] = "";
                }
                if ((dict["Phone"].ToString().Contains("(")) || (dict["Phone"].ToString().Contains(")")))
                {
                    dict["Phone"] = dict["Phone"].ToString().Replace("(", "").Replace(")", "-");
                }
                dict["Phone"] = dict["Phone"].ToString().Replace(" ", "");
                if (dict["Phone"].ToString().Equals("Dial711relayoperator-askfor533-8824"))
                {
                    dict["Phone"] = "Dial 711, ask for 533-8824";
                }
                if (dict["Phone"].ToString() == "")
                {
                    dict["Phone"] = "N/A";
                }
                if (row["Title"].ToString().Contains("?"))
                {
                    dict["Title"] = row["Title"].ToString().Replace(" ? ", " - ").Replace("?s", "'s"); // replacing some possible bad characters from AD data
                }
                list.Add(dict);
            }


            /*   ---FILE CACHING - EXPERIMENTAL---
            DataContractSerializer serializer = new DataContractSerializer(list.GetType(), new[] { typeof(DBNull) });
            string xmlVersion = "";

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter writer = new XmlTextWriter(sw))
                {
                    // add formatting so the XML is easy to read in the log
                    writer.Formatting = Formatting.Indented;

                    serializer.WriteObject(writer, list);

                    writer.Flush();

                    xmlVersion = sw.ToString();
                }
            } 

            System.DateTime currentTime = System.DateTime.Now;

            string filepath = "e:\\WebSites\\external.spokane.edu_dev_5150\\REST\\cached\\data_" + currentTime.Minute + "-" + currentTime.Second + "_.xml";
            File.WriteAllText(filepath, xmlVersion); */

            return Ok(new { data = list });
        }
        else
        {
            return Ok(new { status = "Request not supported." });
        }
        //return Ok( new { blah=7, data = list });
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public IHttpActionResult Post(EmpSearchSpec searchSpec)
    {

        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        //searchSpec = new EmpSearchSpec { DeptMatch="info", DivMatch="dev" };

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            //Correct for empty or other incoming values
            searchSpec.BooleanLogic = searchSpec.BooleanLogic.Trim();
            if (searchSpec.BooleanLogic.Length < 4 || searchSpec.BooleanLogic.Length > 6) {
                searchSpec.BooleanLogic = "ANDOR";
            }
            searchSpec.DivMatch = searchSpec.DivMatch.Trim().Replace(";;", ";").Replace(";;", ";").Replace(";;", ";");
            if (searchSpec.DivMatch.Length < 1 || searchSpec.DivMatch.ToUpper() == "ANY")
            {
                searchSpec.DivMatch = "";
            }
            searchSpec.DeptMatch = searchSpec.DeptMatch.Trim().Replace(";;", ";").Replace(";;", ";").Replace(";;", ";");
            if (searchSpec.DeptMatch.Length < 1 || searchSpec.DeptMatch.ToUpper() == "ANY")
            {
                searchSpec.DeptMatch = "";
            }
            searchSpec.EmailMatch = searchSpec.EmailMatch.Trim().Replace(";;", ";").Replace(";;", ";").Replace(";;", ";");
            if (searchSpec.EmailMatch.Length < 1 || searchSpec.EmailMatch.ToUpper() == "NONE")
            {
                searchSpec.EmailMatch = "";
            }
            searchSpec.EmailExclude = searchSpec.EmailExclude.Trim().Replace(";;", ";").Replace(";;", ";").Replace(";;", ";");
            if (searchSpec.EmailExclude.Length < 1 || searchSpec.EmailExclude.ToUpper() == "NONE")
            {
                searchSpec.EmailExclude = "";
            }
            searchSpec.MaxResults = searchSpec.MaxResults.Trim();
            if (searchSpec.MaxResults.Length < 1 || searchSpec.MaxResults.Length > 3)
            {
                searchSpec.MaxResults = "25";
            }

            DataTable dt = new DataTable();
            //using (var con = new SqlConnection("Data Source=CCS-SQLDEV;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var con = new SqlConnection("Data Source=CCSSQL2\\CCSSQLINT2;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var cmd = new SqlCommand("usp_GetMatchingEmployeeDirectoryEntries", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.AddWithValue("@Search", "");
                cmd.Parameters.AddWithValue("@PageNum", 1);
                cmd.Parameters.AddWithValue("@PageSize", 10000); //large page size to send whole data set
                cmd.Parameters.AddWithValue("@MaxResults", searchSpec.MaxResults);
                cmd.Parameters.AddWithValue("@BooleanLogic", searchSpec.BooleanLogic);
                cmd.Parameters.AddWithValue("@DivMatch", searchSpec.DivMatch);
                cmd.Parameters.AddWithValue("@DeptMatch", searchSpec.DeptMatch);
                cmd.Parameters.AddWithValue("@EmailMatch", searchSpec.EmailMatch);
                cmd.Parameters.AddWithValue("@EmailExclude", searchSpec.EmailExclude);
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }

            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();

                foreach (DataColumn col in dt.Columns)
                {
                    dict[col.ColumnName] = row[col];
                }
                if (row["Name"].ToString().Split(',').Length > 1)
                {
                    dict["FirstName"] = row["Name"].ToString().Split(',')[1].Trim();
                    dict["LastName"] = row["Name"].ToString().Split(',')[0].Trim();
                }
                else
                {
                    dict["FirstName"] = "Unknown";
                    dict["LastName"] = "Unknown";
                }
                //dict["headers"] = origin;
                //dict["Credentials"] = "Currently not available ~ coming soon";
                list.Add(dict);
            }


            /*   ---FILE CACHING - EXPERIMENTAL--- 
            DataContractSerializer serializer = new DataContractSerializer(list.GetType(), new[] { typeof(DBNull) });
            string xmlVersion = "";

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter writer = new XmlTextWriter(sw))
                {
                    // add formatting so the XML is easy to read in the log
                    writer.Formatting = Formatting.Indented;
                    serializer.WriteObject(writer, list);
                    writer.Flush();
                    xmlVersion = sw.ToString();
                }
            }*/

            //return new HttpResponseMessage()
            //{
            //    Content = new StringContent(xmlVersion, Encoding.UTF8, "application/xml")
            //};

            //System.DateTime currentTime = System.DateTime.Now;

            //string filepath = "e:\\WebSites\\external.spokane.edu_dev_5150\\REST\\cached\\data_" + currentTime.Minute + "-" + currentTime.Second + "_.xml";
            //File.WriteAllText(filepath, xmlVersion); 

            return Ok(new { employees = list });
        }
        else
        {
            return null;
            //return Ok(new { status = "Request not supported." });
        }
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
