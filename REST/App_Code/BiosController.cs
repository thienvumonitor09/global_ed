﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BiosController : ApiController
{
    // GET api/<controller>
    public HttpResponseMessage Get(string filter = "notspec")
    {
        var resp = new HttpResponseMessage(HttpStatusCode.OK);

        string downloadString = "";
        try
        {
            WebClient client = new WebClient();
            downloadString = client.DownloadString("http://shared.spokane.edu/Content-Items/Bios/" + filter);
            if (downloadString.ToLower().Contains("<!-- content block content -->"))
            {
                downloadString = downloadString.Substring(downloadString.IndexOf("<!-- content block content -->") + 31);
            }
            if (downloadString.ToLower().Contains("<!--end content block content -->"))
            {
                downloadString = downloadString.Substring(0, downloadString.IndexOf("<!--end content block content -->"));
            }
            downloadString = downloadString.Trim();
        }
        catch
        {
            downloadString = "No bio found.";
        }
        resp.Content = new StringContent(downloadString, System.Text.Encoding.UTF8, "text/plain");
        return resp;
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
