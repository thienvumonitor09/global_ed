﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Http;
using System.Xml;

public class EmployeesController : ApiController
{

    [CacheFilter(TimeDuration=900)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/
    // GET api/<controller>
    
    public IHttpActionResult Get()
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            DataTable dt = new DataTable();
            //using (var con = new SqlConnection("Data Source=CCS-SQLDEV;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var con = new SqlConnection("Data Source=CCSSQL2\\CCSSQLINT2;Initial Catalog=StudentandEmployeeInfo;Integrated Security=SSPI;"))
            using (var cmd = new SqlCommand("usp_GetEmployeeDirectoryEntries", con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.AddWithValue("@Search", "");
                cmd.Parameters.AddWithValue("@PageNum", 1);
                cmd.Parameters.AddWithValue("@PageSize", 10000); //large page size to send whole data set
                cmd.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }

            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();

                foreach (DataColumn col in dt.Columns)
                {
                    dict[col.ColumnName] = row[col];
                    if (col.ColumnName == "Division") {
                        //add unit prefix to division value
                        if (row["Email"].ToString().Contains("@scc") && !row["Division"].ToString().StartsWith("SCC "))
                        { 
                            dict[col.ColumnName] = "SCC " + dict[col.ColumnName];
                        }                        
                        if (row["Email"].ToString().Contains("@sfcc") && !row["Division"].ToString().StartsWith("SFCC "))
                        { 
                            dict[col.ColumnName] = "SFCC " + dict[col.ColumnName];
                        }
                        if (row["Email"].ToString().Contains("@ccs") && !row["Division"].ToString().StartsWith("CCS "))
                        { 
                            dict[col.ColumnName] = "CCS " + dict[col.ColumnName];
                        }
                    }
                }
                //dict["Credentials"] = "Currently not available ~ coming soon";
                if (dict["Credentials"].ToString().ToLower() == "null") {
                    dict["Credentials"] = "";
                }                
                if ((dict["Phone"].ToString().Contains("000-0000"))||(dict["Phone"].ToString().Contains("111-1111")))
                {
                    dict["Phone"] = "";
                }
                if ((dict["Phone"].ToString().Contains("(")) || (dict["Phone"].ToString().Contains(")")))
                {
                    dict["Phone"] = dict["Phone"].ToString().Replace("(", "").Replace(")","-");
                }
                dict["Phone"] = dict["Phone"].ToString().Replace(" ", "");
                if (dict["Phone"].ToString().Equals("Dial711relayoperator-askfor533-8824"))
                {
                    dict["Phone"] = "Dial 711, ask for 533-8824";
                }
                if (dict["Phone"].ToString()=="")
                {
                    dict["Phone"] = "N/A";
                }
                if (row["Title"].ToString().Contains("?"))
                {
                    dict["Title"] = row["Title"].ToString().Replace(" ? ", " - ").Replace("?s", "'s"); // replacing some possible bad characters from AD data
                }
                //dict["Headers"] = origin;
                if ((dict["Name"].ToString().ToUpper() != "TEST, LDAP")&&(dict["Name"].ToString().ToUpper() != "PITCHER, DARREN"))
                {
                    list.Add(dict);
                }
            }


            /*   ---FILE CACHING - EXPERIMENTAL---
            DataContractSerializer serializer = new DataContractSerializer(list.GetType(), new[] { typeof(DBNull) });
            string xmlVersion = "";

            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter writer = new XmlTextWriter(sw))
                {
                    // add formatting so the XML is easy to read in the log
                    writer.Formatting = Formatting.Indented;

                    serializer.WriteObject(writer, list);

                    writer.Flush();

                    xmlVersion = sw.ToString();
                }
            } 

            System.DateTime currentTime = System.DateTime.Now;

            string filepath = "e:\\WebSites\\external.spokane.edu_dev_5150\\REST\\cached\\data_" + currentTime.Minute + "-" + currentTime.Second + "_.xml";
            File.WriteAllText(filepath, xmlVersion); */

            return Ok(new { data = list });
        }
        else
        {
            return Ok(new { status = "Request not supported." });
        }
        //return Ok( new { blah=7, data = list });
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
