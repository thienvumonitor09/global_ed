﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for WebApiConfig
/// </summary>

public static class WebApiConfig
{
    public static void Register(HttpConfiguration config)

    {
        //config.Formatters.JsonFormatter.SupportedMediaTypes
        //  .Add(new MediaTypeHeaderValue("text/html"));
        config.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional, div = RouteParameter.Optional, dept = RouteParameter.Optional, filter = RouteParameter.Optional, mode = RouteParameter.Optional, max = RouteParameter.Optional } // div and dept added to support GET method for MatchingEmployees - not needed for POST method
        );
    }
}