﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;

public class OriginalSportsEventsController : ApiController
{
    // GET api/<controller>
    public HttpResponseMessage Get(string filter = "", string mode = "", string max = "")
    {
        var origin = "not found";
        if (Request.Headers != null)
        {
            origin = this.Request.Headers.ToString();
        }

        bool testMode = false;

        if (testMode || origin.Contains("spokane.edu/"))
        {
            string feedURL = "http://nwacstats.org/composite?print=rss";

            //special handling due to missing data problems with composite feed
            //construct year portion of path
            string firstPart = (DateTime.Now.Year).ToString(); //for July-December, use current year
            if (DateTime.Now.Month < 7)
            {
                firstPart = (DateTime.Now.Year - 1).ToString(); //for January-June, use previous year
            }
            int secondPart = Int32.Parse(firstPart.Substring(2)) + 1;
            string yearPathPortion = firstPart + "-" + secondPart.ToString();

            //Handle max if present
            int imax = 0;
            if (max != "")
            {
                Int32.TryParse(max, out imax);
            }
            if (imax == 0)
            {
                imax = 1000;
            }

            //Handle filter if present
            switch (filter.ToLower())
            {
                case "menxc":
                    filter = "men's cross";
                    break;
                case "womenxc":
                    filter = "women's cross";
                    break;
                case "baseball":
                    filter = "men's cross";
                    break;
                case "menbasketball":
                    filter = "men's basket";
                    feedURL = "http://nwacstats.org/sports/mbkb/" + yearPathPortion + "/schedule?print=rss";
                    break;
                case "womenbasketball":
                    filter = "women's basket";
                    feedURL = "http://nwacstats.org/sports/wbkb/" + yearPathPortion + "/schedule?print=rss";
                    break;
                case "mengolf":
                    filter = "men's golf";
                    break;
                case "womengolf":
                    filter = "women's golf";
                    break;
                case "softball":
                    filter = "softball";
                    break;
                case "mensoccer":
                    filter = "men's soccer";
                    feedURL = "http://nwacstats.org/sports/msoc/" + yearPathPortion + "/schedule?print=rss";
                    break;
                case "womensoccer":
                    filter = "women's soccer";
                    feedURL = "http://nwacstats.org/sports/wsoc/" + yearPathPortion + "/schedule?print=rss";
                    break;
                case "mentennis":
                    filter = "men's tennis";
                    break;
                case "womentennis":
                    filter = "women's tennis";
                    break;
                case "mentf":
                    filter = "men's track";
                    break;
                case "womentf":
                    filter = "women's track";
                    break;
                case "volleyball":
                    filter = "volleyball";
                    feedURL = "http://nwacstats.org/sports/wvball/" + yearPathPortion + "/schedule?print=rss";
                    break;
                default:
                    if (filter.Length > 20) //Eliminate any filter values longer than 20 characters, otherwise any filter will be accepted as a category substring match
                    {
                        filter = "";
                    }
                    break;
            }

            //Handle view mode
            if (mode.ToLower() == "upcoming")
            {
                feedURL += "&view=1";
            }
            else if (mode.ToLower() == "results")
            {
                feedURL += "&view=2";
            }

            ///XmlDocument feedContent = new XmlDocument();
            ///feedContent.Load(feedURL);

            //Read XML from NWAC feed
            XDocument allFeedContent = XDocument.Load(feedURL);

            //Convert to string in order to enable replacing namespaced (colon-including) element names
            //Replace namespaced element names
            string allFeedContentFixed = allFeedContent.ToString().Replace("ps:score", "score");
            allFeedContentFixed = allFeedContentFixed.Replace("dc:date", "date");
            allFeedContentFixed = allFeedContentFixed.Replace("ps:opponent", "opponent");

            //Convert back to XDocument to enable Linq to XML query
            allFeedContent = XDocument.Parse(allFeedContentFixed);

            XDocument result = new XDocument(
                                 new XElement("Events",
                                   new XElement("SpokaneRelated",
                       (from item in allFeedContent.Descendants("item")
                        where (item.Element("title").Value.ToLower().Contains("spokane") && item.Element("category").Value.ToLower().Contains(filter))
                        orderby (string)item.Element("date").Value
                        select new XElement("Event",
                                new XElement("Title", item.Element("title").Value),
                                new XElement("Link", item.Element("link").Value),
                                new XElement("Description", item.Element("description").Value),
                                new XElement("Category", item.Element("category").Value),
                                new XElement("Published", item.Element("pubDate").Value),
                                new XElement("GUID", item.Element("guid").Value),
                                new XElement("Date", item.Element("date").Value),
                                new XElement("Score", item.Element("score").Value),
                                new XElement("Opponent", item.Element("opponent").Value))).Take(imax))));



            ////string feedModified = feedContent.OuterXml;
            string feedModified = result.ToString();

            //return Ok(new { feedModified });
            return new HttpResponseMessage()
            {
                Content = new StringContent(feedModified, Encoding.UTF8, "application/xml")
            };
        }
        else
        {
            return null;
        }
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
}
