﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;

/// <summary>
/// Summary description for QuarterStartDateController
/// </summary>
public class QuarterStartDateController : ApiController
{
    [CacheFilter(TimeDuration = 600)]   //Value of duration is in seconds; 900 = 15 minutes. Seems to work!  http://www.c-sharpcorner.com/article/implementing-caching-in-web-api/
    // GET api/<controller>
    public HttpResponseMessage Get()
    {
        string site = "scc";
        string feedURL = "http://25livepub.collegenet.com/calendars/"+ site +"-important-dates.rss";
        string startDate = (DateTime.Now.Year - 1) + "0101";
        string endDate = (DateTime.Now.Year + 1) + "1231";
        feedURL += "?startdate=" + startDate + "&enddate=" + endDate;
        //feedURL = "https://external.spokane.edu/REST/API/ImpDates?mode=SCCshadow&filter=first-day";
        XDocument allFeedContent = XDocument.Load(feedURL);

        //filter = filter.ToLower().Replace("-", " "); // Format filter specifier for string matching

        XDocument result = new XDocument(
                             new XElement("ImpDates",
                                       from item in allFeedContent.Descendants("item")
                                       where (item.Element("title").Value.Contains("First Day")
                                                || item.Element("title").Value.Contains("New Student Admission Application Deadline (with no prior college)"))
                                       //orderby (string)item.Element("category").Value
                                       select new XElement("Date",
                                              new XElement("Title", item.Element("title").Value),
                                              new XElement("Category", item.Element("category").Value))));
        string feedModified = "";
        feedModified = result.ToString();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        List<String> demo = new List<String>();
        demo.Add("ok1");
        demo.Add("ok2");

        return new HttpResponseMessage()
        {
            //Content = new StringContent(feedModified, Encoding.UTF8, "application/xml")
            Content = new StringContent(js.Serialize(GetQuarterInfoList2()), Encoding.UTF8, "application/json")
        };
        return null;
       
    }
    public static List<QuarterInfo> GetQuarterInfoList2()
    {
        List<QuarterInfo> quarterInfoList = new List<QuarterInfo>();
        List<Quarter> quarters = GetQuarters();
        List<XDocument> xdocs = CreateXDocumentList("important-dates");
        //Label1.Text = "here";
        foreach (Quarter q in GetQuarters())
        {
            List<Date> infoList = new List<Date>();

            //Get XElement
            List<Date> elementList = GetDate2(q);
            foreach (Date element in elementList)
            {
                infoList.Add(element);
            }

            quarterInfoList.Add(new QuarterInfo(q, infoList));
        }
        return quarterInfoList;
    }



    public static List<Quarter> GetQuarters()
    {
        int m = DateTime.Now.Month;
        int year = DateTime.Now.Year;
        int i = 0; // where to start from quarter list
        if (m == 1)
            i = 0;
        else if (m >= 2 && m <= 4)
            i = 1;
        else if (m >= 5 && m <= 7)
            i = 2;
        else if (m >= 8 && m <= 9)
            i = 3;
        else
            i = 4;
        List<Quarter> quarterList = new List<Quarter>();
        string[] quarters = { "Winter", "Spring", "Summer", "Fall" };
        int count = 1;
        int y = year;
        while (count <= 4)
        {
            if (i >= 4)
            {
                y = year + 1;
            }
            quarterList.Add(new Quarter(quarters[i % 4], y));
            count++;
            i++;
        }
        return quarterList;
    }



    public static List<Date> GetDate2(Quarter q)
    {
        List<XDocument> xdocs = CreateXDocumentList("important-dates");
        List<Date> dateList = new List<Date>();
        foreach (XDocument xdocE in xdocs)
        {
            foreach (XElement d in xdocE.Root.Nodes())
            {
                string category = d.Element("Category").Value;
                string title = d.Element("Title").Value;

                if (title.Contains(q.QuarterName))//Look for the quarter
                {
                    //B/c the "first day" sometimes does not contain Year, we have to match year in category
                    if (title.Contains("First Day") && category.Contains(q.Year.ToString())
                       || title.Contains("Deadline") && title.Contains(q.Year.ToString()))
                    {
                        //Only add if the element does not exist in the dateList
                        if (!dateList.Exists(element => element.Category == category && element.Title == title))
                        {
                            if (!string.IsNullOrEmpty(category))
                            {
                                dateList.Add(new Date() { Title = title, Category = category });
                            }
                        }
                    }
                }
            }
        }
        return dateList;
    }



    public static List<XDocument> CreateXDocumentList(string calendarType)
    {
        List<XDocument> xdocList = new List<XDocument>();

        string startDate = (DateTime.Now.Year - 1) + "0101";
        string endDate = (DateTime.Now.Year + 1) + "1231";
        string feedURL = "";
        List<string> siteList = new List<string>();
        string[] schoolList = { "scc", "sfcc" };
        foreach (string school in schoolList)
        {
            siteList.Add(school);
        }
        List<string> feedURLList = new List<String>();
        //calendarType = important-dates (SCC/SFCC) || global
        foreach (string site in siteList)
        {
            feedURL = "http://25livepub.collegenet.com/calendars/" + site + "-important-dates.rss";
            feedURL += "?startdate=" + startDate + "&enddate=" + endDate;
            feedURLList.Add(feedURL);
        }

        foreach (string strURL in feedURLList)
        {
            XDocument xdoc = XDocument.Load(strURL);
            XDocument convertedXDocument = new XDocument(
                                     new XElement("ImpDates",
                                       from item in xdoc.Descendants("item")
                                       where (item.Element("title").Value.Contains("First Day")
                                                || item.Element("title").Value.Contains("New Student Admission Application Deadline (with no prior college)"))
                                       //orderby (string)item.Element("category").Value
                                       select new XElement("Date",
                                              new XElement("Title", item.Element("title").Value),
                                              new XElement("Category", item.Element("category").Value))));
            xdocList.Add(convertedXDocument);
        }
        return xdocList;
    }

}