﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DegreeDescription2.ascx.cs" Inherits="CMSWebParts_Custom_DegreeDescription2" %>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>

<div class="container">
        <div id="degree" class="brown-border-left">
            <asp:Literal ID="litMsg" runat="server"></asp:Literal>
            <section id="quarterDDsection">
                <div class="content-block">
                    <h4>Viewing Program Version for</h4>
                    <p id="quarterDescription"><asp:DropDownList ID="quarterDD" runat="server" onchange="this.form.submit()" Style="width:205px;"></asp:DropDownList></p>
                </div>
            </section>
            <section id="degreeDescription">
                <div class="content-block">
                    <p id="description"></p>
                </div>
            </section>
            <section id="courseOfStudySection">
                <div class="content-block">
                    <h2>Course of Study</h2>
                    <p id="courseOfStudy"></p>
                </div>
            </section>
            <section id="programGoals">
                <div class="content-block">
                    <h2>Learning Outcomes</h2>
                    <p id="learningOutcomes"></p>
                </div>
            </section>
            <section id="careerOpportunitiesSection">
                <div class="content-block">
                    <h2>Career Opportunities</h2>
                    <p id="careerOpportunities"></p>
                </div>
            </section>
            <section id="schedule">
                <h2>Typical Student Schedule <span style="color:#903923;">for <asp:Literal ID="litQuarter" runat="server"></asp:Literal></span></h2>
            </section>
            <section id="disclaimer">
                <div class="content-block">
                    <p>
                        <i><span class="bold"><u>Disclaimer</u>:</span> The college cannot guarantee courses will be 
                        offered in the quarters indicated. During the period this guide is in 
                        circulation, there may be curriculum revisions and program changes. Students 
                        are responsible for consulting the appropriate academic unit or adviser for 
                        more current and specific information. The information in this guide is subject 
                        to change and does not constitute an agreement between the college and the student.</i>
                    </p>
                </div>
            </section>
        </div>
    </div>

 <!-- START Workarounds BN --> 
    <script src="/CMSWebParts/Custom/DegreeDescription_files/bluebird.min.js"></script> <!--to make promises work in IE - https://stackoverflow.com/questions/36016327/how-to-make-promises-work-in-ie11 - bn 2018-05-14 -->
 <!-- END Workarounds BN -->

    <script type="text/javascript">

        $(function (                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ) {

            //add dropdownlist change events
            //$("select[id$='quarterDD']").change(getProgramDetails);
            //$("select[id$='quarterDD']").change(getProgramAttributes);

        });

        //get query string parameters by name/key
        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

        function appendHTML(html, containerID) {
            $(html).appendTo(containerID);
        }

        //get the gainful employment disclosure
        function getGainfulEmploymentDisclosure(gainfulEmploymentID) { //change to pass in container id and section id
            //disclosure URL
            var url = "http://ccs.spokane.edu/GEDT/" + gainfulEmploymentID + "/gedt.html";
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/icatalogjson.asmx/RemoteFileExists",
                data: { URL: url },
                dataType: "jsonp",
                async: false,
                success: function (response) {
                    //if the disclosure file exists
                    if (response == true) {
                        //append the disclosure to the gainfulEmployment section
                        var disclosure = "<p><a href='" + url + "' target='_blank'>" + url + "</a></p>";
                        $(disclosure).appendTo("#gainfulEmployment");
                    } else {
                        $("#gainfulEmploymentSection").hide(); //temporary until the GE Widget comes back
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#gainfulEmploymentSection").hide(); //temporary until the GE Widget comes back
                }
            });
        }

        function getProgramDetails(programID, strm) {
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetProgramByProgramID",
                data: { ProgramID: programID, STRM: strm },
                dataType: "jsonp",
                success: function (data) {
                    $.each(data.Table, function (index, degree) {
                        console.log(degree.ProgramTitle + ": " + degree.ProgramID + "; ");
                        //display the program title
                        //change this later to include the degree title when there is a one-to-one program-to-degree relationship
                        var programTitle = degree.ProgramTitle;
                        if (programTitle != "Associate in Arts") {
                            $(".page-title").html("<h1>" + programTitle + "</h1>");
                        } else {
                            $(".page-title").html("");
                        }

                        //display the description
                        $("#description").append(degree.ProgramDescription);

                        //display when enrollment starts
                        if (degree.ProgramEnrollment != null && degree.ProgramEnrollment != "") {
                            $("#enrollmentStart").append(degree.ProgramEnrollment)
                        } else {
                            $("#enrollmentStartSection").hide();
                        }

                        //display the program website
                        if (degree.WebsiteURL != null && degree.WebsiteURL != "") {
                            $("#websiteURL").append("<a href='" + degree.WebsiteURL + "'>" + degree.WebsiteURL + "</a>");
                        } else {
                            $("#websiteURLSection").hide();
                        }

                        //display the course of study
                        if (degree.ProgramCourseOfStudy != null && degree.ProgramCourseOfStudy != "") {
                            $("#courseOfStudy").append(degree.ProgramCourseOfStudy);
                        } else {
                            $("#courseOfStudySection").hide();
                        }

                        //display the learning outcomes
                        if (degree.ProgramGoals != null && degree.ProgramGoals != "") {
                            $("#learningOutcomes").append(degree.ProgramGoals);
                        } else {
                            $("#programGoals").hide();
                        }

                        //display the career opportunities if the gainful employment id is null
                        if (degree.ProgramCareerOpportunities != null && degree.ProgramCareerOpportunities != "") {
                            $("#careerOpportunities").append(degree.ProgramCareerOpportunities);
                        } else {
                            $("#careerOpportunitiesSection").hide(); //hide this section if the gainful employment id is null or empty in the getOption function
                        }
                    });
                }
            });
        }

        function getProgramAttributes(programID, strm) {
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetProgramAttributesByProgramID",
                data: { STRM: strm, ProgramID: programID },
                dataType: "jsonp",
                success: function (data) {

                    $("#financialAidSection").hide();
                    $("#tuitionSection").hide();

                    $.each(data.Table, function (index, program) {

                        //display financial aid eligibility
                        if (program.FinancialAidEligible == "Y") {
                            $("#financialAid").append("Yes. Read more about <a href=\"/How-to-Pay-for-College\">Financial Aid</a>.");
                            $("#financialAidSection").show();
                        } else if (program.FinancialAidEligible == "N") {
                            $("#financialAid").append("No");
                            $("#financialAidSection").show();
                        }

                    });
                }
            });
        }

        function currencyFormat(num) {
            //return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
            if (num) {
                return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
            }
            return num;
            
        }

        
        
        //if the document is ready
        $(document).ready(function () {
            
           

            //Degrees/Certificates Offered
            //$("#sidebarContent").append("<div class='content-block'><h4>Degrees/Certificates Offered</h4><p>Please holder for links to other degrees/certificates offered in the same area of study. If none exist, this section will be hidden.</p></div>");

            //Enrollment Start
            $("#sidebarContent").append("<div id='enrollmentStartSection' class='content-block'><h4>Start</h4><p id='enrollmentStart'></p></div>");

            //Website URL
            $("#sidebarContent").append("<div id='websiteURLSection' class='content-block'><h4>Website</h4><p id='websiteURL'></p></div>");

            //Locations Offered
            $("#sidebarContent").append("<div id='campusOfferedSection' class='content-block'><h4>Locations Offered</h4><p id='campusOffered'></p></div>");

            //Financial Aid Eligible
            $("#sidebarContent").append("<div id='financialAidSection' class='content-block'><h4>Financial Aid Eligible</h4><p id='financialAid'></p></div>");

            //What to Expect to Pay
            //$("#sidebarContent").append("<div id='tuitionSection' class='content-block'><h4>What to Expect to Pay<span class='accordionDiv'><span class='accordion accordion-toggle'>Details</span><div class='panel accordion-content'>Content</div></span></h4><p id='tuition'></p></div>");
            var divTuitionSection = $("<div id='tuitionSection' class='content-block'/>").appendTo("#sidebarContent");
            //divTuitionSection.append("<h4>What to Expect to Pay<span class='accordionDiv'><span class='accordion accordion-toggle' style='text-decoration: underline;color:#00529a;font-size: 0.63em;'>Details</span><div class='panel accordion-content'><div id='detailsID'></div></div></span></h4>");
            var h4 = $("<h4>What to Expect to Pay</h4>").appendTo(divTuitionSection);
            var accordionDiv = $("<span class='accordionDiv'/>").appendTo(h4);
            $("<span class='accordion accordion-toggle' id='detailsTitle'>Details</span>").appendTo(accordionDiv);
            $("<div class='panel accordion-content'><div id='detailsContent'></div></div>").appendTo(accordionDiv);
            var disclaimerContent = "All dollar figures appearing below reflect the total cost of completing the program described by the Typical Student Schedule located on this web page, which are for the current graduation requirements of this program. Exact totals may vary depending on the catalog requirements you are following, if you have transferred in courses, or if you have been granted substitutions.<br/><br/>";
            disclaimerContent += "Tuition, program fees, and course fees are for the current academic year, as approved by the Community Colleges of Spokane Board of Trustees.  Tuition and fees are subject to change by the Washington State Legislature and/or the Community Colleges of Spokane Board of Trustees.<br/><br/>";
            disclaimerContent += "Resident <strong>tuition</strong> is what Washington residents pay.  Non-Resident tuition is what out-of-state students pay.  International tuition is what out-of-country students pay.  For clarification on residency, see <a target='_blank' href='https://sfcc.spokane.edu/Become-a-Student/Admissions-Requirements/Determining-Your-Residency-Status'>Determining your Residency Status</a>. <br/><br/>"
            disclaimerContent += "<strong>Book</strong> costs are based on what students actually paid last year in this program.<br/><br/>";
            disclaimerContent += "<strong>Program fees</strong> are one-time costs required for the program that are not included in course fees or in tuition.  These fees include things like tools, equipment, and uniforms.  Only a few programs have program fees.<br/><br/>";
            disclaimerContent += "<strong>Course fees</strong> cover the cost of special equipment and materials needed to complete coursework. These fees are not included in tuition. Not all courses have fees."
            //var disclaimerContent = $("<p/>").append("First para");
            //$("<p/>").append("Second para");
            //var disclaimerContent= .append(firstPara);
            $("#detailsContent").append(disclaimerContent);
            divTuitionSection.append("<p id='tuition'></p>");

            //Print Button
            //$("#sidebarContent").append("<div id='printButton' class='button btn-group'><a href='#'onclick='window.print();return false;'>Print this page</a></div>");

            var referrer = getParameterByName('ref');

            if(referrer != null && referrer != ""){
                //add a breadcrumb
                var baseURL = window.location.protocol + "//" + window.location.host;
                var pathArray = referrer.split('/');
                var path = "", breadcrumb = "";
                for (i = 0; i < pathArray.length; i++) {
                    if(pathArray[i] != ""){
                        path += "/" + pathArray[i];
                        breadcrumb += "<a href='" + baseURL + path + "'>" + pathArray[i].replace(/-\(\d\)/g,"").replace(/-/g," ") + "</a>"; // replace calls use Regular Expressions to get rid of numbers in parentheses and then hyphens
                        if (i != (pathArray.length-1)) {
                            breadcrumb += " > ";
                        }
                    }
                }
                $(".breadcrumb").html(breadcrumb);
            }

            //get the id passed in the query string
            var programID = getParameterByName('id');

            //get the term code passed in the query string
            var strm = getParameterByName('strm');

            //create a global array to store the degree title(s)
            var degreeTitle = [];

            // get the default list of terms. Select based on strm passed in or default to current term?

            //display the program details
            getProgramDetails(programID, $("select[id$='quarterDD']").val());

            //display the program attributes
            getProgramAttributes(programID, $("select[id$='quarterDD']").val());

            //get program degree(s) - will change to return one degree per program
            $.ajax({
                //get program degrees
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetProgramDegrees",
                data: { ProgramID: programID, STRM: $("select[id$='quarterDD']").val() },
                dataType: "jsonp"
            }).then(function (programData) {
                //get degree option data
                var promises = [];
                var degreeCount = 0;
                $.each(programData.Table, function (degreeIndex, programDegree) {
                    var promise = $.ajax({
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOption",
                        data: { OptionID: programDegree.OptionID },
                        dataType: "jsonp"
                    });
                    promises.push(promise);
                    degreeTitle.push(programDegree.DegreeLongTitle);
                    degreeCount++;
                });

                //if (degreeCount == 1) {
                //$("#title").text(degreeTitle[0] + " - " + $("#title").text());
                //}
                if ($(".page-title").text() == "") {
                    $(".page-title").html("<h1>" + degreeTitle[0] + "</h1>");
                } else {
                    $(".page-title").html("<h1>" + degreeTitle[0] + " - " + $(".page-title").text() + "</h1>");
                }
                return Promise.all(promises);
            }).then(function (promiseData) {
                $.each(promiseData, function (promiseIndex, promise) {
                    $.each(promise.Table, function (optionIndex, option) {
                        var html = "";

                        //open option div
                        html += "<div id='option" + option.OptionID + "'>";

                        //display the option title
                        if (option.OptionTitle != null && option.OptionTitle != "") {
                            if (option.OptionTitle != "Associate in Arts") { //temporary to show Associate in Arts degree
                                html += "<h3>" + option.OptionTitle + "</h3>";
                            }
                        }

                        //display the option description
                        if (option.OptionDescription != null && option.OptionDescription != "") {
                            html += "<div class='content-block'><p>" + option.OptionDescription + "</p></div>";
                        }
                        
                        //display the gainful employment disclosure by calling function
                        if (option.GainfulEmploymentID != null && option.GainfulEmploymentID != "") {
                            /*
                            html += "<section id='gainfulEmploymentSection" + option.GainfulEmploymentID + "'>";
                            html += "<div id='gainfulEmployment" + option.GainfulEmploymentID + "'></div>";
                            html += "</section>";
                            */
                            $("#sidebarContent").append("<section id='gainfulEmploymentSection'><div class='content-block'><h4>Gainful Employment</h4><p id='gainfulEmployment'></p></div></section>");
                        }

                        //html += "<div id='option" + option.OptionID + "Locations'></div>";
                        html += "<div id='option" + option.OptionID + "Prerequisites'></div>";
                        html += "<div id='option" + option.OptionID + "Courses'></div>";
                        html += "<div id='option" + option.OptionID + "ElectiveGroups'></div>";
                        html += "<div id='option" + option.OptionID + "Footnotes'></div>";
                        html += "</div>";
                        appendHTML(html, "#schedule");

                        if (option.GainfulEmploymentID != null && option.GainfulEmploymentID != "") {
                            getGainfulEmploymentDisclosure(option.GainfulEmploymentID);
                        }
                       
                        //Print Button
                        $("#sidebarContent").append("<div id='printButton' class='button btn-group'><a href='#'onclick='window.print();return false;'>Print this page</a></div>")
               
                        $.ajax({ //get locations
                            crossDomain: true,
                            contentType: "application/json; charset=utf-8",
                            url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOptionLocations",
                            data: { OptionID: option.OptionID },
                            dataType: "jsonp",
                            success: function (locationData) {
                                var locations = "";
                                $.each(locationData.Table, function (locationIndex, location) {
                                    locations += "<p><a href='" + location.LocationWebsiteURL + "'>" + location.LocationTitle + "</a></p>"; //should this be a list?
                                });
                                if (locations != "") {
                                    $("#campusOffered").html(locations);
                                    //appendHTML(locations, "#campusOffered");
                                } else {
                                    $("#campusOfferedSection").hide();
                                }

                            }
                        }).then( //get prerequisites
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOptionPrerequisites",
                                data: { OptionID: option.OptionID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                dataType: "jsonp",
                                success: function (prerequisiteData) {
                                    var prerequisites = "";
                                    var prerequisiteCount = 0;

                                    $.each(prerequisiteData.Table, function (prerequisiteIndex, prerequisite) {
                                        var footnoteNumber = "";
                                        if (prerequisite.FootnoteNumber != null && prerequisite.FootnoteNumber != "") {
                                            footnoteNumber = prerequisite.FootnoteNumber;
                                        }

                                        prerequisites += "<li class='course'>";
                                        //prerequisites += "<div class='column'>&mdash;&nbsp;</div>";
                                        prerequisites += "<div class='column courseOffering'>" + prerequisite.CourseOffering + "</div>";
                                        prerequisites += "<div class='column courseTitle'>" + prerequisite.COURSE_TITLE_LONG + " <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                        prerequisites += "</li>";
                                        prerequisiteCount++;
                                    });

                                    if (prerequisiteCount > 0) {
                                        prerequisites = "<div id='prerequisites'><div class='content-block'><ul class='courseBlock no-bullet'><h4>Prerequisites</h4>" + prerequisites + "</ul></div></div>";
                                    }

                                    appendHTML(prerequisites, "#option" + option.OptionID + "Prerequisites");
                                }
                            })
                        ).then( //get courses and electives
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOptionCoursesAndElectives",
                                data: { OptionID: option.OptionID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                dataType: "jsonp",
                                success: function (courseData) {
                                    var totalUnits = 0, totalUnitsMinimum = 0, totalUnitsMaximum = 0, degreeUnits = 0, degreeUnitsMinimum = 0, degreeUnitsMaximum = 0;
                                    var previousQuarter = 0;
                                    var courses = "<div class='coursesAndElectives' style='width:100%'>";
                                    $.each(courseData.Table, function (courseIndex, course) {
                                        var footnoteNumber = "";
                                        if (course.FootnoteNumber != null && course.FootnoteNumber != "") {
                                            footnoteNumber = course.FootnoteNumber;
                                        }
                                        var currentQuarter = course.Quarter;
                                        //if a prevous quarter existed and a new quarter is starting
                                        if (previousQuarter != 0 && (currentQuarter != previousQuarter)) {

                                            //display the total units for the previous course block
                                            totalUnits = totalUnitsMinimum;
                                            if (totalUnitsMinimum != totalUnitsMaximum) {
                                                totalUnits = totalUnitsMinimum + "-" + totalUnitsMaximum;
                                            }
                                            courses += "<li class='course'>";
                                            courses += "<div class='column courseOffering'></div>";
                                            courses += "<div class='column courseTitle'></div>";
                                            courses += "<div class='column courseUnits totalUnits'>" + totalUnits + "</div>";
                                            courses += "</li>";
                                            //close the previous course block div
                                            courses += "</ul>";
                                            //increment the total units for the degree
                                            degreeUnitsMinimum += totalUnitsMinimum;
                                            degreeUnitsMaximum += totalUnitsMaximum;

                                            //clear the total units for the quarter/group of courses
                                            totalUnits = 0;
                                            totalUnitsMinimum = 0;
                                            totalUnitsMaximum = 0;
                                        }

                                        //if a new course block is started
                                        if (currentQuarter != "" && (currentQuarter != previousQuarter)) {

                                            //store the quarter title
                                            if (currentQuarter == 1) {
                                                courses += "<h4>First Quarter</h4>";
                                            } else if (currentQuarter == 2) {
                                                courses += "<h4>Second Quarter</h4>";
                                            } else if (currentQuarter == 3) {
                                                courses += "<h4>Third Quarter</h4>";
                                            } else if (currentQuarter == 4) {
                                                courses += "<h4>Fourth Quarter</h4>";
                                            } else if (currentQuarter == 5) {
                                                courses += "<h4>Fifth Quarter</h4>";
                                            } else if (currentQuarter == 6) {
                                                courses += "<h4>Sixth Quarter</h4>";
                                            } else if (currentQuarter == 7) {
                                                courses += "<h4>Seventh Quarter</h4>";
                                            } else if (currentQuarter == 8) {
                                                courses += "<h4>Eighth Quarter</h4>";
                                            } else if (currentQuarter == 9) {
                                                courses += "<h4>Ninth Quarter</h4>";
                                            } else if (currentQuarter == 10) {
                                                courses += "<h4>Tenth Quarter</h4>";
                                            } else {
                                                courses += "<h4>Courses</h4>";
                                            }

                                            totalUnitsMaximum = 0;
                                            totalUnitsMaximum = 0;

                                            //create the course block div
                                            courses += "<ul class='courseBlock no-bullet'>";
                                        }

                                        var unitsMinimum = 0, unitsMaximum = 0;
                                        if (course.OverwriteUnits == 1) {
                                            unitsMinimum = course.OverwriteUnitsMinimum;
                                            unitsMaximum = course.OverwriteUnitsMaximum;
                                        } else {
                                            unitsMinimum = course.UNITS_MINIMUM;
                                            unitsMaximum = course.UNITS_MAXIMUM;
                                        }

                                        //store course credits in string as one number or a range if min and max values are different
                                        var strUnits = "";
                                        if (unitsMinimum == unitsMaximum) {
                                            strUnits = unitsMinimum;
                                            totalUnitsMinimum += unitsMinimum;
                                            totalUnitsMaximum += unitsMinimum;
                                        } else {
                                            strUnits = unitsMinimum + "-" + unitsMaximum;
                                            totalUnitsMinimum += unitsMinimum;
                                            totalUnitsMaximum += unitsMaximum;
                                        }

                                        //create the course div
                                        courses += "<li class='course'>";

                                        //course offering
                                        courses += "<div class='column courseOffering'>";
                                        if (course.CourseOffering != "ZZZ") {
                                            courses += course.CourseOffering.replace(" ", "&nbsp;");
                                        }
                                        courses += "</div>";

                                        //course title

                                        courses += "<div class='column courseTitle'>" + course.COURSE_TITLE_LONG;
                                        if (footnoteNumber != null || footnoteNumber != "") {
                                            courses += "<div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div>";
                                        }
                                        courses += "</div>";

                                        //course units
                                        courses += "<div class='column courseUnits'>" + strUnits + "</div>";

                                        //end the course div
                                        courses += "</li>";

                                        previousQuarter = currentQuarter;
                                    });

                                    //display the total units for the last quarter/group of courses
                                    totalUnits = totalUnitsMinimum;
                                    if (totalUnitsMinimum != totalUnitsMaximum) {
                                        totalUnits = totalUnitsMinimum + "-" + totalUnitsMaximum;
                                    }

                                    courses += "<li class='course'>";
                                    courses += "<div class='column courseOffering'></div>";
                                    courses += "<div class='column courseTitle'></div>";
                                    courses += "<div class='column courseUnits totalUnits'>" + totalUnits + "</div>";
                                    courses += "</li>";

                                    //close the course block div
                                    courses += "</ul>";

                                    //increment the total units for the degree
                                    degreeUnitsMinimum += totalUnitsMinimum;
                                    degreeUnitsMaximum += totalUnitsMaximum;

                                    //display total units for the degree
                                    degreeUnits = degreeUnitsMinimum;
                                    if (degreeUnitsMinimum != degreeUnitsMaximum) {
                                        degreeUnits = degreeUnitsMinimum + "-" + degreeUnitsMaximum
                                    }

                                    courses += "<div><br /><p>" + degreeUnits + " credits are required for the <span class='bookcase'>" + degreeTitle[promiseIndex] + "</span></p></div>";

                                    //close the coursesAndElectives div
                                    courses += "</div>";

                                    appendHTML(courses, "#option" + option.OptionID + "Courses");
                                }
                            })
                        ).then( //get elective groups
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOptionElectiveGroups",
                                data: { OptionID: option.OptionID },
                                dataType: "jsonp",
                                success: function (electiveGroupData) {
                                    var electiveGroups = "<div id='electiveGroup" + option.OptionID + "' class='electiveGroup'>";
                                    $.each(electiveGroupData.Table, function (index, electiveGroup) {
                                        if (electiveGroup.ElectiveCount > 0) {
                                            var footnoteNumber = "";
                                            if (electiveGroup.FootnoteNumber != null && electiveGroup.FootnoteNumber != "") {
                                                footnoteNumber = electiveGroup.FootnoteNumber;
                                            }

                                            electiveGroups += "<div class='electiveGroupTitle'><h4>" + electiveGroup.ElectiveGroupTitle + "</h4> <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                            electiveGroups += "<ul id='ulElectiveGroup" + electiveGroup.OptionElectiveGroupID + "' class='courseBlock no-bullet'>";
                                            electiveGroups += "</ul>";
                                        }
                                    });
                                    electiveGroups += "</div>";
                                    appendHTML(electiveGroups, "#option" + option.OptionID + "ElectiveGroups");
                                }
                            }).then(function (electiveGroupData) { //get elective group courses
                                $.each(electiveGroupData.Table, function (index, electiveGroup) {
                                    $.ajax({
                                        crossDomain: true,
                                        contentType: "application/json; charset=utf-8",
                                        url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetElectiveGroupCourses",
                                        data: { OptionElectiveGroupID: electiveGroup.OptionElectiveGroupID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                        dataType: "jsonp",
                                        success: function (electiveData) {
                                            $.each(electiveData.Table, function (electiveIndex, elective) {
                                                var footnoteNumber = "";
                                                if (elective.FootnoteNumber != null && elective.FootnoteNumber != "") {
                                                    footnoteNumber = elective.FootnoteNumber;
                                                }

                                                var unitsMinimum = elective.UNITS_MINIMUM;
                                                var unitsMaximum = elective.UNITS_MAXIMUM;
                                                if (elective.OverwriteUnits == 1) {
                                                    unitsMinimum = elective.OverwriteUnitsMinimum;
                                                    unitsMaximum = elective.OverwriteUnitsMaximum;
                                                }

                                                var units = unitsMinimum;
                                                if (unitsMinimum != unitsMaximum) {
                                                    units = unitsMinimum + "-" + unitsMaximum;
                                                }

                                                var electives = "<li class='course'>";
                                                electives += "<div class='column courseOffering'>" + elective.CourseOffering.replace(" ", "&nbsp;") + "</div>";
                                                electives += "<div class='column courseTitle'>" + elective.COURSE_TITLE_LONG + " <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                                electives += "<div class='column courseUnits'>" + units + "</div>";
                                                electives += "</li>";

                                                appendHTML(electives, "#ulElectiveGroup" + electiveGroup.OptionElectiveGroupID);
                                            });
                                        }
                                    })
                                });
                            })
                        ).then(
                            //get footnotes
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetOptionFootnotes",
                                data: { OptionID: option.OptionID },
                                dataType: "jsonp",
                                success: function (data) {
                                    var footnotes = "<div class='footnotes'><div class='content-block'>";
                                    var footnoteCount = 0;
                                    $.each(data.Table, function (index, footnote) {
                                        footnoteCount++;
                                        footnotes += "<div class='footnote'><div class='footnoteNumber' style='padding-right:5px'><i>" + footnote.FootnoteNumber + "</i></div><div class='footnoteText'>" + footnote.Footnote + "</div></div>";
                                    })
                                    footnotes += "</div></div>";
                                    if (footnoteCount > 0) {
                                        $(footnotes).appendTo("#option" + option.OptionID + "Footnotes");
                                    }
                                }
                            })
                        ).then(
                            //get Tuition
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/icatalogjson.asmx/GetTuition",
                                data: { ACAD_PLAN: option.ACAD_PLAN },
                                dataType: "jsonp",
                            success: function (tuitionData) {
                                    //Start: Add JS for accordion
                                    var acc = document.getElementsByClassName("accordion");
                                    var i;
                                    for (i = 0; i < acc.length; i++) {
                                        acc[i].addEventListener("click", function () {
                                            this.classList.toggle("active");
                                        });
                                    }
                                    $('.accordionDiv').find('.accordion-toggle').click(function () {
                                        //Expand or collapse this panel
                                        $(this).next().slideToggle('fast');
                                        //Hide the other panels
                                        $(".accordion-content").not($(this).next()).slideUp('fast');
                                    });
                                    //End: Add JS for accordion
                                    $.each(tuitionData.Table, function (tuitionIndex, tuition) {
                                        var tuitionArr = [
                                            { type: "Resident", fee: tuition.TuitionResident },
                                            { type: "Non-Resident", fee: tuition.TuitionNonResident },
                                            { type: "International", fee: tuition.TuitionNonResidentInternational },
                                            { type: "Books", fee: tuition.BookCosts },
                                            { type: "Plan Fees", fee: tuition.PlanFees },
                                            { type: "Course Fees", fee: tuition.CourseFees }
                                        ];
                                        var allNot0 = tuitionArr.every(function (item) {
                                            return item.fee != 0;
                                        });
                                        var allTuitionNotNull = tuitionArr.every(function (item) {
                                            if (item.type.indexOf("Resident") > -1 || item.type.indexOf("International") > -1) {
                                                return item.fee;
                                            } else {
                                                return true;
                                            }
                                            //return (item.type.indexOf("Resident") > -1 || item.type.indexOf("International") > -1) && item.fee;
                                            //return true;
                                        });
                                        console.log(allNot0 + option.ACAD_PLAN);
                                        if (allTuitionNotNull)
                                        {
                                            var courses = "<div style='width:400px'>";
                                             courses += "<ul class='no-bullet'>";
                                            courses += "<li class='course'>Tuition</li>";
                                            for (var i = 0; i < tuitionArr.length; i++) { 
                                                var type = tuitionArr[i].type;
                                                var fee = tuitionArr[i].fee;
                                                console.log(type + " " + fee);
                                                if (fee) {
                                                    courses += "<li class='course'>";
                                                    if (type.indexOf("Resident") > -1 || type.indexOf("International") > -1) {
                                                        courses += "<div style='padding-left:10px' class='column tuitionType'>" + type +"</div>";
                                                    } else {
                                                        courses += "<div class='column tuitionType'>" + type +"</div>";
                                                    }
                                                
                                                    courses += "<div class='column dollarSign'>$</div>";
                                                    courses += "<div class='column tuiton'>" + currencyFormat(fee) + "</div>";
                                                    courses += "</li>";
                                                }  
                                            }
                                            courses += "</ul>";
                                            courses += "</div>";
                                            $("#tuition").append(courses);
                                            $("#tuitionSection").show();
                                        }
                                        /*
                                        if (tuition.TuitionResident != 0 && tuition.TuitionNonResident != 0 && tuition.TuitionNonResidentInternational != 0) {
                                            var courses = "<ul class='no-bullet'>";
                                            if (tuition.TuitionResident != 0) {
                                                //string tuitionStr = "<ul class='courseBlock no-bullet'><li class='course'><div class='column courseOffering'>BT&nbsp;  152</div><div class='column courseTitle'>College and Career Strategies<div class='footnoteNumber' style='padding-left:5px;'><i></i></div></div><div class='column courseUnits'>5</div></li><li class='course'><div class='column courseOffering'>HM&nbsp;  110</div><div class='column courseTitle'>Introduction to Hospitality<div class='footnoteNumber' style='padding-left:5px;'><i></i></div></div><div class='column courseUnits'>5</div></li><li class='course'><div class='column courseOffering'>HM&nbsp;  130</div><div class='column courseTitle'>Human Relations<div class='footnoteNumber' style='padding-left:5px;'><i>1</i></div></div><div class='column courseUnits'>5</div></li><li class='course'><div class='column courseOffering'></div><div class='column courseTitle'></div><div class='column courseUnits totalUnits'>15</div></li></ul>";
                                                //$("#tuition").append("<div><div></div></div><strong>Resident: </strong><div style='float:right;padding-right:50px'> " + currencyFormat(tuition.TuitionResident) + "</div><br/>");
                                                
                                                courses += "<li class='course'>";
                                                courses += "<div class='column tuitionType'>Resident</div>";
                                                courses += "<div class='column dollarSign'>$</div>";
                                                courses += "<div class='column tuiton'>" + "99,999" + "</div>";
                                                courses += "</li>";
                                                //close the previous course block div
                                                
                                                //$("#tuition").append(courses);
                                            }
                                            if (tuition.TuitionNonResident != 0) {
                                                //$("#tuition").append("<strong>Non Resident: </strong>" + currencyFormat(tuition.TuitionNonResident) + "<br/>");
                                                courses += "<li class='course'>";
                                                courses += "<div class='column tuitionType'>Non Resident</div>";
                                                courses += "<div class='column dollarSign'>$</div>";
                                                courses += "<div class='column tuiton'>" + currencyFormat(tuition.TuitionNonResident) + "</div>";
                                                courses += "</li>";
                                            }
                                            if (tuition.TuitionNonResidentInternational != 0) {
                                                //$("#tuition").append("<strong>International: </strong>" + currencyFormat(tuition.TuitionNonResidentInternational) + "<br/>");
                                                courses += "<li class='course'>";
                                                courses += "<div class='column tuitionType'>International</div>";
                                                courses += "<div class='column dollarSign'>$</div>";
                                                courses += "<div class='column tuiton'>" + currencyFormat(tuition.TuitionNonResidentInternational) + "</div>";
                                                courses += "</li>";
                                            }
                                            courses += "</ul>";
                                            $("#tuition").append(courses);
                                            $("#tuitionSection").show();
                                        }
                                        */
                                    });

                                }
                            })
                        );
                    });
                });
            })
                

        })

    </script>
