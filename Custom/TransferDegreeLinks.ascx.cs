using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_TransferDegreeLinks : CMSAbstractWebPart
{
    #region "Properties"

    public String WidgetID
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("WidgetID"), null);
        }
        set
        {
            SetValue("WidgetID", value);
        }
    }

    public string TransferDegree
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("TransferDegree"), null);
        }
        set
        {
            SetValue("TransferDegree", value);
        }
    }

    public string DisplayStyle
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("DisplayStyle"), null);
        }
        set
        {
            SetValue("DisplayStyle", value);
        }
    }

    public string Heading
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("Heading"), null);
        }
        set
        {
            SetValue("Heading", value);
        }
    }


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        String strTransferDegree = TransferDegree;
        Int32 index = 0;
        String strLinkHTML = "";
        litHeading.Text = "";
        litLinks.Text = "";
        if (Heading != "" && Heading != null) { litHeading.Text = "<h4>" + Heading + "</h4>"; }
        try
        {
            //FOR TESTING - COMMENT OUT 
            //if (strTransferDegree == "" || strTransferDegree == null) { strTransferDegree = "36"; }
            iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();
            ds = wsICatalog.GetDegrees("", "", TransferDegree, "", "", "", "");
            if (ds != null)
            { dt = ds.Tables[0]; }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        index += 1;
                        if (dr["DegreeID"].ToString() != "0")
                        {
                            strLinkHTML = "<a href=\"/What-To-Study/Transfer-Degree?id=" + dr["DegreeID"].ToString() + "&ref=" + WidgetID + "\">" + dr["DegreeLongTitle"].ToString();
                            if (dr["ProgramTitle"].ToString() != "")
                            {
                                //hide these links for this widget?
                                //strLinkHTML += " - " + dr["ProgramTitle"].ToString();
                            }
                            strLinkHTML += "</a>";
                        }
                        switch (DisplayStyle)
                        {
                            case "Bullets":
                                //First link doesn't get a bullet; its the generic degree
                                //if (index == 2)
                                //{ litLinks.Text += "<ul>"; }
                                if (index == 1)
                                {
                                    litLinks.Text += "<p>" + strLinkHTML + "</p>";
                                }
                                //else if (index > 1)
                                //{
                                    //litLinks.Text += "<li>" + strLinkHTML + "</li>";
                               // }
                                //if (index == dt.Rows.Count && index > 1)
                                //{ litLinks.Text += "</ul>"; }
                                break;
                            case "NoBullets":
                                litLinks.Text += "<p>" + strLinkHTML + "</p>";
                                break;
                            case "SemicolonText":
                                if (index == 1)
                                { litLinks.Text += "<p>"; }
                                litLinks.Text += strLinkHTML;
                                if (index < dt.Rows.Count) { litLinks.Text += "; "; }
                                if (index == dt.Rows.Count)
                                { litLinks.Text += "</p>"; }
                                break;
                        }
                    }
                }
                else { litLinks.Text = "<p>Error Transfer Degree Links: Recordset returned no rows.  Degree ID: " + TransferDegree + "</p>"; }
            }
            else { litLinks.Text = "<p>Error Transfer Degree Links: Recordset null.  Degree ID: " + TransferDegree + "</p>"; }
        }
        catch (Exception ex)
        {
            litLinks.Text = "<p>Error Transfer Degree Links: " + ex.Message + " Stack trace: " + ex.StackTrace + "</p>";
        }

    }


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



