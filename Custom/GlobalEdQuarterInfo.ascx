﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/GlobalEdQuarterInfo.ascx.cs" Inherits="CMSWebParts_Custom_GlobalEdQuarterInfo" %>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="container">
    <asp:label id="myLabel" runat="server" />
    <div id="main_content"></div>
   
    <input type="hidden" id="quarterListJsonID" runat="server" />
    <input type="hidden" id="quarterInfoListJsonID" runat="server" />
    <input type="hidden" id="quarterInfoListJsonID2" runat="server" />
    <input type="hidden" id="siteID" runat="server" />
    <div id="TBL"></div>
</div>



<script type="text/javascript">
    $(function () {
        var siteName = $("input[id$='siteID']").val();
        console.log("site: " + siteName);
        //$("#TBL").append("<p><strong>From server</strong></p>");
        var quarterListJsonID = $("input[id$='quarterListJsonID']");
        var quarterListJson = JSON.parse(quarterListJsonID.val());
        //console.log(quarterListJson);
       // var quarterInfoListJsonID = $("input[id$='quarterInfoListJsonID']");
        //var quarterInfoListJson = JSON.parse(quarterInfoListJsonID.val());
        //displayTable(quarterInfoListJson);
        

        //$("#TBL").append("<p><strong>From server with SCC/SFCC</strong></p>");
        var quarterInfoListJsonID2 = $("input[id$='quarterInfoListJsonID2']");
        var quarterInfoListJson2 = JSON.parse(quarterInfoListJsonID2.val());
        displayTable(quarterInfoListJson2);


        //$("#TBL").append("<br/><p><strong>From client</strong></p>");
        quarterListJson.forEach(function (element) {
            var quarterName = element.QuarterName;
            var year = element.Year;

            console.log(quarterName + " " + year);
            var urlArr = [];
            var applicationDeadline = "";
            var applicationDeadlineDate = "";
            var mode = "SFCCshadow";
            if (siteName.indexOf("scc") > -1) {
                mode = "SCCshadow";
            } else if (siteName.indexOf("sfcc") > -1){
                mode = "SFCCshadow";
            }
            //var filter = "application-deadline-for-" + quarterName.toLowerCase();
            var filter = "application-deadline";
            var applicationDeadlineUrl = "//external.spokane.edu/REST/API/ImpDates?mode=" + mode + "&filter=" + filter;
            urlArr.push(applicationDeadlineUrl);

            
            var transferDeadlineUrl = "//external.spokane.edu/REST/API/ImpDates?mode=Global-Edshadow&filter=academic-students-transfer-application-deadline-for-" + quarterName.toLowerCase();
            urlArr.push(transferDeadlineUrl);

            var ielpDeadlineUrl = "//external.spokane.edu/REST/API/ImpDates?mode=Global-Edshadow&filter=(ielp)-transfer-application-deadline-for-" + quarterName.toLowerCase(); 
            urlArr.push(ielpDeadlineUrl);

            var firtsdayUrl = "//external.spokane.edu/REST/API/ImpDates?mode=" + mode +"&filter=first-day-of-" + quarterName.toLowerCase();
            //var firtsdayUrl = "~/CMSWebparts/Custom/GlobalEdQuarterInfo_files/test_data.xml";
            urlArr.push(firtsdayUrl);
            var deferreds = [];
            var xmlList = [];
            
            $.each(urlArr, function (key, value) {
                var ajax = $.get(value, function (data) {
                    xmlList.push(data);
                }, "xml");
                // Push promise to 'deferreds' array
                deferreds.push(ajax);
            });

            $.when.apply($, deferreds).then(function () {
                //console.log(xmlList);
                //generateTable(quarterName, year, xmlList);
                
            });  
        });  
        //console.log(matchEachWordInString("New  Application Deadline for Winter", "New Student Application deadline"));
    });

    function displayTable(quarterInfoListJson) {
        quarterInfoListJson.forEach(function (element) {
            console.log(element);
            var quarterE = element.QuarterObj.QuarterName;
            var yearE = element.QuarterObj.Year;
            //create table
            var table = $("<table border='1px' cellpadding='1px' cellspacing='1px' class='ccs-table' />").appendTo("#TBL");
            table.append('<thead> <tr><th colspan="2" scope="col">' + quarterE + " " + yearE + '</th></tr></thead>');

            var tbody = $('<tbody>').appendTo(table);

            element.InfoList.forEach(function (value, index) {
                //console.log(value);
                var tr = $('<tr>').appendTo(tbody);
                $('<td>').text(value.Title).appendTo(tr);
                //$('<td>').text((value.Category)).appendTo(tr);
                $('<td>').text(convertDate(value.Category)).appendTo(tr);
            });

        });
    }
    function generateTable(quarterName, year, xmlList) {
        //create table
        var table = $("<table border='1px' cellpadding='1px' cellspacing='1px' class='ccs-table' />").appendTo("#TBL");
        table.append('<thead> <tr><th colspan="2" scope="col">' + quarterName + " " + year + '</th></tr></thead>');
        /*
        var titleDispay = ["Admission Application Deadline",
            "Academic Students Transfer Application Deadline",
            "IELP Transfer Application Deadline",
            quarterName + " Quarter Begins"
        ];
        */
        var tbody = $('<tbody>').appendTo(table);
        xmlList.forEach(function (element, index) {
            //console.log(element);
            var res = getInfo(element, year, quarterName);
            if (res.title != "undefined") {
                var tr = $('<tr>').appendTo(tbody);
                $('<td>').text(res.title).appendTo(tr);
                $('<td>').text(res.category).appendTo(tr);
            }
        });
        
    }

    function getInfo(element, year,quarterName) {
        var res = {};
        res.title = "undefined";
        res.category = "undefined";
        var dateXML = $(element).find("Date");
        
        if (dateXML.length > 0) {
            
            dateXML.each(function () {
                
                var title = $(this).find("Title").text();
                var category = $(this).find("Category").text();
                //console.log(title +" "+ category);
                if (title.toLowerCase().indexOf("first") > -1) { // look for match "first day of, then match year in category
                    //var dateS = $(this).find('Category').text();
                    if (category.indexOf(year) >= 0) {
                        res.title = quarterName + " Quarter Begins";
                        res.category = convertDate($(this).find("Category").text());
                       // return;
                    }
                } else if (matchEachWordInString(title, "academic transfer application deadline") && title.indexOf(year) > -1) { // look for global transfer deadline
                    res.title = "Academic Students Transfer Application Deadline";
                    res.category = convertDate($(this).find("Category").text());
                    //return;
                } else if (matchEachWordInString(title, "ielp") && title.indexOf(year) > -1) { // look for IELP deadline
                    res.title = "IELP Transfer Application Deadline";
                    res.category = convertDate($(this).find("Category").text());
                    //return;
                } else if (matchEachWordInString(title, "New Student Application deadline") && title.toLowerCase().indexOf(quarterName.toLowerCase()) > -1) { //look for application deadline
                    res.title = "Application Admission Deadline";
                    res.category = convertDate($(this).find("Category").text());
                    //return;
                }
                //res.category = convertDate($(this).find("Category").text());
            });
        } 
        return res;
    }

    function convertDate(dateStr) {
        var monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var d = new Date(Date.parse(dateStr));
        return monthName[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
    }

    function getYear(dateStr) {
        var d = new Date(Date.parse(dateStr));
        return d.getFullYear();
    }

    //Given a matchStr contaning words delimited by " ", the fuction will check if those words exist in str
    function matchEachWordInString(str, matchStr) {
        matchStr = matchStr.toLowerCase();
        var arr = matchStr.split(" ");
        for (var i in arr) {
            if (str.toLowerCase().indexOf(arr[i]) == -1)
            {  
                return false;
            }
        }
        return true;
    }
</script>