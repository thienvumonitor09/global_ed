﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMSWebParts_Custom_TestWebParts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WindowsIdentity winID = WindowsIdentity.GetCurrent();
        litDebug.Text = "<p><b>Security context:</b> " + winID.Name + "</p>";

    }
}