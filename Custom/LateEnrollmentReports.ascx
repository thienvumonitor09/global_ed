﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LateEnrollmentReports.ascx.cs" Inherits="CMSWebParts_Custom_LateEnrollmentReports" %>

<section class="white-bg">
<div id="container">
    <section class="col-lg-12 row">
<div class="cs_row col-lg-12">
    <div class='cs_row col-lg-12'><div class='col-md-4'><br /><br /><h3>Select Report Type:</h3><br /></div><div class='col-md-4'><p>&nbsp;</p>
        <asp:DropDownList ID="ddReportChoice" runat="server">
            <asp:ListItem Value="Select one"></asp:ListItem>
            <asp:ListItem Value="Summary">Summary Report</asp:ListItem>
            <asp:ListItem Value="Stuck">No Response List</asp:ListItem>
            <asp:ListItem Value="Export">Export File</asp:ListItem>
            <asp:ListItem Value="Duplicates">Manage Duplicate Records</asp:ListItem>
        </asp:DropDownList><br /></div><div class='col-md-4'><p>&nbsp;</p><asp:Button ID="cmdBtnCreateReport" runat="server" Text="Create Report" OnClick="cmdBtnCreateReport_Click" /></div></div>
    <br /><asp:Literal ID="litMessage" runat="server"></asp:Literal> <p>&nbsp;</p>          
    <div class="cs_row col-lg-12">
                <div id="ReportHead" runat="server" Visible="false">
                    <asp:Literal ID="litReportTitle" runat="server"></asp:Literal>
                    <p><asp:Label ID="lblReportErrMsg" runat="server" Text=""></asp:Label></p>                   
                   <!-- <div class="col-md-2"><p>List all requests  <asp:CheckBox ID="chkAll" runat="server" /></p></div>
                    <div class="col-md-3"><p>OR enter student ID:  <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox></p></div> 
                    <div class="col-md-2"><p><br /><asp:Button ID="btnReport" runat="server" Text="Generate Report" OnClick="cmdbtnReport_Click" /></p></div> -->
                </div>
            </div>
            <div id="ReportBody" runat="server" Visible="false">
                
                <asp:Panel ID="pnlReport" runat="server">    
                </asp:Panel>
                <asp:Panel ID="pnlDeleted" runat="server"></asp:Panel>
                <div id="processBtn" runat="server" style="float:right;margin-right:20px;"><asp:Button ID="btnProcess" runat="server" Text="Process" class="button" OnClick="btnProcess_Click" /></div>
                <div id="deleteBtn" runat="server" Visible="false" style="float:right;margin-right:20px;"><asp:Button ID="btnDelete" runat="server" Text="Delete Records" class="button" OnClick="btnDelete_Click" /></div>
            </div>
    </div>
        </section>
        </div>
    </section>
