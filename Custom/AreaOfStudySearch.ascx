﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/AreaOfStudySearch.ascx.cs" Inherits="CMSWebParts_Custom_AreaOfStudySearch" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%--<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
<input type="hidden" id="hidPostBack" value="" runat="server" />
<input type="hidden" id="hidLtr" value="" runat="server" />
<input type="hidden" id="hidInstitution" value="" runat="server" />
<div class="container areaOfStudy">
    <div class="btn-group">
        <asp:TextBox ID="txtTextSearch" runat="server" style="width:160px;" placeholder="e.g. Accounting" MaxLength="150"></asp:TextBox>
        <!--<span id="searchclear" class="glyphicon glyphicon-remove"></span>-->
        <button id="btnSearch" onclick="clearAlphaSearch();getAreasOfStudy($('input[id$=\'hidInstitution\']').val(),'',$('input[id$=\'txtTextSearch\']').val());" aria-label="Search">
            <span class="glyphicon glyphicon-search"></span>
        </button>
    </div>
    <div id="areaofstudysearchresults" class="searchresults"><div class="working searchinfo collapse"><h2>Searching...</h2></div></div>
    <asp:Panel ID="panAlpha" CssClass="panAlpha" runat="server">
        <ul id="alpha" class="alpha"></ul>
    </asp:Panel>
    <asp:Panel ID="areaOfStudyList" CssClass="areaOfStudyList" runat="server"></asp:Panel>
</div>

<script type="text/javascript">
    //if the document is ready
    $(document).ready(function () {
        var institution = $("input[id$='hidInstitution']").val();

        //populate the alpha character list
        /*$(".alpha").append('<li><a href="#" id="link" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'\',\'\');">ALL</a></li>');
        for (i = 0; i < 26; i++) {
            var alphaCharacter = (i+10).toString(36).toUpperCase();
            $(".alpha").append('<li><a href="#" id="link' + alphaCharacter + '" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'' + alphaCharacter + '\',\'\');">' + alphaCharacter + '</a></li>');
        }*/

        if (institution == "WA171") {
            $("div.areaOfStudyList").addClass("resultsSCC");
        }

        if (institution == "WA172") {
            $("div.areaOfStudyList").addClass("resultsSFCC");
        }

        //if not a post back
        if($("input[id$='hidPostBack']").val() != "true"){
            //populate all areas of study by default
            getAreasOfStudy(institution, "", "");
        } else {
            //populate areas of study by search parameters
            getAreasOfStudy(institution, $("input[id$='hidLtr']").val(), $("input[id$='txtTextSearch']").val());
        }
    })

    //clear search text
    $("#searchclear").click(function () {
        $('input[id$=\'txtTextSearch\']').val('');
    });

    function getAreasOfStudy(institution, alphaCharacter, keyword) {

        $(".searchresults .searchstat").empty();
        $(".working").fadeIn();

        var workingIndicator = setInterval(function () { $(".working h2").append(".") }, 500);

        //remove previously active letter
        $("a[id^='link']").removeClass("active");

        //make the selected letter active
        $("a[id$='link" + alphaCharacter + "']").addClass("active");

        //store the alpha character in a hidden element
        if (alphaCharacter != null && alphaCharacter != "") {
            $("input[id$='hidLtr']").val(alphaCharacter);
        }

        //institution = '';
        //Note: Change Institution: institution to Institution: "" in the following ajax call in order to return matching programs from both institutions - BN 2018-04-10
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/WebServices/icatalogjson.asmx/GetAreasOfStudyByKeyword",
            data: { STRM: "", Institution: institution, AlphaCharacter: "", Keyword: keyword },
            dataType: "jsonp",
            success: function (data) {
                var oldAreaOfStudy = "", oldAlphaCharacter = "", rowCount = 0, transferDegreesCount = 0;

                //clear the area of study list
                $(".areaOfStudyList").empty();
                //clear how many results were found
                //$(".searchresults").empty();

                $.each(data.Table, function (index, areaOfStudy) {
                    var newAreaOfStudy = areaOfStudy.Title;
                    var newAlphaCharacter = newAreaOfStudy.substring(0, 1);
                    var websiteURL = areaOfStudy.WebsiteURL;
                    var offeredBy = areaOfStudy.CollegeShortTitle;
                    if ((!offeredBy) || (offeredBy == '') || (offeredBy == 'null')) { //Use websiteURL if CollegeShortTitle is null - [Brandy, is this approach valid? What would be better?]
                        offeredBy = "SFCC";
                        if ((websiteURL)) {
                            if ((websiteURL.indexOf('sccbeta.spokane.') > 0) || (websiteURL.indexOf('scc.spokane.') > 0)) {
                                offeredBy = "SCC";
                            }
                        }
                    }
                    newAreaOfStudy += ' (' + offeredBy + institution + ')';
                    newAreaOfStudy = newAreaOfStudy.replace(" (SFCCWA172)", "").replace(" (SCCWA171)", "");
                    newAreaOfStudy = newAreaOfStudy.replace("WA172", "").replace("WA171", "");
                    //if the area of study title starts with a new alpha character
                    if (newAlphaCharacter != oldAlphaCharacter) {
                        //display the new alpha character row area of study list
                          $(".areaOfStudyList").append("<div class='row' id='row" + newAlphaCharacter + "'></div>"); 
                        //if the alpha character is not the first in the loop
                        if (oldAlphaCharacter != "") {
                            //display an hr after the previous alpha area of study list
                            /* $("#row" + newAlphaCharacter).append("<div class='col-lg-12'><hr /></div>"); */
                        }
                        //display the new alpha character column in the area of study list
                        /*$("#row" + newAlphaCharacter).append("<div class='col-lg-12 alphaCharacter'><h1>" + newAlphaCharacter + "</h1></div>");*/
                    }

                    //if first time looping through the area of study
                    if (newAreaOfStudy != oldAreaOfStudy) {
                      if ((transferDegreesCount < 2)||(newAreaOfStudy.indexOf("Transfer Degrees")==-1)) {
                        if (!websiteURL || websiteURL == null || websiteURL == "") {
                            websiteURL = "#";
                        }
                        if (newAreaOfStudy.indexOf("Transfer Degrees") > -1) {
                            transferDegreesCount++;
                        }
                        // $("#row" + newAlphaCharacter).append("<div class='col-lg-6'><h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy + "</a></h2></div>");
                        // IF last row was same as current row plus (SCC) or (SFCC), the current row needs to be added BEFORE the last row...
                        if (oldAreaOfStudy.replace(" (SCC)", "").replace(" (SFCC)", "") == newAreaOfStudy) {
                            // capture last row
                            var last_div = $("#row" + newAlphaCharacter + " div:last").clone();
                            // remove last row
                            $("#row" + newAlphaCharacter + " div:last").remove();
                            // add new row]
                            $("#row" + newAlphaCharacter).append("<div class='col-lg-12 " + areaOfStudy.CollegeShortTitle + " " + offeredBy + "'><h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy + "</a></h2></div>");
                            // add captured last row
                            $("#row" + newAlphaCharacter).append(last_div);
                        }
                        else {
                            $("#row" + newAlphaCharacter).append("<div class='col-lg-12 " + areaOfStudy.CollegeShortTitle + " " + offeredBy + "'><h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy + "</a></h2></div>");
                        }
                        rowCount++;
                      }
                    }

                    //store the area of study being looped through
                    oldAreaOfStudy = newAreaOfStudy;
                    //store the alpha character being looped through
                    oldAlphaCharacter = newAlphaCharacter;
                });

                //hide the "Working" indicator
                clearInterval(workingIndicator);
                $(".working").hide();

                //display how many results were found
                if (keyword != null && keyword != "") {
                    $(".searchresults").append(('<div class="searchstat"><h2>Found ' + rowCount + ' areas of study containing "' + keyword + '"</h2></div>').replace("Found 1 areas","Found 1 area").replace("Found 0 areas of study","No areas of study found"));
                }
            },
            error: function (error) {
                $(".searchresults .searchstat").remove();
                $(".working").hide();
                setTimeout(function () { $(".searchresults").append('<div class="searchstat"><h2>Timeout: Service Unavailable</h2></div>'); }, 1000);
            }
        });
    }

    function clearTextSearch() {
         $("input[id$='txtTextSearch']").val("");
    }

    function clearAlphaSearch() {
        $("input[id$='hidLtr']").val("");
    }

</script>
   