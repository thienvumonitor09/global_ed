using System;
using CMS.DocumentEngine.Web.UI;
using CMS.Helpers;
using CMS.PortalEngine.Web.UI;
using System.Data;
using CMS.PortalEngine;
using System.Xml;
using CMS.MacroEngine;
using System.Collections.Generic;
using System.Linq;
using CMS.EventLog;
using CMS.SiteProvider;

public partial class CMSWebParts_Custom_TransformableWebPart : CMSAbstractWebPart
{
    // Indicates whether control was binded
    private bool binded = false;

    #region "MultiRow Properties"

    /// <summary>
    /// Controls if the system should detect MultiPart columns (fields with _# at the end) and convert them into rows.
    /// </summary>
    public bool EnableMultiRow
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("EnableMultiRow"), true);
        }
        set
        {
            SetValue("EnableMultiRow", value);
        }
    }

    /// <summary>
    /// Controls whether to render rows which are found with empty values in all MultiPart columns
    /// </summary>
    public bool DontRenderBlankRows
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DontRenderBlankRows"), true);
        }
        set
        {
            SetValue("DontRenderBlankRows", value);
        }
    }

    /// <summary>
    /// Order by of the data.
    /// </summary>
    public string OrderBy
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("OrderBy"), ""), "");
        }
        set
        {
            SetValue("OrderBy", value);
        }
    }

    /// <summary>
    /// Called "Where" vs. "WhereCondition" because WhereCondition triggered a special security exception.
    /// </summary>
    public string Where
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("Where"), ""), "");
        }
        set
        {
            SetValue("Where", value);
        }
    }

    /// <summary>
    /// What Columns to return.
    /// </summary>
    public string Columns
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("Columns"), ""), "");
        }
        set
        {
            SetValue("Columns", value);
        }
    }

    /// <summary>
    /// Top N Records
    /// </summary>
    public int TopN
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("TopN"), -1);
        }
        set
        {
            SetValue("TopN", value);
        }
    }

    #endregion

    #region "Cache properties"

    public string CacheItemName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("CacheItemName"), ""), CacheDependencies);
        }
        set
        {
            SetValue("CacheItemName", value);
        }
    }

    public int CacheMinutes
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("CacheMinutes"), base.CacheMinutes);
        }
        set
        {
            SetValue("CacheMinutes", value);
        }
    }

    public string CacheDependencies
    {
        get
        {
            // If Default, it was showing ##DEFAULT##\n, so remove that so the GetNotEmpty will pull the base.CacheDependencies which is the web part.
            string CacheDepValue =  DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("CacheDependencies"), "").Trim('\n').Replace("##DEFAULT##",""), base.CacheDependencies);
			// For inline widgets, sometimes the base.CacheDependencies would be ##DEFAULT## as well, causing cache issues.
            if (CacheDepValue == "##DEFAULT##")
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return CacheDepValue;
            }
        }
        set
        {
            SetValue("CacheDependencies", value);
        }
    }

    #endregion

    #region "Transformation properties"

    /// <summary>
    /// Gets or sets the name of the transforamtion which is used for displaying the results.
    /// </summary>
    public string TransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("TransformationName"), ""), "");
        }
        set
        {
            SetValue("TransformationName", value);
            basicRepeater.ItemTemplate = TransformationHelper.LoadTransformation(this, value);
        }
    }


    #endregion

    #region "Widget Configuration Properties"

    /// <summary>
    /// Gets the WebpartCodeName, needed if it is rendering in an inline widget and thus does not contain the WebpartID or CodeName normally, so must be provided by the user.
    /// </summary>
    public string WebpartCodeName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("WebpartCodeName"), ""), "");
        }
        set
        {
            SetValue("WebpartCodeName", value);
        }
    }

    #endregion

    #region "Stop processing"

    /// <summary>
    /// Returns true if the control processing should be stopped.
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
        }
    }

    #endregion

    #region "Get DataSet from Webpart Properties"

    /// <summary>
    /// Gets the webpart ID, either from the PartInfo, or if null (inline widget) then from the Webpart Code Name field that the user must provide.
    /// </summary>
    /// <returns></returns>
    protected int GetWebpartID(bool logEvents = false)
    {
        if (this.PartInfo != null)
        {
            return this.PartInfo.WebPartID;
        }
        else
        {
            if (!string.IsNullOrWhiteSpace(WebpartCodeName))
            {
                var webpartInfo = WebPartInfoProvider.GetWebPartInfo(WebpartCodeName);
                if (webpartInfo == null)
                {
                    if (logEvents)
                    {
                        EventLogProvider.LogEvent("E", "TransformableWebPart.ascx.cs", "NOWEBPARTCODENAME", eventDescription: "A transformable webpart has been rendered as an inline widget, but the provided Webpart Code Name [" + WebpartCodeName + "] was not valid.  Could not find a webpart by that code name.  Please correct.", eventUrl: CMSHttpContext.Current.Request.RawUrl);
                    }
                    return -1;
                }
                else
                {
                    return webpartInfo.WebPartID;
                }
            }
            else
            {
                if (logEvents)
                {
                    EventLogProvider.LogEvent("E", "TransformableWebPart.ascx.cs", "NOWEBPARTCODENAME", eventDescription: "A transformable webpart has been rendered as an inline widget, but the Webpart Code Name was not set in the properties.  Could not resolve which Transformable web part this is.", eventUrl: CMSHttpContext.Current.Request.RawUrl);
                }
                return -1;
            }
        }
    }

    /// <summary>
    /// Gets the Webpart's DataSet with table that will be transformed.
    /// </summary>
    /// <returns></returns>
    protected DataSet GetContentDataSet()
    {
        return CacheHelper.Cache(cs => GetContentDataSetCache(cs), new CacheSettings(CacheMinutes, "GetContentDataSet|" + CacheItemName));
    }

    /// <summary>
    /// Gets the Actual DataSet and data that will be passed to the Repeater
    /// </summary>
    /// <param name="cs">Cache Settings</param>
    /// <returns>The DataSet with Table that will be transformed.</returns>
    private DataSet GetContentDataSetCache(CacheSettings cs)
    {
        DataSet WebPartDataSetOrig = CacheHelper.Cache(DScs => GetWebpartBaseTable(DScs), new CacheSettings(120, "GetWebpartBaseTable|" + GetWebpartID()));
		// Clone so we do not modify the original item.
        DataSet WebPartDataSet = WebPartDataSetOrig.Clone();
        // Ensure only the 1 row is added, ran into issues where it kept adding rows
        WebPartDataSet.Tables[0].Rows.Clear();

        // Detect Columns that have multiple, like MyField_1, MyField_2, MyField_3.  If any found like that, then render into Rows
        List<string> ColumnNames = new List<string>();
        foreach (DataColumn dc in WebPartDataSet.Tables[0].Columns)
        {
            ColumnNames.Add(dc.ColumnName);
        }

        List<string> BaseColumnNamesToRemove = new List<string>();
        // Add any IsMultiPart columns to the table if they aren't there.
        foreach (string columnName in ColumnNames)
        {
            if (IsMultiPartColumn(columnName))
            {
                string baseColumnName = GetBaseColumnName(columnName);
                if (!WebPartDataSet.Tables[0].Columns.Contains(baseColumnName))
                {
                    WebPartDataSet.Tables[0].Columns.Add(new DataColumn(baseColumnName, WebPartDataSet.Tables[0].Columns[columnName].DataType));
                }
                BaseColumnNamesToRemove.Add(baseColumnName.ToLower());
            }
        }

        // Lastly, remove any of the base column names from the listing of the ColumnNames and any system used columns.
        // Didn't remove the MultiRow fields because they may need to be referenced in transformation for additional logic.
        ColumnNames.RemoveAll(x => BaseColumnNamesToRemove.Contains(x.ToLower()));
        ColumnNames.Remove("Where");
        ColumnNames.Remove("OrderBy");
        ColumnNames.Remove("TopN");
        ColumnNames.Remove("Columns");
        ColumnNames.Remove("TransformationName");
        ColumnNames.Remove("CacheItemName");
        ColumnNames.Remove("CacheMinutes");
        ColumnNames.Remove("CacheDependencies");
        

        // Create rows, can be multiple if multipart column found
        
        List<int> SkipRows = new List<int>();
        int maxRows = GetMaxIndexLength(ColumnNames, ref SkipRows);
        for (var r = 0; r < maxRows; r++)
        {
            // If this row index didn't have any values in multipart, then skip.
            if (SkipRows.Contains(r))
            {
                continue;
            }
            DataRow dr = WebPartDataSet.Tables[0].NewRow();
            foreach (string columnName in ColumnNames)
            {
                string BaseColumnName = GetBaseColumnName(columnName);
                // Handle macros
                string stringVal = MacroContext.CurrentResolver.ResolveMacros(ValidationHelper.GetString(GetValue(columnName), ""));
                switch (WebPartDataSet.Tables[0].Columns[columnName].DataType.Name.ToLower())
                {
                    case "string":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = stringVal;
                        }
                        break;
                    case "int32":
                    case "int16":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetInteger((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), 0);
                        }
                        break;
                    case "int64":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetLong((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), 0);
                        }
                        break;
                    case "byte[]":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetBinary((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), new byte[0]);
                        }
                        break;
                    case "boolean":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetBoolean((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), false);
                        }
                        break;
                    case "datetime":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetDateTime((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), new DateTime());
                        }
                        break;
                    case "decimal":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetDecimal((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), 0m);
                        }
                        break;
                    case "double":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetDouble((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), 0);
                        }
                        break;
                    case "timespan":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetTimeSpan((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), new TimeSpan(0));
                        }
                        break;
                    case "guid":
                        if (!IsMultiPartColumn(columnName) || (IsMultiPartColumn(columnName) && MultiPartCorrectIndex(columnName, r, ColumnNames)))
                        {
                            dr[BaseColumnName] = ValidationHelper.GetGuid((string.IsNullOrWhiteSpace(stringVal) ? GetValue(columnName) : stringVal), new Guid());
                        }
                        break;
                }

            }

            // Add the row data
            WebPartDataSet.Tables[0].Rows.Add(dr);
        }

        // Apply view to table if any of these values are set.
        if (!string.IsNullOrWhiteSpace(OrderBy) || !string.IsNullOrWhiteSpace(Where) || !string.IsNullOrWhiteSpace(Columns) || TopN > -1)
        {
            DataView sortedTable = new DataView(WebPartDataSet.Tables[0]);
            DataTable newTable = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(OrderBy))
                {
                    sortedTable.Sort = OrderBy;
                }
                if (!string.IsNullOrWhiteSpace(Where))
                {
                    sortedTable.RowFilter = Where;
                }

                if (!string.IsNullOrWhiteSpace(Columns))
                {
                    newTable = sortedTable.ToTable(sortedTable.Table.TableName, false, Columns.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    newTable = sortedTable.ToTable();
                }

                if (TopN > -1)
                {
                    for (int r = newTable.Rows.Count; r > 0; r--)
                    {
                        if (r > TopN)
                        {
                            newTable.Rows.RemoveAt(r - 1);
                        }
                    }
                }

                WebPartDataSet.Tables.RemoveAt(0);
                WebPartDataSet.Tables.Add(newTable);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("TransformableWebPart", "DataViewError", ex, SiteContext.CurrentSiteID, additionalMessage: "On Webpart " + WebPartID + " on Page " + CurrentDocument.NodeAliasPath);
            }
        }

        if (cs.Cached)
        {
            cs.CacheDependency = CacheHelper.GetCacheDependency(ValidationHelper.GetString(CacheDependencies.Trim('\n'), ""));
        }

        return WebPartDataSet;
    }

    /// <summary>
    /// Gets any - or _ value at the end of the column name, if it's an integer then it's considered a multipart column
    /// </summary>
    /// <param name="ColumnName">The Column Name</param>
    /// <returns></returns>
    private bool IsMultiPartColumn(string ColumnName)
    {
        if (EnableMultiRow) { 
            string[] splitVals = ColumnName.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return ValidationHelper.GetInteger(splitVals[splitVals.Length - 1], -1) > -1;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Gets the Column Name minus the numbers.  Ex "MyValue_ForThis_3" would return "MyValue_ForThis"
    /// </summary>
    /// <param name="ColumnName">The Column Name</param>
    /// <returns>The base of the Column name.</returns>
    private string GetBaseColumnName(string ColumnName)
    {
        if (EnableMultiRow && IsMultiPartColumn(ColumnName))
        {
            string[] splitVals = ColumnName.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string BaseColumnName = "";
            for (int i = 0; i < splitVals.Length - 1; i++)
            {
                BaseColumnName += splitVals[i] + "_";
            }
            return BaseColumnName.Substring(0, BaseColumnName.Length - 1);
        }
        else
        {
            return ColumnName;
        }
    }

    /// <summary>
    /// Detects if the given MultiPart Column is the correct one to get the current row's value from.
    /// </summary>
    /// <param name="ColumnName">The specific column name</param>
    /// <param name="CurrentIndex">Zero based index.</param>
    /// <param name="AllColumnNames">A List of all Column Names to determine if zero index in field name or not.</param>
    /// <returns>If this field is the proper one to get the value from or not for the given row index</returns>
    private bool MultiPartCorrectIndex(string ColumnName, int CurrentIndex, List<string> AllColumnNames)
    {
        // get base of the name
        string[] splitVals = ColumnName.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        int currentColumnIndex = ValidationHelper.GetInteger(splitVals[splitVals.Length - 1], -1);
        string BaseColumnName = GetBaseColumnName(ColumnName);

        // See if zero index or not.
        if (AllColumnNames.Contains(BaseColumnName + "_0"))
        {
            return currentColumnIndex == CurrentIndex;
        }
        else
        {
            return currentColumnIndex == CurrentIndex + 1;
        }
    }

    /// <summary>
    /// Of all the column names, gets the maximum 'rows' generated based on the largest value at the end of the column name.
    /// </summary>
    /// <param name="AllColumnNames">All the columns</param>
    /// <returns>The maximum index Length.  Takes into account if a _0 is found</returns>
    private int GetMaxIndexLength(List<string> AllColumnNames, ref List<int> SkipRowIDs)
    {
        if (!EnableMultiRow)
        {
            return 1;
        }
        int i = 0;
        bool zeroDetected = false;
        bool MultiPartDetected = false;
        List<string> BaseColumnNames = new List<string>();
        foreach (string ColumnName in AllColumnNames)
        {
            if (IsMultiPartColumn(ColumnName))
            {
                MultiPartDetected = true;
                string[] splitVals = ColumnName.Split("_".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int currentColumnIndex = ValidationHelper.GetInteger(splitVals[splitVals.Length - 1], -1);
                if (currentColumnIndex == 0)
                {
                    zeroDetected = true;
                }
                if (i < currentColumnIndex)
                {
                    i = currentColumnIndex;
                }
                string baseName = GetBaseColumnName(ColumnName);
                if (!BaseColumnNames.Contains(baseName))
                {
                    BaseColumnNames.Add(baseName);
                }
            }
        }

        if (!MultiPartDetected)
        {
            return 1;
        }

        // Add 1 to the index if there's a zero index.
        i = i + (zeroDetected ? 1 : 0);

        // Detect if all MultiPartColumns are null for higher indexes, then lower the row count.
        if (i >= 0)
        {
            if (DontRenderBlankRows)
            {
                bool ValueFound = false;
                for (int j = i - (zeroDetected ? -1 : 0); j > (zeroDetected ? -1 : 0); j--)
                {
                    bool ValueFoundForThisRow = false;
                    // Try to find a value, as soon as a value is found, then that will be the total index.
                    foreach (string baseColumnName in BaseColumnNames)
                    {
                        if (AllColumnNames.Contains(baseColumnName + "_" + j))
                        {
                            if (!(GetValue(baseColumnName + "_" + j) == null || GetValue(baseColumnName + "_" + j) == DBNull.Value || string.IsNullOrWhiteSpace(GetValue(baseColumnName + "_" + j).ToString())))
                            {
                                if (!ValueFound)
                                {
                                    ValueFound = true;
                                    i = j + (zeroDetected ? 1 : 0);
                                }
                                ValueFoundForThisRow = true;
                            }
                        }
                    }
                    // If this row is 'blank' then skip it
                    if (!ValueFoundForThisRow)
                    {
                        SkipRowIDs.Add((zeroDetected ? j : j-1));
                    }
                }
                // If no value found, then we'll consider this no rows.
                if (!ValueFound)
                {
                    i = 0;
                }
            }
            return i;
        }
        else
        {
            return 1;
        }

    }

    /// <summary>
    /// Creates a Dataset with the Table that has the Columns and types based on the Web Part's configuration.
    /// </summary>
    /// <param name="cs">Cache Setting</param>
    /// <returns>DataSet with a table that has a columns corresponding to the Properties of the Web Part</returns>
    private DataSet GetWebpartBaseTable(CacheSettings cs)
    {
        DataSet WebPartDataSet = new DataSet("TransformableWebPartDS");
        DataTable dt = new DataTable("CustomContentTable");

        // Go through each field and add the values to the columns
        var WebpartInfo = WebPartInfoProvider.GetWebPartInfo(GetWebpartID());
        XmlDocument xmlWebpartProperties = new XmlDocument();
        xmlWebpartProperties.LoadXml(WebpartInfo.WebPartProperties);

        foreach (XmlNode field in xmlWebpartProperties.SelectNodes("//field"))
        {
            // Get the field name
            string fieldName = ValidationHelper.GetString(field.Attributes["column"].Value, "");

            // Get the field type
            string fieldType = "";
            // If inherited, must look to the parent in order to get the field type
            if (ValidationHelper.GetInteger(WebpartInfo.WebPartParentID, -1) > 0)
            {
                Guid fieldGuid = ValidationHelper.GetGuid(field.Attributes["guid"].Value, new Guid());
                var parentWebPart = WebPartInfoProvider.GetWebPartInfo(WebpartInfo.WebPartParentID);
                XmlDocument parentWebpartProperties = new XmlDocument();
                parentWebpartProperties.LoadXml(parentWebPart.WebPartProperties);
                var parentField = parentWebpartProperties.SelectSingleNode("//field[@guid='" + fieldGuid.ToString().ToLower() + "']");

                // The System properties do not contain the columntype, so will return as string always and user will have to adjust with Convert.ToWhatever
                if (parentField == null)
                {
                    fieldType = "text";
                }
                else
                {
                    fieldType = parentField.Attributes["columntype"].Value;
                }
            }
            else
            {
                fieldType = field.Attributes["columntype"].Value;
            }

            switch (fieldType)
            {
                case "longtext":
                case "text":
                default:
                    dt.Columns.Add(fieldName, typeof(string));
                    break;
                case "binary":
                    dt.Columns.Add(fieldName, typeof(byte[]));
                    break;
                case "boolean":
                    dt.Columns.Add(fieldName, typeof(Boolean));
                    break;
                case "date":
                    dt.Columns.Add(fieldName, typeof(DateTime));
                    break;
                case "datetime":
                    dt.Columns.Add(fieldName, typeof(DateTime));
                    break;
                case "decimal":
                    dt.Columns.Add(fieldName, typeof(Decimal));
                    break;
                case "double":
                    dt.Columns.Add(fieldName, typeof(Double));
                    break;
                case "integer":
                    dt.Columns.Add(fieldName, typeof(Int32));
                    break;
                case "longinteger":
                    dt.Columns.Add(fieldName, typeof(Int64));
                    break;
                case "timespan":
                    dt.Columns.Add(fieldName, typeof(TimeSpan));
                    break;
                case "guid":
                    dt.Columns.Add(fieldName, typeof(Guid));
                    break;
            }
        }

        WebPartDataSet.Tables.Add(dt);

        if (cs.Cached)
        {
            cs.CacheDependency = CacheHelper.GetCacheDependency("cms.webpart|byid|" + GetWebpartID());
        }

        return WebPartDataSet;
    }

    #endregion

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing || GetWebpartID(true) == -1)
        {
            // do nothing
        }
        else
        {
            basicRepeater.ItemTemplate = TransformationHelper.LoadTransformation(this, TransformationName);
            basicRepeater.ReloadData(true);
        }
    }

    /// <summary>
    /// Binds datasource control data to the viewer control
    /// </summary>
    private void BindControl()
    {

        basicRepeater.DataSource = GetContentDataSet();
        basicRepeater.ItemTemplate = TransformationHelper.LoadTransformation(this, TransformationName);
        basicRepeater.DataBind();
        binded = true;
    }

    protected override void OnPreRender(EventArgs e)
    {
        // Notice to user so they can fix.
        if (StopProcessing)
        {

        }
        else if (GetWebpartID() == -1)
        {
            basicRepeater.ZeroRowsText = "[An error has occured: Unable to get Webpart ID.  Please see event log.]";
            basicRepeater.HideControlForZeroRows = false;
        }
        else
        {
            // Datasource data
            object ds = null;

            // Set transformations if data source is not empty
            // Get data from datasource
            ds = GetContentDataSet();

            // Check whether data exist
            if ((!DataHelper.DataSourceIsEmpty(ds)) && (!binded))
            {
                basicRepeater.DataSource = ds;
                basicRepeater.ItemTemplate = TransformationHelper.LoadTransformation(this, TransformationName);
                basicRepeater.DataBind();
            }

            base.OnPreRender(e);

            // Hide control for zero rows
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                Visible = false;
            }
        }
    }

    /// <summary>
    /// Reload data.
    /// </summary>
    public override void ReloadData()
    {
        SetupControl();
        base.ReloadData();
    }

}