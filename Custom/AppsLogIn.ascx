﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppsLogIn.ascx.cs" Inherits="CMSWebParts_Custom_AppsLogIn" %>

    <link id="Link1" href="AppsLogIn_files/CCSApps.css" rel="stylesheet" type="text/css" />
    <link href="AppsLogIn_files/Forms.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        p {
            margin: 5px 0px;           
        }
    </style>
    <script type="text/javascript" src="AppsLogIn_files/pushToHttps.js"></script>
<div style="margin: auto;">
            <fieldset class="loginbox FormBGColor">
                <legend>Login</legend>
                <p>Please login with your CCS User Principal Name (UPN):</p>
                <div id="divStudentExample" runat="server" style="border: 1px solid #0059A9; padding: 3px; border-radius: 10px; background-color: White;">
                    <p style="text-align: center;"><strong>Student Example</strong></p>
                    <p style="padding-left: 40px;">
                        <span style="font-size: x-small;">(FIRST NAME + LAST INITIAL + LAST 4 DIGITS OF ctcLink ID)</span><br />
                        KellyB2436@bigfoot.spokane.edu
                    </p>
                    <div style="margin-left: 50px; margin-bottom: 5px;">
                        <strong>&#187;&nbsp;Forgot your password?</strong><br />
                        Contact the <a href="mailto:ITSupportCenter@ccs.spokane.edu">IT Support Center</a>: (509) 533-4357
                    </div>
                </div>
                <div class="BlueBGColor" style="border: 1px solid #0059A9; padding: 3px; border-radius: 10px; margin-top: 5px;">
                    <p style="text-align: center;"><strong>Employee Examples</strong></p>
                    <div style="padding-left: 40px;">
                        John.Smyth@ccs.spokane.edu<br />
                        John.Smyth@scc.spokane.edu<br />
                        John.Smyth@sfcc.spokane.edu
                    </div>
                </div>
                <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                <br />
                <div style="padding: 7px;">
                    <asp:Label ID="Label2" runat="server">Username:</asp:Label><br />
                    <asp:TextBox ID="txtUsername" runat="server" Width="300"></asp:TextBox><br />
                </div>
                <div style="padding: 7px;">
                    <asp:Label ID="Label3" runat="server">Password:</asp:Label><br />
                    <asp:TextBox ID="txtPassword" runat="server" Width="300" TextMode="Password">"</asp:TextBox><br />
                </div>
                <div style="padding: 7px;">
                    <asp:CheckBox ID="chkPersist" runat="server" Text="Remember Me" />
                </div>
                <div style="text-align: center; padding-top: 5px;">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="button" OnClick="Login_Click"></asp:Button>
                </div>
                <div style="padding: 7px;">
                    <asp:Label ID="errorLabel" runat="server" ForeColor="#ff3300"></asp:Label>
                </div>
                <asp:Literal ID="litGPPMessage" runat="server"></asp:Literal>
                <p>Problems logging in? For assistance, please contact the <a href="mailto:ITSupportCenter@ccs.spokane.edu">IT Support Center</a>: (509) <strong>533 - H-E-L-P</strong> (533-4357)</p>
            </fieldset>
        </div>