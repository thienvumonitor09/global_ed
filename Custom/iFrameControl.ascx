<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Custom_iFrameControl"  CodeFile="~/CMSWebParts/Custom/iFrameControl.ascx.cs" %>
<style>
    .embed-container { position: relative; padding-bottom: <asp:Literal ID="litframeHeight" runat="server"></asp:Literal>%; height: 0; overflow: hidden; max-width: 100%; } 
    .embed-container iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
</style>
<div class="embed-container">
    <iframe <asp:Literal ID="litURL" runat="server"></asp:Literal> style="border:0px;overflow-y:hidden;background-color:White;"></iframe>
</div>