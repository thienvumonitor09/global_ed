﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/AreaOfStudyAtoZ.ascx.cs" Inherits="CMSWebParts_Custom_AreaOfStudyAtoZ" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%--<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
<input type="hidden" id="hidPostBack" value="" runat="server" />
<input type="hidden" id="hidLtr" value="" runat="server" />
<input type="hidden" id="hidInstitution" value="" runat="server" />
<div class="container areaOfStudy">
    <div class="btn-group">
        <asp:TextBox ID="txtTextSearch" runat="server" style="width:160px;" placeholder="e.g. Accounting" MaxLength="150"></asp:TextBox>
        <!--<span id="searchclear" class="glyphicon glyphicon-remove"></span>-->
        <button id="btnSearch" onclick="clearAlphaSearch();getAreasOfStudy($('input[id$=\'hidInstitution\']').val(),'',$('input[id$=\'txtTextSearch\']').val());" aria-label="Search">
            <span class="glyphicon glyphicon-search"></span>
        </button>
    </div>
    <div class="results"><div class="working searchinfo collapse"><h2>Searching...</h2></div></div>
    <asp:Panel ID="panAlpha" CssClass="panAlpha" runat="server">
        <ul id="alpha" class="alpha"></ul>
    </asp:Panel>
    <asp:Panel ID="areaOfStudyList" CssClass="areaOfStudyList" runat="server"></asp:Panel>
</div>

<script type="text/javascript">
    //if the document is ready
    $(document).ready(function () {
        var institution = $("input[id$='hidInstitution']").val();

        //populate the alpha character list
        $(".alpha").append('<li><a href="#" id="link" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'\',\'\');">ALL</a></li>');
        for (i = 0; i < 26; i++) {
            var alphaCharacter = (i+10).toString(36).toUpperCase();
            $(".alpha").append('<li><a href="#" id="link' + alphaCharacter + '" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'' + alphaCharacter + '\',\'\');">' + alphaCharacter + '</a></li>');
        }

        //if not a post back
        if($("input[id$='hidPostBack']").val() != "true"){
            //populate all areas of study by default
            getAreasOfStudy(institution, "", "");
        } else {
            //populate areas of study by search parameters
            getAreasOfStudy(institution, $("input[id$='hidLtr']").val(), $("input[id$='txtTextSearch']").val());
        }
    })

    //clear search text
    $("#searchclear").click(function () {
        $('input[id$=\'txtTextSearch\']').val('');
    });

    function getAreasOfStudy(institution, alphaCharacter, keyword) {

        $(".results .searchstat").empty();
        $(".working").fadeIn();

        var workingIndicator = setInterval(function () { $(".working h2").append(".") }, 500);

        //remove previously active letter
        $("a[id^='link']").removeClass("active");

        //make the selected letter active
        $("a[id$='link" + alphaCharacter + "']").addClass("active");

        //store the alpha character in a hidden element
        if (alphaCharacter != null && alphaCharacter != "") {
            $("input[id$='hidLtr']").val(alphaCharacter);
        }

        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/WebServices/icatalogjson.asmx/GetAreasOfStudyByKeyword",
            data: { STRM: "", Institution: institution, AlphaCharacter: alphaCharacter, Keyword: keyword },
            dataType: "jsonp",
            success: function (data) {
                var oldAreaOfStudy = "", oldAlphaCharacter = "", rowCount = 0;

                //clear the area of study list
                $(".areaOfStudyList").empty();
                //clear how many results were found
                //$(".results").empty();

                $.each(data.Table, function (index, areaOfStudy) {
                    var newAreaOfStudy = areaOfStudy.Title;
                    var newAlphaCharacter = newAreaOfStudy.substring(0, 1);
                    var websiteURL = areaOfStudy.WebsiteURL;

                    //if the area of study title starts with a new alpha character
                    if (newAlphaCharacter != oldAlphaCharacter) {
                        //display the new alpha character row area of study list
                        $(".areaOfStudyList").append("<div class='row' id='row" + newAlphaCharacter + "'></div>");
                        //if the alpha character is not the first in the loop
                        if (oldAlphaCharacter != "") {
                            //display an hr after the previous alpha area of study list
                            $("#row" + newAlphaCharacter).append("<div class='col-lg-12'><hr /></div>");
                        }
                        //display the new alpha character column in the area of study list
                        $("#row" + newAlphaCharacter).append("<div class='col-lg-12 alphaCharacter'><h1>" + newAlphaCharacter + "</h1></div>");
                    }

                    //if first time looping through the area of study
                    if (newAreaOfStudy != oldAreaOfStudy) {
                        if (websiteURL == null || websiteURL == "") {
                            websiteURL = "#";
                        }
                        $("#row" + newAlphaCharacter).append("<div class='col-lg-6'><h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy + "</a></h2></div>");
                        rowCount++;
                    }

                    //store the area of study being looped through
                    oldAreaOfStudy = newAreaOfStudy;
                    //store the alpha character being looped through
                    oldAlphaCharacter = newAlphaCharacter;
                });

                //hide the "Working" indicator
                clearInterval(workingIndicator);
                $(".working").hide();

                //display how many results were found
                if (keyword != null && keyword != "") {
                    $(".results").append(('<div class="searchstat"><h2>Found ' + rowCount + ' areas of study containing "' + keyword + '"</h2></div>').replace("Found 1 areas", "Found 1 area").replace("Found 0 areas of study", "No areas of study found"));
                } else {
                    if (alphaCharacter != null && alphaCharacter != "") {
                        $(".results").append(('<div class="searchstat"><h2>Found ' + rowCount + ' areas of study beginning with "' + alphaCharacter + '"</h2></div>').replace("Found 1 areas", "Found 1 area").replace("Found 0 areas of study", "No areas of study found"));
                    }
                }
            },
            error: function (error) {
                $(".results .searchstat").remove();
                $(".working").hide();
                setTimeout(function () { $(".results").append('<div class="searchstat"><h2>Timeout: Service Unavailable</h2></div>'); }, 1000);
            }
        });
    }

    function clearTextSearch() {
         $("input[id$='txtTextSearch']").val("");
    }

    function clearAlphaSearch() {
        $("input[id$='hidLtr']").val("");
    }

</script>
   