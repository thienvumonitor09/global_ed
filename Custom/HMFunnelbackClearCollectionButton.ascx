﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HMFunnelbackClearCollectionButton.ascx.cs" Inherits="CMSWebParts_Custom_HMFunnelbackClearCollectionButton" %>


<asp:Button id="btnClearAll" runat="server" OnClick="btnClearAll_Click" CommandArgument="" Text="Clear All Documents from Collection" />
<asp:Literal id="ltlResult" runat="server" text=""></asp:Literal>

<ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" TargetControlID="btnClearAll" ConfirmText="Are you sure you want to clear all documents from the collection?" />