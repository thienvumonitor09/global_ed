﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/SearchStream.ascx.cs" Inherits="CMSWebParts_Custom_SearchStream" %>
<asp:Literal ID="SearchOutput" runat="server" />
<script type="text/javascript">
    // Clear form post for Kentico
    var url = window.location.pathname;
    var rootPageName = url.substring(url.lastIndexOf('/') + 1);
    //alert(rootPageName);
    document.forms[0].action = '/' + rootPageName;
</script>