﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_Custom_ClassSearch : CMSAbstractWebPart {
    //initialize web service
    ClassScheduleXML.ClassScheduleXML wsClassSchedule = new ClassScheduleXML.ClassScheduleXML();
    protected void Page_Load(object sender, EventArgs e) {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/ClassSearch_files/ClassSearch.css";
        Page.Header.Controls.Add(styleLink);
        error.Visible = false;

        try {
            //check if the Replicated Class Data Exists
            if (wsClassSchedule.ReplicatedDataExists()) {
                bool displayClasses = true;

                if (IsPostBack) {
                    panClasses.Visible = false;
                    panMsg.Visible = false;
                }

                //populate the terms/quarter drop down list
                cboQuarter.Items.Clear();
                DataSet dsTerms = wsClassSchedule.GetTerms();
                Int32 termCount = dsTerms.Tables[0].Rows.Count;
                if (termCount > 0) {
                    for (Int16 i = 0; i < termCount; i++) {
                        if (Convert.ToDateTime(dsTerms.Tables[0].Rows[i]["SSR_SSENRLDISP_BDT"]) <= DateTime.Now) {
                            cboQuarter.Items.Add(dsTerms.Tables[0].Rows[i]["Quarter"].ToString());
                            cboQuarter.Items[i].Value = dsTerms.Tables[0].Rows[i]["STRM"].ToString();
                        }
                    }

                    try {
                        cboQuarter.SelectedValue = Request.Form[cboQuarter.UniqueID];
                    } catch {
                        //do nothing
                    }

                    if (!IsPostBack) {

                        try {
                            //select the most recent quarter populated
                            String registrationTerm = wsClassSchedule.GetRegistrationTerm();
                            cboQuarter.SelectedValue = registrationTerm;
                            if (cboQuarter.SelectedValue != registrationTerm) {
                                cboQuarter.SelectedIndex = cboQuarter.Items.Count - 1;
                            }
                        } catch {
                            //do nothing
                        }

                        try {
                            //if not a postback, institution is grabbed from the querystring.
                            hidInst.Value = Request.QueryString["inst"].ToLower();
                            if (hidInst.Value == "scc" || hidInst.Value == "sccel") {
                                cboInstitution.SelectedValue = "WA171";
                                if (hidInst.Value == "sccel") {
                                    chkAct2.Checked = true;
                                }
                            } else if (hidInst.Value == "sfcc") {
                                cboInstitution.SelectedValue = "WA172";
                            }
                        } catch {
                            //do nothing
                        }

                        String classTimes = Request.QueryString["times"];
                        if (classTimes != null && classTimes != "") {
                            try {
                                cboClassTimes.SelectedValue = classTimes;
                            } catch {
                                //do nothing
                            }
                        }
                    }

                    PopulateLocation();

                    if (!IsPostBack) {
                        String location = Request.QueryString["location"];
                        if (location != null && location != "") {
                            location = location.ToUpper();
                            hidLocation.Value = location;
                            cboLocation.SelectedValue = location;
                            if (cboLocation.SelectedValue != location) {
                                String quarter = cboQuarter.SelectedItem.Text;
                                quarter = quarter.Substring(0, 1) + quarter.Substring(1).ToLower();
                                msg.InnerText = wsClassSchedule.GetLocation(location) + " does not offer classes for " + quarter + ". Please select a different quarter or location.";
                                panMsg.Visible = true;
                                panClasses.Visible = false;
                                displayClasses = false;
                            }
                        }
                    } else {

                        msg.InnerText = "No classes were found matching your search criteria.";

                        try {
                            //Response.Write("<script>alert('Request.Form[" + hidLocation.UniqueID + "] = " + Request.Form[hidLocation.UniqueID] + "<br />Request.Form[" + cboLocation.UniqueID + "] = " + Request.Form[cboLocation.UniqueID] + "');</script>");
                            String queryLocation = Request.Form[hidLocation.UniqueID];
                            String selectedLocation = Request.Form[cboLocation.UniqueID];
                            if (queryLocation != null && queryLocation != "" && (selectedLocation == null)) {
                                cboLocation.SelectedValue = queryLocation;
                            } else {
                                cboLocation.SelectedValue = selectedLocation;
                            }
                        } catch {
                            //do nothing
                        }
                    }

                    PopulateSubject();

                    try {
                        cboSubject.SelectedValue = Request.Form[cboSubject.UniqueID];
                    } catch {
                        //do nothing
                    }

                    PopulateInstructionMode();

                    if (!IsPostBack) {
                        String instructionMode = Request.QueryString["mode"];
                        if (instructionMode != null && instructionMode != "") {
                            try {
                                cboInstructionMode.SelectedValue = instructionMode.ToUpper();
                            } catch {
                                //do nothing
                            }
                        }
                    } else {
                        try {
                            cboInstructionMode.SelectedValue = Request.Form[cboInstructionMode.UniqueID];
                        } catch {
                            //do nothing
                        }
                    }

                    if (hidFilters.Value == "true") {
                        filters.Attributes["style"] = "";
                        arrow.Attributes["class"] = "down-arrow";
                        more.Attributes["style"] = "border-bottom: 1px solid #D8D8D8;text-align:right;";
                    } else if (hidFilters.Value == "false") {
                        filters.Attributes["style"] = "display:none;";
                        arrow.Attributes["class"] = "right-arrow";
                        more.Attributes["style"] = "border:none;float:right;";
                    }

                    if (displayClasses) {
                        DisplayClasses();
                    }
                } else {
                    DisplayErrorMessage();
                }
            } else {
                DisplayErrorMessage();
            }
        } catch {
            DisplayErrorMessage();
        }
    }

    private void DisplayErrorMessage() {
        error.InnerHtml = "<p>Sorry, something went wrong. The class search feature is currently unavailable. Please check back later.</p>";
        error.Visible = true;
        panClasses.Visible = false;
        panSearch.Visible = false;
    }

    protected void cmdSubmit_Click(object sender, EventArgs e) {
        //display classes
        DisplayClasses();
    }

    protected void cmdReset_Click(object sender, EventArgs e) {
        try {
            //select the most recent quarter populated
            String registrationTerm = wsClassSchedule.GetRegistrationTerm();
            cboQuarter.SelectedValue = registrationTerm;
            if (cboQuarter.SelectedValue != registrationTerm) {
                cboQuarter.SelectedIndex = cboQuarter.Items.Count - 1;
            }
        } catch {
            //do nothing
        }

        try {
            if (hidInst.Value == "scc" || hidInst.Value == "sccel") {
                cboInstitution.SelectedValue = "WA171";
            } else if (hidInst.Value == "sfcc") {
                cboInstitution.SelectedValue = "WA172";
            } else {
                cboInstitution.SelectedValue = "";
            }
        } catch {
            cboInstitution.SelectedValue = "";
        }

        cboLocation.SelectedValue = "";
        cboSubject.SelectedValue = "";
        txtCourseNumber.Text = "";
        cboEnrlStatus.SelectedValue = "O";
        txtInstructor.Text = "";
        cboInstructionMode.SelectedValue = "";
        txtTextSearch.Text = "";
        cboClassTimes.Text = "";
        chkWritingIntensive.Checked = false;
        chkDiversity.Checked = false;
        chkHonors.Checked = false;
        chkLCCM.Checked = false;
        chkSOER.Checked = false;

        PopulateLocation();
        PopulateSubject();
        PopulateInstructionMode();

        DisplayClasses();
    }

    protected void cmdSave_Click(object sender, EventArgs e) {
        Response.Clear();
        Response.ContentType = "text/plain";
        Response.AddHeader("content-disposition", "attachment;filename=CommunityCollegesOfSpokaneClassList.txt");

        Response.Write("Community Colleges of Spokane - " + cboQuarter.SelectedItem.Text);

        String[] strClassList = hidClasses.Value.Substring(1).Split(',');
        for (Int16 i = 0; i < strClassList.Length; i++) {
            DataSet dsClass = wsClassSchedule.GetClasses(cboQuarter.SelectedValue, "", "", Convert.ToInt16(strClassList[i]), "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, "", "", "");
            String PreviousClassNbr = "", PreviousClassSubject = "", PreviousClassDays = "", PreviousClassTimes = "", PreviousClassLocation = "", PreviousClassInstructor = "", ClassNbr = "";

            for (Int16 j = 0; j < dsClass.Tables[0].Rows.Count; j++) {
                String CurrentInstitution = dsClass.Tables[0].Rows[j]["INSTITUTION"].ToString();
                String CurrentClassNbr = dsClass.Tables[0].Rows[j]["CLASS_NBR"].ToString();
                String CurrentClassSubject = dsClass.Tables[0].Rows[j]["SUBJECT"].ToString() + " " + dsClass.Tables[0].Rows[j]["CATALOG_NBR"].ToString();
                String CurrentClassStartTime = dsClass.Tables[0].Rows[j]["ClassTimeStart"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                String CurrentClassEndTime = dsClass.Tables[0].Rows[j]["ClassTimeEnd"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                String CurrentClassTimes = "", CurrentClassDays = ""; //Will have repeating rows, need to concatinate to a complete string
                String CurrentClassLocation = dsClass.Tables[0].Rows[j]["FacilityTable_DESCR"].ToString(); //Will have repeating rows, need to concatinate to a complete string
                String CurrentClassInstructor = dsClass.Tables[0].Rows[j]["FIRST_NAME"].ToString() + " " + dsClass.Tables[0].Rows[j]["LAST_NAME"].ToString();
                String Mon = dsClass.Tables[0].Rows[j]["MON"].ToString();
                String Tues = dsClass.Tables[0].Rows[j]["TUES"].ToString();
                String Wed = dsClass.Tables[0].Rows[j]["WED"].ToString();
                String Thur = dsClass.Tables[0].Rows[j]["THURS"].ToString();
                String Fri = dsClass.Tables[0].Rows[j]["FRI"].ToString();
                String Sat = dsClass.Tables[0].Rows[j]["SAT"].ToString();
                String Sun = dsClass.Tables[0].Rows[j]["SUN"].ToString();

                if (PreviousClassNbr != CurrentClassNbr) {
                    Response.Write("\r\n");
                }

                //store the class times
                if (CurrentClassStartTime == null || CurrentClassStartTime.Trim() == "") {
                    CurrentClassTimes = "TBA";
                } else {
                    CurrentClassTimes = CurrentClassStartTime;
                }

                if (CurrentClassEndTime == null || CurrentClassEndTime.Trim() == "") {
                    CurrentClassTimes = "TBA";
                } else {
                    CurrentClassTimes += " - " + CurrentClassEndTime;
                }

                //store the class days
                if (Mon == "Y") {
                    CurrentClassDays += " Mo";
                }

                if (Tues == "Y") {
                    CurrentClassDays += " Tu";
                }

                if (Wed == "Y") {
                    CurrentClassDays += " We";
                }

                if (Thur == "Y") {
                    CurrentClassDays += " Th";
                }

                if (Fri == "Y") {
                    CurrentClassDays += " Fr";
                }

                if (Sat == "Y") {
                    CurrentClassDays += " Sa";
                }

                if (Sun == "Y") {
                    CurrentClassDays += "Su";
                }

                if (CurrentClassDays == null || CurrentClassDays.Trim() == "") {
                    CurrentClassDays = dsClass.Tables[0].Rows[j]["STND_MTG_PAT"].ToString();
                    if (CurrentClassDays.Trim() == "") {
                        CurrentClassDays = "TBA";
                    }
                } else {
                    CurrentClassDays = CurrentClassDays.Substring(1);
                }

                if (CurrentClassDays == "ARR" && CurrentClassTimes == "TBA") {
                    CurrentClassTimes = "ARR";
                }

                if (CurrentInstitution == "WA171") {
                    CurrentInstitution = "SCC";
                } else if (CurrentInstitution == "WA172") {
                    CurrentInstitution = "SFCC";
                }

                //store the class location
                if (CurrentClassLocation == null || CurrentClassLocation.Trim() == "") {
                    CurrentClassLocation = "TBA";
                }

                //if writing the first record of a new class
                if (PreviousClassNbr != CurrentClassNbr) {
                    //write the class number, subject, and title
                    Response.Write(ClassNbr = "\r\n\r\n" + CurrentClassNbr + " - " + CurrentClassSubject + " - " + dsClass.Tables[0].Rows[j]["COURSE_TITLE_LONG"].ToString() + " (" + CurrentInstitution + ")");
                }

                //if the class days, times, or location is different
                if (PreviousClassDays != CurrentClassDays || PreviousClassTimes != CurrentClassTimes || PreviousClassLocation != CurrentClassLocation) {
                    //write the class days, times, location, and instructor
                    Response.Write("\r\n\r\n       " + CurrentClassDays + "     " + CurrentClassTimes + "     " + CurrentClassLocation + "     " + CurrentClassInstructor);
                } else if (PreviousClassInstructor != CurrentClassInstructor) //if the days, times, and location are the same, but the instructor is different
                  {
                    //append the class instructor
                    Response.Write(", " + CurrentClassInstructor);
                }

                PreviousClassNbr = CurrentClassNbr;
                PreviousClassSubject = CurrentClassSubject;
                PreviousClassDays = CurrentClassDays;
                PreviousClassTimes = CurrentClassTimes;
                PreviousClassLocation = CurrentClassLocation;
                PreviousClassInstructor = CurrentClassInstructor;
            }
        }

        Response.Flush();
        Response.End();
    }

    private void PopulateLocation() {
        //populate the location drop down list
        cboLocation.Items.Clear();
        cboLocation.AppendDataBoundItems = true;
        cboLocation.Items.Add("ALL");
        cboLocation.Items[0].Value = "";
        DataSet dsLocations = wsClassSchedule.GetLocations(cboQuarter.SelectedValue, cboInstitution.SelectedValue);
        for (Int16 i = 0; i < dsLocations.Tables[0].Rows.Count; i++) {
            String location = dsLocations.Tables[0].Rows[i]["LOCATION"].ToString();
            if (location == "FMAIN") {
                dsLocations.Tables[0].Rows[i].SetField("DESCR", "Spokane Falls Community College");
            } else if (location == "SMAIN") {
                dsLocations.Tables[0].Rows[i].SetField("DESCR", "Spokane Community College");
            }
        }
        DataView dvLocations = new DataView(dsLocations.Tables[0]);
        dvLocations.Sort = "DESCR";
        cboLocation.DataSource = dvLocations;
        cboLocation.DataTextField = "DESCR";
        cboLocation.DataValueField = "LOCATION";
        cboLocation.DataBind();
    }

    private void PopulateSubject() {
        //populate the subject drop down list
        cboSubject.Items.Clear();
        cboSubject.Items.Add("ALL");
        cboSubject.Items[0].Value = "";
        Int32 index = 1;
        DataSet dsSubject = wsClassSchedule.GetSubjects(cboQuarter.SelectedValue, cboInstitution.SelectedValue, cboLocation.SelectedValue);
        for (Int16 i = 0; i < dsSubject.Tables[0].Rows.Count; i++) {
            String subject = dsSubject.Tables[0].Rows[i]["SUBJECT"].ToString().Replace("&", "");
            String subjectDesc = dsSubject.Tables[0].Rows[i]["DESCR"].ToString();
            //remove ampersand from subject description
            if (subjectDesc.Substring(subjectDesc.Length - 1) == "&") {
                subjectDesc = subjectDesc.Substring(0, subjectDesc.Length - 1);
            }
            //check that the subject hasn't already been added
            if (!cboSubject.Items.Contains(cboSubject.Items.FindByValue(subject))) {
                cboSubject.Items.Add(subject + " - " + subjectDesc);
                cboSubject.Items[index].Value = subject;
                index++;
            }
        }
    }

    private void PopulateInstructionMode() {
        //populate the instruction mode drop down list
        cboInstructionMode.Items.Clear();
        cboInstructionMode.Items.Add("ALL");
        cboInstructionMode.Items[0].Value = "";
        DataSet dsInstructionMode = wsClassSchedule.GetInstructionModes(cboQuarter.SelectedValue, cboInstitution.SelectedValue, cboLocation.SelectedValue);
        for (Int16 i = 0; i < dsInstructionMode.Tables[0].Rows.Count; i++) {
            cboInstructionMode.Items.Add(dsInstructionMode.Tables[0].Rows[i]["DESCR"].ToString());
            cboInstructionMode.Items[i + 1].Value = dsInstructionMode.Tables[0].Rows[i]["INSTRUCTION_MODE"].ToString();
        }
    }

    private void DisplayClasses() {
        //Store bit values
        Byte WritingInstensive = 0, Diversity = 0, Honors = 0, LCCM = 0, SOER = 0, Credit = 0, Noncredit = 0, Act2 = 0;

        if (chkWritingIntensive.Checked) {
            WritingInstensive = 1;
        }
        if (chkDiversity.Checked) {
            Diversity = 1;
        }
        if (chkHonors.Checked) {
            Honors = 1;
        }
        if (chkLCCM.Checked) {
            LCCM = 1;
        }
        if (chkSOER.Checked) {
            SOER = 1;
        }
        if (chkCredit.Checked) {
            Credit = 1;
        }
        if (chkNonCredit.Checked) {
            Noncredit = 1;
        }
        if (chkAct2.Checked) {
            Act2 = 1;
        }

        //store class nbr value
        Int16 classNbr = 0;
        try {
            classNbr = Convert.ToInt16(txtClassNumber.Text);
        } catch {
            classNbr = 0;
        }

        //store start time values
        String startTimeBegin = "", startTimeEnd = "";
        if (cboClassTimes.SelectedValue == "morning") {
            startTimeBegin = "12:00AM";
            startTimeEnd = "11:59AM";
        } else if (cboClassTimes.SelectedValue == "afternoon") {
            startTimeBegin = "12:00PM";
            startTimeEnd = "4:59PM";
        } else if (cboClassTimes.SelectedValue == "evening") {
            startTimeBegin = "5:00PM";
            startTimeEnd = "11:59PM";
        }

        //store class list
        DataSet dsClasses = wsClassSchedule.GetClassList(cboQuarter.SelectedValue, cboInstitution.SelectedValue, "", classNbr, cboLocation.SelectedValue, cboSubject.SelectedValue, txtCourseNumber.Text, cboEnrlStatus.SelectedValue, txtInstructor.Text, cboInstructionMode.SelectedValue, Credit, Noncredit, Act2, WritingInstensive, Diversity, Honors, LCCM, SOER, txtTextSearch.Text, startTimeBegin, startTimeEnd);

        String PreviousSubject = "", PreviousClassNbr = "", PreviousClassLocation = "", PreviousClassDays = "", PreviousClassTimes = "", ClassSchedule = "";
        Int32 ClassCount = dsClasses.Tables[0].Rows.Count;

        if (ClassCount > 0) {
            //clear all classes
            divClasses.Controls.Clear();

            #region Large and Medium View Column Header

            HtmlGenericControl divRow = new HtmlGenericControl("DIV");
            divRow.Attributes["class"] = "row classHeaderRow hidden-xs hidden-sm";
            divRow.Attributes["style"] = "white-space: nowrap;";

            //Class Nbr Header
            HtmlGenericControl divClassNbr = new HtmlGenericControl("DIV");
            divClassNbr.Attributes["class"] = "classNbrHeader classCell col-lg-1 col-md-1";
            divClassNbr.Attributes["style"] = "nowrap";
            divClassNbr.Controls.Add(new LiteralControl("Class Nbr"));
            divRow.Controls.Add(divClassNbr);

            //Course Header
            HtmlGenericControl divCourse = new HtmlGenericControl("DIV");
            divCourse.Attributes["class"] = "courseHeader classCell col-lg-1 col-md-1";
            divCourse.Controls.Add(new LiteralControl("Course"));
            divRow.Controls.Add(divCourse);

            //Course Title Header
            HtmlGenericControl divCourseTitle = new HtmlGenericControl("DIV");
            divCourseTitle.Attributes["class"] = "courseTitleHeader classCell col-lg-2 col-md-2";
            divCourseTitle.Controls.Add(new LiteralControl("Course Title"));
            divRow.Controls.Add(divCourseTitle);

            //Credits Header
            HtmlGenericControl divCredits = new HtmlGenericControl("DIV");
            divCredits.Attributes["class"] = "creditsHeader classCell col-lg-1 col-md-1";
            divCredits.Controls.Add(new LiteralControl("Credits"));
            divRow.Controls.Add(divCredits);

            //Outer column for Days, Time, and Location
            HtmlGenericControl divOuterCol = new HtmlGenericControl("DIV");
            divOuterCol.Attributes["class"] = "col-lg-6 col-md-6";

            //Nested row for Days, Time, and Location
            HtmlGenericControl divNestedRow = new HtmlGenericControl("DIV");
            divNestedRow.Attributes["class"] = "row";

            //Days Header
            HtmlGenericControl divDays = new HtmlGenericControl("DIV");
            divDays.Attributes["class"] = "daysHeader classCell col-lg-3 col-md-3";
            divDays.Attributes["style"] = "padding-left:10px;";
            divDays.Controls.Add(new LiteralControl("Days"));
            divNestedRow.Controls.Add(divDays);

            //Time Header
            HtmlGenericControl divTime = new HtmlGenericControl("DIV");
            divTime.Attributes["class"] = "timeHeader classCell col-lg-4 col-md-4";
            divTime.Attributes["style"] = "padding-left:10px;";
            divTime.Controls.Add(new LiteralControl("Time"));
            divNestedRow.Controls.Add(divTime);

            //Location Header
            HtmlGenericControl divLocation = new HtmlGenericControl("DIV");
            divLocation.Attributes["class"] = "locationHeader classCell col-lg-5 col-md-5";
            divLocation.Attributes["style"] = "padding-left:10px;";
            divLocation.Controls.Add(new LiteralControl("Location"));
            divNestedRow.Controls.Add(divLocation);

            //add Days, Time, and Location to a nested row
            divOuterCol.Controls.Add(divNestedRow);
            divRow.Controls.Add(divOuterCol);

            //Status Header
            HtmlGenericControl divStatus = new HtmlGenericControl("DIV");
            divStatus.Attributes["class"] = "statusHeader classCell col-lg-1 hidden-md";
            divStatus.Controls.Add(new LiteralControl("Status"));
            divRow.Controls.Add(divStatus);

            divClasses.Controls.Add(divRow);

            #endregion

            //Last Updated Date/Time
            String LastReplicationDate = String.Format("{0:g}", dsClasses.Tables[0].Rows[0]["LASTREPLICATIONDATE"]);
            lblLastUpdated.Text = "Last Updated " + LastReplicationDate;

            //loop through the class list
            for (Int32 i = 0; i < ClassCount; i++) {
                String CurrentClassNbr = dsClasses.Tables[0].Rows[i]["CLASS_NBR"].ToString();
                String CurrentSubject = dsClasses.Tables[0].Rows[i]["SUBJECT"].ToString() + " " + dsClasses.Tables[0].Rows[i]["CATALOG_NBR"].ToString();
                String CourseTitle = dsClasses.Tables[0].Rows[i]["COURSE_TITLE_LONG"].ToString();
                String UnitsMinimum = dsClasses.Tables[0].Rows[i]["UNITS_MINIMUM"].ToString();
                String UnitsMaximum = dsClasses.Tables[0].Rows[i]["UNITS_MAXIMUM"].ToString();
                String Units = "";
                String ClassStartTime = dsClasses.Tables[0].Rows[i]["ClassTimeStart"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                String ClassEndTime = dsClasses.Tables[0].Rows[i]["ClassTimeEnd"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                String CurrentClassTimes = "", CurrentClassDays = ""; //Will have repeating rows, need to concatinate to a complete string
                String Mon = dsClasses.Tables[0].Rows[i]["MON"].ToString();
                String Tues = dsClasses.Tables[0].Rows[i]["TUES"].ToString();
                String Wed = dsClasses.Tables[0].Rows[i]["WED"].ToString();
                String Thur = dsClasses.Tables[0].Rows[i]["THURS"].ToString();
                String Fri = dsClasses.Tables[0].Rows[i]["FRI"].ToString();
                String Sat = dsClasses.Tables[0].Rows[i]["SAT"].ToString();
                String Sun = dsClasses.Tables[0].Rows[i]["SUN"].ToString();
                String CurrentClassLocation = dsClasses.Tables[0].Rows[i]["FacilityTable_DESCR"].ToString(); //Will have repeating rows, need to concatinate to a complete string
                String EnrollmentStatus = dsClasses.Tables[0].Rows[i]["ENRL_STAT"].ToString();
                String CombinedSection = dsClasses.Tables[0].Rows[i]["COMBINED_SECTION"].ToString();
                String CombinedSectionEnrollmentTotal = dsClasses.Tables[0].Rows[i]["SectionCombinedTable_ENRL_TOT"].ToString();
                String CombinedSectionEnrollmentCapacity = dsClasses.Tables[0].Rows[i]["SectionCombinedTable_ENRL_CAP"].ToString();
                String EnrollmentTotal = dsClasses.Tables[0].Rows[i]["ENRL_TOT"].ToString();
                String EnrollmentCapacity = dsClasses.Tables[0].Rows[i]["ENRL_CAP"].ToString();
                String Institution = dsClasses.Tables[0].Rows[i]["INSTITUTION"].ToString();
                String STRM = dsClasses.Tables[0].Rows[i]["STRM"].ToString();

                //store the class units/credits
                if (UnitsMinimum != null && UnitsMinimum != "") {
                    UnitsMinimum = Convert.ToDouble(UnitsMinimum).ToString();
                }
                if (UnitsMaximum != null && UnitsMaximum != "") {
                    UnitsMaximum = Convert.ToDouble(UnitsMaximum).ToString();
                }
                if (UnitsMinimum != UnitsMaximum) {
                    Units = UnitsMinimum + "-" + UnitsMaximum;
                } else {
                    Units = UnitsMinimum;
                }

                //store the class times
                if (ClassStartTime == null || ClassStartTime.Trim() == "") {
                    CurrentClassTimes = "TBA";
                } else {
                    CurrentClassTimes = ClassStartTime;
                }

                if (ClassEndTime == null || ClassEndTime.Trim() == "") {
                    CurrentClassTimes = "TBA";
                } else {
                    CurrentClassTimes += " - " + ClassEndTime;
                }

                //store the class days
                if (Mon == "Y") {
                    CurrentClassDays += " Mo";
                }

                if (Tues == "Y") {
                    CurrentClassDays += " Tu";
                }

                if (Wed == "Y") {
                    CurrentClassDays += " We";
                }

                if (Thur == "Y") {
                    CurrentClassDays += " Th";
                }

                if (Fri == "Y") {
                    CurrentClassDays += " Fr";
                }

                if (Sat == "Y") {
                    CurrentClassDays += " Sa";
                }

                if (Sun == "Y") {
                    CurrentClassDays += "Su";
                }

                if (CurrentClassDays == null || CurrentClassDays.Trim() == "") {
                    CurrentClassDays = dsClasses.Tables[0].Rows[i]["STND_MTG_PAT"].ToString();
                    if (CurrentClassDays.Trim() == "") {
                        CurrentClassDays = "TBA";
                    }
                } else {
                    CurrentClassDays = CurrentClassDays.Substring(1);
                }

                if (CurrentClassDays == "ARR" && CurrentClassTimes == "TBA") {
                    CurrentClassTimes = "ARR";
                }

                //store the class location
                if (CurrentClassLocation == null || CurrentClassLocation.Trim() == "") {
                    CurrentClassLocation = "TBA";
                }

                //store the class enrollment status
                if (EnrollmentStatus == "O") {
                    if (CombinedSectionEnrollmentCapacity != null && CombinedSectionEnrollmentCapacity != "" && CombinedSectionEnrollmentTotal != null && CombinedSectionEnrollmentTotal != "") {
                        Int16 csEnrollmentCapacity = Convert.ToInt16(CombinedSectionEnrollmentCapacity), csEnrollmentTotal = Convert.ToInt16(CombinedSectionEnrollmentTotal);
                        if (csEnrollmentTotal >= csEnrollmentCapacity) {
                            EnrollmentStatus = "Wait List";
                        } else {
                            EnrollmentStatus = "Open";
                        }
                    } else if (EnrollmentCapacity != null && EnrollmentCapacity != "" && EnrollmentTotal != null && EnrollmentTotal != "") {
                        if (Convert.ToInt16(EnrollmentTotal) >= Convert.ToInt16(EnrollmentCapacity)) {
                            EnrollmentStatus = "Wait List";
                        } else {
                            EnrollmentStatus = "Open";
                        }
                    } else {
                        EnrollmentStatus = "Open";
                    }
                } else if (EnrollmentStatus == "C") {
                    EnrollmentStatus = "Closed";
                } else if (EnrollmentStatus == "W") {
                    EnrollmentStatus = "Wait List";
                }

                //store repeating days, times, locations, and instructors
                if ((PreviousClassNbr != CurrentClassNbr) || (PreviousClassDays != CurrentClassDays || PreviousClassTimes != CurrentClassTimes || PreviousClassLocation != CurrentClassLocation)) {
                    if (ClassSchedule != "") {
                        ClassSchedule += "<div class=\"row\" style=\"padding-top:5px;\">";
                    } else {
                        ClassSchedule += "<div class=\"row hidden-lg hidden-md\">";
                        ClassSchedule += "<div class=\"col-sm-3 col-xs-3\" style=\"padding-left:10px;\"><strong>Days</strong></div>";
                        ClassSchedule += "<div class=\"col-sm-4 col-xs-4\" style=\"padding-left:10px;\"><strong>Time</strong></div>";
                        ClassSchedule += "<div class=\"col-sm-5 col-xs-5\" style=\"padding-left:10px;\"><strong>Location</strong></div>";
                        ClassSchedule += "</div>";
                        ClassSchedule += "<div class=\"row\">";
                    }
                    ClassSchedule += "<div class=\"days col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding\" style=\"white-space:normal;padding-left:10px;\">" + CurrentClassDays + "</div>";
                    ClassSchedule += "<div class=\"time col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding\" style=\"white-space:normal;padding-left:10px;\">" + CurrentClassTimes + "</div>";
                    ClassSchedule += "<div class=\"location col-lg-5 col-md-5 col-sm-5 col-xs-5 nopadding\" style=\"white-space:normal;padding-left:10px;\">" + CurrentClassLocation + "</div>";
                    ClassSchedule += "</div>";
                }

                //display the class
                if (i == (ClassCount - 1) || CurrentClassNbr != dsClasses.Tables[0].Rows[i + 1]["CLASS_NBR"].ToString()) {

                    //Class Row
                    divRow = new HtmlGenericControl("DIV");
                    divRow.ID = "row" + i;
                    divRow.Attributes["class"] = "expand row classRow";
                    divRow.Attributes["style"] = "white-space: nowrap;";
                    divRow.Attributes["onclick"] = "toggleDetails('row" + i + "','tr" + CurrentClassNbr + "','" + STRM + "','" + Institution + "','" + CurrentClassNbr + "');";
                    divRow.Attributes["title"] = "View Class Details";

                    //Outer column for Checkbox and Course for small and x-small display
                    divOuterCol = new HtmlGenericControl("DIV");
                    divOuterCol.Attributes["class"] = "hidden-lg hidden-md col-sm-12 col-xs-12";

                    //Nested row for Checkbox and Course for small and x-small display
                    divNestedRow = new HtmlGenericControl("DIV");
                    divNestedRow.Attributes["class"] = "row";
                    divNestedRow.Attributes["style"] = "white-space:nowrap;";

                    //Checkbox for small and x-small display
                    HtmlGenericControl divCheckBox = new HtmlGenericControl("DIV");
                    divCheckBox.Attributes["class"] = "checkbox divCheckBox classCell col-sm-1 col-xs-2";
                    divCheckBox.Attributes["style"] = "";
                    divCheckBox.Controls.Add(new LiteralControl("<input type=\"checkbox\" id=\"chkSmall" + CurrentClassNbr + "\" class=\"cs_checkbox\" onclick=\"saveClassSmallDisplay(event, " + CurrentClassNbr + ");\" title=\"Select Class\""));
                    if (hidClasses.Value.Contains("," + CurrentClassNbr)) {
                        divCheckBox.Controls.Add(new LiteralControl(" checked "));
                    }
                    divCheckBox.Controls.Add(new LiteralControl(" />"));
                    divNestedRow.Controls.Add(divCheckBox);

                    //Course for small and x-small display
                    divCourse = new HtmlGenericControl("DIV");
                    divCourse.Attributes["class"] = "divCourse classCell col-sm-10 col-xs-9";
                    divCourse.Attributes["style"] = "width:85%;padding-bottom:10px;padding-left:10px;padding-top:1px;white-space:normal;";
                    divCourse.Controls.Add(new LiteralControl("<strong>" + CurrentSubject + " - " + CourseTitle + "</strong>"));
                    divNestedRow.Controls.Add(divCourse);

                    //add Checkbox and Course to a nested row
                    divOuterCol.Controls.Add(divNestedRow);
                    divRow.Controls.Add(divOuterCol);

                    //Outer column for Checkbox and Class Nbr for large and medium display
                    divOuterCol = new HtmlGenericControl("DIV");
                    divOuterCol.Attributes["class"] = "col-lg-1 col-md-1 hidden-sm hidden-xs";
                    divOuterCol.Attributes["style"] = "white-space:nowrap;";

                    //Nested row for Checkbox and Class Nbr for large and medium display
                    divNestedRow = new HtmlGenericControl("DIV");
                    divNestedRow.Attributes["class"] = "row";

                    //Checkbox for large and medium display
                    divCheckBox = new HtmlGenericControl("DIV");
                    divCheckBox.Attributes["class"] = "checkbox classCheckBox classCell col-lg-4 col-md-4";
                    divCheckBox.Attributes["style"] = "width:30px;margin-top:1px;margin-left:1px;";
                    divCheckBox.Controls.Add(new LiteralControl("<input type=\"checkbox\" id=\"chkLarge" + CurrentClassNbr + "\" class=\"cs_checkbox\" onclick=\"saveClassLargeDisplay(event, " + CurrentClassNbr + ");\" title=\"Select Class\""));
                    if (hidClasses.Value.Contains("," + CurrentClassNbr)) {
                        divCheckBox.Controls.Add(new LiteralControl(" checked "));
                    }
                    divCheckBox.Controls.Add(new LiteralControl(" />"));
                    divNestedRow.Controls.Add(divCheckBox);

                    //Class Nbr for large and medium display
                    divClassNbr = new HtmlGenericControl("DIV");
                    divClassNbr.Attributes["class"] = "classNbr classCell col-lg-6 col-md-6";
                    divClassNbr.Attributes["style"] = "white-space:nowrap;padding-left:0";
                    divClassNbr.Controls.Add(new LiteralControl("<div style=\" white-space: nowrap\" class=\" hidden-lg hidden-md\"><strong>Class Nbr</strong></div>" + CurrentClassNbr));
                    divNestedRow.Controls.Add(divClassNbr);

                    //add Checkbox, Class Nbr Label, and Class Nbr text to a nested row
                    divOuterCol.Controls.Add(divNestedRow);
                    divRow.Controls.Add(divOuterCol);

                    //Course for large and medium display
                    divCourse = new HtmlGenericControl("DIV");
                    //divCourse.ID = CurrentSubject.Replace(" ", "") + "_" + i;
                    divCourse.Attributes["class"] = "section course classCell col-lg-1 col-md-1 hidden-sm hidden-xs"; //check into the section class, CurrentSubject class removed for now
                    divCourse.Controls.Add(new LiteralControl(CurrentSubject));
                    divRow.Controls.Add(divCourse);

                    //Course Title for large and medium display
                    divCourseTitle = new HtmlGenericControl("DIV");
                    divCourseTitle.Attributes["class"] = "courseTitle classCell col-lg-2 col-md-2 hidden-sm hidden-xs";
                    divCourseTitle.Attributes["style"] = "white-space:normal;";
                    divCourseTitle.Controls.Add(new LiteralControl(CourseTitle));
                    divRow.Controls.Add(divCourseTitle);

                    //Credits for large and medium display
                    divCredits = new HtmlGenericControl("DIV");
                    divCredits.Attributes["class"] = "credits classCell col-lg-1 col-md-1 hidden-sm hidden-xs";
                    divCredits.Controls.Add(new LiteralControl(Units));
                    divRow.Controls.Add(divCredits);

                    //Schedule for large and medium display
                    HtmlGenericControl divSchedule = new HtmlGenericControl("DIV");
                    divSchedule.Attributes["class"] = "schedule classCell col-lg-6 col-md-6 col-sm-12";
                    divSchedule.Controls.Add(new LiteralControl(ClassSchedule));
                    divRow.Controls.Add(divSchedule);

                    //Status for Large and medium display
                    divStatus = new HtmlGenericControl("DIV");
                    divStatus.Attributes["class"] = "status classCell col-lg-1 hidden-md hidden-sm hidden-xs";
                    divStatus.Controls.Add(new LiteralControl(EnrollmentStatus));
                    divRow.Controls.Add(divStatus);

                    //Status for small and x-small display
                    divStatus = new HtmlGenericControl("DIV");
                    divStatus.Attributes["class"] = "status classCell hidden-lg hidden-md hidden-sm hidden-xs";
                    divStatus.Attributes["style"] = "margin-left:30px;padding-bottom:15px;";
                    divStatus.Controls.Add(new LiteralControl("<div><strong>Status</strong></div>" + EnrollmentStatus));
                    divRow.Controls.Add(divStatus);

                    //add class to divClasses
                    divClasses.Controls.Add(divRow);

                    //Class Details Row
                    divRow = new HtmlGenericControl("DIV");
                    divRow.ID = "tr" + CurrentClassNbr;
                    divRow.Attributes["class"] = "row details";
                    divRow.Attributes["style"] = "display:none;";

                    divClasses.Controls.Add(divRow);

                    ClassSchedule = "";
                    PreviousSubject = CurrentSubject;
                }

                PreviousClassDays = CurrentClassDays;
                PreviousClassTimes = CurrentClassTimes;
                PreviousClassLocation = CurrentClassLocation;
                PreviousClassNbr = CurrentClassNbr;
            }

            //Footer Row
            divRow = new HtmlGenericControl("DIV");
            divRow.ID = "footerRow";
            divRow.Attributes["class"] = "footerRow col-lg-12 row";
            divRow.Attributes["style"] = "display:none;";

            //Footer
            HtmlGenericControl divFooter = new HtmlGenericControl("DIV");
            divFooter.ID = "footerCell";
            divFooter.Attributes["class"] = "footerCell";
            divFooter.Controls.Add(new LiteralControl("<div id='leftButtons' class='col-lg-8 col-md-8 col-sm-12 col-xs-12' style='display:none;'><input type='button' id='btnPrint' value='Print Classes' class='button med_gray hidden-xs hidden-sm hidden-md' onclick='printClassSelection();' />"));

            Button cmdSave = new Button();
            cmdSave.Attributes["runat"] = "server";
            cmdSave.ID = "cmdSave";
            cmdSave.Text = "Save Classes";
            cmdSave.CssClass = "button med_gray";
            cmdSave.OnClientClick = "return checkClassSelection();";
            cmdSave.Click += new EventHandler(cmdSave_Click);

            divFooter.Controls.Add(cmdSave);
            divFooter.Controls.Add(new LiteralControl("<input type='button' id='btnClear' value='Clear Classes' class='button med_gray' onclick='clearSelections();' />"));
            divFooter.Controls.Add(new LiteralControl("</div>"));
            divFooter.Controls.Add(new LiteralControl("<div id='rightButtons' class='col-lg-4 col-md-4 col-sm-12 col-xs-12'>Registration:&nbsp;<a href='https://scc.spokane.edu/Become-a-Student/Register-for-Classes' target='_blank'>SCC</a>,&nbsp;<a href='https://sfcc.spokane.edu/Become-a-Student/Register-for-Classes' target='_blank'>SFCC</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='#container'>^ Top</a></div>"));

            divRow.Controls.Add(divFooter);
            divClasses.Controls.Add(divRow);

            panClasses.Visible = true;
            panMsg.Visible = false;
        } else {
            panClasses.Visible = false;
            panMsg.Visible = true;
        }
    }
}