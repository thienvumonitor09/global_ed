﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/LateEnrollment.ascx.cs" Inherits="CMSWebParts_Custom_WebUserControl" %>

    <section class="col-lg-12 row"  style="background-color:white;margin-left:0px;">
        <div class="container">
            <div class="cs_row col-lg-12">
                <div id="instructions" runat="server">
                    <asp:Label ID="lblAlreadyActed" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblInstruction" runat="server" Text=""></asp:Label>
                </div>
                <div id="baseData" runat="server">
                    <h2>Student Information</h2>
                    <p>
                        <asp:Label for="lblCtcLinkID" runat="server" Font-Bold="true" Width="35%" Text="Student ctcLink ID:"></asp:Label>
                        <asp:Label ID="lblCtcLinkID" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label for="lblStudentName" runat="server" Font-Bold="true" Width="35%" Text="StudentName:"></asp:Label>
                        <asp:Label ID="lblStudentName" runat="server" Text=""></asp:Label>
                    </p>
                    <!--
                    <p>
                        <asp:Label for="subjectOrCategory" runat="server" Font-Bold="true" Width="35%" Text="Subject/Category:"></asp:Label>
                        <asp:Label ID="subjectOrCategory" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label for="program" runat="server" Font-Bold="true" Width="35%" Text="Program:"></asp:Label>
                        <asp:Label ID="program" runat="server" Text=""></asp:Label>
                    </p>
                    -->
                    <h5>Please check the accuracy of the course information provided to avoid the need to pursue corrections.</h5>
                    <div id="showOnline" runat="server" visible="false">
                        <p>
                            <asp:Label for="lblOnlineClass" runat="server" Font-Bold="true" Width="35%" Text="Online Class?"></asp:Label>
                            <asp:Label ID="lblOnlineClass" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <p>
                        <asp:Label for="txtCourseName" ID="courseName" runat="server" Font-Bold="true" Width="35%" Text="Course Name:"></asp:Label>
                        <asp:TextBox ID="txtCourseName" runat="server" Width="350px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label for="courseNumber" runat="server" Font-Bold="true" Width="35%" Text="Course Number:"></asp:Label>
                      <!--  <asp:Label ID="XXcourseNumber" runat="server" Text=""></asp:Label> -->
                        <asp:TextBox ID="txtCourseNumber" runat="server" Width="150px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label for="classNumber" runat="server" Font-Bold="true" Width="35%" Text="Class Number:"></asp:Label>
                        <!-- <asp:Label ID="XXclassNumber" runat="server" Text=""></asp:Label> -->
                        <asp:TextBox ID="txtClassNumber" runat="server" Width="100px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label for="lblFirstAttend" runat="server" Font-Bold="true" Width="35%" Text="Date First Attend:"></asp:Label>
                        <asp:Label ID="lblFirstAttend" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <span style="vertical-align:top;"><asp:Label for="reasonLate" runat="server" Font-Bold="true" Width="35%" Text="Reason for Late Enrollment:"></asp:Label></span>
                        <asp:Label ID="reasonLate" runat="server" Width="55%" Text=""></asp:Label>
                    </p>
                    <p>
                      <!--  <asp:Label for="agreeTerms" runat="server" Font-Bold="true" Width="35%" Text=""></asp:Label> -->
                        <asp:Label ID="agreeTerms" runat="server" Text=""></asp:Label>
                    </p>
                    <div id="droppedInfo" runat="server">
                        <h5>Course to drop</h5>
                        <p>
                            <asp:Label for="txtDropCourseDescription" runat="server" Font-Bold="true" Width="35%" Text="Drop Course Name:"></asp:Label>
                            <asp:TextBox ID="txtDropCourseDescription" runat="server" Width="350px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label for="txtDropCourseNumber" runat="server" Font-Bold="true" Width="35%" Text="Drop Course Number:"></asp:Label>
                            <asp:TextBox ID="txtDropCourseNumber" runat="server" Width="100px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label for="txtDropClassNumber" runat="server" Font-Bold="true" Width="35%" Text="Drop Class Number:"></asp:Label>
                            <asp:TextBox ID="txtDropClassNumber" runat="server" Width="100px"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div id="FacInput" runat="server">
                    <h2>Instructor Response</h2>                  
                    <p>
                        <asp:Label for="rdoFacApprove" runat="server" Font-Bold="true" Width="35%" Text="Faculty:"></asp:Label><br />
                        <asp:RadioButtonList ID="rdoFacApprove" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Y" Text="Approved"></asp:ListItem>
                            <asp:ListItem Value="N" Text="Denied"></asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="whySucceed" runat="server" Font-Bold="true" Width="60%" Text="OPTIONAL:  Please explain the reason for approving or denying this request:"></asp:Label><br />
                        <textarea id="whySucceed" cols="60" rows="4" runat="server"></textarea>
                    </p>
                    <div id="notifyStudent" runat="server" Visible="false">
                        <p>
                            <asp:Label ID="lblNotifyStudent" runat="server" Text="The student will be notified of your denial."></asp:Label>
                        </p>
                   </div>
                    <div id="DeanID" runat="server" Visible="false">                       
                        <asp:Literal ID="litPickDean" runat="server"></asp:Literal>
                        <p>
                            <asp:Label for="ddlDeanName" runat="server" Font-Bold="true" Width="35%" Text="Dean's Name:"></asp:Label><br />
                          <!--  <asp:TextBox ID="txtDeanName" runat="server" Width="300px"></asp:TextBox> -->
                            <asp:DropDownList id="ddlDeanName" onchange="getEmail()" runat="server">
                                <asp:ListItem Value="Select one"></asp:ListItem>
                            </asp:DropDownList> 
                        </p>
                        <p>
                            <asp:Label for="txtDeanEmail" runat="server" Font-Bold="true" Width="35%" Text="Dean's Email address:"></asp:Label><br />
                            <asp:TextBox id="txtDeanEmail" runat="server" Width="300px"></asp:TextBox>
                        </p>
                    </div>
                    
                </div>
                <div id="DeanInput" runat="server">
                    <h2>Dean Response</h2>
                    <p>
                        <asp:Label for="rdoDeanApprove" runat="server" Font-Bold="true" Width="35%" Text="Dean:"></asp:Label><br />
                        <asp:RadioButtonList ID="rdoDeanApprove" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Y" Text="Approved"></asp:ListItem>
                            <asp:ListItem Value="N" Text="Denied"></asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <asp:Label ID="lblDeanAct" runat="server" Text=""></asp:Label>
                    <div id="DeanDenyInput" runat="server" Visible="true">  
                        <p>
                            <asp:Label for="whyDeny" runat="server" Font-Bold="true" Width="60%" Text="OPTIONAL:  Please explain the reason for approving or denying this request:"></asp:Label><br />
                            <textarea id="whyDeny" cols="60" rows="4" runat="server"></textarea>
                        </p>
                    </div>
                    <div id="NotifyStudentDean" runat="server" Visible="false">
                        <p>
                            <asp:Label ID="lblNotifyStudentDean" runat="server" Text="The student will be notified of your denial."></asp:Label>
                        </p>
                   </div>
                </div>
                <div id="overDay5" runat="server" visible="false">
                    <p>
                        <asp:Label ID="lblNotifyVP" runat="server" Text="<span style='color:#DD0000'>Because it is past day 5 of the quarter, the VP of Learning will be notified and must approve or deny this request.</span>"></asp:Label>
                    </p>
                </div>
                <div id="VPInput" runat="server" visible="false">
                    <h2>VP of Learning Response</h2>
                    <p>
                        <asp:Label for="rdoVPApprove" runat="server" Font-Bold="true" Width="35%" Text="VP Learning:"></asp:Label><br />
                        <asp:RadioButtonList ID="rdoVPApprove" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Y" Text="Approved"></asp:ListItem>
                            <asp:ListItem Value="N" Text="Denied"></asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <asp:Label ID="lblVPAct" runat="server" Text=""></asp:Label>
                    <p>
                        <asp:Label for="vpComment" runat="server" Font-Bold="true" Width="60%" Text="OPTIONAL:  Please explain the reason for approving or denying this request:"></asp:Label><br />
                        <textarea id="vpComment" cols="60" rows="4" runat="server"></textarea>
                    </p>
                </div>
                <div id="divButtons" runat="server">
                    <p>
                        <div class="button btn-group"><asp:Button ID="btnFacSubmit" runat="server" Text="Faculty Submit" OnClick="cmdbtnFacSubmit_Click" /></div>
                        <div class="button btn-group"><asp:Button ID="btnDeanSubmit" runat="server" Text="Dean Submit" OnClick="cmdbtnDeanSubmit_Click "/></div>
                        <div class="button btn-group"><asp:Button ID="btnVPSubmit" runat="server" Text="VP Learning Submit" OnClick="cmdbtnVPSubmit_Click" /></div>
                    </p>
                   <!-- <br /><hr /><br /> -->
                </div>
                
        </div>
            <!--
            <div class="cs_row col-lg-12">
                <div id="ReportHead" runat="server">
                    <h2>Summary Report</h2>
                    <p><asp:Label ID="lblReportErrMsg" runat="server" Text=""></asp:Label></p>                   
                    <div class="col-md-2"><p>List all requests  <asp:CheckBox ID="chkAll" runat="server" /></p></div>
                    <div class="col-md-3"><p>OR enter student ID:  <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox></p></div>
                    <div class="col-md-2"><p><br /><asp:Button ID="btnReport" runat="server" Text="Generate Report" OnClick="cmdbtnReport_Click" /></p></div>
                </div>
            </div>
            <div id="ReportBody" runat="server">
                
                <asp:Panel ID="pnlReport" runat="server">    
                </asp:Panel>
                <div style="float:right;margin-right:20px;"><asp:Button ID="btnProcess" runat="server" Text="Process" class="button" OnClick="btnProcess_Click" /></div>
            </div>
            -->
        </div>
    </section>
    <asp:HiddenField ID="hdnInstructorName" runat="server" />
    <asp:HiddenField ID="hdnInstructorEmail" runat="server" />
    <asp:HiddenField ID="hdnDeanName" runat="server" />
    <asp:HiddenField ID="hdnDeanEmail" runat="server" />
    <asp:HiddenField ID="hdnStudentEmail" runat ="server" />
    <asp:HiddenField ID="hdnStudentName" runat="server" />
    <asp:HiddenField ID="hdnAgreeTerms" runat="server" />
    <asp:HiddenField ID="hdnOnline" runat="server" />

<script type="text/javascript">
    function getEmail() {
        var ddl = document.getElementById("p_lt_ctl02_pageplaceholder_p_lt_ctl00_CCSSystem_LateEnrollmentForm_ddlDeanName");        
        var txtDeanEamil = document.getElementById("p_lt_ctl02_pageplaceholder_p_lt_ctl00_CCSSystem_LateEnrollmentForm_txtDeanEmail");
        txtDeanEamil.value = "";
        if (ddl.value == "Linda Beane-Boose")
            txtDeanEamil.value = "Linda.Beane-boose@sfcc.spokane.edu";
        else if (ddl.value == "Jim Brady")
            txtDeanEamil.value = "Jim.Brady@sfcc.spokane.edu";
        else if (ddl.value == "Bonnie Brunt")
            txtDeanEamil.value = "Bonnie.Brunt@sfcc.spokane.edu";
        else if (ddl.value == "Ken Burrus")
            txtDeanEamil.value = "Ken.Burrus@ccs.spokane.edu";
        else if (ddl.value == "Elodie Goodman")
            txtDeanEamil.value = "Elodie.Goodman@sfcc.spokane.edu";
        else if (ddl.value == "Lora Senf")
            txtDeanEamil.value = "Lora.Senf@sfcc.spokane.edu";
        else if (ddl.value == "Cynthia Vigil")
            txtDeanEamil.value = "Cynthia.Vigil@sfcc.spokane.edu";
        else if (ddl.value == "Jeff Brown")
            txtDeanEamil.value = "Jeff.Brown@scc.spokane.edu";
        else if (ddl.value == "Ken Burrus")
            txtDeanEamil.value = "Ken.Burrus@ccs.spokane.edu";
        else if (ddl.value == "David Cox")
            txtDeanEamil.value = "Dave.Cox@scc.spokane.edu";
        else if (ddl.value == "Jaclyn Jacot")
            txtDeanEamil.value = "Jaclyn.Jacot@scc.spokane.edu";
        else if (ddl.value == "JL Henriksen")
            txtDeanEamil.value = "JL.Henridsen@scc.spokane.edu";
        else if (ddl.value == "Gwendolyn James")
            txtDeanEamil.value = "Gwendolyn.James@scc.spokane.edu";
        else if (ddl.value == "Sherri Fujita")
            txtDeanEamil.value = "Sherri.Fujita@scc.spokane.edu";
        else if (ddl.value == "Laura Padden")
            txtDeanEamil.value = "Laura.padden@ccs.spokane.edu";
        else if (ddl.value == "Erika Naccarato")
            txtDeanEamil.nodeValue = "Erika.Naccarato@scc.spokane.edu";
        else
            txtDeanEamil.value = "";
    }

</script>