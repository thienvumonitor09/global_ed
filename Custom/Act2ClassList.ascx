﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Act2ClassList.ascx.cs" Inherits="CMSWebParts_Custom_Act2ClassList" %>
        <style type="text/css">    
            /*  Also uses classes PaneHeader, SelectedPaneHeader, PaneContent
                found in ccs.css style sheet in Kentico.
            */       
            th.alignLeft {text-align:left;}
            th.alignRight {text-align:right;}
        </style>

        <asp:Literal ID="litSubj" runat="server"></asp:Literal>
        <div class="classList">
            <asp:Table ID="tblClass" runat="server" Width="100%" BorderWidth="0" GridLines="None" CellSpacing="2">
                    <asp:TableHeaderRow >
                        <asp:TableHeaderCell CssClass="alignLeft" Width="10%">Class #</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignLeft" Width="30%">Title</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignLeft" Width="28%">Location</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignRight" Width="6%">Cost</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignRight" Width="13%">Time</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignRight" Width="13%">Dates</asp:TableHeaderCell>                  
                    </asp:TableHeaderRow>
            </asp:Table>
        </div>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
