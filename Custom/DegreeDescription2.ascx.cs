﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.Data;
using System.Globalization;

public partial class CMSWebParts_Custom_DegreeDescription2 : CMSAbstractWebPart
{
    //initialize web service
    ClassScheduleXML.ClassScheduleXML wsClassSchedule = new ClassScheduleXML.ClassScheduleXML();
    //initialize iCatalog web service
    // iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();
    String registrationTerm = "";
    string quarterName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/DegreeDescription_files/DegreeDescription.css";
        Page.Header.Controls.Add(styleLink);

        //get the current term
        //String currTerm = wsClassSchedule.GetCurrentTerm();
        if(!IsPostBack) { 
            if(Request.QueryString["strm"] != null)
            {
                registrationTerm = Request.QueryString["strm"].ToString();
                if (registrationTerm.Length > 4)
                    registrationTerm = Request.QueryString["strm"].Substring(0, 4);
            } else
            {
               // currTerm = wsClassSchedule.GetCurrentTerm();
                registrationTerm = wsClassSchedule.GetRegistrationTerm();
            }
        }
        if(IsPostBack)
        {
            registrationTerm = quarterDD.SelectedValue;
        }

        // added by LP 10/5/2018 to cover gap between 
        // registration term as return by the web service and the SSR_SSENRLDIST_BDT for that term
        // e.g.  today is 10/5/2018
        // registration term returned is 2191 (Winter 2019)
        // BDT returned for 2191 is 10/06/2018
        // Winter 2019 is not included in the group to check because it's date is > today's date
        // Therefore no selection is made for the drop-down and it defaults to the first one 2177 (Fall 2017)
        // I saved the last valid term information so when the loop is exited, if no selection is made, it defaults to the
        // last valid term.
        string lastValidSTRM = "";
        string lastValidQuarterName = "";
        //populate the terms/quarter drop down list
        quarterDD.Items.Clear();
        DataSet dsTerms = wsClassSchedule.GetTerms();
        for (Int16 i = 0; i < dsTerms.Tables[0].Rows.Count; i++)
        {
            if (Convert.ToDateTime(dsTerms.Tables[0].Rows[i]["SSR_SSENRLDISP_BDT"]) <= DateTime.Now)
            {
                quarterName = dsTerms.Tables[0].Rows[i]["Quarter"].ToString();
                quarterName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(quarterName.ToLower());
                quarterDD.Items.Add(quarterName);
                lastValidQuarterName = quarterName;
                quarterDD.Items[i].Value = dsTerms.Tables[0].Rows[i]["STRM"].ToString();
                lastValidSTRM = dsTerms.Tables[0].Rows[i]["STRM"].ToString();
                if (quarterDD.Items[i].Value == registrationTerm) { 
                    quarterDD.Items[i].Selected = true;                  
                    litQuarter.Text = quarterName;
                }
            }
        }
        if (quarterDD.SelectedIndex == 0)
        {
            quarterDD.SelectedValue = lastValidSTRM;
            litQuarter.Text = lastValidQuarterName;
        }
        try
        {
          //  quarterDD.SelectedValue = Request.Form[quarterDD.UniqueID];
        }
        catch
        {
            //do nothing
        }

       
    }
}