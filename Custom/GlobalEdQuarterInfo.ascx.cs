﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
public partial class CMSWebParts_Custom_GlobalEdQuarterInfo : CMSAbstractWebPart
{
    public static XDocument importantDateXML;
    public static XDocument globalXML;
    public static List<XDocument> importantDateXMLList;
    public static List<XDocument> globalXMLList;
    private static string[] schoolList = {"scc" , "sfcc" };
    protected void Page_Load(object sender, EventArgs e)
    {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/GlobalEdQuarterInfo_files/GlobalEdQuarterInfo.css";
        Page.Header.Controls.Add(styleLink);


        //myLabel.Text = urlHostStr;

        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;

        importantDateXML = QuarterInfoUtil.CreateXDocument("important-dates");
        globalXML = QuarterInfoUtil.CreateXDocument("global");

        importantDateXMLList = QuarterInfoUtil.CreateXDocumentList("important-dates");
        globalXMLList = QuarterInfoUtil.CreateXDocumentList("global");

        List<QuarterInfo> quarterInfoList2 = QuarterInfoUtil.GetQuarterInfoList2();
        quarterInfoListJsonID2.Value = js.Serialize(quarterInfoList2);
        //myLabel.Text += globalXML.Root.Nodes().Count();
        // myLabel.Text += result.Root.Elements().Count();


        //List<Quarter> quarterList = GetQuarters(DateTime.Now.Month, DateTime.Now.Year);
        //List<QuarterInfo> quarterInfoList = QuarterInfoUtil.GetQuarterInfoList();
        //quarterInfoListJsonID.Value = js.Serialize(quarterInfoList);


        siteID.Value = QuarterInfoUtil.GetSite();
        quarterListJsonID.Value = QuarterInfoUtil.getQuarterListJson();

    }
    
}