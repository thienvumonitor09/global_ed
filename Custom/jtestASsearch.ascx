﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="jtestASsearch.ascx.cs" Inherits="CMSWebParts_Custom_jtestASsearch" %>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

<input type="hidden" id="hidLtr" value="" runat="server" />
<input type="hidden" id="hidCategory" value="ALL" runat="server" />
<input type="hidden" id="hidFilters" runat="server" />
<div id="container" class="container degrees">
    <nav id="degreeListNav">
        <section id="search" class="light-grey-bg col-lg-12 row" style="margin-bottom:3rem;">
            <div class="cs_row col-lg-12">
                <div class="col">
                    <label for="cboQuarter"><strong>Quarter</strong></label><br />
                    <asp:DropDownList ID="cboQuarter" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
                <div class="col">
                    <label for="cboLocation"><strong>Location</strong></label><br />
                    <asp:DropDownList ID="cboLocation" CssClass="cboLocation" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
                <div class="col">
                    <label for="txtTextSearch"><strong>Keyword Search</strong></label><br />
                    <asp:TextBox ID="txtTextSearch" CssClass="txtTextSearch" runat="server" placeholder="e.g. Accounting" MaxLength="150"></asp:TextBox>
                    <button id="btnSearch" onclick="document.getElementById('frmiCatalog').submit()" aria-label="Search">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </div>
            </div>
            <div id="more" class="cs_row moreFilters" style="float: right" runat="server">
                <div class="col" style="padding-right: 0px; margin-top: 5px;">
                    <a id="filterLink" onclick="toggleFilters();" runat="server"><strong>More Filters</strong></a>
                </div>
                <div class="col" style="padding-top: 2px; padding-right: 10px; padding-left: 2px; width: 10px">
                    <div id="arrow" class="right-arrow" onclick="toggleFilters();" runat="server"></div>
                </div>
            </div>
            <div id="filters" class="filters" style="display: none;" runat="server">
                <div style="display: inline-block">
                    <div class="cs_row">
                        <div class="col">
                            <label for="cboDegree"><strong>Degree/Certificate</strong></label><br />
                            <asp:DropDownList ID="cboDegree" CssClass="cboDegree" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboDegree_Change"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="cs_row">
                        <div class="col">
                            <strong>Degrees/Certificates that Include the Following Course(s)</strong><br />
                            <asp:DropDownList ID="cboSubject" CssClass="cboSubject" runat="server" AutoPostBack="true" aria-label="Course Subject"></asp:DropDownList>
                            <asp:DropDownList ID="cboCatalogNumber" runat="server" AutoPostBack="true" aria-label="Course Catalog Number"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div id="buttonContainer" runat="server" style="padding-left:16px;">
                <asp:Button ID="cmdReset" runat="server" Text="Reset" CssClass="button med_gray" Style="width: 100px; margin-left: 0px; margin-top:30px;" OnClick="cmdReset_Click" />
            </div>
            <div class="clear"></div>
        </section>
    </nav>
    <asp:Panel ID="panView" runat="server" style="margin-bottom:3rem;">
        <strong><label for="cboView">View by</label>&nbsp;</strong>
        <asp:DropDownList ID="cboView" runat="server" AutoPostBack="true">
            <asp:ListItem Value="alpha" Selected="True">Area of Study A-Z</asp:ListItem>
            <asp:ListItem Value="degree">Degree/Certificate</asp:ListItem>
            <asp:ListItem Value="category">Category</asp:ListItem>
        </asp:DropDownList>
    </asp:Panel>
    <asp:Panel ID="panAlpha" CssClass="panAlpha" runat="server">
        <ul id="alpha" class="alpha"></ul>
    </asp:Panel>
    <asp:Panel ID="panCategory" CssClass="panCategory" runat="server">
        <div class="viewFilter">
            <strong><label for="cboCategory">Category</label>&nbsp;</strong>
        </div>
        <div class="viewFilter">
            <asp:DropDownList ID="cboCategory" runat="server" AutoPostBack="true"></asp:DropDownList>
        </div>
    </asp:Panel>
    <asp:Panel ID="panDegree" CssClass="panDegree" runat="server">
        <div class="viewFilter">
            <strong><label for="cboDegree2">Degree/Certificate</label>&nbsp;</strong>
        </div>
        <div class="viewFilter">
            <asp:DropDownList ID="cboDegree2" CssClass="cboDegree2" runat="server" AutoPostBack="true" onchange="onDegreeChange()"></asp:DropDownList>
        </div>
    </asp:Panel>
    <asp:Panel ID="panDegreeList" CssClass="panDegreeList" runat="server"></asp:Panel>
</div>

<script type="text/javascript">
    function onDegreeChange() {
        var cboDegree = $("select[id$='cboDegree']");
        var cboDegree2 = $("select[id$='cboDegree2']");
        cboDegree.val(cboDegree2.val());
    }

    function toggleFilters() {
        $("div[id$='filters']").toggle();

        if ($("div[id$='filters']").is(":visible")) {
            $("div[id$='arrow']").attr("class", "down-arrow");
            $("div[id$='more']").attr("style", "border-bottom: 1px solid #D8D8D8;text-align:right;");
            $("input[id$='hidFilters']").val("true");
        } else {
            $("div[id$='arrow']").attr("class", "right-arrow");
            $("div[id$='more']").attr("style", "border:none;float:right;");
            $("input[id$='hidFilters']").val("false");
        }
    }

    //if the document is ready
    $(document).ready(function () {
        var hidLtr = $("input[id$='hidLtr']").attr('id');

        //populate the alpha character list
        $(".alpha").append('<li><a href="#" id="link" onclick="$(\'#' + hidLtr + '\').val(\'\');document.getElementById(\'form\').submit();">ALL</a></li>');
        for (i = 0; i < 26; i++) {

            var alphaCharacter = (i + 10).toString(36).toUpperCase();
            $(".alpha").append('<li><a href="#" id="link" onclick="$(\'#' + hidLtr + '\').val(\'' + alphaCharacter + '\');document.getElementById(\'form\').submit();">' + alphaCharacter + '</a></li>');
        }

        var letter = $("input[id$='hidLtr']").val();
        $("#link" + letter).addClass("active");
    })
</script>
