﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_Custom_WebUserControl : CMSAbstractWebPart
{
    String strStep = "";
    String strSQL = "";
    String strOnline = "";
    String postBackControlName = "";
    int intFormID;

    // for Email
    string SFCCprocessEmail = "Geoff.Lake@sfcc.spokane.edu";
    string SFCCprocessName = "Geoff Lake";
    string SCCprocessEmail = "Erika.Naccarato@scc.spokane.edu";
    // string SCCprocessEmail = "laura.padden@ccs.spokane.edu";
    string SCCVPofLearningEmail = "Jenni.Martin@scc.spokane.edu";
    // string SCCVPofLearningEmail = "laura.padden@ccs.spokane.edu";
    string SCCVPofLearningName = "Jenni Martin";
    string SCCprocessName = "Erika Naccarato";
    string sendTo = "";
    string sentFrom = "";
    string strCCaddresses = "";
    string strSubject = "";
    string strEmailResult = "";
    string strBody = "";
    string strSite = "";
    bool studentEmailSent = false;
    bool alreadyDone = false;
    bool day6Plus = false;

    LateEnrollment leInfo = new LateEnrollment();
    protected void Page_Load(object sender, EventArgs e)
    {
        litPickDean.Text = "";
        strStep = Request.QueryString["s"];
        intFormID = Convert.ToInt32(Request.QueryString["i"]);
        if (Request.QueryString["site"] != null)
            strSite = Request.QueryString["site"];
        lblDeanAct.Text = "";
        lblVPAct.Text = "";
        //    day6Plus = true;
        strSQL = String.Format("SELECT * FROM [CCSGen_ctcLink_ODS].[dbo].[vw_Quarter_MetaData] " +
                             "WHERE CurrentQtr = 1");
        DataTable dtQtr = leInfo.RunGetQueryODS(strSQL);
        if (dtQtr != null)
        {
            if (dtQtr.Rows.Count > 0)
            {
                TimeSpan numDays = DateTime.Now.Subtract(Convert.ToDateTime(dtQtr.Rows[0]["qtrFirstDayOfClass"].ToString()));
             //   Response.Write("num: " + numDays.TotalDays.ToString() + "<br />");
                if (numDays.TotalDays > 5)
                    day6Plus = true;
                else
                    day6Plus = false;
            }
        }

        if (!IsPostBack)
        {
            if (strStep != "3")
            {
                ReportHead.Visible = false;
                ReportBody.Visible = false;
                if (strSite != "SCC") {
                strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                    "WHERE LateEnrollmentFormID = {0}", intFormID);
                    // populate Dean drop down
                    ddlDeanName.Items.Add(new ListItem("Linda Beane-Boose", "Linda Beane-Boose"));
                    ddlDeanName.Items.Add(new ListItem("Jim Brady", "Jim Brady"));
                    ddlDeanName.Items.Add(new ListItem("Bonnie Brunt", "Bonnie Brunt"));
                    ddlDeanName.Items.Add(new ListItem("Ken Burrus", "Ken Burrus"));
                    ddlDeanName.Items.Add(new ListItem("Elodie Goodman", "Elodie Goodman"));
                    ddlDeanName.Items.Add(new ListItem("Lora Senf", "Lora Senf"));
                    ddlDeanName.Items.Add(new ListItem("Cynthia Vigil", "Cynthia Vigil"));
                    ddlDeanName.Items.Add(new ListItem("Laura Padden", "Laura Padden"));
                }  else
                {
                    strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                    "WHERE LateEnrollmentForm_SCCID = {0}", intFormID);
                    // populate Dean drop down
                    ddlDeanName.Items.Add(new ListItem("Jeff Brown", "Jeff Brown"));
                    ddlDeanName.Items.Add(new ListItem("Ken Burrus", "Ken Burrus"));
                    ddlDeanName.Items.Add(new ListItem("David Cox", "David Cox"));
                    ddlDeanName.Items.Add(new ListItem("JL Henriksen", "JL Henriksen"));
                    ddlDeanName.Items.Add(new ListItem("Jaclyn Jacot", "Jaclyn Jacot"));
                    ddlDeanName.Items.Add(new ListItem("Gwendolyn James", "Gwendolyn James"));
                    ddlDeanName.Items.Add(new ListItem("Sherri Fujita", "Sherri Fujita"));
                    
                    ddlDeanName.Items.Add(new ListItem("Laura Padden", "Laura Padden"));
                }
               // Response.Write("site: " + strSite + ";  " + strSQL + "<br />");
                DataTable dtStuInfo = leInfo.RunGetQuery(strSQL);
                if (dtStuInfo != null)
                {
                    if (dtStuInfo.Rows.Count > 0)
                    {
                        if (strSite == "SCC")
                        {
                            showOnline.Visible = true;
                            lblOnlineClass.Text = dtStuInfo.Rows[0]["OnlineClass"].ToString();
                            hdnOnline.Value = dtStuInfo.Rows[0]["OnlineClass"].ToString();
                            
                            /* HAD CALCULATIONS OF # OF DAYS HERE BUT MOVED TO STRAIGHT PAGE LOAD */
                        }
                        if (strStep == "1")
                        {
                            if (dtStuInfo.Rows[0]["FacApproveDeny"].ToString() != "")
                            {
                                // already approved/denied
                                alreadyDone = true;
                                lblAlreadyActed.Text = "<p><span style='color:#DD0000;'> You have already " + dtStuInfo.Rows[0]["FacApproveDeny"].ToString() + " this request. You may view the information but cannot modify this record</span>.</p>";
                            }
                            else
                            {
                                lblInstruction.Text = "<h5>" + dtStuInfo.Rows[0]["InstructorName"].ToString().Trim().Replace("''", "'") + ", please review this student request, fill in the appopriate form fields and either approve or deny the request.</h5>";
                                alreadyDone = false;
                            }
                        }
                        else if(strStep == "2")
                        {
                            // step = 2 so Dean is accessing
                            if (dtStuInfo.Rows[0]["DeanApproveDeny"].ToString() != "")
                            {
                                alreadyDone = true;
                                lblAlreadyActed.Text = "<p><span style='color:#DD0000;'> You have already " + dtStuInfo.Rows[0]["DeanApproveDeny"].ToString() + " this request. You may view the information but cannot modify this record</span>.</p>";
                            }
                            else
                            {
                                lblInstruction.Text = "<h5>" + dtStuInfo.Rows[0]["DeanName"].ToString().Trim().Replace("''", "'") + ", please review this student request and faculty response.  Then fill in the appopriate form fields to either approve or deny the request. ";
                                lblInstruction.Text += "Upon submission, this information will be sent to the instructor, the student, ";
                                if (strSite != "SCC")
                                    lblInstruction.Text += SFCCprocessName + ".</h5>";
                                else
                                {
                                    if(day6Plus)
                                        lblInstruction.Text += SCCprocessName + ", and, if approved, the VP of Learning</h5>";
                                    else
                                        lblInstruction.Text += "and " + SCCprocessName + ".</h5>";
                                }
                                alreadyDone = false;
                            }// end if
                        } else
                        {
                            // step = 4 so VP of Learning is accessing
                            if (dtStuInfo.Rows[0]["VPApproveDeny"].ToString() != "")
                            {
                                alreadyDone = true;
                                lblAlreadyActed.Text = "<p><span style='color:#DD0000;'> You have already " + dtStuInfo.Rows[0]["VPApproveDeny"].ToString() + " this request. You may view the information but cannot modify this record</span>.</p>";
                            }
                            else
                            {
                                lblInstruction.Text = "<h5>" + SCCVPofLearningName + ", please review this student request";
                                if (hdnOnline.Value == "Yes")
                                    lblInstruction.Text += ", faculty and Dean response. ";
                                else
                                    lblInstruction.Text += " and faculty response. ";
                                lblInstruction.Text += "Then fill in the appopriate form fields to either approve or deny the request. ";
                                lblInstruction.Text += "Upon submission, this information will be sent to the instructor, the student, and ";
                                if (strSite != "SCC")
                                    lblInstruction.Text += SFCCprocessName + ".</h5>";
                                else
                                    lblInstruction.Text += SCCprocessName + ".</h5>";
                                alreadyDone = false;
                            }// end if
                        }
 //                       Response.Write("alreadyDone: " + alreadyDone.ToString() + "<br />");
                        hdnInstructorName.Value = dtStuInfo.Rows[0]["InstructorName"].ToString().Trim().Replace("''", "'");
                        hdnInstructorEmail.Value = dtStuInfo.Rows[0]["InstructorEmail"].ToString().Trim().Replace("''", "'");
                        lblCtcLinkID.Text = dtStuInfo.Rows[0]["ctcLinkID"].ToString();
                        lblStudentName.Text = dtStuInfo.Rows[0]["First_Name"].ToString().Replace("''", "'") + " " + dtStuInfo.Rows[0]["Last_Name"].ToString().Replace("''", "'");
                       // subjectOrCategory.Text = dtStuInfo.Rows[0]["Category"].ToString().Split('_')[1];
                       // program.Text = dtStuInfo.Rows[0]["Program"].ToString();
                        txtCourseName.Text = dtStuInfo.Rows[0]["CourseName"].ToString().Replace("''", "'");
                        txtCourseNumber.Text = dtStuInfo.Rows[0]["CourseNumber"].ToString();
                        txtClassNumber.Text = dtStuInfo.Rows[0]["ClassNumber"].ToString();
                        hdnAgreeTerms.Value = dtStuInfo.Rows[0]["AgreePayTerms"].ToString();
                        if (dtStuInfo.Rows[0]["DropClass"].ToString()=="Yes")
                        {
                            droppedInfo.Visible = true;
                            txtDropCourseDescription.Text = dtStuInfo.Rows[0]["DropCourseDescription"].ToString().Replace("''", "'");
                            txtDropCourseNumber.Text = dtStuInfo.Rows[0]["DropCourseNumber"].ToString().Replace("''", "'");
                            txtDropClassNumber.Text = dtStuInfo.Rows[0]["DropClassNumber"].ToString().Replace("''", "'");

                        } else
                        {
                            droppedInfo.Visible = false;
                        }
                        if(strStep == "1")
                        {
                            txtCourseName.Enabled = false;
                            txtCourseNumber.Enabled = false;
                            txtClassNumber.Enabled = false;
                            txtDropClassNumber.Enabled = false;
                            txtDropCourseDescription.Enabled = false;
                            txtDropCourseNumber.Enabled = false;
                        }
                        if (alreadyDone)
                        {
//                            Response.Write("value: " + dtStuInfo.Rows[0]["FacApproveDeny"].ToString() + "<br />");
                            if (dtStuInfo.Rows[0]["FacApproveDeny"].ToString() == "Approved")
                                rdoFacApprove.SelectedValue = "Y";
                            else
                                rdoFacApprove.SelectedValue = "N";
                            rdoFacApprove.Enabled = false;
                            whySucceed.Value = dtStuInfo.Rows[0]["WeNeedABrief"].ToString().Replace("''", "'");
                            txtCourseName.Enabled = false;
                            txtCourseNumber.Enabled = false;
                            txtClassNumber.Enabled = false;
                            txtDropClassNumber.Enabled = false;
                            txtDropCourseDescription.Enabled = false;
                            txtDropCourseNumber.Enabled = false;
                            whySucceed.Disabled = true;
                        }
                        if (dtStuInfo.Rows[0]["FirstDateAttend"].ToString() != "")
                        {
                            DateTime shortDate = Convert.ToDateTime(dtStuInfo.Rows[0]["FirstDateAttend"].ToString());
                            lblFirstAttend.Text = shortDate.ToShortDateString();
                        }
                        else
                            lblFirstAttend.Text = "";
                        reasonLate.Text = dtStuInfo.Rows[0]["ReasonForLateEnrollment"].ToString().Replace("''", "'");
                        if (dtStuInfo.Rows[0]["AgreePayTerms"].ToString() == "True")
                            agreeTerms.Text = "<b>I agree to the terms of payment</b>.";
                        else
                            agreeTerms.Text = "<b>I do NOT agree to the terms of payment</b>.";
                        hdnStudentName.Value = dtStuInfo.Rows[0]["First_Name"].ToString().Replace("''", "'") + " " + dtStuInfo.Rows[0]["Last_Name"].ToString().Replace("''", "'");
                        hdnStudentEmail.Value = dtStuInfo.Rows[0]["Email_Address"].ToString().Trim().Replace("''", "'");
                        if (strStep == "2" || strStep == "4")
                        {
                            whySucceed.Value = dtStuInfo.Rows[0]["WeNeedABrief"].ToString().Replace("''", "'");
                            if (dtStuInfo.Rows[0]["FacApproveDeny"].ToString() == "Approved")
                                rdoFacApprove.SelectedValue = "Y";
                            else
                                rdoFacApprove.SelectedValue = "N";
                            hdnDeanName.Value = dtStuInfo.Rows[0]["DeanName"].ToString().Trim().Replace("''", "'");
                            hdnDeanEmail.Value = dtStuInfo.Rows[0]["DeanEmail"].ToString().Trim().Replace("''", "'");
                            hdnInstructorName.Value = dtStuInfo.Rows[0]["InstructorName"].ToString().Trim().Replace("''", "'");
                            hdnInstructorEmail.Value = dtStuInfo.Rows[0]["InstructorEmail"].ToString().Trim().Replace("''", "'");
                            hdnStudentName.Value = dtStuInfo.Rows[0]["First_Name"].ToString().Replace("''", "'") + " " + dtStuInfo.Rows[0]["Last_Name"].ToString().Replace("''", "'");
                            hdnStudentEmail.Value = dtStuInfo.Rows[0]["Email_Address"].ToString().Trim().Replace("''", "'");
                            hdnAgreeTerms.Value = dtStuInfo.Rows[0]["AgreePayTerms"].ToString();
                            txtClassNumber.Enabled = false;
                            txtCourseNumber.Enabled = false;
                            txtCourseName.Enabled = false;
                            txtDropCourseDescription.Enabled = false;
                            txtDropCourseNumber.Enabled = false;
                            txtDropClassNumber.Enabled = false;
                            rdoFacApprove.Enabled = false;
                            whySucceed.Disabled = true;
                            if(alreadyDone && strStep == "4")
                            {
                                if (dtStuInfo.Rows[0]["VPApproveDeny"].ToString() == "Approved")
                                    rdoVPApprove.SelectedValue = "Y";
                                else
                                    rdoVPApprove.SelectedValue = "N";
                                if(dtStuInfo.Rows[0]["VPDenialReason"].ToString().Trim() != "")
                                    vpComment.Value = dtStuInfo.Rows[0]["VPDenialReason"].ToString().Trim().Replace("''", "'");
                                vpComment.Disabled = true;
                            }
                        }
                       if((strStep == "2" && alreadyDone) || (strStep == "4")) { 
                            if (dtStuInfo.Rows[0]["DeanApproveDeny"].ToString() == "Approved")
                                rdoDeanApprove.SelectedValue = "Y";
                            else
                                rdoDeanApprove.SelectedValue = "N";
                            rdoDeanApprove.Enabled = false;
                            if (dtStuInfo.Rows[0]["DeanDenialReason"].ToString().Trim() != "")
                                whyDeny.Value = dtStuInfo.Rows[0]["DeanDenialReason"].ToString().Trim().Replace("''", "'");
                            DeanDenyInput.Visible = true;
                            whyDeny.Disabled = true;
                        }// end step = 2 - Dean
                    }
                }
                else
                {
                    lblInstruction.Text = "No record found!";
                }
                if (strStep == "1")
                {
                    // in step for the faculty member to enter reason why the student would be successful
                    // and approve then send onto Dean or deny
                    // populate the static part of the form

                    FacInput.Visible = true;
                    DeanInput.Visible = false;
                    VPInput.Visible = false;
                    if (alreadyDone)
                        btnFacSubmit.Visible = false;
                    else
                        btnFacSubmit.Visible = true;
                    btnDeanSubmit.Visible = false;
                    btnVPSubmit.Visible = false;
                }
                else if(strStep == "2")
                {
                    // step = 2 - Dean approval
                    FacInput.Visible = true;
                    DeanID.Visible = false;
                    DeanInput.Visible = true;
                    VPInput.Visible = false;
                    if (alreadyDone)
                        btnDeanSubmit.Visible = false;
                    else
                        btnDeanSubmit.Visible = true;
                    btnFacSubmit.Visible = false;
                    btnVPSubmit.Visible = false;
                }
                else
                {
                    // step = 4
                    FacInput.Visible = true;
                    if (hdnOnline.Value == "Yes")
                        DeanInput.Visible = true;
                    else
                        DeanInput.Visible = false;
                    VPInput.Visible = true;
                    if (alreadyDone)
                        btnVPSubmit.Visible = false;
                    else
                        btnVPSubmit.Visible = true;
                    btnFacSubmit.Visible = false;
                    btnDeanSubmit.Visible = false;
                }
            }
            else
            {
                // step = 3 so display report information
                ReportHead.Visible = true;
                ReportBody.Visible = false;
                baseData.Visible = false;
                FacInput.Visible = false;
                DeanInput.Visible = false;
                divButtons.Visible = false;
                divButtons.Visible = false;
            }
        }
        if (!Page.IsPostBack)
            postBackControlName = "";
        else
        {
            postBackControlName = Page.Request.Params["__EVENTTARGET"];
//            Response.Write("control name: " + postBackControlName + "<br />");
        }
        if (IsPostBack && postBackControlName.Trim().Contains("rdoFacApprove"))
        {
            strOnline = hdnOnline.Value;
         //   Response.Write("selected: " + rdoFacApprove.SelectedValue + "; site: " + strSite + "; online: " + strOnline + "; over 6: " + day6Plus.ToString() + "<br />");
            if ((rdoFacApprove.SelectedValue == "Y" && strSite == "SFCC") || (rdoFacApprove.SelectedValue == "Y" && strSite == "SCC" && strOnline == "Yes"))
            {
                DeanID.Visible = true;
                notifyStudent.Visible = false;
                overDay5.Visible = false;
            }
            else if (rdoFacApprove.SelectedValue == "Y" && strSite == "SCC" && strOnline == "No" && day6Plus)
            {
                // VPID.Visible = true;
                // notifyStudent.Visible = false;
                // notify that VP will have to approve
                overDay5.Visible = true;
                notifyStudent.Visible = false;
            } else if (rdoFacApprove.SelectedValue == "Y" && strSite == "SCC" && strOnline == "No" && !day6Plus)
            {
                //fac approve, SCC, NOT online and < day 6 so no other steps???

            } else 
            {
                //faculty denied
                DeanID.Visible = false;
                VPInput.Visible = false;
                overDay5.Visible = false;
                notifyStudent.Visible = true;
            }

        }// end is postback from rdoFacApprove

        if (IsPostBack && postBackControlName.Trim().Contains("rdoDeanApprove"))
        {
            if (rdoDeanApprove.SelectedValue == "N")
            {
                DeanDenyInput.Visible = true;
                NotifyStudentDean.Visible = true;
            }
            else
            {
                DeanDenyInput.Visible = true;
                NotifyStudentDean.Visible = false;
            }
        }// end is postback from rdoFacApprove
    }// end page load

    protected void cmdbtnFacSubmit_Click(object sender, EventArgs e)
    {
        string strAorD = "";
        int intResult;
        string strDeanName = "";
        string strDeanEmail = "";
        string strSalutation = "";

        /******************************************************************************************** 
         * PROCESS
         * SFCC
         *  Faculty Deny --> email to student and processer (Geoff Lake) to notify of denial
         *  Faculty Approve --> email to student and selected Dean for next approval step
         * SCC
         *  Faculty Deny --> email to student and processer (Erika Naccarato) to notify of denial
         *  Faculty Approve
         *      If ONLINE class --> email to student and selected Dean for next approval step
         *      IF NOT ONLINE class AND days 1-5 of quarter --> email to student and processer 
         *      If NOT ONLINE class AND days 6+ of quarter --> email to student, processer, 
         *          and VP of Learning for next approval step
         * 
         * ******************************************************************************************/

        if (rdoFacApprove.SelectedValue == "Y")
        {
            strAorD = "Approved";
            if((strSite == "SFCC" || (strSite == "SCC" && hdnOnline.Value == "Yes")) && txtDeanEmail.Text.Trim() == "")
            {
                litPickDean.Text = "<p><span style='color:#DD0000;'>If you approve the request, you must select a Dean' Name and email for the next step.</span></p>";
                ddlDeanName.Focus();
                return;
            }
            if (strSite == "SFCC" || (strSite == "SCC" && hdnOnline.Value == "Yes")) {
                strDeanEmail = txtDeanEmail.Text.Trim().Replace("'", "''");
                strDeanName = ddlDeanName.SelectedValue;
            } else
            {
                strDeanName = "";
                strDeanEmail = "";
            }
        }
        else
            strAorD = "Denied";
        //update table storing form fields
        if(strSite != "SCC")
        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
            "SET WeNeedABrief = '{0}', FacApproveDeny = '{1}', CourseNumber = '{2}', ClassNumber = '{3}', DeanName = '{4}', DeanEmail = '{5}', CourseName = '{6}' " +
            "WHERE LateEnrollmentFormID = {7}", whySucceed.Value.ToString().Trim().Replace("'", "''"), strAorD, txtCourseNumber.Text.Trim().Replace("'", "''"), txtClassNumber.Text.Trim().Replace("'", "''"), strDeanName, strDeanEmail, txtCourseName.Text.Trim().Replace("'", "''"), intFormID);
        else
            strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
            "SET WeNeedABrief = '{0}', FacApproveDeny = '{1}', CourseNumber = '{2}', ClassNumber = '{3}', DeanName = '{4}', DeanEmail = '{5}', CourseName = '{6}' " +
            "WHERE LateEnrollmentForm_SCCID = {7}", whySucceed.Value.ToString().Trim().Replace("'", "''"), strAorD, txtCourseNumber.Text.Trim().Replace("'", "''"), txtClassNumber.Text.Trim().Replace("'", "''"), strDeanName, strDeanEmail, txtCourseName.Text.Trim().Replace("'", "''"), intFormID);

        intResult = leInfo.runSPQuery(strSQL, false);

        Utility uEmail = new Utility();

        lblInstruction.Text = "qs value: " + intFormID.ToString() + "<br />";

        // generate the email to the student
        
        if (hdnStudentEmail.Value != "" && strStep == "1")
        {
            if (strAorD == "Approved")
            {
                sendTo = hdnStudentEmail.Value;
                if (strSite != "SCC")
                    strCCaddresses = String.Format("{0}", hdnInstructorEmail.Value);
                else
                    strCCaddresses = String.Format("{0}", hdnInstructorEmail.Value);
            }
            else
            {
                sendTo = hdnStudentEmail.Value;
                if (strSite != "SCC")
                    strCCaddresses = String.Format("{0}, {1}", SFCCprocessEmail, hdnInstructorEmail.Value);
                else
                    strCCaddresses = String.Format("{0}, {1}", SCCprocessEmail, hdnInstructorEmail.Value);
            }

            if (strSite != "SCC")
                sentFrom = "SFCCInfo@sfcc.spokane.edu";
            else
                sentFrom = "SCCInfo@scc.spokane.edu";
            if (strAorD == "Approved")
                strSubject = "Faculty Approved Late Enrollment Request";
            else
                strSubject = "Faculty Denied Late Enrollment Request";
            strBody = "";
            strBody = "<p>Dear " + hdnStudentName.Value + ", </p>";
            strBody += "<p>Your request for Late Enrollment has been submitted and ";
            if (strAorD == "Approved")
                strBody += "approved ";
            else
                strBody += "denied ";
            strBody += "by the instructor, " + hdnInstructorName.Value + ".</p>";
            if (strAorD == "Approved")
            {
                if (strSite == "SFCC")
                    strBody += "<p>The request has been forwarded to the Dean who will make the final decision.  You will be notified of the final outcome.</p>";
                else if(strSite == "SCC" && hdnOnline.Value == "Yes")
                    strBody += "<p>The request has been forwarded to the Dean.  You will be notified of the Dean's decision.</p>";
                else if (strSite == "SCC" && day6Plus)
                    strBody += "<p>The request has been forwarded to the VP of Learning who will make the final decision.  You will be notified of the outcome.</p>";
                //if SCC and days 1-5 approval is DONE???
            }
            strBody += "<p>The request is for late enrollment for:</p>";
            strBody += "<table border='0' cellpadding='1' cellspacing='1' style='width:85%;'><tbody><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Course Name:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseName.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Course Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseNumber.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Class Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtClassNumber.Text.Trim() + "</td></tr>";
            if(txtDropCourseDescription.Text.Trim() != "")
            {
                strBody += "<tr><td colspan='2'><p><br />You also requested to drop the following course:</p></td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Name:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseDescription.Text.Trim() + "</td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Number:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseNumber.Text.Trim() + "</td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Class Number:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropClassNumber.Text.Trim() + "</td></tr><tr>";
                strBody += "<td colspan='2' style='vertical-align: top;'><br />Reason for decision:</td></tr><tr>";
                strBody += "<td colspan='2' style='vertical-align: top; width:65%;'>" + whySucceed.Value.Trim() + "</td></tr>";
            }
            strBody += "</tbody></table>";
         //   Response.Write("to: " + sendTo + "; from: " + sentFrom + "; cc: " + strCCaddresses + "<br />");
               strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);
               if (strEmailResult != "Success")
                   studentEmailSent = false;
               else
                   studentEmailSent = true;
               
        }
        
        if (strAorD == "Approved")
        {  
            if (strSite == "SFCC" || (strSite == "SCC" && hdnOnline.Value == "Yes"))
            {
                /* ************************************************************************
                 * generate email to the Dean  
                 *  if request APPROVED and SFCC
                 *  if request APPROVED and SCC and ONLINE class
                 * generate email to the VP of Learning
                 *  if request APPROVED and SCC and NOT ONLINE and AFTER DAY 5 of quarter
                 * ************************************************************************/
                //   sendTo = txtDeanEmail.Text.Trim();

                if (strSite != "SCC")
                {
                    sentFrom = "SFCCInfo@sfcc.spokane.edu";
                    sendTo = txtDeanEmail.Text.Trim();
                    strSalutation = "<p>Dear " + ddlDeanName.SelectedValue + ", </p>";
                }
                else
                {
                    // site is SCC
                    sentFrom = "SCCInfo@scc.spokane.edu";
                    // if ONLINE class, email goes to DEAN
                    if (hdnOnline.Value == "Yes")
                    {
                        sendTo = txtDeanEmail.Text.Trim();
                        strSalutation = "<p>Dear " + ddlDeanName.SelectedValue + ", </p>";
                    }
                }
            }
            else
            {
                if (strSite == "SCC" && day6Plus)
                {
                    // NOT ONLINE so goes to VP of Learning if after day 5 of quarter
                    sendTo = SCCVPofLearningEmail;
                    strSalutation = "<p>Dear " + SCCVPofLearningName + ", </p>";
                }
                else
                {
                    // NOT ONLINE and in days 1-5 so approval goes through??? Just email processor??
                    sendTo = SCCprocessEmail;
                    strSalutation = "<p>Dear " + SCCprocessName + ", </p>";
                }
            }
            strSubject = "Faculty Approved Late Enrollment Request";
            strCCaddresses = "";
            // prepare the body of the email going to the VP
            strBody = "";
            strBody = strSalutation;
            strBody += "<p>A request for Late Enrollment has been submitted by " + hdnStudentName.Value + " and approved by the Instructor, " + hdnInstructorName.Value + ".</p>";
            strBody += "<p>Please click the following link to review the request and either approve or deny it.</p>";
            if(strSite == "SFCC")
                strBody += String.Format("<p><a href='http://sfcc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Faculty-and-Dean-Response.aspx?s=2&i={0}&site={1}' target='_blank'>Dean Approve/Deny</a></p>", intFormID.ToString(), strSite);
            else if(strSite == "SCC" && hdnOnline.Value == "Yes")
                strBody += String.Format("<p><a href='http://scc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Faculty-and-Dean-Response.aspx?s=2&i={0}&site={1}' target='_blank'>Dean Approve/Deny</a></p>", intFormID.ToString(), strSite);
            else if(strSite == "SCC" && hdnOnline.Value == "No" && day6Plus)
                strBody += String.Format("<p><a href='http://scc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Faculty-and-Dean-Response.aspx?s=4&i={0}&site={1}' target='_blank'>VP of Learning Approve/Deny</a></p>", intFormID.ToString(), strSite);
            strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

            if (strEmailResult == "Success")
            {
                // Display confirmation
                lblInstruction.Text += "Yay!";
            }
            else
            {
                // error sending email so let user know
                lblInstruction.Text += strEmailResult;
                //return;
            }
        }// end email to dean on faculty approval
        if (strSite != "SCC")
            Response.Redirect("http://sfcc.spokane.edu/For-Our-Students/Getting-Help/Form-Confirmation");
        else {
            Response.Redirect("http://scc.spokane.edu/For-Our-Students/Getting-Help/Form-Confirmation");
            ;
        }
    }

    protected void cmdbtnDeanSubmit_Click(object sender, EventArgs e)
    {
        string strAorD = "";
        string strSalutation = "";
        string allEmails = "";
        int intResult;
        if(rdoDeanApprove.SelectedIndex == -1)
        {
            lblDeanAct.Text += "<p><span style='color:#DD0000'>You must approve or deny the request</p>";
            return;
        }
        if (rdoDeanApprove.SelectedValue == "Y")
            strAorD = "Approved";
        else
            strAorD = "Denied";
        //update table storing form fields
        if(strSite != "SCC")
            strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                "SET DeanApproveDeny = '{0}', DeanDenialReason = '{1}' " +
                "WHERE LateEnrollmentFormID = {2}", strAorD, whyDeny.Value.Trim().Replace("'", "''"), intFormID);
        else
            strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                "SET DeanApproveDeny = '{0}', DeanDenialReason = '{1}' " +
                "WHERE LateEnrollmentForm_SCCID = {2}", strAorD, whyDeny.Value.Trim().Replace("'", "''"), intFormID);

        intResult = leInfo.runSPQuery(strSQL, false);

        // generate the email to all
        Utility uEmail = new Utility();

        //All done if SFCC or SCC and days 1-5 of quarter
      //  if ((strSite == "SFCC") || ((strSite == "SCC" && !day6Plus)))
      //  {
            // SFCC: approve or deny
            // SCC < 6 days so no VP:  approve or denty
            lblInstruction.Text = "qs value: " + intFormID.ToString() + "<br />";
            
            if (strSite != "SCC")
                allEmails = String.Format("{0}, {1}, {2}, {3}", hdnInstructorEmail.Value, hdnStudentEmail.Value, hdnDeanEmail.Value, SFCCprocessEmail);
            else
                allEmails = String.Format("{0}, {1}, {2}", hdnInstructorEmail.Value, hdnStudentEmail.Value, SCCprocessEmail);

            sendTo = allEmails;
            //    Response.Write("all emails: " + sendTo + "<br />");
            //    sendTo = hdnInstructorEmail.Value;
            sentFrom = hdnDeanEmail.Value;
            if (strAorD == "Approved")
                strSubject = "Dean Approved Late Enrollment Request";
            else
                strSubject = "Dean Denied Late Enrollment Request";
            strCCaddresses = "";
            // prepare the body of the email 
            strBody = "";
            strBody = "<p>To all, </p>";
            strBody += "<p>A request for Late Enrollment has been submitted by the student " + hdnStudentName.Value + " and approved by the Instructor " + hdnInstructorName.Value + ".</p>";
            strBody += "<p>As Dean of the Department, I have ";
            if (strAorD == "Approved")
                strBody += "approved ";
            else
                strBody += "denied ";
            strBody += "the late enrollment for student " + hdnStudentName.Value + ".";
            if (strAorD == "Denied")
                strBody += "<p>The reason for denial is as follows: <br />" + whyDeny.Value.Trim();
            strBody += "</p><p>Sincerely,<br />" + hdnDeanName.Value + "</p>";
            strBody += "<p>The request is for late enrollment in:</p>";
            strBody += "<table border='0' cellpadding='1' cellspacing='1' style='width:85%;'><tbody><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Course Name:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseName.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Course Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseNumber.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Class Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtClassNumber.Text.Trim() + "</td></tr>";
            if (txtDropCourseDescription.Text.Trim() != "")
            {
                strBody += "<tr><td colspan='2'><p><br />The student also requested to drop the following course:</p></td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Name:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseDescription.Text.Trim() + "</td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Number:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseNumber.Text.Trim() + "</td></tr><tr>";
                strBody += "<td style='width:35%;vertical-align:top;'>Drop Class Number:</td>";
                strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropClassNumber.Text.Trim() + "</td></tr>";
            }
            strBody += "</tbody></table>";
            //if (rdoDeanApprove.SelectedValue == "Y")
            // strBody += "<p>The student ";
            if (rdoDeanApprove.SelectedValue == "Y")
            {
                if (hdnAgreeTerms.Value == "True")
                    strBody += "<p>This student agrees to the terms of payment</b>.</p>";
                else
                    strBody += "<b><p>This student does NOT agree to the terms of payment</b>.</p>";
            }

            strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

            if (strEmailResult == "Success")
            {
                // Display confirmation
                lblInstruction.Text += "Yipee!";
            }
            else
            {
                // error sending email so let user know
                lblInstruction.Text += strEmailResult;
                //return;
            }
      //  } // end SFCC or SCC and days 1-5
        if (strSite == "SCC" && strAorD == "Approved") 
        {
            if (hdnOnline.Value == "Yes" && day6Plus)
            {
                // SCC and days 6+ so email VP of Learning for his/her approval
                sentFrom = "SCCInfo@scc.spokane.edu";
                sendTo = SCCVPofLearningEmail;
                strSalutation = "<p>Dear " + SCCVPofLearningName + ", </p>";
                strSubject = "Dean Approved Late Enrollment Request";

               // allEmails = String.Format("{0}, {1}", hdnInstructorEmail.Value, hdnStudentEmail.Value);
                strCCaddresses = "";
                // prepare the body of the email going to the VP
                strBody = "";
                strBody = strSalutation;
                strBody += "<p>A request for Late Enrollment has been submitted by " + hdnStudentName.Value + ", approved by the Instructor, " + hdnInstructorName.Value + " and the Dean, " + hdnDeanName.Value + ".</p>";
                strBody += "<p>Please click the following link to review the request and either approve or deny it.</p>";
                strBody += String.Format("<p><a href='http://scc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Faculty-and-Dean-Response.aspx?s=4&i={0}&site={1}' target='_blank'>VP of Learning Approve/Deny</a></p>", intFormID.ToString(), strSite);

                strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

                if (strEmailResult == "Success")
                {
                    // Display confirmation
                    lblInstruction.Text += "Yay!";
                }
                else
                {
                    // error sending email so let user know
                    lblInstruction.Text += strEmailResult;
                    //return;
                }
            }// end send to VP: online = yes OR online = no but day 6 or more of quarter
        }// end SCC and Dean approved so check on sending to VP

        if (strSite != "SCC")
            Response.Redirect("http://sfcc.spokane.edu/For-Our-Students/Getting-Help/Form-Confirmation");
        else
            Response.Redirect("http://scc.spokane.edu/For-Our-Students/Getting-Help/Form-Confirmation");

    }

    protected void cmdbtnVPSubmit_Click(object sender, EventArgs e)
    {
        /************************************************************************************************ 
         * PROCESS
         * SCC ONLY!!  SFCC does not route through the VP of Learning
         *  VP Deny --> email to student and processer (Erika Naccarato) to notify of denial
         *  VP Approve
         *      If ONLINE class --> email to student and selected Dean and processer.  This is final step
         *      IF NOT ONLINE class email to student and processer.  This is final step
         * 
         * **********************************************************************************************/
        string strAorD = "";
        int intResult;

        if (rdoVPApprove.SelectedIndex == -1)
        {
            lblVPAct.Text += "<p><span style='color:#DD0000'>You must approve or deny the request</p>";
            return;
        }
        if (rdoVPApprove.SelectedValue == "Y")
            strAorD = "Approved";
        else
            strAorD = "Denied";

        //update table storing form fields
        if (strSite == "SCC")
        {
            strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                "SET VPApproveDeny = '{0}', VPDenialReason = '{1}', VPName = '{2}', VPEmail = '{3}' " +
                "WHERE LateEnrollmentForm_SCCID = {4}", strAorD, vpComment.Value.Trim().Replace("'", "''"), SCCVPofLearningName, SCCVPofLearningEmail, intFormID);
            intResult = leInfo.runSPQuery(strSQL, false);
        }
        else
            lblInstruction.Text += "<p>This should only be SCC!</p>";
        // generate the email to all
        Utility uEmail = new Utility();

        lblInstruction.Text = "qs value: " + intFormID.ToString() + "<br />";
        string allEmails = "";
        if (strSite == "SCC" && hdnOnline.Value == "Yes")
            allEmails = String.Format("{0}, {1}, {2}, {3}", hdnInstructorEmail.Value, hdnStudentEmail.Value, hdnDeanEmail.Value, SCCprocessEmail);
        else if(strSite == "SCC" && hdnOnline.Value == "No")
            allEmails = String.Format("{0}, {1}, {2}", hdnInstructorEmail.Value, hdnStudentEmail.Value, SCCprocessEmail);
        else
        lblInstruction.Text += "<p>This should only be SCC!</p>";

        sendTo = allEmails;
        sentFrom = SCCVPofLearningEmail;
        if (rdoVPApprove.SelectedValue == "Y")
            strSubject = "VP of Learning Approved Late Enrollment Request";
        else
            strSubject = "VP of Learning Denied Late Enrollment Request";
        strCCaddresses = "";
        // prepare the body of the email 
        strBody = "";
        strBody = "<p>To all, </p>";
        
        if (hdnOnline.Value == "Yes")
        {
            strBody += "<p>A request for Late Enrollment has been submitted by " + hdnStudentName.Value + ", approved by Instructor " + hdnInstructorName.Value;
            strBody += ", and approved by Dean " + hdnDeanName.Value + ".</p>";
        }
        else
            strBody += "<p>A request for Late Enrollment has been submitted by " + hdnStudentName + " and approved by Instructor " + hdnInstructorName.Value + ".</p>";
        strBody += "<p>As VP of Learning, I have ";
        if (rdoVPApprove.SelectedValue == "Y")
            strBody += "approved ";
        else
            strBody += "denied ";
        strBody += "the late enrollment for student " + hdnStudentName.Value + ".";
        if (rdoVPApprove.SelectedValue == "N")
            strBody += "<p>The reason for denial is as follows: <br />" + vpComment.Value.Trim();
        strBody += "</p><p>Sincerely,<br />" + SCCVPofLearningName + "</p>";
        strBody += "<p>The request is for late enrollment in:</p>";
        strBody += "<table border='0' cellpadding='1' cellspacing='1' style='width:85%;'><tbody><tr>";
        strBody += "<td style='width:35%;vertical-align:top;'>Course Name:</td>";
        strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseName.Text.Trim() + "</td></tr><tr>";
        strBody += "<td style='width:35%;vertical-align:top;'>Course Number:</td>";
        strBody += "<td style='vertical-align: top; width:65%;'>" + txtCourseNumber.Text.Trim() + "</td></tr><tr>";
        strBody += "<td style='width:35%;vertical-align:top;'>Class Number:</td>";
        strBody += "<td style='vertical-align: top; width:65%;'>" + txtClassNumber.Text.Trim() + "</td></tr>";
        if (txtDropCourseDescription.Text.Trim() != "")
        {
            strBody += "<tr><td colspan='2'><p><br />The student also requested to drop the following course:</p></td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Name:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseDescription.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Drop Course Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropCourseNumber.Text.Trim() + "</td></tr><tr>";
            strBody += "<td style='width:35%;vertical-align:top;'>Drop Class Number:</td>";
            strBody += "<td style='vertical-align: top; width:65%;'>" + txtDropClassNumber.Text.Trim() + "</td></tr>";
        }
        strBody += "</tbody></table>";

        if (rdoVPApprove.SelectedValue == "Y")
        {
            if (hdnAgreeTerms.Value == "True")
                strBody += "<p>This student agrees to the terms of payment</b>.</p>";
            else
                strBody += "<b><p>This student does NOT agree to the terms of payment</b>.</p>";
        }

        strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);

        if (strEmailResult == "Success")
        {
            // Display confirmation
            lblInstruction.Text += "Yipee!";
        }
        else
        {
            // error sending email so let user know
            lblInstruction.Text += strEmailResult;
            //return;
        }
        if (strSite == "SCC")
            Response.Redirect("http://scc.spokane.edu/For-Our-Students/Getting-Help/Form-Confirmation");
        else
            lblInstruction.Text = "WE HAVE A PROBLEM, HOUSTON!";
    }
        protected void cmdbtnReport_Click(object sender, EventArgs e)
    {
        string studentId = "";
        int stuID;
        bool isNumber = false;

        lblReportErrMsg.Text = "";

        studentId = txtStudentID.Text.Trim();
        if(chkAll.Checked && txtStudentID.Text.Trim() != "")
        {
            lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>Select either the check box OR enter a student ID. Both cannot be selected.</p>";
            chkAll.Focus();
            return;
        }
        if (txtStudentID.Text.Trim() != "")
        {
            
            if (studentId.Length != 9)
            {
                lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>The student ID must contain 9-digits.</p>";
                txtStudentID.Focus();
                return;
            }
            isNumber = int.TryParse(studentId, out stuID);
            if (!isNumber)
            {
                lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>The student ID must be a 9-digit number.</p>";
                txtStudentID.Focus();
                return;
            }
        }// end validation of student ID
        if(txtStudentID.Text.Trim() == "" && !chkAll.Checked)
        {
                lblReportErrMsg.Text = "<p><span style='color:#DD0000;'> You must select the box to report all requests or enter a student ID to report on one student.</p>";
                chkAll.Focus();
                return;
        }
        if (chkAll.Checked)
        {
            // report all records
            strSQL = String.Format("SELECT * FROM[KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                "ORDER BY ctcLinkID");
        }
        else if (studentId != "")            
        {
            
            //Seach for one student record
            strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                       "WHERE ctcLinkID = '{0}'", studentId);
        }
        DataTable dtStuInfo = leInfo.RunGetQuery(strSQL);
        if (dtStuInfo != null)
        {
            if (dtStuInfo.Rows.Count > 0)
            {
              //  List<CheckBox> checkboxes = new List<CheckBox>();
                for (int i = 0; i < dtStuInfo.Rows.Count; i++)
                {
                    Label litBody = new Label();
                    if(i != 0)
                    {
                        litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div></div>";
                    }
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div></div>";
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtStuInfo.Rows[i]["ctcLinkID"].ToString() + "</p></div>";
                    litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtStuInfo.Rows[i]["First_Name"].ToString().Replace("''", "'") + " " + dtStuInfo.Rows[i]["Last_Name"].ToString().Replace("''", "'") + "</p></div>";
                    litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtStuInfo.Rows[i]["Email_Address"].ToString().Replace("''", "'") + "</p></div>";
                    if (dtStuInfo.Rows[i]["AgreePayTerms"].ToString() == "True")
                        litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Agree to payment Terms</p></div></div>";
                    else
                        litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Does NOT agree to payment Terms</p></div></div>";
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Class Information</b></p></div><div class='col-md-3'><p><b>Instructor Response</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Dean Response</b></p></div><div class='col-md-3'><p><b>Office</b></p></div></div>";
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p>" + dtStuInfo.Rows[i]["Program"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["CourseName"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["CourseNumber"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["ClassNumber"].ToString() + "</p></div>";

                    litBody.Text += "<div class='col-md-3'><p>" + dtStuInfo.Rows[i]["InstructorName"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["InstructorEmail"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["FacApproveDeny"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["WeNeedABrief"].ToString() + "</p></div>";

                    litBody.Text += "<div class='col-md-3' style='background-color:#DDDDDD;'><p>" + dtStuInfo.Rows[i]["DeanName"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["DeanEmail"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["DeanApproveDeny"].ToString() + "<br />";
                    litBody.Text += dtStuInfo.Rows[i]["DeanDenialReason"].ToString().Replace("''", "'") + "</p></div>";

                    //   litBody.Text += "<div class='col-md-1'><p>" + dtStuInfo.Rows[i]["Process"].ToString() + "</p></div>";
                    string ctcID = "";
                    ctcID = "chk | " + i + " | " + dtStuInfo.Rows[i]["ctcLinkID"].ToString();
                    CheckBox chk = new CheckBox();
                    chk.ID = "chk|" + i + "|" + dtStuInfo.Rows[i]["ctcLinkID"].ToString();
                  //  chk.Text = "Processed";
                  //  this.Controls.Add(chk);
                   // checkboxes.Add(chk);
                 //   litBody.Text += "<div class='col-md-3' style='align:left;'><p>Processed:&nbsp;&nbsp;";
                 //   litBody.Controls.Add(chk);
                //    litBody.Text += "<textarea cols='25' rows='2' runat='server' id='txtArea|" + i + "|" + dtStuInfo.Rows[i]["ctcLinkID"].ToString() + "'></textarea></p></div></div>";
                    litBody.Text += "<div class='col-md-3' style='align:left;'><p>Processed:&nbsp;&nbsp;";
                    //  litBody.Controls.Add(chk);
                    pnlReport.Controls.Add(litBody);
                    pnlReport.Controls.Add(chk);
                    Label litBody2 = new Label();
                    litBody2.Text = "<textarea cols='25' rows='2' runat='server' id='txtArea|" + i + "|" + dtStuInfo.Rows[i]["ctcLinkID"].ToString() + "'></textarea></p></div></div>";          
                  //  litBody2.Text += "<div class='cs_row col-lg-12'><div class='col-md-12'><hr /></div></div>";
                    pnlReport.Controls.Add(litBody2);
                }
                ReportBody.Visible = true;
            }
            else
            {
                // no recored returned
                lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>No records found.</span></p>";
            }
        }// record set is null
    }// end cmdbtnReport

    protected void btnProcess_Click(object sender, EventArgs e)
    {
 //       Response.Write("in process<br />");
        int chkCount = pnlReport.Controls.Count;
 //       Response.Write("count of controls: " + chkCount.ToString() + "<br />");
        CheckBox ck;
        for (int x = 0; x < chkCount; x++)
        {
 //           Response.Write("value of x: " + x.ToString() + "<br />");
            if (pnlReport.Controls[x] is CheckBox)
            {
                ck = (CheckBox)this.Controls[x];
 //               Response.Write("id: " + ck.ID + "; " + ck.Checked.ToString() + "<br /");
            }
        }

            
    }
}
