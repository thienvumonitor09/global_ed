﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/QuarterStartDate.ascx.cs" Inherits="CMSWebParts_Custom_QuarterStartDate" %>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<asp:Label ID="Label1" runat="server"  />
<input type="hidden" id="jsonListID" runat="server" />
<input type="hidden" id="quarterListJsonID" runat="server" />
<div class="container" id="containerDiv">
   <asp:label id="myLabel" runat="server" />   
    
  <div class="row-5">

<div class="container skinny FastFacts2Column" id="quarterStartDateID2" > 
   

</div>
</div>
</div>
       
<script type="text/javascript">
    $(function () {
        //var hf1 = $('input[id$=hf1]');
        //Create 4 blank tiles first without populating data so the JS can fill colors 
        var main = $('#quarterStartDateID2');
        for (var i = 0; i < 4; i++) {
            var article = $('<article class="col-xs-12 col-sm-6">').appendTo(main);
            var div1 = $('<div class="colored-overlay NeedBackgroundColor">').appendTo(article);
            var div2 = $('<div class="flex-height content-area FF2">').appendTo(div1);
            var div3 = $('<div class="overlay-image">').appendTo(div2);
            $('<img/>').appendTo(div3);
            $('<div class="content">').appendTo(div2);
        }
       
       //Get data as JSON type from REST API
        $.ajax({
            type: "GET",
            url: "//external.spokane.edu/REST/API/QuarterStartDate",
            //contentType: "application/json",
            dataType: "json"
        })
        .done(function (data) {
                //alert("success");
            console.log(data);
            fillDataTiles(data);   
        })
        .fail(function (xhr) {
            console.log('error', xhr);
        });

       
    });

    //Function to populate data into 4 tiles
    function fillDataTiles(data) {
        var main = $('#quarterStartDateID2');
        var contentDivs = main.find('.content');
        var imgTags = main.find('img');
        $.each(data,function (index,element) {
            var quarterE = element.QuarterObj.QuarterName;
            var yearE = element.QuarterObj.Year;
            var firstDayDate = "N/A";
            var applicationDeadline = "";
            var countDeadline = 0;
            element.InfoList.forEach(function (value, index) {
                if (value.Title.indexOf("First Day") > -1) {
                    firstDayDate = convertDate(value.Category);
                } else if (value.Title.indexOf("Deadline") > -1) {
                    if (countDeadline == 0) {
                        applicationDeadline += convertDate2(value.Category);
                    } else {
                        applicationDeadline += "(SCC)" +  convertDate2(value.Category) +"(SFCC)";
                    }
                    countDeadline++;
                } 
            });
            var imgStr = "/ccsglobal/media/Global/QuarterStartDate/qtrBg" + quarterE + ".jpg";
            imgTags[index].src = imgStr;
            imgTags[index].alt = "";
            
            var firstPara = firstDayDate;
            var secondPara = "First Day of " + quarterE + " " + yearE +" Quarter";
            var thirdPara = "New Student Application Deadline";
            var fourthPara = applicationDeadline;
            if (quarterE == "Winter" && yearE == "2019") {
                fourthPara = "12/12/2018";
            }
            $('<h1 style="text-align: center;">' + firstPara + '</h1><h2 style="text-align: center;font-size:2em;">' + secondPara + '</h2><h4 style="text-align: center;font-size:1.5em;"><em>' + thirdPara + '<br>' + fourthPara + '</em></h4><p></p>').appendTo(contentDivs[index]);                

        });
    }
    

    /*
    function fillColorTiles() {
         var colorCodes  = ["blue","green","maroon","purple","brown","orange"];
        var tiles = $('.NeedBackgroundColor').not("[class*='-bg']");
        var tiles2 = $('.NeedBackgroundColor').filter("[class*='-bg']");
        var lastTileColor = tiles2[tiles.length - 1].className;
        var lastColor = lastTileColor.substring(lastTileColor.lastIndexOf(" "), lastTileColor.lastIndexOf("-"));
        console.log(lastColor);
        var k = -1; 
        $.each(colorCodes, function (index, value) {
            if (lastTileColor.indexOf(value) > -1) {
                k = index;
            }
        });
        
        console.log(k);
        for(var i = 0; i < tiles.length; i++){
            if (tiles[i].classList.contains("trans")) {
                tiles[i].className += (" " + colorCodes[++k % colorCodes.length] + "-bg-trans");
            }
            else {
                tiles[i].className += (" " + colorCodes[++k % colorCodes.length] + "-bg");
            }
                
        }
    }

*/
    function convertDate(dateStr) {
        var monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Augt", "Sep", "Oct", "Nov", "Dec"];

        var d = new Date(Date.parse(dateStr));
        var monthS = monthName[d.getMonth()];
        var dateS = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
        return monthS + " " + dateS;
    }

    //format mm/dd/yyyy
    function convertDate2(dateStr) {
        var d = new Date(Date.parse(dateStr));
        var monthS = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
        var dateS = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
        return monthS + "/" + dateS + "/" + d.getFullYear();
    }
</script>