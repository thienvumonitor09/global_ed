﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/WorkforceClassList.ascx.cs" Inherits="CMSWebParts_Custom_WorkforceClassList" %>
        <style type="text/css">           
            /*  Also uses classes PaneHeader, SelectedPaneHeader, PaneContent
                found in ccs.css style sheet in Kentico.
            */       
            th.alignLeft {text-align:left;}
            th.alignRight {text-align:right;}
        </style>

        <asp:Literal ID="litSubj" runat="server"></asp:Literal>
        <div class="classList">
            <asp:Table ID="tblClass" runat="server" Width="100%" BorderWidth="0" GridLines="None" CellSpacing="2">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell CssClass="alignLeft" Width="13%">Class #</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignLeft" Width="35%">Title</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignLeft" Width="12%">Type</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignRight" Width="10%">Cost</asp:TableHeaderCell>
                        <asp:TableHeaderCell CssClass="alignRight" Width="30%">Dates</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
            </asp:Table>
			<asp:Literal ID="litNoClasses" runat="server"></asp:Literal>
            <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        </div> 
