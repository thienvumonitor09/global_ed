﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;

public partial class CMSWebParts_Custom_QuarterStartDate : CMSAbstractWebPart
{
    //public static XDocument importantDateXML;
    //public static XDocument globalXML;
    public static List<XDocument> importantDateXMLList;
    public static List<XDocument> globalXMLList;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/QuarterStartDate_files/QuarterStartDate.css";
        Page.Header.Controls.Add(styleLink);
        //myLabel.Text = "Dynamic";
        
        //firstQDateID1.Text = "JAN 02";
        //JavaScriptSerializer js = new JavaScriptSerializer();
        //js.MaxJsonLength = Int32.MaxValue;
        //Label1.Text = js.Serialize(GetQuarterInfoList2());
        //jsonListID.Value = js.Serialize(GetQuarterInfoList2());
    }

    public static List<QuarterInfo> GetQuarterInfoList2()
    {
        List<QuarterInfo> quarterInfoList = new List<QuarterInfo>();
        List<Quarter> quarters = GetQuarters();
        List<XDocument> xdocs = CreateXDocumentList("important-dates");
        //Label1.Text = "here";
        foreach (Quarter q in GetQuarters())
        {
            List<Date> infoList = new List<Date>();
           
            //Get XElement
            List<Date> elementList = GetDate2(q);
            foreach (Date element in elementList)
            {
                infoList.Add(element);
            }
            
            quarterInfoList.Add(new QuarterInfo(q, infoList));
        }
        return quarterInfoList;
    }

    

    public static List<Quarter> GetQuarters()
    {
        int m = DateTime.Now.Month;
        int year = DateTime.Now.Year;
        int i = 0; // where to start from quarter list
        if (m == 1)
            i = 0;
        else if (m >= 2 && m <= 4)
            i = 1;
        else if (m >= 5 && m <= 7)
            i = 2;
        else if (m >= 8 && m <= 9)
            i = 3;
        else
            i = 4;
        List<Quarter> quarterList = new List<Quarter>();
        string[] quarters = { "Winter", "Spring", "Summer", "Fall" };
        int count = 1;
        int y = year;
        while (count <= 4)
        {
            if (i >= 4)
            {
                y = year + 1;
            }
            quarterList.Add(new Quarter(quarters[i % 4], y));
            count++;
            i++;
        }
        return quarterList;
    }

    

    public static List<Date> GetDate2(Quarter q)
    {
        List<XDocument> xdocs = CreateXDocumentList("important-dates");
        List<Date> dateList = new List<Date>();
        foreach (XDocument xdocE in xdocs)
        {
            foreach (XElement d in xdocE.Root.Nodes())
            {
                string category = d.Element("Category").Value;
                string title = d.Element("Title").Value;
               
                if (title.Contains(q.QuarterName))//Look for the quarter
                {
                    //B/c the "first day" sometimes does not contain Year, we have to match year in category
                    if(title.Contains("First Day") && category.Contains(q.Year.ToString())
                       || title.Contains("Deadline") && title.Contains(q.Year.ToString())) 
                    {
                        //Only add if the element does not exist in the dateList
                        if (!dateList.Exists(element => element.Category == category && element.Title == title))
                        {
                            if (!string.IsNullOrEmpty(category))
                            {
                                dateList.Add(new Date() { Title = title, Category = category });
                            }
                        }
                    }
                    
                }
                
                
            }
            //dateList.Add(new Date() { Title = title, Category = category });
            //add new Date when date of SCC <> date of SFCC
            
        }

        //if both SCC and SFCC added, must change the title to indicate which school
        /*
        if (dateList.Count > 1)
        {
            var i = 0;
            foreach (Date dateE in dateList)
            {
                dateE.Title += " " + schoolList[i++].ToUpper();
            }
        }
        */
        return dateList;
    }
    

   
    public static List<XDocument> CreateXDocumentList(string calendarType)
    {
        List<XDocument> xdocList = new List<XDocument>();

        string startDate = (DateTime.Now.Year - 1) + "0101";
        string endDate = (DateTime.Now.Year + 1) + "1231";
        string feedURL = "";
        List<string> siteList = new List<string>();
        string[] schoolList = { "scc", "sfcc" };
        foreach (string school in schoolList)
        {
            siteList.Add(school);
        }
        List<string> feedURLList = new List<String>();
        //calendarType = important-dates (SCC/SFCC) || global
        foreach (string site in siteList)
        {
            feedURL = "http://25livepub.collegenet.com/calendars/" + site + "-important-dates.rss";
            feedURL += "?startdate=" + startDate + "&enddate=" + endDate;
            feedURLList.Add(feedURL);
        }
        
        foreach (string strURL in feedURLList)
        {
            XDocument xdoc = XDocument.Load(strURL);
            XDocument convertedXDocument = new XDocument(
                                     new XElement("ImpDates",
                                       from item in xdoc.Descendants("item")
                                           where (item.Element("title").Value.Contains("First Day") 
                                                    || item.Element("title").Value.Contains("New Student Admission Application Deadline (with no prior college)"))
                                           //orderby (string)item.Element("category").Value
                                       select new XElement("Date",
                                              new XElement("Title", item.Element("title").Value),
                                              new XElement("Category", item.Element("category").Value))));
            xdocList.Add(convertedXDocument);
        }
        return xdocList;
    }
}