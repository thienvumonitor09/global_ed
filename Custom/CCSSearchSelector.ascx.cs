using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine;
using CMS.Helpers;
using MyCustomModule;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using CMS.FormEngine.Web.UI;
using CMS.PortalEngine.Web.UI;
using CMS.DocumentEngine.Web.UI;
using CMS.DataEngine;
using CMS.SiteProvider;
using System.Linq;
using CMS.EventLog;


public partial class CMSWebParts_Custom_CCSSearchSelector : CMSAbstractWebPart
{
    #region "Properties"

    public string Transformation
    {
        get
        {
            return GetStringValue("Transformation", "");
        }
       
    }

    public int ItemLimit
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("ItemLimit"), 0);
        }

    }
    #endregion


    #region "Methods"
    ///<summary>
    ///Takes the stored value of the web part and converts to List. Value is stored: guid1^^title1&&guid2^^title2&&
    ///</summary>
    protected List<SelectedItemInfo> GetSelectedItems(string val)
    {
        string items = "";

        if (val != null)
        {
            items = val;
        }

        if (items != null && items != "")
        {
            List<SelectedItemInfo> data = new List<SelectedItemInfo>();

            var splitItems = items.Split(new string[] { "&&" }, StringSplitOptions.None);

            foreach (var item in splitItems)
            {
                string[] delimited = item.Split(new string[] { "^^" }, StringSplitOptions.None);

                if (item != "")
                {
                    try
                    {
                        SelectedItemInfo curItem = new SelectedItemInfo();
                        curItem.SelectedItemGuid = new Guid(delimited[0]);
                        curItem.SelectedItemName = delimited[1];

                        data.Add(curItem);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            return data;
        }

        else
        {
            return null;
        }

    }

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    /// <summary>
    /// Render method override. Using value stored in webpart field(s), retrieve data from Funnelback and output transformed HTML.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
        List<string> itemGUIDs = new List<string>();

        DataTable tbl = new DataTable();

        var savedValue = "";
        if (this.GetValue("searchselection") != null)
        {
            savedValue = this.GetValue("searchselection").ToString();
        }

        if (savedValue != "")
        {
            var itemsToRetrieve = GetSelectedItems(savedValue);
            var itemsToRetrieveCount = itemsToRetrieve.Count;
            if (ItemLimit < itemsToRetrieveCount)
            {
                itemsToRetrieveCount = ItemLimit;
            }
            
            foreach(var itm in itemsToRetrieve.Take(itemsToRetrieveCount))
            { 
                itemGUIDs.Add(itm.SelectedItemGuid.ToString());
            }

            string[] items = itemGUIDs.ToArray();
            try
            {
                string searchBase = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".FunnelbackSearchURI");
                string metaCollection = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".FunnelbackMetaCollection");

                FunnelbackHelper.ConnectionHelper conn = new FunnelbackHelper.ConnectionHelper();
                //EventLogProvider.LogInformation(EventType.INFORMATION, "SS_Query", "searchBase = " + searchBase + "|metaCollection = " + metaCollection + "|items = " + items.ToString());

                JObject r = conn.RequestJSON(searchBase, metaCollection, "documentguid", "or", items);

                tbl = conn.ConvertResponseToTable(r);

                //EventLogProvider.LogInformation(EventType.INFORMATION, "SS_Info", "items: " + items.Join(" "));
                
                tbl = FunnelbackHelper.ConnectionHelper.RemoveDuplicateTableRowsByDate(tbl, "DocumentModifiedWhen", "DocumentGuid");

                //Order rows in DataTable to match order of values according to a column
                tbl = FunnelbackHelper.ConnectionHelper.OrderDataTable(tbl, items, "documentguid");
                //EventLogProvider.LogInformation(EventType.INFORMATION, "SS_InfoOrdered", "ordered items: " + items.Join(" "));

                if (Transformation != "")
                {
                    rptResults.ItemTemplate = TransformationHelper.LoadTransformation(this, Transformation);
                    rptResults.DataSource = tbl;
                    rptResults.DataBind();

                    gridView.DataSource = tbl;
                    gridView.DataBind();
                }
               
            }
            catch (Exception ex)
            {
                EventLogProvider.LogInformation(EventType.ERROR, "SearchSelector", ex.ToString());
            }
        }

        base.Render(writer);
    }

    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {

        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



