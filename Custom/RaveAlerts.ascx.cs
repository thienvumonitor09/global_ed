﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_RaveAlerts : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // *** RAVE MOBILE MESSAGES ***
        
        string strEventName = "";
        string strDescription = "";
        string strWeb = "";
        

        alert.Visible = false;

        RaveMobile myMsg = new RaveMobile();
        strEventName = myMsg.eventname;
        strDescription = myMsg.description;
        /*strWeb = myMsg.web;*/

 //       strDescription = "This is a TEST and this is the description of the TEST. 2 This is a TEST and this is the description of the TEST. 3 - This is a TEST and this is the description of the TEST. 4 - This is a TEST and this is the description of the TEST. ";
 //       strEventName = "THIS IS A TEST of RAVE MOBILE<br />";
        strEventName = strEventName.ToUpper();
        strWeb = "http://ccsbeta.spokane.edu/Alert";
        
        if (strEventName != "")
        {
            // Build alert message - comment out truncations for complete text
            // *** TRUNCATIONS ***
            int length1 = strEventName.Length;
            int length2 = strDescription.Length;

            if (length1 > 37) //cap eventname length at 40 characters and truncate if necessary
            {
                strEventName = strEventName.Substring(0, 37);
                //Make sure message ends with a whole word
                int length3 = strEventName.LastIndexOf(" ");
                strEventName = strEventName.Substring(0, length3);
                strEventName += "...";
                length1 = strEventName.Length;    //Adjusted length for all one line format
            }
            //if (length2 > 90) //cap length at 90 characters and truncate if necessary (title and event on 2 lines)
            if (length1 + length2 > 90) //cap combined length at 90 characters and truncate if necessary (all one line)
            {
                //strDescription = strDescription.Substring(0, 90); // Two lines
                strDescription = strDescription.Substring(0, (90 - length1));
                //Make sure message ends with a whole word
                int length4 = strDescription.LastIndexOf(" ");
                strDescription = strDescription.Substring(0, length4);
                strDescription += "...";
            }
            // *** END TRUNCATIONS ***
            string htmlAlert = "<div class=\"alertbanner\">[Alert details]</div>";
            htmlAlert = htmlAlert.Replace("[Alert details]", "<h2></h2><div></div>");
            htmlAlert = htmlAlert.Replace("<h2></h2>", "<p><span>" + strEventName + " - "  + "</span>");  //all one line 
            htmlAlert = htmlAlert.Replace("<div></div>", strDescription + " &#187;&nbsp;<a href=\"" + strWeb + "\">View Details</a></p>");
            //htmlAlert = htmlAlert.Replace("<h2></h2>", "<h2>" + strEventName + "</h2>");    //Two lines
            //htmlAlert = htmlAlert.Replace("<div></div>", "<div>" + strDescription + " &#187;<a href=\"" + strWeb + "\">View Details</a></div>");

            // Display alert banner
            alert.Text = htmlAlert;
            alert.Visible = true;
        }
        // ***END RAVE MOBILE ***
    }
}