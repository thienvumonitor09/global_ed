﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ADlogon.ascx.cs" Inherits="CMSWebParts_Custom_ADlogon" %>

<style type="text/css">
     p {
            margin: 5px 0px;  
            padding-left:10px;
        }
 .loginbox
{
    border: 1px solid #0059A9;
	width: 580px;
	border-radius:10px;
	margin:auto;
    font-size:1.4rem;
    line-height:1.7;
}
 legend{
     padding-bottom: 20px;
     border-bottom: 1px solid #F8F6ED;
 }

/* #F8F6ED = pale manila color */
.FormBGColor
{
    background-color: #F8F6ED;
    margin-top: -10px;
}

/* Form Field styling */    /* Form Field styling */    /* Form Field styling */
.appsFormButton 
{
    background-color: #2A59A5;
    color: #fff;
    padding: 5px 10px 5px 10px;
    border-radius: .5em;
    -webkit-border-radius: .5em;
	-moz-border-radius: .5em;
    border: 1px solid #2A59A5;
    letter-spacing: .05em;
    font-weight:bold;
}

.appsFormButton:hover
{
    background-color: #fff;
    color: #2A59A5;
    border: 1px solid #2A59A5;
    cursor: pointer;
}

.BlueBGColor
{
    background-color: #eff6fc;
}
    </style>
    <script type="text/javascript" src="/_js/pushToHttps.js"></script>
        <div style="margin: auto;">
            <fieldset class="loginbox FormBGColor">
                <legend>Login</legend>
                <p>Please login with your CCS User Principal Name (UPN):</p>
                <div id="divStudentExample" runat="server" style="border: 1px solid #0059A9; padding: 3px; border-radius: 10px; background-color: White;">
                    <p style="text-align: center;"><strong>Student Example</strong></p>
                    <p style="padding-left: 40px;">
                        <span style="font-size: small;">(FIRST NAME + LAST INITIAL + LAST 4 DIGITS OF ctcLink ID)</span><br />
                        KellyB2436@bigfoot.spokane.edu
                    </p>
                    <div style="margin-left: 50px; margin-bottom: 5px;">
                        <strong>&#187;&nbsp;Forgot your password?</strong><br />
                        Contact the <a href="mailto:ITSupportCenter@ccs.spokane.edu">IT Support Center</a>: (509) 533-4357
                    </div>
                </div>
                <div class="BlueBGColor" style="border: 1px solid #0059A9; padding: 3px; border-radius: 10px; margin-top: 5px;">
                    <p style="text-align: center;"><strong>Employee Examples</strong></p>
                    <div style="padding-left: 40px;">
                        John.Smyth@ccs.spokane.edu<br />
                        John.Smyth@scc.spokane.edu<br />
                        John.Smyth@sfcc.spokane.edu<br /><br />
                    </div>
                </div>
                <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                <br />
                <div style="padding: 10px;">
                    <asp:Label ID="Label2" runat="server">Username:</asp:Label><br />
                    <asp:TextBox ID="txtUsername" runat="server" Width="300"></asp:TextBox><br />
                </div>
                <div style="padding: 10px;">
                    <asp:Label ID="Label3" runat="server">Password:</asp:Label><br />
                    <asp:TextBox ID="txtPassword" runat="server" Width="300" TextMode="Password">"</asp:TextBox><br />
                </div>
                <div style="padding: 10px;">
                    <asp:CheckBox ID="chkPersist" runat="server" Text="&nbsp;&nbsp;&nbsp;Remember Me" />
                </div>
                <div style="text-align: center; padding-top: 5px;">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="appsFormButton" OnClick="Login_Click"></asp:Button>
                </div>
                <div style="padding: 7px;">
                    <asp:Label ID="errorLabel" runat="server" ForeColor="#ff3300"></asp:Label>
                </div>
                <asp:Literal ID="litGPPMessage" runat="server"></asp:Literal>
                <p>Problems logging in? For assistance, please contact the <a href="mailto:ITSupportCenter@ccs.spokane.edu">IT Support Center</a>: (509) <strong>533 - H-E-L-P</strong> (533-4357)</p><br />
            </fieldset><br />
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </div>
