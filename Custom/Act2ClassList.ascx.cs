﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_Act2ClassList : CMSAbstractWebPart
{
    public string ClassID
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ClassID"), null);
        }
        set
        {
            SetValue("ClassID", value);
        }
    }

    public string DisplayStartDate
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("DisplayStartDate"), null);
        }
        set
        {
            SetValue("DisplayStartDate", value);
        }
    }

    public string DisplayTitle
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("DisplayTitle"), null);
        }
        set
        {
            SetValue("DisplayTitle", value);
        }

    }

    ContinuingEdClasses db;
    string sql = "";
    string sectionTitle = "";
    string classID = "";
    string colCD = "";
    string curTRM = "";
    string trmDesc = "";
    string strTempDate = "";
    string strTime = "";
    string oldClassNbr = "";
    decimal totalCost = 0.00m;
    int classCount = 0;
    DateTime dtStartDisplay;
    DataSet dsFees = new DataSet();

    string[,] arrCategory = new string[,] {
        {"SRHUM", "Consider Your Humanity"},
        {"SRGEN", "Discover Your Ancestry"},
        {"SRHOM", "Enhance Your Wellbeing"},
        {"SRCOM", "Express Yourself"},
        {"SRART", "Find Your Inner Artist"},
        {"SRPE", "Get Fit"},
        {"SRAG", "Grow a Green Thumb"},
        {"SRLAN","Learn a Language"},
        {"SRTEC", "Master Your Technology"},
        {"SRHIS", "Study History/Culture"},
        {"SRBUS", "Take Care of Business"},
        {"SRSCI", "Be Scientific"},
        {"SRCLF", "Classes in Colfax"},
        {"SRCOL", "Classes in Colville"},
        {"SRCHE", "Classes in Chewelah"},
        {"SRDER", "Classes in Deer Park"},
        {"SRFAI", "Classes in Fairfield"},
        {"SRFRE", "Classes in Freeman"},
        {"SRION", "Classes in Ione"},
        {"SRNEW", "Classes in Newport"},
        {"SRREP", "Classes in Republic"}
    };

    // SQL PARAMETER DEFINITIONS
    ArrayList p = new ArrayList();
    ArrayList row = new ArrayList();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            db = new ContinuingEdClasses();
            classID = ClassID;
            colCD = "WA171";
            litSubj.Text = "";
            p.Clear();
            row.Clear();
            sql = "usp_ClassSchedule_GetTerms";
            ArrayList arrTRM = db.SPQueryRO(sql, p, true);
            if (arrTRM.Count > 0)
            {
                for (int i = 0; i < arrCategory.Length; i++)
                {
                    if (arrCategory[i, 0].ToString() == classID)
                    {
                        sectionTitle = arrCategory[i, 1];
                        break;
                    }
                }
                if (DisplayTitle == "Yes")
                {
                    if (sectionTitle.Contains("Classes in"))
                        litSubj.Text += "<h2>" + sectionTitle + "</h2>";
                    else
                        litSubj.Text += "<h2>Classes for " + sectionTitle + " - Spokane</h2>";
                }
                /*
                if (colCD == "WA171")
                    litSubj.Text += " at SCC";
                else
                    litSubj.Text += " at SFCC";
                */
                // Only take 2 quarters, current quarter and next one
                // if already past arrTRM count then exit - means only 1 quarter returned
                for (int x = 4; x < 6; x++)
                {
                    if (x > arrTRM.Count)
                        break;
                    else
                    {
                        if (x == 4)
                        {
                            dtStartDisplay = new DateTime();
                            // dtStartDisplay = Convert.ToDateTime(((ArrayList)arrTRM[x])[2].ToString().Trim());
                            //Response.Write("[x][2]: " + ((ArrayList)arrTRM[x])[2].ToString().Trim() + "; Display start date: " + dtStartDisplay.ToShortDateString()); 
                            //  dtStartDisplay = dtStartDisplay.AddDays(-1);
                            dtStartDisplay = Convert.ToDateTime(DisplayStartDate);
                        }
                        //  Response.Write("today: " + DateTime.Today.ToString() + ": start: " + dtStartDisplay + "<br />");
                        if (x == 4 || (x == 5 && DateTime.Today >= dtStartDisplay))
                        {
                            curTRM = ((ArrayList)arrTRM[x])[1].ToString().Trim();
                            trmDesc = ((ArrayList)arrTRM[x])[0].ToString().Trim();
                            // Response.Write("x: " + x.ToString() + "<br />");
                            // get the class detail for class sent in
                            p.Clear();
                            row.Clear();
                            row.AddRange(new string[] { "@subj", "8", "input", classID });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            row = new ArrayList();
                            row.AddRange(new string[] { "@colCode", "5", "input", colCD });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            row = new ArrayList();
                            row.AddRange(new string[] { "@term", "4", "input", curTRM });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);

                            sql = "usp_ACT2_GetClassList";
                            // if (x == 1)
                            //     Response.Write("classID: " + classID + "; colcode: " + colCD + "; curTRM: " + curTRM + "<br />");
                            ArrayList allClasses = db.SPQueryRO(sql, p, true);
                            // if (x == 1)
                            //     Response.Write("count: " + allClasses.Count.ToString() + "<br />");
                            TableRow trSTRM = new TableRow();
                            TableCell tdSTRM = new TableCell();
                            tdSTRM.ColumnSpan = 4;
                            tdSTRM.HorizontalAlign = HorizontalAlign.Left;
                            tdSTRM.Text = "<strong>" + ((ArrayList)arrTRM[x])[0].ToString().Trim() + "</strong>";
                            if (allClasses.Count == 0)
                                tdSTRM.Text += " - no classes currently listed for this quarter";
                            trSTRM.Cells.Add(tdSTRM);
                            tblClass.Rows.Add(trSTRM);
                            if (allClasses.Count > 0)
                            {
                                // get the section title using the classID field


                                if (classCount == 0)
                                {
                                    classCount++;
                                }
                                oldClassNbr = "";
                                foreach (ArrayList oneClass in allClasses)
                                //   for (int i = 0; i < allClasses.Count; i++)
                                {
                                    if (oneClass[13].ToString().Trim() == "Y" && oldClassNbr != oneClass[1].ToString().Trim())
                                    {
                                        TableRow tr1 = new TableRow();
                                        TableCell tdNum = new TableCell();
                                        TableCell tdTitle = new TableCell();
                                        TableCell tdLocation = new TableCell();
                                        TableCell tdTime = new TableCell();
                                        TableCell tdCost = new TableCell();
                                        TableCell tdStartEnd = new TableCell();

                                        oldClassNbr = oneClass[1].ToString().Trim();
                                        tdNum.HorizontalAlign = HorizontalAlign.Left;
                                        tdNum.VerticalAlign = VerticalAlign.Top;
                                        tdNum.Text = oneClass[1].ToString().Trim();

                                        tdTitle.HorizontalAlign = HorizontalAlign.Left;
                                        tdTitle.VerticalAlign = VerticalAlign.Top;
                                        tdTitle.Text = oneClass[2].ToString().Trim();

                                        // Get the location separately
                                        tdLocation.HorizontalAlign = HorizontalAlign.Left;
                                        tdLocation.VerticalAlign = VerticalAlign.Top;
                                        p.Clear();
                                        row.Clear();
                                        row.AddRange(new string[] { "@crseID", "6", "input", oneClass[0].ToString().Trim() });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        row = new ArrayList();
                                        row.AddRange(new string[] { "@strm", "4", "input", curTRM });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        row = new ArrayList();
                                        row.AddRange(new string[] { "@section", "4", "input", oneClass[11].ToString().Trim() });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        sql = "usp_ContinuingEd_GetFacilityName";
                                        ArrayList facilityName = db.SPQueryRO(sql, p, true);

                                        if (facilityName.Count > 0)
                                            tdLocation.Text = ((ArrayList)facilityName[0])[6].ToString().Trim();
                                        else
                                            tdLocation.Text = "";

                                        tdTime.HorizontalAlign = HorizontalAlign.Right;
                                        tdTime.VerticalAlign = VerticalAlign.Top;
                                        String strStart = "";
                                        String strEnd = "";
                                        DateTime dtStart;
                                        DateTime dtEnd;
                                        if (DateTime.TryParse(oneClass[7].ToString(), out dtStart))
                                        {
                                            //Remove the leading zero
                                            strStart = dtStart.ToString("hh:mm tt");
                                            if (strStart.Substring(0, 1) == "0") { strStart = strStart.Substring(1, strStart.Length - 1);  }
                                            tdTime.Text = strStart + "-<br />";
                                        }
                                        else
                                        {
                                            tdTime.Text = " <br />";
                                        }
                                        if (DateTime.TryParse(oneClass[8].ToString(), out dtEnd))
                                        {
                                            //Remove the leading zero
                                            strEnd = dtEnd.ToString("hh:mm tt");
                                            if (strEnd.Substring(0, 1) == "0") { strEnd = strEnd.Substring(1, strEnd.Length - 1); }
                                            tdTime.Text += strEnd;
                                        }
                                        else
                                        {
                                            tdTime.Text += "";
                                        }
                                        // DateTime dtStart = DateTime.Parse(oneClass[7].ToString());
                                        //  DateTime dtEnd = DateTime.Parse(oneClass[8].ToString());

                                        //   tdTime.Text = dtStart.ToString("hh:mm tt") + "-<br />" + dtEnd.ToString("hh:mm tt");
                                        //  Response.Write("time: " + dtStart.ToString("hh:mm tt") + "<br />");

                                        // find all the costs associated with this class
                                        dsFees.Clear();
                                        p.Clear();
                                        row.Clear();
                                        row.AddRange(new string[] { "@crseID", "6", "input", oneClass[0].ToString().Trim() });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        row = new ArrayList();
                                        row.AddRange(new string[] { "@strm", "4", "input", curTRM });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        row = new ArrayList();
                                        row.AddRange(new string[] { "@section", "4", "input", oneClass[11].ToString().Trim() });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        row = new ArrayList();
                                        row.AddRange(new string[] { "@sessionCD", "3", "input", oneClass[12].ToString().Trim() });
                                        row.Add(SqlDbType.VarChar);
                                        p.Add(row);
                                        sql = "usp_ContinuingEd_GetClassFees";
                                        dsFees = db.SPQueryDS(sql, p, true);
                                        if (dsFees.Tables[0].Rows.Count > 0)
                                        {
                                            for (int k = 0; k < dsFees.Tables[0].Rows.Count; k++)
                                            {
                                                // Response.Write("fee: " + dsFees.Tables[0].Rows[k]["ITEM_TYPE"].ToString() + "; amount1: " + dsFees.Tables[0].Rows[k]["AMT_PER_UNIT"].ToString() + "; amount2: " + dsFees.Tables[0].Rows[k]["FLAT_AMT"].ToString() + "<br />");
                                                totalCost = totalCost + System.Convert.ToDecimal(dsFees.Tables[0].Rows[k]["AMT_PER_UNIT"].ToString()) + System.Convert.ToDecimal(dsFees.Tables[0].Rows[k]["FLAT_AMT"].ToString());
                                            }
                                        }
                                        else
                                        {
                                            totalCost = 0.00m;
                                        }

                                        tdCost.HorizontalAlign = HorizontalAlign.Right;
                                        tdCost.VerticalAlign = VerticalAlign.Top;
                                        tdCost.Text = "$" + String.Format("{0:0.00}", totalCost.ToString());
                                        totalCost = 0.00m;

                                        tdStartEnd.HorizontalAlign = HorizontalAlign.Right;
                                        tdStartEnd.VerticalAlign = VerticalAlign.Top;
                                        DateTime startDte = Convert.ToDateTime(oneClass[6].ToString());
                                        DateTime endDte = Convert.ToDateTime(oneClass[5].ToString());
                                        strTempDate = startDte.ToShortDateString();
                                        strTempDate = strTempDate.Substring(0, strTempDate.Length - 4) + strTempDate.Substring(strTempDate.Length - 2, 2);
                                        tdStartEnd.Text = strTempDate;
                                        //if start and end date are the same, suppress end date (one-day class)
                                        if (startDte != endDte)
                                        {
                                            tdStartEnd.Text += " -<br />";
                                        }
                                        strTempDate = endDte.ToShortDateString();
                                        strTempDate = strTempDate.Substring(0, strTempDate.Length - 4) + strTempDate.Substring(strTempDate.Length - 2, 2);
                                        if (startDte != endDte)
                                        {
                                            tdStartEnd.Text += strTempDate + "&nbsp;&nbsp;";
                                        }
                                        tdStartEnd.Style["padding-bottom"] = "7px";

                                        tr1.Cells.Add(tdNum);
                                        tr1.Cells.Add(tdTitle);
                                        tr1.Cells.Add(tdLocation);
                                        tr1.Cells.Add(tdCost);
                                        tr1.Cells.Add(tdTime);
                                        tr1.Cells.Add(tdStartEnd);
                                        //   tr1.Style["padding-bottom"] = "10px";

                                        tblClass.Rows.Add(tr1);
                                    } // end SCHEDULE_PRINT was 'Y'
                                } // end foreach class

                            } // if allclasses count > 0 
                        } // end x=0 or x=1 and dates are correct
                    }// end processing a term
                    classCount = 0;
                } // end for loop

            }// arrTrm.Count > 0

        } // end try
        catch (Exception ex)
        {
            litDebug.Text = ex.Message + "<br />";
            litDebug.Text += ex.StackTrace + "<br />";
            return;
        }
    }
}