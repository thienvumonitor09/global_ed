﻿using CMS.DocumentEngine;
using CMS.FormEngine.Web.UI;
using HM.Funnelback;
using HM.Funnelback.KenticoPush;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.EventLog;

public partial class CMSWebParts_Custom_HMFunnelbackPushAllButton : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnPushAll_Click(object sender, EventArgs e)
    {
        UIContext context = UIContext.Current;

        FunnelbackPushClassInfo editedObject = (FunnelbackPushClassInfo)context.EditedObject;

        string className = editedObject.PushClassName;
        string targetCollection = editedObject.PushCollectionName;


        //Get Columns settings from Module settings
        List<string> columns = FunnelbackModuleHelpers.GetPushClassNameColumns(className);

        if (columns.Count > 0)
        {
            columns.AddRange(ModuleConstants.SystemColumns);
            bool includeCategories = FunnelbackModuleHelpers.GetPushClassIncludeCategories(className);

            DocumentQuery documents = DocumentHelper.GetDocuments(className).Columns(columns).Published();
            if (targetCollection != "")
            {
                foreach (var doc in documents)
                {
                    try
                    {
                        string response = KenticoXML.SubmitKenticoXML(targetCollection, String.Join(",", columns), "", className, doc.NodeSiteName, "", includeCategories, doc.DocumentGUID, doc.AbsoluteURL);
                        EventLogProvider.LogInformation("PushAllResult", "INFORMATION", "Response = " + response);
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogInformation("PushAllError", "INFORMATION", ex.ToString());
                    }
                }
            }
        }


    }

}