﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassSearch.ascx.cs" Inherits="CMSWebParts_Custom_ClassSearch" %>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>

<input type="hidden" id="hidClasses" runat="server" />
<input type="hidden" id="hidInst" runat="server" />
<input type="hidden" id="hidFilters" runat="server" />
<input type="hidden" id="hidLocation" runat="server" />

<div id="container">
    <div id="error" class="alert alert-danger" style="padding:10px;" role="alert" runat="server"></div>
    <asp:Panel ID="panSearch" runat="server">
        <nav id="classListNav">
            <section id="search" class="light-grey-bg col-lg-12 row">
                <div class="cs_row col-lg-12">
                    <div class="col">
                        <strong>Quarter</strong><br />
                        <asp:DropDownList ID="cboQuarter" runat="server" Style="width:205px;"></asp:DropDownList>
                    </div>
                    <div class="col">
                        <strong>College</strong><br />
                        <asp:DropDownList ID="cboInstitution" runat="server" Style="width:285px">
                            <asp:ListItem Value="">ALL</asp:ListItem>
                            <asp:ListItem Value="WA171">Spokane Community College</asp:ListItem>
                            <asp:ListItem Value="WA172">Spokane Falls Community College</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col">
                        <strong>Location</strong><br />
                        <asp:DropDownList ID="cboLocation" runat="server" Style="min-width:285px;">
                            <asp:ListItem Value="">ALL</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col">
                        <strong>Keyword Search</strong><br />
                        <asp:TextBox ID="txtTextSearch" runat="server" Style="width:285px;" placeholder="e.g. ENGL 101"></asp:TextBox>
                    </div>
                </div>
                <div id="more" class="cs_row moreFilters" style="float: right" runat="server">
                    <div class="col" style="padding-right: 0px; margin-top: 5px;">
                        <a id="filterLink" onclick="toggleFilters();" runat="server"><strong>More Filters</strong></a>
                    </div>
                    <div class="col" style="padding-top: 2px; padding-right: 10px; padding-left: 2px; width: 10px">
                        <div id="arrow" class="right-arrow" onclick="toggleFilters();" runat="server"></div>
                    </div>
                </div>
                <div id="filters" class="filters" style="display:none;" runat="server">
                    <div class="cs_inputFields">
                        <div class="cs_row">
                            <div class="col">
                                <strong>Class Nbr</strong><br />
                                <asp:TextBox ID="txtClassNumber" runat="server" Style="width:205px;" placeholder="e.g. 1553"></asp:TextBox>
                            </div>
                            <div class="col">
                                <strong>Course Subject</strong><br />
                                <asp:DropDownList ID="cboSubject" runat="server" Style="width:285px;">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col">
                                <strong>Course Number</strong><br />
                                <asp:TextBox ID="txtCourseNumber" runat="server" Style="width:125px;" placeholder="e.g. 101"></asp:TextBox>
                            </div>
                            <div class="col">
                                <strong>Status</strong><br />
                                <asp:DropDownList ID="cboEnrlStatus" runat="server">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                    <asp:ListItem Value="O" Selected="True">Open</asp:ListItem>
                                    <asp:ListItem Value="C">Closed</asp:ListItem>
                                    <asp:ListItem Value="W">Wait List</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="cs_row">
                            <div class="col">
                                <strong>Class Times</strong><br />
                                <asp:DropDownList ID="cboClassTimes" runat="server" Style="width:205px">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                    <asp:ListItem Value="morning">Morning (before noon)</asp:ListItem>
                                    <asp:ListItem Value="afternoon">Afternoon (noon to 5 PM)</asp:ListItem>
                                    <asp:ListItem Value="evening">Evening (5 PM and later)</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col">
                                <strong>Instructor</strong><br />
                                <asp:TextBox ID="txtInstructor" runat="server" Style="width:285px;" placeholder="e.g. Smith"></asp:TextBox>
                            </div>
                            <div class="col">
                                <strong>Instruction Mode</strong><br />
                                <asp:DropDownList ID="cboInstructionMode" runat="server" Style="width:232px">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="cs_row cs_checkboxes">
                        <div class="cs_credit">
                            <div>
                                <asp:CheckBox ID="chkCredit" runat="server" />
                                <strong>Credit</strong>
                            </div>
                            <div>
                                <asp:CheckBox ID="chkNonCredit" runat="server" />
                                <strong>Noncredit</strong>
                            </div>
                            <div>
                                <asp:CheckBox ID="chkAct2" runat="server" />
                                <strong>Act 2</strong>
                            </div>
                        </div>
                        <div class="cs_class">
                            <div>
                                <asp:CheckBox ID="chkWritingIntensive" runat="server" />
                                <strong>Writing Intensive</strong>
                            </div>
                            <div>
                                <asp:CheckBox ID="chkDiversity" runat="server" />
                                <strong>Diversity</strong>
                            </div>
                            <div>
                                <asp:CheckBox ID="chkHonors" runat="server" />
                                <strong>Honors</strong>
                            </div>
                        </div>
                        <div class="cs_cost">
                            <div>
                                <asp:CheckBox ID="chkLCCM" runat="server" />
                                <strong>Low Cost Course Textbook</strong>
                            </div>
                            <div>
                                <asp:CheckBox ID="chkSOER" runat="server" />
                                <strong>Free Course Textbook</strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="buttonContainer" runat="server" class="col-lg-4" style="float:left;margin-top:20px;padding-right:0;">
                    <asp:Button ID="cmdSubmit" runat="server" Text="Search" CssClass="button med_gray" OnClick="cmdSubmit_Click" /><asp:Button ID="cmdReset" runat="server" Text="Reset" CssClass="button med_gray" Style="margin-right:0;" OnClick="cmdReset_Click" />
                </div>
                <div class="clear"></div>
            </section>
        </nav>
    </asp:Panel>
    <asp:Panel ID="panClasses" CssClass="panClasses" runat="server" Visible="false">
        <div style="float: right; padding-bottom: 5px; padding-top: 0px;">
            <asp:Label ID="lblLastUpdated" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>
        <asp:Panel ID="divClasses" CssClass="divClasses" runat="server"></asp:Panel>
    </asp:Panel>
    <asp:Panel ID="panMsg" runat="server" Visible="false">
        <p id="msg" runat="server" style="padding-left: 5px; padding-top: 10px; font-size: 12pt">No classes were found matching your search criteria.</p>
    </asp:Panel>
</div>
<script type="text/javascript">

    $(function () {

        //add dropdownlist change events
        $("select[id$='cboQuarter']").change(getLocations);
        $("select[id$='cboInstitution']").change(getLocations);
        $("select[id$='cboLocation']").change(getSubjects);

        //class data table
        var divClasses = $("div[id$='divClasses']");

        // divClasses header row variables used to calculate scroll value
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;

        //divClasses dimensions 
        var divClassesWidth = divClasses.width();
        var divClassesHeight = divClasses.height();
        var headerRow = $('.classHeaderRow');
        var footerRow = $("div[id$='footerRow']");

        $.fn.scrollBottom = function () {
            return $(document).height() - this.scrollTop() - this.height();
        };

        // set the width of last row of the divClasses container for position: fixed;                            
        $(footerRow).width(divClassesWidth);
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $(window).resize(function () {
            delay(function () {
                var divClassesWidth = divClasses.width();
                $(headerRow).width(divClassesWidth + 28);
                $(footerRow).width(divClassesWidth);
            }, 500);
        });

        //Detect scroll handler
        $(window).scroll(function (event) {
            didScroll = true;
        });

        //Set scroll check interval and reset condition
        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        //Check divClasses height set footer to relative on condition (short page)
        if (Math.abs(divClassesHeight) <= 250) {
            $(footerRow).css('position', 'relative');
        }

        //if the footer is in a relative position
        if ($(footerRow).css("position") == "relative") {
            //show the footer
            $(footerRow).show();
        }

        //Main function will set the action of the first and last rows of the divClasses container on scroll
        function hasScrolled() {
            var windowHeight = $(window).height();
            var st = $(this).scrollTop();
            var offset = $(".divClasses").offset();
            divClassesHeight = divClasses.height();

            /*
            console.log('st = ' + st);
            console.log('lastScrollTop = ' + lastScrollTop);
            console.log('offset = ' + offset.top);
            console.log('windowHeight = ' + windowHeight)
            console.log('divClassesHeight = ' + divClassesHeight);
            console.log('(divClassesHeight + offset) - windowHeight = ' + Math.abs((divClassesHeight + offset.top) - windowHeight));
            */

            // Make sure they scroll more than delta (keeps small scrolls out)
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            if (Math.abs(divClassesHeight) <= 500) {
                $(footerRow).css('position', 'relative');
            } else if (lastScrollTop <= (Math.abs((divClassesHeight + offset.top) - windowHeight))) {
                $(footerRow).css('position', 'fixed');
            } else {
                $(footerRow).css('position', 'relative');
            }

            if ($(footerRow).css("position") == "relative") {
                $(footerRow).show();
            }

            //divClasses header display settings based on scroll position                   
            if (lastScrollTop > offset.top) {
                var divClassesWidth = divClasses.width() + 28; //add 28 pixels for padding and border - BV
                // Snap the class header row to the top of the page on scroll down
                $('.classHeaderRow').removeClass('header-down').addClass('header-up').width(divClassesWidth);
                $(footerRow).show();
            } else {
                // Snap the class header row back to the original position on Scroll Up condition met (reaches bottom of search nav)
                if (st <= $(window).height()) {
                    $('.classHeaderRow').removeClass('header-up').addClass('header-down').width(divClassesWidth);  //moves the divClasses header to the top of the window and sets the width
                }
                if ($(footerRow).css("position") == "fixed") {
                    $(footerRow).hide();
                }
            }
            lastScrollTop = st;
        }
    });

    function getShortDateString(date) {
        date = new Date(date);
        date = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
        return date;
    }

    function getDateTimeString(date) {
        date = new Date(date);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = (hours >= 12) ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12;
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        date = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " " + hours + ":" + minutes + " " + ampm;
        return date;
    }

    function toggleFilters() {
        var filters = $("div[id$='filters']");
        var arrow = $("div[id$='arrow']");
        var moreFilters = $("div[id$='more']");
        var hidFilters = $("input[id$='hidFilters']");

        filters.toggle();

        if (filters.is(":visible")) {
            arrow.attr("class", "down-arrow");
            moreFilters.attr("style", "border-bottom: 1px solid #D8D8D8;text-align:right;");
            hidFilters.val("true");
        } else {
            arrow.attr("class", "right-arrow");
            moreFilters.attr("style", "border:none;float:right;");
            hidFilters.val("false");
        }
    }

    function toggleDetails(classRowID, detailsRowID, strm, institution, classNbr) {
        //class row clicked on
        var classRow = $("div[id$='" + classRowID + "']");
        //class details row
        var detailsRow = $("div[id$='" + detailsRowID + "']");

        //toggle (hide/show) the class details row
        detailsRow.toggle();

        //if class details row is visible
        if (detailsRow.is(":visible")) {

            //get the class details
            getClassDetails(strm, institution, classNbr, detailsRowID);

            //set class row attributes
            classRow.attr("title", "Close Class Details");
            if (classRow.attr("class").indexOf("white") >= 0) {
                classRow.attr("class", classRow.attr("class").replace("white", "light_gray"));
            } else {
                classRow.attr("class", classRow.attr("class") + " light_gray");
            }
            detailsRow.attr("style", "background:#F8F8F8");

        //if class details row is hidden
        } else {
            //set class row attributes
            classRow.attr("title", "View Class Details");
            classRow.attr("class", classRow.attr("class").replace("light_gray", "white"));
        }
    }

    function checkClassSelection() {
        if ($("input[id$='hidClasses']").val() == "") {
            alert("No classes have been selected. Please select a class first.");
            return false;
        } else {
            return true;
        }
    }

    function printClassSelection() {
        if (checkClassSelection()) {
            openWindow('/What-to-Study/Print-Classes/' + $("select[id$='cboQuarter']").val() + '/' + $("input[id$='hidClasses']").val(), 'Class List', '900', '800');
        }
    }

    function saveClassSmallDisplay(e, ClassNumber) {
        e.stopImmediatePropagation();
        var hidClasses = $("input[id$='hidClasses']");

        if (document.getElementById("chkSmall" + ClassNumber).checked) {
            hidClasses.val(hidClasses.val() + "," + ClassNumber);
        } else {
            hidClasses.val(hidClasses.val().replace(("," + ClassNumber), ""));
        }

        if (hidClasses.val() != null && hidClasses.val() != "") {
            $("#leftButtons").show();
            $("div[id$='footerRow']").show();
        } else {
            $("#leftButtons").hide();
        }
    }

    function saveClassLargeDisplay(e, ClassNumber) {
        e.stopImmediatePropagation();
        var hidClasses = $("input[id$='hidClasses']");

        if (document.getElementById("chkLarge" + ClassNumber).checked) {
            hidClasses.val(hidClasses.val() + "," + ClassNumber);
        } else {
            hidClasses.val(hidClasses.val().replace(("," + ClassNumber), ""));
        }

        if (hidClasses.val() != null && hidClasses.val() != "") {
            $("#leftButtons").show();
            $("div[id$='footerRow']").show();
        } else {
            $("#leftButtons").hide();
        }
    }

    function clearSelections() {
        var classList = $("input[id$='hidClasses']").val().substring(1).split(',');
        for (var i = 0; i < classList.length; i++) {
            try {
                document.getElementById("chkSmall" + classList[i]).checked = false;
                document.getElementById("chkLarge" + classList[i]).checked = false;
            } catch (e) {
                //do nothing
            }
        }
        $("input[id$='hidClasses']").val("");
        $("#leftButtons").hide();
    }

    function openWindow(url, title, h, w) {
        // Fixes dual-screen position  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow  
        if (window.focus) {
            newWindow.focus();
        }
    }

    function getLocations() {
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/webservices/ClassScheduleJSON.asmx/GetLocations",
            data: { STRM: $("select[id$='cboQuarter']").val(), INSTITUTION: $("select[id$='cboInstitution']").val() },
            dataType: "jsonp",
            success: function (data) {
                //remove all options from the location dropdownlist
                var cboLocation = $("select[id$='cboLocation']");
                $(cboLocation).empty();

                //populate the location dropdownlist
                $(cboLocation).append($("<option></option>").attr("value", "").text("ALL"));
                $.each(data.Table, function (index, location) {
                    var description = location.DESCR;
                    if (location.LOCATION == "FMAIN") {
                        //Change the location description of SFCC to Spokane Falls Community College instead of Main Campus - Spokane Falls
                        description = "Spokane Falls Community College";
                    } else if (location.LOCATION == "SMAIN") {
                        //Change the location description of SCC to Spokane Community College instead of Main Campus - Spokane CC
                        description = "Spokane Community College";
                    }

                    //if the location doesn't already exist (Pullman has duplicate entries in the ctcLink database)
                    if (cboLocation.find("option[value='" + location.LOCATION + "']").length > 0 == false) {
                        //add the location to the dropdownlist
                        $(cboLocation).append($("<option></option>").attr("value", location.LOCATION).text(description));
                    }
                });

                //sort location dropdownlist options by their text
                cboLocation.html(cboLocation.find('option').sort(function (x, y) {
                    // to change to descending order switch "<" for ">"
                    return $(x).text() > $(y).text() ? 1 : -1;
                }));

                //select All Locations by default
                cboLocation.val("");

                //get course subjects
                getSubjects();
            }
        });
    }

    function getSubjects() {
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/webservices/ClassScheduleJSON.asmx/GetSubjects",
            data: { STRM: $("select[id$='cboQuarter']").val(), INSTITUTION: $("select[id$='cboInstitution']").val(), LOCATION: $("select[id$='cboLocation']").val() },
            dataType: "jsonp",
            success: function (data) {
                var cboSubject = $("select[id$='cboSubject']");

                //remove all options from the subject dropdownlist
                $(cboSubject).empty();

                //populate the subject dropdownlist
                $(cboSubject).append($("<option></option>").attr("value", "").text("ALL"));
                var previousValue = "";
                $.each(data.Table, function (index, subject) {
                    var optionValue = subject.SUBJECT.replace("&", "");
                    var optionText = subject.DESCR;

                    //if the subject doesn't exist in the dropdownlist
                    if (optionValue != previousValue) {

                        //if the subject description contains an ampersand at the end of the string
                        if (optionText.substring(optionText.length - 1) == "&") {
                            //remove the ampersand
                            optionText = optionText.substring(0, optionText.length - 1);
                        }

                        //add the subject and description to the dropdownlist
                        $(cboSubject).append($("<option></option>").attr("value", optionValue).text(optionValue + " - " + optionText));
                    }

                    previousValue = optionValue;
                });

                getInstructionModes();
            }
        });
    }

    function getInstructionModes() {
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/webservices/ClassScheduleJSON.asmx/GetInstructionModes",
            data: { STRM: $("select[id$='cboQuarter']").val(), INSTITUTION: $("select[id$='cboInstitution']").val(), LOCATION: $("select[id$='cboLocation']").val() },
            dataType: "jsonp",
            success: function (data) {
                var cboInstructionMode = $("select[id$='cboInstructionMode']");

                //remove all options from the subject dropdownlist
                $(cboInstructionMode).empty();

                //populate the subject dropdownlist
                $(cboInstructionMode).append($("<option></option>").attr("value", "").text("ALL"));
                $.each(data.Table, function (index, mode) {
                    //add the subject and description to the dropdownlist
                    $(cboInstructionMode).append($("<option></option>").attr("value", mode.INSTRUCTION_MODE).text(mode.DESCR));
                });
            }
        });
    }

    function getClassDetails(strm, institution, classNbr, detailsRowID) {
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/webservices/ClassScheduleJSON.asmx/GetClassDetails",
            data: { STRM: strm, INSTITUTION: institution, CLASS_NBR: classNbr },
            dataType: "jsonp",
            success: function (data) {
                var classInstructor = "", courseAttributes = "", courseTopics = "", courseDisplay = "", currentClassNbr = "";
                var availableSeats = "", enrollmentRequirements = "", classNotes = "", instructionMode = "", institution = "", units = "";
                var enrollmentStatus = "", classStartDate = "", classEndDate = "", courseDescription = "", lastReplicationDate = "";
                var unitsMinimum = "", unitsMaximum = "", combinedSection = "", combinedSectionEnrollmentTotal = "", combinedSectionEnrollmentCapacity = "";
                var enrollmentTotal = "", enrollmentCapacity = "";

                $.each(data.Table, function (index, classDetail) {
                    currentClassNbr = classDetail.CLASS_NBR;
                    availableSeats = classDetail.ENRL_CAP - classDetail.ENRL_TOT;
                    enrollmentRequirements = classDetail.DESCR254A;
                    classNotes = classDetail.ClassNotes_DESCRLONG;
                    instructionMode = classDetail.InstructionMode_DESCR;
                    institution = classDetail.INSTITUTION;
                    enrollmentStatus = classDetail.ENRL_STAT;
                    classStartDate = classDetail.START_DT;
                    classEndDate = classDetail.END_DT;
                    courseDescription = classDetail.CourseCatalog_DESCRLONG;
                    lastReplicationDate = classDetail.LASTREPLICATIONDATE;
                    unitsMinimum = classDetail.UNITS_MINIMUM;
                    unitsMaximum = classDetail.UNITS_MAXIMUM;
                    combinedSection = classDetail.COMBINED_SECTION;
                    combinedSectionEnrollmentTotal = classDetail.SectionCombinedTable_ENRL_TOT;
                    combinedSectionEnrollmentCapacity = classDetail.SectionCombinedTable_ENRL_CAP;
                    enrollmentTotal = classDetail.ENRL_TOT;
                    enrollmentCapacity = classDetail.ENRL_CAP;

                    var currentCourseTopics = classDetail.CourseTopics_DESCR; //Will have repeating rows, need to concatinate to a complete string
                    var currentCourseAttributes = classDetail.CourseAttributesTable_DESCR; //Will have repeating rows, need to concatinate to a complete string 
                    var currentClassInstructor = classDetail.FIRST_NAME + " " + classDetail.LAST_NAME; //Will have repeating rows, need to concatinate to a complete string

                    //store the class instructor
                    if (currentClassInstructor == "null null" || currentClassInstructor.trim() == "") {
                        currentClassInstructor = "TBA";
                    }

                    //store repeating course topics
                    if (courseTopics.indexOf(currentCourseTopics) < 0) {
                        if (currentCourseTopics != null &&
                            (currentCourseTopics.indexOf("Diversity") >= 0 || currentCourseTopics.indexOf("Writing Intensive") >= 0 || currentCourseTopics.indexOf("Honors") >= 0)) {
                                courseTopics += ", " + currentCourseTopics;
                        }
                    }

                    //store repeating course attributes
                    if (courseAttributes.indexOf(currentCourseAttributes) < 0) {
                        if (currentCourseAttributes != null &&
                            (currentCourseAttributes.indexOf("Low Cost Course Material") >= 0 || currentCourseAttributes.indexOf("Learning Communities") >= 0 ||
                            currentCourseAttributes.indexOf("Open Educational Resources") >= 0 || currentCourseAttributes.indexOf("Pre-College Crse Level") >= 0 || 
                            currentCourseAttributes.indexOf("Free Course Materials") >= 0 || currentCourseAttributes.indexOf("Free Course Textbook") >= 0 ||
                            currentCourseAttributes.indexOf("No textbook to purchase") >= 0)) {
                                courseAttributes += ", " + currentCourseAttributes;
                        }
                    }

                    //store repeating class instructors
                    if (classInstructor.indexOf(currentClassInstructor) < 0) {
                        classInstructor += ", " + currentClassInstructor;
                    }
                });

                //store the credits/units
                if (unitsMinimum != unitsMaximum) {
                    units = unitsMinimum + "-" + unitsMaximum;
                } else {
                    units = unitsMinimum;
                }

                //store the class enrollment status
                if (enrollmentStatus == "O") { //open
                    if (combinedSectionEnrollmentCapacity != null && combinedSectionEnrollmentCapacity != "" && combinedSectionEnrollmentTotal != null && combinedSectionEnrollmentTotal != "") {
                        var csEnrollmentCapacity = combinedSectionEnrollmentCapacity, csEnrollmentTotal = combinedSectionEnrollmentTotal;
                        if (csEnrollmentTotal >= csEnrollmentCapacity) {
                            enrollmentStatus = "Wait List";
                        } else {
                            enrollmentStatus = "Open";
                        }
                        //recalculate available seats for combined section class
                        availableSeats = csEnrollmentCapacity - csEnrollmentTotal;
                    } else if (enrollmentCapacity != null && enrollmentCapacity != "" && enrollmentTotal != null && enrollmentTotal != "") {
                        if (enrollmentTotal >= enrollmentCapacity) {
                            enrollmentStatus = "Wait List";
                        } else {
                            enrollmentStatus = "Open";
                        }
                    } else {
                        enrollmentStatus = "Open";
                    }
                } else if (enrollmentStatus == "C") { //closed
                    enrollmentStatus = "Closed";
                } else if (enrollmentStatus == "W") { //wait list
                    enrollmentStatus = "Wait List";
                }

                //get the class details row
                detailsRow = $("div[id$='" + detailsRowID + "']");

                //clear existing html
                detailsRow.empty();

                //add class details column
                detailsRow.append("<div class='col-lg-12' style='padding-left:0px;padding-right:0px;'>");

                //ClassNbr
                detailsRow.append("<div class='hidden-lg hidden-md' style='padding-top:5px'><p class='inlineBlock'><strong>Class Nbr</strong></br>" + currentClassNbr + "</p></div>");

                //Credits
                detailsRow.append("<div class='hidden-lg hidden-md'><p class='inlineBlock'><strong>Credits</strong></br>" + units + "</p></div>");

                //Status
                detailsRow.append("<div class='hidden-lg'><p class='inlineBlock'><strong>Status</strong><br />" + enrollmentStatus + "</p></div>");

                //Institution
                if (institution == "WA171") {
                    detailsRow.append("<p><strong>Offered By</strong><br />Spokane Community College</p>");
                } else if (institution == "WA172") {
                    detailsRow.append("<p><strong>Offered By</strong><br />Spokane Falls Community College</p>");
                }

                //Available Seats
                detailsRow.append("<p><strong>Available Seats</strong><br />" + availableSeats + " as of " + getDateTimeString(lastReplicationDate) + "</p>");

                //Enrollment Requirements
                if (enrollmentRequirements != null && enrollmentRequirements != "") {
                    detailsRow.append("<p><strong>Enrollment Requirements</strong><br />" + enrollmentRequirements + "</p>");
                }

                //Instructor(s)
                detailsRow.append("<p><strong>Instructor</strong><br />" + classInstructor.substring(1) + "</p>");

                //Instruction Mode
                detailsRow.append("<p><strong>Instruction Mode</strong><br />" + instructionMode + "</p>");

                //Class Dates
                detailsRow.append("<p><strong>Class Dates</strong><br />" + getShortDateString(classStartDate) + " - " + getShortDateString(classEndDate) + "</p>");

                //Class Attributes
                courseDisplay = courseTopics + courseAttributes;
                if (courseDisplay != "") {
                    courseDisplay = courseDisplay.replace("Open Educational Resources", "Free Course Textbook").replace("Materials", "Textbook").replace("Material", "Textbook").replace("OER", "Open educational resources").substring(1);
                    detailsRow.append("<p><strong>Class Attributes</strong><br />" + courseDisplay + "</p>");
                }

                //Course Description
                detailsRow.append("<p><strong>Description</strong><br />" + courseDescription + "</p>");

                //Class Notes
                if (classNotes != null && classNotes != "") {
                    detailsRow.append("<p><strong>Notes</strong><br />" + classNotes + "</p>");
                }

                //close the class details column
                detailsRow.append("</div>");
            }
        });
    }

</script>
   