using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_LibraryCatalogSearchBar : CMSAbstractWebPart
{
    #region "Properties"

    public string strImageURL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("strImageURL"), null);
        }
        set
        {
            SetValue("strImageURL", value);
        }
    }


    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {

        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        litImage1.Text = strImageURL.Replace("http://","https://");
    }
}



