using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_iFrameControl : CMSAbstractWebPart
{
    #region "Properties"
    public string URL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("URL"), null);
        }
        set
        {
            SetValue("URL", value);
        }
    }

    public string frameHeight
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("frameHeight"), null);
        }
        set
        {
            SetValue("frameHeight", value);
        }
    }


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        litURL.Text = "src=\"" + URL + "\"";
        litframeHeight.Text = frameHeight;
    }

    #region "Methods"

        /// <summary>
        /// Content loaded event handler.
        /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



