using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_CCSCreditCourseListByDepartment : CMSAbstractWebPart
{
    #region "Properties"

    public string College
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("College"), null);
        }
        set
        {
            SetValue("College", value);
        }
    }

    public string CourseAbbreviation
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CourseAbbreviation"), null);
        }
        set
        {
            SetValue("CourseAbbreviation", value);
        }
    }

    public string ExclusionList
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ExclusionList"), null);
        }
        set
        {
            SetValue("ExclusionList", value);
        }
    }

    public string PageTitle
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("PageTitle"), null);
        }
        set
        {
            SetValue("PageTitle", value);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        //Displays one or both colleges' outcomes PDF links at the same time. DISABLED 5/8/2018 BY KC UNTIL ACCESSIBILITY ISSUES ETC RESOLVED
        String sfccBaseURL = "http://catalog.spokane.edu/CourseOutcomes/SFCC/"; //Added for SFCC course outcomes link
        String sccBaseURL = "http://catalog.spokane.edu/CourseOutcomes/SCC/"; //Added for SCC course outcomes link
        Boolean showSFCCLink = false; //Added for SFCC course outcomes link
        Boolean showSCCLink = false; //Added for SCC course outcomes link
        String sfccLinkText = "View SFCC Course Learning Outcomes"; //Added for SFCC course outcomes link text
        String sccLinkText = "View SCC Course Learning Outcomes"; //Added for SCC course outcomes link text
        */
        String subject = CourseAbbreviation;
        String catalogNumber = "";  //Empty string passed to web service for purposes of this widget
        String strm = "";
        StringBuilder objBuilder = new StringBuilder(20000);
        String strExculsionList = ExclusionList;
        DataSet dsCourseDescription = new DataSet();
        Int32 intUpBound = 0;
        Boolean blContinue = false;
        String courseOffering = "";
        String courseDescription = "";
        //Return ""  'default value returned
        iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();
        String course = "";
        //String offeredAt = "";
        String courseOutcome = "";
        String catalogNumbers = ""; //Used to store the numeric portion of a course number for later concatenation
        String courseFileName = "";
        String strPageTitle = "";

        Quarter_MetaData myQMD = new Quarter_MetaData();
        strm = myQMD.NextSTRM;
        if (PageTitle != "" && PageTitle != null) { strPageTitle = PageTitle; }
        litPageTitle.Text = "<h2>" + strPageTitle + "</h2>";
        try {

            dsCourseDescription = wsICatalog.GetCourseDescriptions(subject, College, strm, catalogNumber);
            Int32 courseCount = dsCourseDescription.Tables[0].Rows.Count;
            if (courseCount > 0) {
                for (Int32 i = 0; i < courseCount; i++) {
                    //showSCCLink = false;
                    //showSFCCLink = false;
                    Int32 courseID = Convert.ToInt32(dsCourseDescription.Tables[0].Rows[i]["CourseID"]);
                    catalogNumbers = dsCourseDescription.Tables[0].Rows[i]["CATALOG_NBR_A"].ToString();
                    courseOffering = dsCourseDescription.Tables[0].Rows[i]["SUBJECT"].ToString() + "-" + catalogNumbers;
                    course = dsCourseDescription.Tables[0].Rows[i]["SUBJECT"].ToString() + " " + dsCourseDescription.Tables[0].Rows[i]["CATALOG_NBR_A"].ToString();
                    courseFileName = dsCourseDescription.Tables[0].Rows[i]["SUBJECT"].ToString().Replace("&", "") + dsCourseDescription.Tables[0].Rows[i]["CATALOG_NBR_A"].ToString();
                    courseDescription = dsCourseDescription.Tables[0].Rows[i]["DESCRLONG"].ToString();
                    if (strExculsionList != "" && strExculsionList != null)
                    {
                        Char delimiter = ';';
                        String[] strExclusionArray = strExculsionList.Split(delimiter);
                        intUpBound = strExclusionArray.GetUpperBound(0);

                        if (intUpBound < 1) {
                            if (strExclusionArray[0].ToString() == courseOffering) {
                                blContinue = false;
                            } else {
                                blContinue = true;
                            }
                        } else if (intUpBound > 0) {
                            for (Int32 j = 0; j < intUpBound; j++) {
                                if (strExclusionArray[j].ToString() == courseOffering) {
                                    blContinue = false;
                                    break;
                                } else {
                                    blContinue = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        blContinue = true;
                    }
                    if (blContinue) {
                        objBuilder.Append("<div class=\"accordion accordion-toggle\"><p>" + course + "&nbsp;&mdash;&nbsp;" +  dsCourseDescription.Tables[0].Rows[i]["COURSE_TITLE_LONG"].ToString() + "&nbsp;&mdash;&nbsp;");
                        if (dsCourseDescription.Tables[0].Rows[i]["UNITS_MAXIMUM"].ToString() == dsCourseDescription.Tables[0].Rows[i]["UNITS_MINIMUM"].ToString())
                        {
                            objBuilder.Append(dsCourseDescription.Tables[0].Rows[i]["UNITS_MAXIMUM"].ToString() + System.Environment.NewLine + " Credits");
                        }
                        else {
                            objBuilder.Append(dsCourseDescription.Tables[0].Rows[i]["UNITS_MINIMUM"].ToString() + "-" + dsCourseDescription.Tables[0].Rows[i]["UNITS_MAXIMUM"].ToString() + " Credits");
                        }
                        /*
                        DataSet dsCourseCollege = wsICatalog.GetCourseCollege(courseID);
                        offeredAt = "";
                        for (Int32 j = 0; j < dsCourseCollege.Tables[0].Rows.Count; j++)
                        {
                            String collegeShortTitle = dsCourseCollege.Tables[0].Rows[j]["CollegeShortTitle"].ToString();
                            offeredAt += ", " + collegeShortTitle;
                            if (collegeShortTitle == "SFCC")
                            {
                                showSFCCLink = true;
                            }
                            if (collegeShortTitle == "SCC")
                            {
                                showSCCLink = true;
                            }
                        }
                        offeredAt = "(" + offeredAt.Substring(2) + ")";
                        //Create the course outcomes link for SFCC courses
                        course = dsCourseDescription.Tables[0].Rows[i]["SUBJECT"] + catalogNumbers;

                        //Build the link string to append to the course description based on the colleges offering the course
                        //courseOutcome = "<p>";
                        String courseOutcome = "";

                        //Remove offending ampersand to match file name
                        course = course.Replace("&", "").Replace(" ", "");

                        if (showSCCLink)
                        {
                            courseOutcome += "<br /><a href=\"" + sccBaseURL + course + ".pdf" + "\" target=\"_blank\">" + sccLinkText + "</a>";
                        }
                        if (showSFCCLink)
                        {
                            courseOutcome += "<br /><a href=\"" + sfccBaseURL + course + ".pdf" + "\" target=\"_blank\">" + sfccLinkText + "</a>";
                        }
                        //courseOutcome += "</p>";
                        */
                        //Complete accordion layout divs
                        objBuilder.Append("</p></div>" + System.Environment.NewLine);
                        objBuilder.Append("<div class=\"panel accordion-content\"><p>" + courseDescription + courseOutcome + "</p></div>" + System.Environment.NewLine);
                    }
                }
                litCourseList.Text = objBuilder.ToString();
            }
        } catch (Exception ex) {
            objBuilder.Append("<p> Error: " + ex.Message + "</p>");
        }
    }



    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



