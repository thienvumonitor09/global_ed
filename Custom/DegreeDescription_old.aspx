﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DegreeDescription_old.aspx.cs" Inherits="CMSWebParts_Custom_DegreeDescription" %>
<%@ Register TagPrefix="UC" TagName="DegreeDescription" Src="~/CMSWebParts/Custom/DegreeDescription.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>
<body>
<form id="form1" runat="server">
    <cms:CMSWebPartZone ZoneID="HeaderContent" runat="server" />
    <section class="two-column">
        <div class="container">
            <div class="row">
                <section class="col-xs-12">
                    <cms:CMSWebPartZone ZoneID="BreadCrumbs" runat="server" />
                    <div class="page-title interior">
                        <h1> <cms:CMSWebPartZone ZoneID="TitleZone" runat="Server" /> </h1>
                    </div>
                </section>
                <section class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 brown-border-right">
                            <cms:CMSWebPartZone ZoneID="MainContent" runat="server" />
                                <UC:DegreeDescription ID="DegreeUC" runat="server" />
                        </div>
                        <!--col-xs-12 col-md-7 brown-border-right-->
                        <div class="col-xs-12 col-md-4 brown-border-left">
                            <cms:CMSWebPartZone ZoneID="SidebarContent" runat="server" />
                        </div>
                        <!---->
                    </div>
                    <!--row-->
                </section>
            </div>
        </div>
    </section>
</form>
</body>
</html>
