﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;


public partial class CMSWebParts_Custom_WorkforceClassList : CMSAbstractWebPart
{
    #region "Properties"
    public string ClassID
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("ClassID"), null);
        }
        set
        {
            SetValue("ClassID", value);
        }
    }

    public string CollegeCode
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CollegeCode"), null);
        }
        set
        {
            SetValue("CollegeCode", value);
        }
    }

    public string FirstDisplayEndDate
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("FirstDisplayEndDate"), null);
        }
        set
        {
            SetValue("FirstDisplayEndDate", value);
        }
    }
    public string SecondDisplayStartDate
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("SecondDisplayStartDate"), null);
        }
        set
        {
            SetValue("SecondDisplayStartDate", value);
        }
    }

    public string DisplayTitle
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("DisplayTitle"), null);
        }
        set
        {
            SetValue("DisplayTitle", value);
        }

    }

    #endregion
    ContinuingEdClasses db;
    string sql = "";
    string sectionTitle = "";
    string classID = "";
    string colCD = "";
    string curTRM = "";
    string trmDesc = "";
    string[,] arrTitles = new string[,] { {"BUSPD", "Business & Professional Development"},
                                        {"CRTPG", "Certificate Programs"},
                                        {"COMTE", "Computers & Technology"},
                                        {"EDCE", "Teacher Education"},
                                        {"HEMED", "Health & Medical"},
                                        {"CEES", "Continuing Education for the Environmental Sciences"},
                                        {"ARTDE", "Art & Design"},
                                        {"DANCE", "Dance"},
                                        {"MUSC", "Music"},
                                        {"FITNS", "Fitness & Sports"},
                                        {"SI", "Special Interest"},
                                        {"PI", "Personal Interest"},
                                        {"CUSIN", "Cuisine & Libations"},
                                        {"YC", "Youth Camps"},
                                        {"YSC", ""} };

    //string[,] arrStartTimes = new string[,] { {"FALL 2017", "7/11/2017"},
    //                                    {"WINTER 2018", "11/6/2017"},
    //                                    {"SPRING 2018", "2/5/2018"} };
    string compareSTRM = "";
    int classCount = 0;
    int allClassesCount = 0;
    DateTime dtStartDisplay;
    DateTime dtEndDisplay;
    DateTime dtStart2ndDisplay;
    DateTime dtEnd1stDisplay;
    DateTime dtTest;
    DateTime startDate;
    DateTime endDate;
    // SQL PARAMETER DEFINITIONS
    ArrayList p = new ArrayList();
    ArrayList row = new ArrayList();
    protected void Page_Load(object sender, EventArgs e)
    {
        db = new ContinuingEdClasses();
        classID = ClassID;
        colCD = CollegeCode;

        p.Clear();
        row.Clear();
        sql = "usp_ClassSchedule_GetTerms";
        ArrayList arrTRM = db.SPQueryRO(sql, p, true);

        if (arrTRM.Count > 0)
        {
            for (int x = 4; x < 6; x++)
            {
                if (x > arrTRM.Count)
                    break;
                else
                {
                    curTRM = ((ArrayList)arrTRM[x])[1].ToString().Trim();
                    trmDesc = ((ArrayList)arrTRM[x])[0].ToString().Trim();
                    // Response.Write("currTerm: " + curTRM + "<br />");
                    dtStartDisplay = Convert.ToDateTime(((ArrayList)arrTRM[x])[2].ToString().Trim());
                    //   Response.Write("process date: " + dtStartDisplay.ToShortDateString() + "; end date: " + dtEndDisplay.ToShortDateString() + "<br />");
                    if (x == 4)
                    {
                        // Determine the date to end display of the first quarter classes.  Will end display 10 days before term end date
                        dtEnd1stDisplay = Convert.ToDateTime(FirstDisplayEndDate);
                        //   Response.Write("end 1st Display date: " + dtEnd1stDisplay.ToShortDateString() + ";  ");
                    }
                    if (x == 5)
                    {

                        //dtStart2ndDisplay = Convert.ToDateTime("11/27/2017");
                        dtStart2ndDisplay = Convert.ToDateTime(SecondDisplayStartDate);
                        // Response.Write("first end: " + dtEnd1stDisplay.ToShortDateString() + "&nbps;&nbsp;calc 2nd display start date: " + dtStart2ndDisplay.ToShortDateString() + "<br />");
                    }
                    if ((x == 4 && DateTime.Now < dtEnd1stDisplay) || (x == 5 && DateTime.Now >= dtStart2ndDisplay))
                    //if (dtStartDisplay < dtEndDisplay)
                    {
                        // get the class detail for class sent in                          
                        p.Clear();
                        row.Clear();
                        if (classID != "MUSC")
                        {
                            // Response.Write("subj: " + classID + "; colCD: " + colCD + "; term: " + curTRM + "<br />");
                            row.AddRange(new string[] { "@subj", "8", "input", classID });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            row = new ArrayList();
                            row.AddRange(new string[] { "@colCode", "5", "input", colCD });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            row = new ArrayList();
                            row.AddRange(new string[] { "@term", "4", "input", curTRM });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            //  sql = "usp_ContinuingEd_GetNonMusicClasses";
                            sql = "usp_ContinuingEd_GetNonMusicClassesAllFees";
                        }
                        else
                        {
                            row.AddRange(new string[] { "@colCode", "5", "input", colCD });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            row = new ArrayList();
                            row.AddRange(new string[] { "@term", "4", "input", curTRM });
                            row.Add(SqlDbType.VarChar);
                            p.Add(row);
                            sql = "usp_ContinuingEd_GetMusicClasses";
                        }

                        ArrayList allClasses = db.SPQueryRO(sql, p, true);
                        // get the section title using the classID field
                        allClassesCount += allClasses.Count;
                        sectionTitle = "";
                        try
                        { 
                            for (int i = 0; i < arrTitles.Length; i++)
                            {
                                if (arrTitles[i, 0].ToString() == classID)
                                {
                                    sectionTitle = arrTitles[i, 1];
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            litDebug.Text += "<!-- " + ex.Message + " -->";
                        }
                        if (DisplayTitle == "Yes") { 
                            litSubj.Text = "<h3>Classes for ";
                            litSubj.Text += sectionTitle;
                            if (colCD == "WA171")
                                litSubj.Text += " at SCC";
                            else
                                litSubj.Text += " at SFCC";
                            litSubj.Text += "</h3>";
                        }
                        TableRow trSTRM = new TableRow();
                        TableCell tdSTRM = new TableCell();
                        tdSTRM.ColumnSpan = 4;
                        tdSTRM.HorizontalAlign = HorizontalAlign.Left;
                        tdSTRM.Text = "<strong>" + ((ArrayList)arrTRM[x])[0].ToString().Trim() + "</strong>";
                        trSTRM.Cells.Add(tdSTRM);
                        tblClass.Rows.Add(trSTRM);
                        
                        if (allClasses.Count > 0)
                        {

                            if (classCount == 0)
                            {
                                classCount++;
                            }
                            string classNbr = "";
                            string classTitle = "";
                            string classType = "";
                            decimal totalFees = 0.00m;
                            decimal decValue;
                            string startEndDates = "";
                            // string crseAttr = "";
                            bool firstTime = true;
                            foreach (ArrayList oneClass in allClasses)
                            {
                                // Response.Write("print: " + oneClass[14].ToString() + "<br />");
                                TableRow tr1 = new TableRow();
                                if (oneClass[14].ToString().Trim() == "Y")
                                {
                                    if ((classNbr != oneClass[1].ToString().Trim()) && firstTime)
                                    {
                                        // first time through so classNbr not match record read
                                        // no printing just populate fields
                                        classNbr = oneClass[1].ToString().Trim();
                                        classTitle = oneClass[2].ToString().Trim();
                                        classType = oneClass[4].ToString().Trim();
                                        //   crseAttr = oneClass[15].ToString().Trim();
                                        if (Decimal.TryParse(oneClass[10].ToString().Trim(), out decValue))
                                            totalFees = decValue;
                                        //totalFees = Convert.ToDecimal(oneClass[10].ToString().Trim());
                                        if (DateTime.TryParse(oneClass[6].ToString(), out startDate))
                                        {
                                            startEndDates = startDate.ToShortDateString();
                                            //if dates are equal, suppress end date - one day class
                                            if (startDate.ToShortDateString() != endDate.ToShortDateString()) {
                                                startEndDates += " - ";
                                            }
                                        
                                        }
                                        else
                                        {
                                            startEndDates = " ";
                                        }
                                        if (DateTime.TryParse(oneClass[5].ToString(), out endDate))
                                        {
                                            if (startDate.ToShortDateString() != endDate.ToShortDateString())
                                            {
                                                startEndDates += endDate.ToShortDateString();
                                            }
                                        }
                                        //Remove trailing dashes when compare doesn't quite work
                                        if(startEndDates.Substring(startEndDates.Length - 3, 3) == " - ") { startEndDates = startEndDates.Substring(0, startEndDates.Length - 3);  }
                                        firstTime = false;
                                    }
                                    else if ((classNbr != oneClass[1].ToString().Trim()) && !firstTime)
                                    {
                                        // classNbr of next record does NOT match previous record
                                        // print previous record
                                        // set totalFees to 0.0 and re-populate print fields with new data
                                        TableCell tdNum = new TableCell();
                                        tdNum.HorizontalAlign = HorizontalAlign.Left;
                                        tdNum.VerticalAlign = VerticalAlign.Top;
                                        tdNum.Text = classNbr;

                                        TableCell tdTitle = new TableCell();
                                        tdTitle.HorizontalAlign = HorizontalAlign.Left;
                                        tdTitle.VerticalAlign = VerticalAlign.Top;
                                        tdTitle.Text = classTitle;

                                        TableCell tdType = new TableCell();
                                        tdType.HorizontalAlign = HorizontalAlign.Left;
                                        tdType.VerticalAlign = VerticalAlign.Top;
                                        tdType.Text = classType;

                                        TableCell tdCost = new TableCell();
                                        tdCost.HorizontalAlign = HorizontalAlign.Right;
                                        tdCost.VerticalAlign = VerticalAlign.Top;
                                        tdCost.Text = "$" + String.Format("{0:0.00}", totalFees);
                                        totalFees = 0.0m;


                                        TableCell tdStartEnd = new TableCell();
                                        tdStartEnd.HorizontalAlign = HorizontalAlign.Right;
                                        tdStartEnd.VerticalAlign = VerticalAlign.Top;
                                        tdStartEnd.Text = startEndDates;

                                        tr1.Cells.Add(tdNum);
                                        tr1.Cells.Add(tdTitle);
                                        tr1.Cells.Add(tdType);
                                        tr1.Cells.Add(tdCost);
                                        tr1.Cells.Add(tdStartEnd);

                                        tblClass.Rows.Add(tr1);

                                        classNbr = oneClass[1].ToString().Trim();
                                        classTitle = oneClass[2].ToString().Trim();
                                        classType = oneClass[4].ToString().Trim();
                                        if (Decimal.TryParse(oneClass[10].ToString().Trim(), out decValue))
                                            totalFees = decValue;

                                        //   totalFees = Convert.ToDecimal(oneClass[10].ToString().Trim());
                                        if (DateTime.TryParse(oneClass[6].ToString(), out startDate))
                                        {
                                            startEndDates = startDate.ToShortDateString();
                                            //if dates are equal, suppress end date - one day class
                                            if (startDate.ToShortDateString() != endDate.ToShortDateString())
                                            {
                                                startEndDates += " - ";
                                            }
                                        }
                                        else
                                        {
                                            startEndDates = " ";
                                        }
                                        if (DateTime.TryParse(oneClass[5].ToString(), out endDate))
                                        {
                                                if (startDate.ToShortDateString() != endDate.ToShortDateString())
                                                {
                                                    startEndDates += endDate.ToShortDateString();
                                                }
                                        }

                                    }
                                    else
                                    {

                                        if (Decimal.TryParse(oneClass[10].ToString().Trim(), out decValue))
                                            totalFees += decValue;

                                    }
                                    //Remove trailing dashes when compare doesn't quite work
                                    if (startEndDates.Substring(startEndDates.Length - 3, 3) == " - ") { startEndDates = startEndDates.Substring(0, startEndDates.Length - 3); }
                                }// end SCHEDULE_PRINT = 'Y'
                            } // end foreach class
                              // Need to print the information for the last class
                            TableCell tdNumLast = new TableCell();
                            tdNumLast.HorizontalAlign = HorizontalAlign.Left;
                            tdNumLast.VerticalAlign = VerticalAlign.Top;
                            tdNumLast.Text = classNbr;

                            TableCell tdTitleLast = new TableCell();
                            tdTitleLast.HorizontalAlign = HorizontalAlign.Left;
                            tdTitleLast.VerticalAlign = VerticalAlign.Top;
                            tdTitleLast.Text = classTitle;

                            TableCell tdTypeLast = new TableCell();
                            tdTypeLast.HorizontalAlign = HorizontalAlign.Left;
                            tdTypeLast.VerticalAlign = VerticalAlign.Top;
                            tdTypeLast.Text = classType;

                            TableCell tdCostLast = new TableCell();
                            tdCostLast.HorizontalAlign = HorizontalAlign.Right;
                            tdCostLast.VerticalAlign = VerticalAlign.Top;
                            tdCostLast.Text = "$" + String.Format("{0:0.00}", totalFees);
                            totalFees = 0.0m;


                            TableCell tdStartEndLast = new TableCell();
                            tdStartEndLast.HorizontalAlign = HorizontalAlign.Right;
                            tdStartEndLast.VerticalAlign = VerticalAlign.Top;
                            tdStartEndLast.Text = startEndDates;

                            TableRow trLast = new TableRow();
                            trLast.Cells.Add(tdNumLast);
                            trLast.Cells.Add(tdTitleLast);
                            trLast.Cells.Add(tdTypeLast);
                            trLast.Cells.Add(tdCostLast);
                            trLast.Cells.Add(tdStartEndLast);

                            tblClass.Rows.Add(trLast);
                        } // if allclasses count > 0
                        else
                        {
                            // Response.Write("no classes found<br />");
                        }
                    } // end check on start date

                } // end processing a STRM
                classCount = 0;
            } // end for each STRM
            if (allClassesCount == 0)
            {
                litNoClasses.Text = "<p style='padding-left:5px;'>No class list currently available.</p>";
            }
            else
            {
                allClassesCount = 0;
            }


        } // end returned STRM records
        db.CloseCon();
    }

    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {

        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}