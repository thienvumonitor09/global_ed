﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.Data;
using System.Globalization;

public partial class CMSWebParts_Custom_DegreeDescription : CMSAbstractWebPart
{
    //initialize web service
    // ClassScheduleXML.ClassScheduleXML wsClassSchedule = new ClassScheduleXML.ClassScheduleXML();
    //initialize iCatalog web service
    iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();
    String registrationTerm = "";
    string quarterName = "";
    string currentRegistrationTerm = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/DegreeDescription_files/DegreeDescription.css";
        Page.Header.Controls.Add(styleLink);

        //get the current term
        //String currTerm = wsClassSchedule.GetCurrentTerm();
        currentRegistrationTerm = wsICatalog.GetRegistrationTerm();
        if (!IsPostBack)
        {
            if (Request.QueryString["strm"] != null)
            {
                registrationTerm = Request.QueryString["strm"].ToString();
                if (registrationTerm.Length > 4)
                    registrationTerm = Request.QueryString["strm"].Substring(0, 4);
            }
            else
            {
                // registrationTerm = wsClassSchedule.GetRegistrationTerm();
                registrationTerm = currentRegistrationTerm;
            }
        }
        if (IsPostBack)
        {
            registrationTerm = quarterDD.SelectedValue;
        }
        //   int intCurrentRegistrationTerm = 0;     // for conversion to int for comparison and subtraction
        //   int intSTRMinLoop = 0;                  // for conversion to int forcomparison
        int itemCount = 0;                      // so index for quarterDD is OK as no longer as many in DD as records looping through
        //populate the terms/quarter drop down list with the current registration quarter and 1 year before that of choices
        quarterDD.Items.Clear();
        //   if (int.TryParse(currentRegistrationTerm, out intCurrentRegistrationTerm))
        //   {
        //DataSet dsTerms = wsClassSchedule.GetTerms();
        DataSet dsTerms = wsICatalog.GetProgramTermList();
        for (Int16 i = 0; i < dsTerms.Tables[0].Rows.Count; i++)
        {
            //  if (int.TryParse(dsTerms.Tables[0].Rows[i]["STRM"].ToString(), out intSTRMinLoop))
            //  {
            // if ((intSTRMinLoop >= (intCurrentRegistrationTerm - 10)) && (intSTRMinLoop <= intCurrentRegistrationTerm))
            //  {
            quarterName = dsTerms.Tables[0].Rows[i]["DESCR"].ToString();
            quarterName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(quarterName.ToLower());
            quarterDD.Items.Add(quarterName);
            quarterDD.Items[itemCount].Value = dsTerms.Tables[0].Rows[i]["STRM"].ToString();
            if (quarterDD.Items[itemCount].Value == registrationTerm)
            {
                quarterDD.Items[itemCount].Selected = true;
                litQuarter.Text = quarterName;
            }
            itemCount++;
            //  }// current record within boundry of between 1 year before current registraiton term or equal to current registration term

            //  }// table strm entry parsed to int successfully
            //  if (dsTerms.Tables[0].Rows[i]["STRM"].ToString() == currentRegistrationTerm)
            //  break;
        }// for loop
         //  }// current registration strm can be converted to int
        try
        {
            //  quarterDD.SelectedValue = Request.Form[quarterDD.UniqueID];
        }
        catch
        {
            //do nothing
        }


    }
}