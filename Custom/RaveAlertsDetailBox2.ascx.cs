﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalEngine.Web.UI;
using CMS.Helpers;
using System.Xml.Linq;
using System.Xml;
public partial class CMSWebParts_Custom_RaveAlertsDetailBox2 : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string htmlAlert = "<div id=\"raveAlertBoxBlue\">[Alert details]</div>";
        bool pullFreshAlertInfo = true;
        int secondsToWait = 60;
        int hoursToLive = 24; //24 normally
        DateTime currentTime = System.DateTime.Now;

        // Check and/or set Application variable to limit hits on remote CAP alert channel
        // Check status string to determine (based on secondsToWait) whether to refresh
        //Label1.Text = "Hello Rave";
        try
        {
            DateTime dtLastPulled = Convert.ToDateTime(Application["RaveChannel1DataLastPulled"].ToString());
            if (dtLastPulled.AddSeconds(secondsToWait) > currentTime)
            {
                pullFreshAlertInfo = false;
            }
        }
        catch
        {
            //If any error encountered, leave flag set to pull and store alert information
            pullFreshAlertInfo = true;
        }
        string sent = "";
        string eventname = "";
        string description = "";
        string web = "";
        string data = "";

        pullFreshAlertInfo = true;
        if (pullFreshAlertInfo)
        {
            string url = "http://www.getrave.com/cap/ccs/channel1";
            XDocument xdoc = XDocument.Load(url);
            //XNamespace ns = xdoc.Root.GetDefaultNamespace();
            try
            {

                sent = "[]";  //initial population in case of error (catch)
                sent = xdoc.Descendants().Where(x => x.Name.LocalName == "sent").FirstOrDefault().Value;
                //Label1.Text += sent;
                //Label1.Text += "null";
                eventname = "Error";  //initial population in case of error (catch)

                XElement eventE = xdoc.Descendants().Where(x => x.Name.LocalName == "event").FirstOrDefault();
                if (eventE != null)
                {
                    eventname = eventE.Value;
                }


                //Label1.Text += eventname;
                description = "Unable to access or populate alert information"; //initial population in case of error (catch)
                //description = data.Substring(data.IndexOf("<description>") + 13, data.IndexOf("</description>") - data.IndexOf("<description>") - 13);
                XElement descriptionE = xdoc.Descendants().Where(x => x.Name.LocalName == "description").FirstOrDefault();
                if (eventE != null)
                {
                    description = descriptionE.Value;
                }
                //Label1.Text += description;
                web = xdoc.Descendants().Where(x => x.Name.LocalName == "web").FirstOrDefault().Value;
            }
            catch
            {
                if (sent == "[]")
                {
                    sent = System.DateTime.Now.AddHours(-96).ToString(); //if error encountered with sent value, assume old alert
                }
                if (description == "Unable to access or populate alert information")
                {
                    //empty or missing description caused error in try block
                    sent = System.DateTime.Now.AddHours(-96).ToString(); //if error encountered with description value, treat as old alert
                }
                web = "http://www.ccs.spokane.edu/alert.aspx"; //default
            }

            //Update Application variables
            Application.Lock();
            Application["RaveChannel1DataLastPulled"] = System.DateTime.Now;
            Application["RaveChannel1_sent"] = sent;
            Application["RaveChannel1_eventname"] = eventname.Trim();
            Application["RaveChannel1_description"] = description.Trim();
            Application["RaveChannel1_web"] = web.Trim();
            Application.UnLock();
        }
        bool assumeAllClear = true;
        //Finalize alert data for display
        //Is it too old? [24-hour expiry based on sent date; may need more complexity if and when start and stop datetimes are included in the alert]
        try
        {
            DateTime dtSent = Convert.ToDateTime(Application["RaveChannel1_sent"].ToString());
            if (dtSent.AddHours(hoursToLive) > currentTime)
            {
                assumeAllClear = false;
            }
        }
        catch
        {
            //If any error encountered, leave assumeAllClear set to true
            assumeAllClear = true;
        }

        if (eventname.ToLower().Trim() == "remove message")
        {
            //If "Remove Message" (any case) is the event name, leave assumeAllClear set to true
            assumeAllClear = true;
        }

        htmlAlert = htmlAlert.Replace("[Alert details]", "<h2></h2><div></div>");
        assumeAllClear = false;
        if (assumeAllClear)
        {
            // Build All Clear message (banner display should not occur in this case)
            htmlAlert = htmlAlert.Replace("<h2>", "<h2>OPERATIONS NORMAL");
            htmlAlert = htmlAlert.Replace("<div>", "<div>No alert is in effect at this time.");
        }
        else
        {
            // Build alert message (banner display should also occur in this case - see subpage.master.cs
            string alignSpec = "";
            if (Application["RaveChannel1_description"].ToString().Length > 80)
            {
                alignSpec = " style=\"text-align:left;\"";
            }
            htmlAlert = htmlAlert.Replace("<h2>", "<h2>" + Application["RaveChannel1_eventname"]);
            htmlAlert = htmlAlert.Replace("<div>", "<div" + alignSpec + ">" + Application["RaveChannel1_description"]).Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");
            //  htmlAlert = htmlAlert.Replace("<h2>", "<h2>" + "On No!! Something bad has happened!");
            //  htmlAlert = htmlAlert.Replace("<div>", "<div" + alignSpec + ">" + "Oh No Details. Oh No Details. Oh No Details. Oh No Details. Oh No Details. Oh No Details. ");
            htmlAlert = htmlAlert.Replace("raveAlertBoxBlue", "raveAlertBoxYellow");
        }

        // Display alert information on page
        raveContent.Text = htmlAlert;

        /*
        foreach (XElement d in xdoc.Root.Nodes())
        {

            Label1.Text += d.Element(ns + "sender").Value;
        }
        */
        //XElement ed = xdoc.Descendants().Where(x => x.Name.LocalName == "event").FirstOrDefault();
        //Label1.Text += ed.Value;
        string url2 = "http://www.getrave.com/cap/ccs/channel1";
        XDocument xdoc1 = XDocument.Load(url2);
        XNamespace ns2 = xdoc1.Root.GetDefaultNamespace();
        //Label1.Text += ns2;
        Label1.Text = xdoc1.Root.Element(ns2 + "sender").Value;
        Label1.Text += xdoc1.Descendants(ns2 + "sender").FirstOrDefault().Value;
        //d.Element("Title").Value
        foreach (XElement d in xdoc1.Root.Nodes())
        {
            if (d.Name.Equals(ns2 + "sender"))
            {
                //Label1.Text += d.Value;
            }
            //Label1.Text += " " + d.Value;

        }

       



    }
}