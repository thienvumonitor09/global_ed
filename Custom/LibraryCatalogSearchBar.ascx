<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Custom_LibraryCatalogSearchBar"  CodeFile="~/CMSWebParts/Custom/LibraryCatalogSearchBar.ascx.cs" %>
<script type="text/javascript">
    function searchPrimo() {

        if (typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }


        var strSearchOption = "";
        var pqf = document.getElementById("primoQuery").value;
        if (pqf == "" || pqf == null) {
            pqf = document.getElementById("primoQueryTemp").value;
        }
        if (document.getElementById('r1').checked) {
            strSearchOption = document.getElementById('r1').value;
        }
        else {
            strSearchOption = document.getElementById('r2').value;
        }

        var httpString = "http://01win-primo.hosted.exlibrisgroup.com/primo_library/libweb/action/dlSearch.do?queryTemp=+" + pqf + "&institution=01WIN_CCSPOK&vid=CCS&group=GUEST&onCampus=true&displayMode=full&search_scope=" + strSearchOption + "&query=" + "any%2Ccontains%2C+" + pqf

        window.open(httpString);
    }

    function searchKeyPress(e) {
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13) {
            document.getElementById('go').click();
        }
    }

</script>
<style type="text/css">
    input {  
        font-family: Merriweather;
        font-size: 1.5rem;
        line-height: 1.7;
    }
    #librarySearch p {
        margin: 0px;
        padding: 0px;
        line-height:1.0;
    }
    #librarySearch p a {
        text-decoration: none;
    }
    #librarySearch p a:hover {
        text-decoration: underline;
    }
</style>

<section class="hero-image study-page">
    <img src="<asp:literal ID="litImage1" runat="server"></asp:literal>" alt="Student reading a book in the library isle" />
    <div class="content-over-image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 page-title">
                    <h1>Find&nbsp;What&nbsp;You&nbsp;Need</h1> 
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                    <div id="librarySearch" class="study-search" style="height:152px;">
                        <div class="search desk-show">
                            <p>Search&nbsp;books,&nbsp;articles,&nbsp;journals,&nbsp;databases,&nbsp;research&nbsp;guides&nbsp;and&nbsp;more!</p>
                            <input type="text" id="primoQuery" value="" placeholder="Enter search words" class="form-control" style="width:60%;"  />&nbsp;&nbsp;<input id="go" title="Search" onclick="searchPrimo()" type="button" class="button" value="Search" alt="Search">       
                            <p>Books &amp; Videos <input type="radio" name="search_scope" id="r1" value="alma_scope" checked />&nbsp; 
                            Everything <input type="radio" name="search_scope" id="r2" value="everything_scope" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://01win-primo.hosted.exlibrisgroup.com/pds?func=load-login&calling_system=primo&institute=01WIN_CCSPOK&lang=eng&url=https://01win-primo.hosted.exlibrisgroup.com:443/primo_library/libweb/pdsLogin?targetURL=https%3A%2F%2F01win-primo.hosted.exlibrisgroup.com%2Fprimo-explore%2Fsearch%3Fvid%3DCCS%26search_scope%3Dalma%25255Fscope%26query%3Dany%25252Ccontains%25252C%26sortby%3Drank%26lang%3Den_US%26from-new-ui%3D1%26authenticationProfile%3DPDS" target="_blank">Log into your account &gt;</a></p>
                        </div>
                        <div class="search mobile-only">
                            <input type="text" id="primoQueryTemp" value="" placeholder="Search books &amp; more" class="form-control" style="margin:2px;" />
                            <input id="go2" title="Search" onclick="searchPrimo()" type="button" value="Search" alt="Search" style="margin:2px;">   
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</section>