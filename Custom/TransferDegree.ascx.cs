using System;
using System.Data;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_TransferDegree : CMSAbstractWebPart
{
    #region "Properties"



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebParts/Custom/DegreesAndCertificates_files/DegreesAndCertificates.css";
        Page.Header.Controls.Add(styleLink);

        //initialize iCatalog web service
        iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();

        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;

        String degreeID = "";
        if (Request.QueryString["id"] != null)
        {
            degreeID = Request.QueryString["id"];
            if (degreeID.Length > 4)
            {
                degreeID = Request.QueryString["id"].Substring(0, 4);
            }
        }

        String strm = "";
        if (Request.QueryString["strm"] != null)
        {
            strm = Request.QueryString["strm"];
            if (degreeID.Length > 4)
            {
                degreeID = Request.QueryString["strm"].Substring(0, 4);
            }
        }

        try
        {
            //DataSet dsOptions = wsICatalog.GetDegrees(cboQuarter.SelectedValue, cboLocation.SelectedValue, cboDegree.SelectedValue, "", cboSubject.SelectedValue, cboCatalogNumber.SelectedValue, txtTextSearch.Text.Trim());
            DataSet dsOptions = wsICatalog.GetDegrees(strm, "", degreeID, "Transfer", "", "", "");
            Int32 optionCount = 0;
            if (dsOptions != null)
            {
                if (dsOptions.Tables.Count > 0)
                {
                    optionCount = dsOptions.Tables[0].Rows.Count;
                }
            }
            if (optionCount > 0)
            {

                HtmlGenericControl degreeSection = new HtmlGenericControl("section");
                String oldDegreeTitle = "";

                for (Int32 i = 0; i < optionCount; i++)
                {
                    String collegeShortTitle = dsOptions.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                    String programTitle = dsOptions.Tables[0].Rows[i]["ProgramTitle"].ToString();
                    String optionTitle = dsOptions.Tables[0].Rows[i]["OptionTitle"].ToString();
                    String strDegreeID = dsOptions.Tables[0].Rows[i]["DegreeID"].ToString();
                    String degreeShortTitle = dsOptions.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                    String programID = dsOptions.Tables[0].Rows[i]["ProgramID"].ToString();
                    String strProgramVersionID = dsOptions.Tables[0].Rows[i]["ProgramVersionID"].ToString();
                    String strOptionID = dsOptions.Tables[0].Rows[i]["OptionID"].ToString();
                    String degreeLongTitle = dsOptions.Tables[0].Rows[i]["DegreeLongTitle"].ToString();
                    String degreeType = dsOptions.Tables[0].Rows[i]["DegreeType"].ToString();
                    String documentTitle = dsOptions.Tables[0].Rows[i]["DocumentTitle"].ToString();
                    String degreeDescription = dsOptions.Tables[0].Rows[i]["DegreeDescription"].ToString();


                    if (oldDegreeTitle != degreeLongTitle)
                    {

                        if (oldDegreeTitle != "")
                        { //if a different degree type existed in the previous loop
                            degreeSection.InnerHtml += "</ul>"; //close the unordered list of the previous category
                            panDegreeList.Controls.Add(degreeSection);
                        }

                        //add the new degree section
                        degreeSection = new HtmlGenericControl("section");
                        degreeSection.InnerHtml += "<h2>" + degreeLongTitle + "</h2>";
                        if (degreeDescription != null && degreeDescription != "")
                        {
                            degreeSection.InnerHtml += "<p>" + degreeDescription + "</p>";
                        }
                        degreeSection.InnerHtml += "<ul>";
                    }

                    if (programTitle == null || programTitle == "")
                    { 
                        //if a general transfer degree (without a program outline)
                        //display general transfer degree
                        try
                        {
                            String fileName = wsICatalog.GetDegreeRequirementWorksheet(documentTitle, "");

                            if (fileName != "" && fileName != null)
                            {
                                //add a new degree list item
                                degreeSection.InnerHtml += "<li><a href=\"http://catalog.spokane.edu/degworksheet/" + fileName + "\" target=\"_blank\">Requirements / Worksheet - SCC, SFCC</a></li>";
                            }
                        }
                        catch
                        {
                            //display nothing
                        }

                    }
                    else if (programID != "0")
                    { 
                        //a specific transfer degree (with a program outline)
                        degreeSection.InnerHtml += "<li><a href=\"Degree-Description?id=" + programID + "&strm=" + strm + "\">" + programTitle + " - " + collegeShortTitle + "</a></li>";
                    }

                    oldDegreeTitle = degreeLongTitle;

                    if (i == (optionCount - 1))
                    {
                        degreeSection.InnerHtml += "</ul>"; //close the unordered list of the previous degree
                        panDegreeList.Controls.Add(degreeSection);
                    }
                }

            }
            else {
                panDegreeList.Controls.Add(new LiteralControl("<p>Your search for degree ID " + degreeID + " returned no results.</p>"));
            }

        }
        catch (Exception ex)
        {
            panDegreeList.Controls.Add(new LiteralControl("<p>Error opening dataset for Degree ID " + degreeID + ": " + ex.Message + " <br />Stack Trace: " + ex.StackTrace + "</p>"));
        }
    }


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



