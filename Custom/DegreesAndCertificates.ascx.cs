﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.Data;


public partial class CMSWebParts_Custom_DegreesAndCertificates : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {

        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebParts/Custom/DegreesAndCertificates_files/DegreesAndCertificates.css";
        Page.Header.Controls.Add(styleLink);

        //initialize iCatalog web service
        iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();

        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;

        String location = "", institution = "";
        
        if (Request.QueryString["loc"] != null)
        {
            location = Request.QueryString["loc"];
            if (location.Length > 4)
            {
                location = Request.QueryString["loc"].Substring(0, 4);
            }
        }

        if (Request.QueryString["inst"] != null) {
            institution = Request.QueryString["inst"].ToLower();
            if(institution == "scc") {
                location = "3";
            }else if(institution == "sfcc") {
                location = "4";
            }
        }

        panAlpha.Visible = false;
        panCategory.Visible = false;
        panDegree.Visible = false;

        if (!IsPostBack)
        {

            cboLocation.Items.Add("View All");
            cboLocation.Items[0].Value = "";

            DataSet dsLocations = wsICatalog.GetLocations();
            for (Int32 i = 0; i < dsLocations.Tables[0].Rows.Count; i++)
            {
                cboLocation.Items.Add(dsLocations.Tables[0].Rows[i]["LocationTitle"].ToString());
                cboLocation.Items[i + 1].Value = dsLocations.Tables[0].Rows[i]["LocationID"].ToString();
            }

            //add program term search
            DataSet dsQuarter = wsICatalog.GetProgramTermList();
            for (Int32 i = 0; i < dsQuarter.Tables[0].Rows.Count; i++)
            {
                String strm = dsQuarter.Tables[0].Rows[i]["STRM"].ToString();
                String termDescription = dsQuarter.Tables[0].Rows[i]["DESCR"].ToString();

                int termDescriptionLength = termDescription.Length;
                String temp1 = termDescription.Substring(0, (termDescriptionLength - 4)).Trim();
                temp1 = temp1.Substring(0, 1) + temp1.Substring(1, temp1.Length - 1).ToLower();
                String temp2 = termDescription.Substring((termDescriptionLength - 4), 4);
                termDescription = temp1 + " " + temp2;

                cboQuarter.Items.Add(termDescription);
                cboQuarter.Items[i].Value = strm;
            }

            //preset location and category view if paramter passed in.
            if (location != "")
            {
                cboLocation.SelectedValue = location;
                //cboView.SelectedValue = "category";
            }

            //preset quarter based on current registration quarter
            cboQuarter.SelectedValue = wsICatalog.GetRegistrationTerm();

            //add program category list
            cboCategory.Items.Add("All Categories");
            cboCategory.Items[0].Value = "";
            DataSet dsCategory = wsICatalog.GetProgramCategories();
            for (Int32 i = 0; i < dsCategory.Tables[0].Rows.Count; i++)
            {
                cboCategory.Items.Add(dsCategory.Tables[0].Rows[i]["CategoryTitle"].ToString());
                cboCategory.Items[i + 1].Value = dsCategory.Tables[0].Rows[i]["CategoryID"].ToString();
            }
        }

        if (hidFilters.Value == "true")
        {
            filters.Attributes["style"] = "";
            arrow.Attributes["class"] = "down-arrow";
            more.Attributes["style"] = "border-bottom: 1px solid #D8D8D8;text-align:right;";
        }
        else if (hidFilters.Value == "false")
        {
            filters.Attributes["style"] = "display:none;";
            arrow.Attributes["class"] = "right-arrow";
            more.Attributes["style"] = "border:none;float:right;";
        }

        //clear degree/certificate list
        cboDegree.Items.Clear();
        cboDegree2.Items.Clear();

        //add degree/certificate list
        cboDegree.Items.Add("View All");
        cboDegree.Items[0].Value = "";
        cboDegree2.Items.Add("View All");
        cboDegree2.Items[0].Value = "";
        DataSet dsDegree = wsICatalog.GetDegreeTitles(cboQuarter.SelectedValue);
        for (Int32 i = 0; i < dsDegree.Tables[0].Rows.Count; i++)
        {
            cboDegree.Items.Add(dsDegree.Tables[0].Rows[i]["DegreeLongTitle"].ToString());
            cboDegree.Items[i + 1].Value = dsDegree.Tables[0].Rows[i]["DegreeID"].ToString();
            cboDegree2.Items.Add(dsDegree.Tables[0].Rows[i]["DegreeLongTitle"].ToString());
            cboDegree2.Items[i + 1].Value = dsDegree.Tables[0].Rows[i]["DegreeID"].ToString();
        }

        try
        {
            cboDegree.SelectedValue = Request.Form[cboDegree.UniqueID];
            cboDegree2.SelectedValue = Request.Form[cboDegree2.UniqueID];
        }
        catch
        {
            //do nothing
        }

        //clear course subject selection list
        cboSubject.Items.Clear();

        //add course subject list
        DataSet dsSubject = wsICatalog.GetCourseSubjectAreas("", cboQuarter.SelectedValue);
        cboSubject.Items.Add("View All");
        cboSubject.Items[0].Value = "";
        for (Int32 i = 0; i < dsSubject.Tables[0].Rows.Count; i++)
        {
            String subject = dsSubject.Tables[0].Rows[i]["SUBJECT"].ToString();
            String subjectDescription = textInfo.ToTitleCase(dsSubject.Tables[0].Rows[i]["SubjectArea_DESCR"].ToString().ToLower()).Replace(" And ", " and ").Replace("Cad", "CAD").Replace("Cnc", "CNC");
            cboSubject.Items.Add(subject + " - " + subjectDescription);
            cboSubject.Items[i + 1].Value = subject;
        }

        try
        {
            cboSubject.SelectedValue = Request.Form[cboSubject.UniqueID];
        }
        catch
        {
            //do nothing
        }

        //clear course number list
        cboCatalogNumber.Items.Clear();

        //add course number list
        DataSet dsCatalogNumber = wsICatalog.GetCourseCatalogNumbers(cboSubject.SelectedValue, cboQuarter.SelectedValue, "");
        cboCatalogNumber.Items.Add("View All");
        cboCatalogNumber.Items[0].Value = "";
        for (Int32 intDSRow = 0; intDSRow < dsCatalogNumber.Tables[0].Rows.Count; intDSRow++)
        {
            cboCatalogNumber.Items.Add(dsCatalogNumber.Tables[0].Rows[intDSRow]["CATALOG_NBR"].ToString());
        }

        try
        {
            cboCatalogNumber.SelectedValue = Request.Form[cboCatalogNumber.UniqueID];
        }
        catch
        {
            //do nothing
        }

        DisplayDegrees();

    }

    public void DisplayDegrees() {

        //initialize iCatalog web service
        iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();

        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;

        if (cboView.SelectedValue == "alpha") {

            panAlpha.Visible = true;

            DataSet dsOptions = wsICatalog.GetDegreesByAreaOfStudy(cboQuarter.SelectedValue, cboLocation.SelectedValue, cboDegree.SelectedValue, hidLtr.Value, "", cboSubject.SelectedValue, cboCatalogNumber.SelectedValue, txtTextSearch.Text.Trim());

            try {
                Int32 optionCount = dsOptions.Tables[0].Rows.Count;

                if (optionCount > 0) {
                    HtmlGenericControl areaOfStudySection = new HtmlGenericControl("section");
                    String strOldAreaOfStudy = "";

                    for (Int32 i = 0; i < optionCount; i++) {
                        String strProgramVersionID = dsOptions.Tables[0].Rows[i]["ProgramVersionID"].ToString();
                        String strProgramDegreeID = dsOptions.Tables[0].Rows[i]["ProgramDegreeID"].ToString();
                        String newProgramTitle = dsOptions.Tables[0].Rows[i]["ProgramTitle"].ToString();
                        String optionTitle = dsOptions.Tables[0].Rows[i]["OptionTitle"].ToString();
                        String strOptionID = dsOptions.Tables[0].Rows[i]["OptionID"].ToString();
                        String collegeShortTitle = dsOptions.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                        String degreeShortTitle = dsOptions.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                        String degreeLongTitle = dsOptions.Tables[0].Rows[i]["DegreeLongTitle"].ToString();
                        String programWebsiteURL = dsOptions.Tables[0].Rows[i]["ProgramWebsiteURL"].ToString();
                        String strAreaOfStudy = dsOptions.Tables[0].Rows[i]["Title"].ToString();
                        String documentTitle = dsOptions.Tables[0].Rows[i]["DocumentTitle"].ToString();
                        String degreeType = dsOptions.Tables[0].Rows[i]["DegreeType"].ToString();
                        String programID = dsOptions.Tables[0].Rows[i]["ProgramID"].ToString();
                        String websiteURL = dsOptions.Tables[0].Rows[i]["WebsiteURL"].ToString();

                        String collegeURL = "https://scc.spokane.edu"; //Adding collegeURL specificity to avoid problem we were having with Google indexing degree description views at the wrong college sites - 2018-05-25 bn

                        if (collegeShortTitle.ToUpper().Contains("SFCC"))
                        {
                            collegeURL = "https://sfcc.spokane.edu";
                        }

                        if (degreeShortTitle != null && degreeShortTitle != "") {

                            if (optionTitle != null && optionTitle != "") {
                                newProgramTitle += " " + optionTitle;
                            }

                            if (strAreaOfStudy != strOldAreaOfStudy) { //display the area of study

                                if (strOldAreaOfStudy != "") { //if a different area of study existed in the previous loop
                                    areaOfStudySection.InnerHtml += "</ul>"; //close the unordered list of the previous area of study
                                    panDegreeList.Controls.Add(areaOfStudySection);
                                }

                                if (strAreaOfStudy == "Transfer Degrees") {
                                    programWebsiteURL = "";
                                }

                                //add the new area of study section
                                areaOfStudySection = new HtmlGenericControl("section");
                                areaOfStudySection.InnerHtml += "<h3><a href=\"" + websiteURL.Replace("http://","https://") + "\" target=\"self\"  title=\"View Area of Study\">" + strAreaOfStudy + "</a></h3><ul>";
                            }

                            //display the degrees/certificates under an area of study
                            if (strAreaOfStudy == "Transfer Degrees") {
                                if (newProgramTitle == null || newProgramTitle == "") { //if a general transfer degree (without a program outline)
                                    //display general transfer degree
                                    try {
                                        String fileName = wsICatalog.GetDegreeRequirementWorksheet(documentTitle, cboQuarter.SelectedValue);

                                        //add a new degree list item to the current area of study
                                        areaOfStudySection.InnerHtml += "<li><a href=\"http://catalog.spokane.edu/degworksheet/" + fileName + "\" target=\"_blank\">" + degreeLongTitle + " - SCC, SFCC</a></li>";

                                    } catch {
                                        //display nothing
                                    }

                                } else { //a specific transfer degree (with a program outline)

                                    //display program transfer degree
                                    if (degreeType == "Transfer") {
                                        areaOfStudySection.InnerHtml += "<li><a href=\"" + collegeURL + "/What-to-Study/Degree-Description?id=" + programID + "&strm=" + cboQuarter.SelectedValue + "\" title=\"View Degree/Certificate\">" + degreeLongTitle + " - " + newProgramTitle + " - " + collegeShortTitle + "</a></li>";
                                    }
                                }

                            } else {
                                areaOfStudySection.InnerHtml += "<li><a href=\"" + collegeURL + "/What-to-Study/Degree-Description?id=" + programID + "&strm=" + cboQuarter.SelectedValue + "\" title=\"View Degree/Certificate\">" + degreeLongTitle + " - " + newProgramTitle + " - " + collegeShortTitle + "</a></li>";
                            }

                            strOldAreaOfStudy = strAreaOfStudy;

                            if (i == (optionCount - 1)) {
                                areaOfStudySection.InnerHtml += "</ul>"; //close the unordered list of the previous area of study
                                panDegreeList.Controls.Add(areaOfStudySection);
                            }
                        }
                    }
                } else {
                    DisplayMessage();
                }

            } catch {
                DisplayMessage();
            }

        } else if (cboView.SelectedValue == "degree") {

            panDegree.Visible = true;

            DataSet dsOptions = wsICatalog.GetDegrees(cboQuarter.SelectedValue, cboLocation.SelectedValue, cboDegree.SelectedValue, "", cboSubject.SelectedValue, cboCatalogNumber.SelectedValue, txtTextSearch.Text.Trim());

            try {
                Int32 optionCount = dsOptions.Tables[0].Rows.Count;

                if (optionCount > 0) {

                    HtmlGenericControl degreeSection = new HtmlGenericControl("section");
                    String oldDegreeTitle = "";

                    for (Int32 i = 0; i < optionCount; i++) {
                        String strProgramVersionID = dsOptions.Tables[0].Rows[i]["ProgramVersionID"].ToString();
                        String strProgramDegreeID = dsOptions.Tables[0].Rows[i]["ProgramDegreeID"].ToString();
                        String optionTitle = dsOptions.Tables[0].Rows[i]["OptionTitle"].ToString();
                        String strOptionID = dsOptions.Tables[0].Rows[i]["OptionID"].ToString();
                        String collegeShortTitle = dsOptions.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                        String degreeShortTitle = dsOptions.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                        String degreeLongTitle = dsOptions.Tables[0].Rows[i]["DegreeLongTitle"].ToString();
                        String degreeType = dsOptions.Tables[0].Rows[i]["DegreeType"].ToString();
                        String documentTitle = dsOptions.Tables[0].Rows[i]["DocumentTitle"].ToString();
                        String degreeDescription = dsOptions.Tables[0].Rows[i]["DegreeDescription"].ToString();
                        String programTitle = dsOptions.Tables[0].Rows[i]["ProgramTitle"].ToString();
                        String programID = dsOptions.Tables[0].Rows[i]["ProgramID"].ToString();

                        String collegeURL = "https://scc.spokane.edu"; //Adding collegeURL specificity to avoid problem we were having with Google indexing degree description views at the wrong college sites - 2018-05-25 bn

                        if (collegeShortTitle.ToUpper().Contains("SFCC"))
                        {
                            collegeURL = "https://sfcc.spokane.edu";
                        }

                        if (degreeShortTitle != null && degreeShortTitle != "") {

                            if (optionTitle != null && optionTitle != "") {
                                programTitle += " " + optionTitle;
                            }

                            if (oldDegreeTitle != degreeLongTitle) {

                                if (oldDegreeTitle != "") { //if a different degree type existed in the previous loop
                                    degreeSection.InnerHtml += "</ul>"; //close the unordered list of the previous category
                                    panDegreeList.Controls.Add(degreeSection);
                                }

                                //add the new area of study section
                                degreeSection = new HtmlGenericControl("section");
                                degreeSection.InnerHtml += "<h3>" + degreeLongTitle + "</h3>";
                                if (degreeDescription != null && degreeDescription != "") {
                                    degreeSection.InnerHtml += "<p>" + degreeDescription + "</p>";
                                }
                                degreeSection.InnerHtml += "<ul>";
                            }

                            if (programTitle == null || programTitle == "") { //if a general transfer degree (without a program outline)
                                //display general transfer degree
                                try {
                                    String fileName = wsICatalog.GetDegreeRequirementWorksheet(documentTitle, cboQuarter.SelectedValue);

                                    //add a new degree list item to the current area of study
                                    degreeSection.InnerHtml += "<li><a href=\"http://catalog.spokane.edu/degworksheet/" + fileName + "\" target=\"_blank\">Requirements / Worksheet - SCC, SFCC</a></li>";

                                } catch {
                                    //display nothing
                                }

                            } else { //a specific transfer degree (with a program outline)

                                //new degree page
                                degreeSection.InnerHtml += "<li><a href=\"" + collegeURL + "/What-to-Study/Degree-Description?id=" + programID + "&strm=" + cboQuarter.SelectedValue + "\" title=\"View Degree/Certificate\">" + degreeLongTitle + " - " + programTitle + " - " + collegeShortTitle + "</a></li>";
                            }

                            oldDegreeTitle = degreeLongTitle;

                            if (i == (optionCount - 1)) {
                                degreeSection.InnerHtml += "</ul>"; //close the unordered list of the previous degree
                                panDegreeList.Controls.Add(degreeSection);
                            }
                        }
                    }

                } else {
                    DisplayMessage();
                }

            } catch {
                DisplayMessage();
            }

        } else if (cboView.SelectedValue == "category") {

            panCategory.Visible = true;

            DataSet dsOptions = wsICatalog.GetDegreesByCategory(cboQuarter.SelectedValue, cboLocation.SelectedValue, cboDegree.SelectedValue, cboCategory.SelectedValue, cboSubject.SelectedValue, cboCatalogNumber.SelectedValue, txtTextSearch.Text.Trim());

            try {
                Int32 optionCount = dsOptions.Tables[0].Rows.Count;

                if (optionCount > 0) {

                    HtmlGenericControl categorySection = new HtmlGenericControl("section");
                    String oldCategoryTitle = "", strOldProgramVersionID = "";

                    for (Int32 i = 0; i < optionCount; i++) {
                        String strProgramVersionID = dsOptions.Tables[0].Rows[i]["ProgramVersionID"].ToString();
                        String strProgramDegreeID = dsOptions.Tables[0].Rows[i]["ProgramDegreeID"].ToString();
                        String newProgramTitle = dsOptions.Tables[0].Rows[i]["ProgramTitle"].ToString();
                        String optionTitle = dsOptions.Tables[0].Rows[i]["OptionTitle"].ToString();
                        String newCategoryTitle = dsOptions.Tables[0].Rows[i]["CategoryTitle"].ToString();
                        String strOptionID = dsOptions.Tables[0].Rows[i]["OptionID"].ToString();
                        String collegeShortTitle = dsOptions.Tables[0].Rows[i]["CollegeShortTitle"].ToString();
                        String degreeShortTitle = dsOptions.Tables[0].Rows[i]["DegreeShortTitle"].ToString();
                        String degreeLongTitle = dsOptions.Tables[0].Rows[i]["DegreeLongTitle"].ToString();
                        String programWebsiteURL = dsOptions.Tables[0].Rows[i]["ProgramWebsiteURL"].ToString();
                        String documentTitle = dsOptions.Tables[0].Rows[i]["DocumentTitle"].ToString();
                        String programID = dsOptions.Tables[0].Rows[i]["ProgramID"].ToString();

                        String collegeURL = "https://scc.spokane.edu"; //Adding collegeURL specificity to avoid problem we were having with Google indexing degree description views at the wrong college sites - 2018-05-25 bn

                        if (collegeShortTitle.ToUpper().Contains("SFCC"))
                        {
                            collegeURL = "https://sfcc.spokane.edu";
                        }

                        if (degreeShortTitle != null && degreeShortTitle != "") {

                            if (optionTitle != null && optionTitle != "") {
                                newProgramTitle += " " + optionTitle;
                            }

                            if (oldCategoryTitle != newCategoryTitle) {

                                if (oldCategoryTitle != "") { //if a different category existed in the previous loop
                                    categorySection.InnerHtml += "</ul>"; //close the unordered list of the previous category
                                    panDegreeList.Controls.Add(categorySection);
                                }

                                //add the new area of study section
                                categorySection = new HtmlGenericControl("section");
                                categorySection.InnerHtml += "<h3>" + newCategoryTitle + "</h3><ul>";
                            }

                            if (newProgramTitle == null || newProgramTitle == "") { //if a general transfer degree (without a program outline)
                                //display general transfer degree
                                try {
                                    String fileName = wsICatalog.GetDegreeRequirementWorksheet(documentTitle, cboQuarter.SelectedValue);

                                    //add a new degree list item to the current area of study
                                    categorySection.InnerHtml += "<li><a href=\"http://catalog.spokane.edu/degworksheet/" + fileName + "\" target=\"_blank\">" + degreeLongTitle + " - SCC, SFCC</a></li>";

                                } catch {
                                    //display nothing
                                }

                            } else { //a specific transfer degree (with a program outline)
                                categorySection.InnerHtml += "<li><a href=\"" + collegeURL + "/What-to-Study/Degree-Description?id=" + programID + "&strm=" + cboQuarter.SelectedValue + "\" title=\"View Degree/Certificate\">" + degreeLongTitle + " - " + newProgramTitle + " - " + collegeShortTitle + "</a></li>";
                            }

                            strOldProgramVersionID = strProgramVersionID;
                            oldCategoryTitle = newCategoryTitle;

                            if (i == (optionCount - 1)) {
                                categorySection.InnerHtml += "</ul>"; //close the unordered list of the previous category
                                panDegreeList.Controls.Add(categorySection);
                            }
                        }
                    }

                } else {
                    DisplayMessage();
                }
            } catch {
                DisplayMessage();
            }
        }
    }

    public void DisplayMessage()
    {
        String selectedView = "", selectedLtr = "", selectedLocation = "all locations", selectedDegree = " offering any degrees/certificates", strSelectedCategory = " in all categories";
        if (cboLocation.SelectedValue != "")
        {
            selectedLocation = cboLocation.SelectedItem.Text;
        }

        if (cboDegree.SelectedValue != "")
        {
            selectedDegree = cboDegree.SelectedItem.Text;
            String selectedDegreeLtr = selectedDegree.Substring(0, 1).ToUpper();
            if (selectedDegreeLtr == "A" || selectedDegreeLtr == "E" || selectedDegreeLtr == "I" || selectedDegreeLtr == "O" || selectedDegreeLtr == "U")
            {
                selectedDegree = " offering an " + selectedDegree;
            }
            else
            {
                selectedDegree = " offering a " + selectedDegree;
            }
        }

        if (cboCategory.SelectedValue != "")
        {
            strSelectedCategory = " in the category " + cboCategory.SelectedItem.Text;
        }

        // Added code to prevent bot-generated null reference errors - KC 7/5/2017
        string selectedQuarter = "";
        if (cboQuarter != null)
        {
            if (cboQuarter.SelectedItem != null)
            {
                if (cboQuarter.SelectedItem.Text != null)
                {
                    selectedQuarter = cboQuarter.SelectedItem.Text;
                }
            }
        }

        if (hidLtr.Value != "")
        {
            selectedLtr = " starting with the letter " + hidLtr.Value;
        }

        if (cboView.SelectedValue == "alpha")
        {
            selectedView = "Areas of Study" + selectedLtr;
        }
        else if (cboView.SelectedValue == "degree")
        {
            selectedView = "degrees and certificates";
        }
        else if (cboView.SelectedValue == "category")
        {
            selectedView = "degrees and certificates" + strSelectedCategory;
        }

        panDegreeList.Controls.Add(new LiteralControl("<p>Your search for " + selectedView + " offered " + selectedQuarter + " at " + selectedLocation + selectedDegree + " returned no results.</p>"));
    }

    protected void cboDegree_Change(object sender, EventArgs e)
    {
        cboDegree2.SelectedValue = cboDegree.SelectedValue;
    }

    protected void cmdReset_Click(object sender, EventArgs e)
    {
        //initialize iCatalog web service
        iCatalogXML.iCatalogXML wsICatalog = new iCatalogXML.iCatalogXML();
        cboQuarter.SelectedValue = wsICatalog.GetRegistrationTerm();

        cboLocation.SelectedValue = "";
        txtTextSearch.Text = "";
        cboSubject.SelectedValue = "";
        cboCatalogNumber.SelectedValue = "";
        cboDegree.SelectedValue = "";
        cboDegree2.SelectedValue = "";

        panDegreeList.Controls.Clear();

        DisplayDegrees();
    }
}

