﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.IO;

public partial class CMSWebParts_Custom_LateEnrollmentEmail : CMSAbstractWebPart
{
    string strSQL = "";
    int intResult;
    DataTable dt;
    LateEnrollment leInfo = new LateEnrollment();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            ShowData();
    }
    protected void ShowData()
    {
        dt = new DataTable();
        strSQL = String.Format("SELECT LateEnrollmentFormID, ctcLinkID, First_Name, Last_Name, InstructorName, InstructorEmail " +
            "FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] ORDER BY Last_Name");
        dt = leInfo.RunGetQuery(strSQL);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                grdViewEmails.DataSource = dt;
                grdViewEmails.DataBind();
            }// end count > 0
        }// end if not null
    }// end ShowData

    protected void grdViewEmails_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {   
        //NewEditIndex property used to determine the index of the row being edited.   
        grdViewEmails.EditIndex = e.NewEditIndex;   
        ShowData();   
    }

    protected void grdViewEmails_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e) {   
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
        grdViewEmails.EditIndex = -1;   
        ShowData();   
    }

    protected void grdViewEmails_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        //Finding the controls from Gridview for the row which is going to update   
        Label formId = grdViewEmails.Rows[e.RowIndex].FindControl("lblFormID") as Label;
        Label stuID = grdViewEmails.Rows[e.RowIndex].FindControl("lblctcLinkID") as Label;
        Label stuFirstName = grdViewEmails.Rows[e.RowIndex].FindControl("lblbFirstName") as Label;
        Label stuLastName = grdViewEmails.Rows[e.RowIndex].FindControl("lblbLastName") as Label;
        Label instrName = grdViewEmails.Rows[e.RowIndex].FindControl("lblbInstructorName") as Label;
        TextBox instrEmail = grdViewEmails.Rows[e.RowIndex].FindControl("txtEmail") as TextBox;

        //updating the record   
        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] SET InstructorEmail = '{0}' " +
            "WHERE LateEnrollmentFormID = {1}", instrEmail.Text, Convert.ToInt32(formId.Text));
        intResult = leInfo.runSPQuery(strSQL, false);
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
        grdViewEmails.EditIndex = -1;
        //Call ShowData method for displaying updated data   
        ShowData();
    }
}