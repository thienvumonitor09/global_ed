﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StudentRecommendation.ascx.cs" Inherits="CMSWebParts_Custom_StudentRecommendation" %>
<style>
    #rateContainer {
        width:100%;
    }
    #rateContainer .header {
        float:left;
        font-size:1.7rem;
        line-height:1.7;
        font-weight:600;
        margin-right:20px;
    }
    .staticInfo {
        background-color:#e4e0e0;
        padding-left:10px;
    }
    #rateContainer .rowContent {
        float:left;
        margin-right:50px;
    }
    input[type="radio"] {
     /*   margin: 4px 4px 10px 0px; */
        margin-left: 15px;
        margin-right: 5px;
    }
    input[type="checkbox"]{
        margin-right: 10px;
    }

</style>
    <section class="col-lg-12 row" style="background-color:white;margin-left:0px;">
        <div class="container">
            <div id="signIn" runat="server" visible="true">
                <div class="cs_row col-lg-12">
                    <div class="staticInfo" runat="server">
                        <p>&nbsp;</p>
                        <p>
                            <asp:Label ID="lblLog" runat="server" Text="Please login with your CCS credentials, e.g. John.Smith@sfcc.spokane.edu"></asp:Label>
                        </p>
                        <p>
                            <asp:Label for="txtUName" runat="server" Font-Bold="true" Width="35%" Text="User Name:"></asp:Label>
                            <asp:TextBox ID="txtUName" runat="server" Width="300px"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label for="txtPWord" runat="server" Font-Bold="true" Width="35%" Text="Password:"></asp:Label>
                            <asp:TextBox ID="txtPWord" runat="server" TextMode="Password" Width="300px"></asp:TextBox>
                        </p>
                        <p>
                            <div id="divLogBtn" runat="server" class="button" style="width:35%;margin-left:43%;">
                                <asp:Button ID="btnLogIn" runat="server" Text="Login" OnClick="btnLogIn_Click" />
                            </div>
                        </p>
                        <p>
                            Problems loggin in? For assistance, please contact the <a href="mailto:ITSupportCenter@ccs.spokane.edu">Customer Support Center</a>: (509) 533-H-E-L-P (533-4357)
                        </p>
                    </div>
                </div>
            </div>
            <div id="noMatchID" runat="server">
                 <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
            <div id="matchID" runat="server">
            <div class="cs_row col-lg-12" runat="server">
                <div id="instructions" runat="server">
                    <asp:Label ID="lblAlreadyActed" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblInstruction" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                </div>
                <div id="baseData" runat="server">
                    <h2>SFCC Recommendation</h2>
                    <p>
                        <asp:Label ID="lblSendWhere" runat="server" Text=""></asp:Label>
                    </p>
                    <div class="staticInfo" runat="server">
                        <p>
                            <asp:Label for="lblCtcLinkID" runat="server" Font-Bold="true" Width="35%" Text="Student ctcLink ID:"></asp:Label>
                            <asp:Label ID="lblCtcLinkID" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:Label for="lblStudentName" runat="server" Font-Bold="true" Width="35%" Text="Student Name:"></asp:Label>
                            <asp:Label ID="lblStudentName" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:Label for="lblStudentEmail" runat="server" Font-Bold="true" Width="35%" Text="Student Email:"></asp:Label>
                            <asp:Label ID="lblStudentEmail" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:Label for="litDatesAttend" ID="DatesAttend" runat="server" Font-Bold="true" Width="35%" Text="Dates of Attendance at SFCC:"></asp:Label>
                            <asp:Literal ID="litDatesAttend" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Label for="litProgramOfStudy" ID="lblProgramOfStudy" runat="server" Font-Bold="true" Width="35%" Text="Student's Program of Study:"></asp:Label>
                            <asp:Literal ID="litProgramOfStudy" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Label for="litDepartment" runat="server" Font-Bold="true" Width="35%" Text="Department of Faculty / Staff Member:"></asp:Label>
                            <asp:Literal ID="litDepartment" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Label for="litFacultyStaffName" runat="server" Font-Bold="true" Width="35%" Text="Faculty / Staff Name:"></asp:Label>
                            <asp:Literal ID="litFacultyStaffName" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Label for="litFacultyStaffEmail" runat="server" Font-Bold="true" Width="35%" Text="Faculty / Staff Email:"></asp:Label>
                            <asp:Literal ID="litFacultyStaffEmail" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Label for="lblLengthKnown" runat="server" Font-Bold="true" Width="35%" Text="How long have you known Faculty / Staff:"></asp:Label>
                            <asp:Label ID="lblLengthKnown" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="lblBlank" runat="server" Text="" Width="5%"></asp:Label>
                            <asp:Label for="lblWhatCapacity" runat="server" Font-Bold="true" Width="30%" Text="In the following capacities:"></asp:Label>
                            <asp:Label ID="lblWhatCapacity" runat="server" Width="55%" Font-Bold="true" Text="Duration"></asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="lblBlank2" runat="server" Text="" Width="5%"></asp:Label>
                            <asp:Label ID="lblListCapacity" runat="server" Text="" Width="30%"></asp:Label>
                            <asp:Label ID="lblListDuration" runat="server" Text="" Width="55%"></asp:Label>
                        </p>
                        <p>
                          <!--  <asp:Label for="agreeTerms" runat="server" Font-Bold="true" Width="35%" Text=""></asp:Label> -->
                            <asp:Label ID="agreeTerms" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <h4>Faculty / Staff please complete the following</h4>
                    <p>
                        <asp:Label for="rblAcadAbility" runat="server" Font-Bold="true" Width="35%" Text="Academic Ability:"></asp:Label>
                        <asp:RadioButtonList ID="rblAcadABility" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblVerbal" runat="server" Font-Bold="true" Width="35%" Text="Verbal Communication Skills:"></asp:Label>
                        <asp:RadioButtonList ID="rblVerbal" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblWritten" runat="server" Font-Bold="true" Width="35%" Text="Written Communication Skills:"></asp:Label>
                        <asp:RadioButtonList ID="rblWritten" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblDetail" runat="server" Font-Bold="true" Width="35%" Text="Attention to Detail:"></asp:Label>
                        <asp:RadioButtonList ID="rblDetail" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblAttendance" runat="server" Font-Bold="true" Width="35%" Text="Attendance:"></asp:Label>
                        <asp:RadioButtonList ID="rblAttendance" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblTeamwork" runat="server" Font-Bold="true" Width="35%" Text="Teamwork:"></asp:Label>
                        <asp:RadioButtonList ID="rblTeamwork" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblLeadership" runat="server" Font-Bold="true" Width="35%" Text="Leadership Ability:"></asp:Label>
                        <asp:RadioButtonList ID="rblLeadership" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblAccountablility" runat="server" Font-Bold="true" Width="35%" Text="Accountablility:"></asp:Label>
                        <asp:RadioButtonList ID="rblAccountability" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblInitiative" runat="server" Font-Bold="true" Width="35%" Text="Personal Initiative:"></asp:Label>
                        <asp:RadioButtonList ID="rblInitiative" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblFeedback" runat="server" Font-Bold="true" Width="35%" Text="Ability to Apply Feedback to future work:"></asp:Label>
                        <asp:RadioButtonList ID="rblFeedback" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblTime" runat="server" Font-Bold="true" Width="35%" Text="Time Management:"></asp:Label>
                        <asp:RadioButtonList ID="rblTime" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblResources" runat="server" Font-Bold="true" Width="35%" Text="Ability to Manage Resources / Network:"></asp:Label>
                        <asp:RadioButtonList ID="rblResources" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                    <p>
                        <asp:Label for="rblThinking" runat="server" Font-Bold="true" Width="35%" Text="Critical Thinking Skills:"></asp:Label>
                        <asp:RadioButtonList ID="rblThinking" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                            <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                            <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                            <asp:ListItem Value="Average">Average</asp:ListItem>
                            <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                            <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                            <asp:ListItem Value="N/A">N/A</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                     <p>
                        <asp:Label for="txtComment" runat="server" Font-Bold="true" Width="35%" Text="Faculty Comments <i>(Optional)</i>:" style="vertical-align:top;"></asp:Label>
                         <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" width="60%" Height="100px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Label for="uplRecommendFile" runat="server" Font-Bold="true" Width="35%" Text="Upload written recommendation (PDF only!):"></asp:Label>
                        <asp:FileUpload ID="uplRecommendFile" runat="server" style="float:right;width:65%" />
                        <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="uplRecommendFile" ErrorMessage="<span style='color:#DD0000;font-weight:700;'>Only PDF files are allowed</span>" ValidationExpression="(.*\.([pP][dD][fF])$)"></asp:RegularExpressionValidator>
                        <asp:Label ID="lblFileUploaded" runat="server" Text="" style="float:left;"></asp:Label>
                    </p>
                    </div>
                    <div id="studentOptions1" runat="server" visible="false">
                    <h4>Student Requested Areas to Rate</h4>
                        <p>
                            <asp:Label for="rblStuOption1" ID="stuOption1" runat="server" Font-Bold="true" Width="35%" Text=""></asp:Label>
                            <asp:RadioButtonList ID="rblStuOption1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                                <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                                <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                                <asp:ListItem Value="Average">Average</asp:ListItem>
                                <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                                <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                                <asp:ListItem Value="N/A">N/A</asp:ListItem>
                            </asp:RadioButtonList>
                        </p>
                    </div>
                    <div id="studentOptions2" runat="server" visible="false">
                        <p>
                            <asp:Label for="rblStuOption2" ID="stuOption2" runat="server" Font-Bold="true" Width="35%" Text=""></asp:Label>
                            <asp:RadioButtonList ID="rblStuOption2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                                <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                                <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                                <asp:ListItem Value="Average">Average</asp:ListItem>
                                <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                                <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                                <asp:ListItem Value="N/A">N/A</asp:ListItem>
                            </asp:RadioButtonList>
                        </p>
                    </div>
                    <div id="studentOptions3" runat="server" visible="false">
                        <p>
                            <asp:Label for="rblStuOption3" runat="server" ID="stuOption3" Font-Bold="true" Width="35%" Text=""></asp:Label>
                            <asp:RadioButtonList ID="rblStuOption3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" style="margin-left:-10px;">
                                <asp:ListItem Value="Improvement Needed">Improvement Needed</asp:ListItem>
                                <asp:ListItem Value="Below Average">Below Average</asp:ListItem>
                                <asp:ListItem Value="Average">Average</asp:ListItem>
                                <asp:ListItem Value="Above Average">Above Average</asp:ListItem>
                                <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                                <asp:ListItem Value="N/A">N/A</asp:ListItem>
                            </asp:RadioButtonList>
                        </p>
                    </div>
<!--
                    <p>
                        <asp:CheckBox ID="chkFac" runat="server" Text="By checking the box, I affirm that the entries above are true" />
                    </p>
-->
                    <p>
                        The student has requested a recommendation created and submitted by the faculty on record in the document created through SFCC. Please accept this as the official verification.
                    </p>
                    <p>
                        <div id="buttonLine" runat="server" class="button" style="float:right;">
                            <asp:Button ID="btnCmdSubmit" runat="server" Text="Submit" OnClick="btnCmdSubmit_Click" />
                        </div>
                    </p>
                <asp:HiddenField ID="hdnRecSent" runat="server" Value="" />
                <asp:HiddenField ID="hdnFacStaffEmail" runat="server" Value="" />
                <asp:HiddenField ID="hdnSendToEmail" runat="server" Value="" />
                <asp:HiddenField ID="hdnSendInstitution" runat="server" Value="" />
                <asp:HiddenField ID="hdnStudentEmail" runat="server" Value="" />
                <asp:HiddenField ID="hdnFERPAwaiver" runat="server" Value="" />
                <asp:HiddenField ID="hdnStudentName" runat="server" Value="" />
                </div>
                </div>
            </div>
        </section>
