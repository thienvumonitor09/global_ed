﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassSearchPrint.ascx.cs" Inherits="CMSWebParts_Custom_ClassSearchPrint" %>
<style type="text/css">
    table {
        padding: 0 10px 10px 41px;
    }

    th {
        text-align: left;
    }

    td {
        padding-right: 25px;
    }

    .indent {
        padding-left: 45px;
    }

    .button {
        width: 135px;
        background: #4897d2;
        color: #fff;
        border-radius: 9px;
        border-width: 0;
        font-weight: 500;
        text-decoration: none;
        text-align: center;
        font-size: 1.2em;
        line-height: 1.3;
        font-family: 'Open Sans',sans-serif;
        display: inline-block;
        margin-right: 0;
        vertical-align: top;
    }

    .button:hover{
        cursor: pointer;
        background: #00529a;
    } 

    #buttons {
        width: 100%;
        text-align: center;
        padding: 20px 0 20px 0;
    }
</style>
<div>
    <asp:Panel ID="panContent" runat="server"></asp:Panel>
</div>
<div id="buttons">
    <input type="button" class="button" value="Print" onclick="window.print();" />&nbsp;&nbsp;
    <input type="button" class="button" value="Close" onclick="self.close();" />
</div>
