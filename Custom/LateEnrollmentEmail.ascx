﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LateEnrollmentEmail.ascx.cs" Inherits="CMSWebParts_Custom_LateEnrollmentEmail" %>

<style>
        th, td
        {
            padding-left: 6px;
            padding-right: 6px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
    </style>
<section class="white-bg">
    <div id="container">
        <section class="col-lg-12 row">
            <div class="cs_row col-lg-12">
                <asp:GridView ID="grdViewEmails" runat="server" AutoGenerateColumns="False" CellPadding="20" OnRowCancelingEdit="grdViewEmails_RowCancelingEdit" OnRowEditing="grdViewEmails_RowEditing" OnRowUpdating="grdViewEmails_RowUpdating">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FormID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormID" runat="server" Text='<%#Eval("LateEnrollmentFormID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student ID">
                            <ItemTemplate>
                                <asp:Label ID="lblctcLinkID" runat="server" Text='<%#Eval("ctcLinkID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="First Name">
                            <ItemTemplate>
                                <asp:Label ID="lblbFirstName" runat="server" Text='<%#Eval("First_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Name">
                            <ItemTemplate>
                                <asp:Label ID="lblbLastName" runat="server" Text='<%#Eval("Last_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Instructor Name">
                            <ItemTemplate>
                                <asp:Label ID="lblbInstructorName" runat="server" Text='<%#Eval("InstructorName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Instructor Email">
                            <ItemTemplate>
                                <asp:Label ID="lblbInstructorEmail" runat="server" Text='<%#Eval("InstructorEmail") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEmail" runat="server" Text='<%#Eval("InstructorEmail") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#595959" ForeColor="#ffffff" />
                    <RowStyle BackColor="#e6e7e7" ForeColor="#1c3d71" />
                </asp:GridView>
            </div>
        </section>
    </div>
</section>