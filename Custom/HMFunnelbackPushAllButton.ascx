﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HMFunnelbackPushAllButton.ascx.cs" Inherits="CMSWebParts_Custom_HMFunnelbackPushAllButton" %>

<asp:Button id="btnPushAll" runat="server" OnClick="btnPushAll_Click" CommandArgument="" Text="Push all published documents" />
<asp:Literal id="lblResult" runat="server" text=""></asp:Literal>
<ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" TargetControlID="btnPushAll" ConfirmText="Are you sure you want to push all the documents of this page type?" />