﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.IO;


public partial class CMSWebParts_Custom_LateEnrollmentReports : CMSAbstractWebPart
{
    string strSQL = "";
    string strSite = "";
    // for Email
    string sendTo = "";
    string sentFrom = "";
    string strCCaddresses = "";
    string strSubject = "";
    string strEmailResult = "";
    string strBody = "";
    string postBackControlName = "";
    string studentId = "";
    int stuID;
    bool isNumber = false;
    bool reportSummary = false;
    bool reportNoResponse = false;
    bool exportFile = false;
    bool duplicateRecords = false;
    bool day6Plus = false;

    LateEnrollment leInfo = new LateEnrollment();
    protected void Page_Load(object sender, EventArgs e)
    {
        ReportHead.Visible = false;
        ReportBody.Visible = false;
        litMessage.Text = "";
        lblReportErrMsg.Text = "";
        reportNoResponse = false;
        reportSummary = false;
        duplicateRecords = false;
        btnDelete.Visible = false;
        pnlReport.Controls.Clear();
        pnlDeleted.Controls.Clear();
     //   btnProcess.Visible = false;

        strSite = Request.Url.AbsoluteUri;
        if (strSite.Contains("sfcc.spokane.edu"))
            strSite = "SFCC";
        else
            strSite = "SCC";

        strSQL = String.Format("SELECT * FROM [CCSGen_ctcLink_ODS].[dbo].[vw_Quarter_MetaData] " +
                            "WHERE CurrentQtr = 1");
        DataTable dtQtr = leInfo.RunGetQueryODS(strSQL);
        if (dtQtr != null)
        {
            if (dtQtr.Rows.Count > 0)
            {
                TimeSpan numDays = DateTime.Now.Subtract(Convert.ToDateTime(dtQtr.Rows[0]["qtrFirstDayOfClass"].ToString()));
                if (numDays.TotalDays > 5)
                    day6Plus = true;
                else
                    day6Plus = false;
            }
        } else
        {
          //  Response.Write("is null<br />");
        }

        if (IsPostBack)
        {
            
            if (ddReportChoice.SelectedValue == "Select one")
            {
                litMessage.Text = "<span style='color:#DD0000;'>Please select from the Report Type drop-down</span>.";
                return;
            }
            else
            {
                if (ddReportChoice.SelectedValue.ToString() == "Summary")
                {
                    reportSummary = true;
                    reportNoResponse = false;
                    exportFile = false;
                    duplicateRecords = false;
                }
                if(ddReportChoice.SelectedValue.ToString() == "Stuck")
                {
                    reportNoResponse = true;
                    reportSummary = false;
                    exportFile = false;
                    duplicateRecords = false;
                }
                if(ddReportChoice.SelectedValue == "Export")
                {
                    exportFile = true;
                    reportNoResponse = false;
                    reportSummary = false;
                    duplicateRecords = false; 
                }
                if(ddReportChoice.SelectedValue == "Duplicates")
                {
                    reportNoResponse = false;
                    reportSummary = false;
                    exportFile = false;
                    duplicateRecords = true;
                }
                if (reportSummary)
                {
                    // report completed records
                    if (strSite == "SFCC")
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                            "WHERE FacApproveDeny is not NULL AND DeanApproveDeny is not NULL " +
                            "ORDER BY FormInserted DESC");
                    }
                    else
                    {
                        // SCC - get all where faculty have approved then filter in report generation as must check
                        //       on whether online or not and # days into quarter to determine who else signs
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                            "WHERE FacApproveDeny is not NULL " +
                            "ORDER BY FormInserted DESC");
                    }
                    

                    DataTable dtStuInfo = leInfo.RunGetQuery(strSQL);
                    if (dtStuInfo != null)
                    {
                        if (dtStuInfo.Rows.Count > 0)
                        {
                            litReportTitle.Text = "<h2>Summary Report</h2>";
                            printReport(dtStuInfo);
                        }
                        else
                        {
                            lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>No records found.</span></p>";
                        }
                    }
                    else
                    {
                        lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>No records found.</span></p>";
                    }
                }
                else if (reportNoResponse)
                {
                    // No Responses Report
                    // report all records
                    if (strSite == "SFCC")
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                            "WHERE FacApproveDeny is NULL OR (FacApproveDeny = '{0}' AND DeanApproveDeny is NULL) " +
                            "ORDER BY FormInserted DESC", "Approved");
                    }
                    else
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                            "WHERE FacApproveDeny is NULL OR (FacApproveDeny = '{0}' AND DeanApproveDeny is NULL) " +
                            "OR (FacApproveDeny = '{0}' AND VPApproveDeny is NULL) " + 
                            "ORDER BY FormInserted DESC", "Approved");
                    }
                    DataTable dtNoResponse = leInfo.RunGetQuery(strSQL);
                    if (dtNoResponse != null)
                    {
                        if (dtNoResponse.Rows.Count > 0)
                        {
                            litReportTitle.Text = "<h2>No Response List</h2>";
                            printNoShows(dtNoResponse);
                        }
                        else
                        {
                            lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>No records found.</span></p>";
                        }
                    }// data table not null
                }// end report No Responses
                else if (duplicateRecords)
                {
                    if (strSite == "SFCC")
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                            "WHERE ctcLinkID IN (SELECT ctcLinkID FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                            "GROUP BY ctcLinkID HAVING count(*) > 1)");
                    }
                    else
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                            "WHERE ctcLinkID IN (SELECT ctcLinkID FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                            "GROUP BY ctcLinkID HAVING count(*) > 1)");
                    }
                    DataTable dtDuplicates = leInfo.RunGetQuery(strSQL);
                    if (dtDuplicates != null)
                    {
                        if (dtDuplicates.Rows.Count > 0)
                        {
                            litReportTitle.Text = "<h2>Duplicate List</h2>";
                            printDuplicates(dtDuplicates);
                        }
                        else
                        {
                            lblReportErrMsg.Text = "<p><span style='color:#DD0000;'>No records found.</span></p>";
                        }
                    } // end data table exists
                } // end duplicates
                else if (exportFile)
                {
                    exportData();
                }
            }// picked a report type
        }// end IS a postback
     
}// end page load
   


protected void cmdbtnReport_Click(object sender, EventArgs e)
{
       
}

protected void btnProcess_Click(object sender, EventArgs e)
{
        ReportBody.Visible = true;
        pnlReport.Visible = true;
        int controlCount = pnlReport.Controls.Count;
        CheckBox ck;
        TextBox txtB;
        string strSQL = "";
        int formID;
        bool isInt = false;
        Utility uEmail = new Utility();
        if (strSite == "SFCC")
            sentFrom = "SFCCInfo@sfcc.spokane.edu";
        else
            sentFrom = "SCCInfo@scc.spokane.edu";
        strSubject = "Late Enrollment request processed";

        for (int x = 0; x < controlCount; x++)
        {
            if (pnlReport.Controls[x] is CheckBox)
            {
                ck = (CheckBox)pnlReport.Controls[x];
            //    Response.Write("id: " + ck.ID + "; " + ck.Checked.ToString() + "; ");
                if(ck.Checked)
                {
                    // generate email to student, instructor, dean
                    // if value in text box then standard message is being over-written
                    // get the LateEnrollmentFormID from the checkbox id
                    string[] idPieces = ck.ID.Split('|');
                    string tableID = idPieces[1];
                 //   Response.Write("chkBox ID: " + tableID + "<br />");

                    txtB = (TextBox)pnlReport.Controls[x + 1];
                    string[] txPieces = txtB.ID.Split('|');
                    string txtBoxID = txPieces[1];
                    string txtComment = txtB.Text.Trim().Replace("'", "''");
                    
                    // pull the record from the db to know who to send it to
                    isInt = int.TryParse(tableID, out formID);
                    if (strSite == "SFCC")
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                        "WHERE LateEnrollmentFormID = {0}", formID);
                    } else
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                        "WHERE LateEnrollmentForm_SCCID = {0}", formID);
                    }
                    DataTable dtOneStudent = leInfo.RunGetQuery(strSQL);
                    if (dtOneStudent != null)
                    {
                        if (dtOneStudent.Rows[0]["Processed"].ToString() != "Yes")
                        {
                            if (dtOneStudent.Rows.Count > 0)
                            {
                                // generate the email
                                if (strSite == "SFCC")
                                    sendTo = String.Format("geoff.lake@sfcc.spokane.edu, {0}, {1}, {2}", dtOneStudent.Rows[0]["Email_Address"].ToString(), dtOneStudent.Rows[0]["InstructorEmail"].ToString(), dtOneStudent.Rows[0]["DeanEmail"].ToString());
                                else
                                {
                                   // sendTo = String.Format("laura.padden@ccc.spokane.edu, {0}, {1}, {2}", dtOneStudent.Rows[0]["Email_Address"].ToString(), dtOneStudent.Rows[0]["InstructorEmail"].ToString(), dtOneStudent.Rows[0]["DeanEmail"].ToString());
                                    sendTo = String.Format("Erika.Naccarato@scc.spokane.edu, {0}, {1}, {2}", dtOneStudent.Rows[0]["Email_Address"].ToString(), dtOneStudent.Rows[0]["InstructorEmail"].ToString(), dtOneStudent.Rows[0]["DeanEmail"].ToString());
                                }
                                if (txtB.Text.Trim() != "")
                                {
                                    strBody = "<p>" + txtB.Text + "</p>";
                                }
                                else
                                {
                                    strBody = "<p>This transaction has been processed, please visit your Student Center in ctcLink to review your account and make arrangements to pay any balances owed immediately.</p>";
                                }
                                strBody += "<table width='95%'><tbody>";
                                strBody += "<tr><td style='width:30%'><b>" + dtOneStudent.Rows[0]["First_Name"].ToString() + " " + dtOneStudent.Rows[0]["Last_Name"].ToString() + "</b></td>";
                                strBody += "<td style='width:70%'><b>" + dtOneStudent.Rows[0]["ctcLinkID"].ToString() + "</b></td></tr>";
                                strBody += "<tr><td colspan='2'><b>Course for late Enrollment</b></td></tr>";
                                strBody += "<tr><td style='width:30%'><b>Course Name: </b></td>";
                                strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["CourseName"].ToString() + "</td></tr>";
                                strBody += "<tr><td style='width:30%'><b>Course Number: </b></td>";
                                strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["CourseNumber"].ToString() + "</td></tr>";
                                strBody += "<tr><td style='width:30%'><b>Class Number: </b></td>";
                                strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["ClassNumber"].ToString() + "</td></tr>";
                                if (dtOneStudent.Rows[0]["DropClass"].ToString() == "Yes")
                                {
                                    strBody += "<tr><td colspan='2'><p><b><br />Course to Drop</b></p></td></tr>";
                                    strBody += "<tr><td style='width:30%'><b>Course Description: </b></td>";
                                    strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["DropCourseDescription"].ToString() + "</td></tr>";
                                    strBody += "<tr><td style='width:30%'><b>Course Number: </b></td>";
                                    strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["DropCourseNumber"].ToString() + "</td></tr>";
                                    strBody += "<tr><td style='width:30%'><b>Class Number: </b></td>";
                                    strBody += "<td style='width:70%'>" + dtOneStudent.Rows[0]["DropClassNumber"].ToString() + "</td></tr>";
                                }
                                strBody += "</tbody></table>";
                                // send the email
                                strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);
                                if (strEmailResult != "Success")
                                {
                                    lblReportErrMsg.Text = "<span style='color:#DD0000;'>ERROR! The following email was NOT sent for student: " + dtOneStudent.Rows[0]["First_Name"].ToString() + " " + dtOneStudent.Rows[0]["Last_Name"].ToString() + "</span><br />";
                                }
                                else
                                {
                                    int intResult;
                                    //update the record to Processed = 'Yes'
                                    if (strSite == "SFCC")
                                    {
                                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                                            "SET Processed = '{0}', FinalComment = '{1}' " +
                                            "WHERE LateEnrollmentFormID = {2}", "Yes", txtComment, formID);
                                    } else
                                    {
                                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                                           "SET Processed = '{0}', FinalComment = '{1}' " +
                                           "WHERE LateEnrollmentForm_SCCID = {2}", "Yes", txtComment, formID);
                                    }
                                    intResult = leInfo.runSPQuery(strSQL, false);
                                }
                                strBody = "";
                            } // have at least one record - should be only one
                        }// record found already processed so skipped
                    }// dataTable is NOT null
                }// end check box is checked
            }
             
        }// end loop
        if (strSite == "SFCC")
        {
            // Response.Redirect("https://sfcc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Late-Enrollment-Reports");
            Response.Redirect(Request.RawUrl);
        }
        else
        {
           // Response.Redirect("https://shared.spokane.edu/TEST/test-page2.aspx");
            Response.Redirect(Request.RawUrl);
        }
    }// end Process

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        ReportBody.Visible = true;
        pnlReport.Visible = false;
        deleteBtn.Visible = false;
        pnlDeleted.Visible = true;
        Label litDeleted = new Label();
        litDeleted.Text = "";
        int controlCount = pnlReport.Controls.Count;
        int intResult;
        CheckBox ck;
        string strSQL = "";
        int formID;
        bool isInt = false;

        Utility uEmail = new Utility();
        if (strSite == "SFCC")
            sentFrom = "SFCCInfo@sfcc.spokane.edu";
        else
            sentFrom = "SCCInfo@scc.spokane.edu";
        strSubject = "Late Enrollment duplicate records deleted";
        strCCaddresses = "";

        // generate the email
        if (strSite == "SFCC")
            sendTo = "geoff.lake@sfcc.spokane.edu";
        //   sendTo = String.Format("Erika.Naccarato@scc.spokane.edu, {0}, {1}, {2}", dtOneStudent.Rows[0]["Email_Address"].ToString(), dtOneStudent.Rows[0]["InstructorEmail"].ToString(), dtOneStudent.Rows[0]["DeanEmail"].ToString());

        strBody = "<h2>Deleted Records</h2>";
        strBody += "<table width='95%'><tbody>";
        strBody += "<table><tbody><tr><th style='width:25%'>Form ID</th><th style='width:25%'>Student ID</th><th style='width:25%'>Student Name</th><th style='width:25%'>&nbsp;</th><tr>";

     //   litReportTitle.Text = "<h2>Deleted Records</h2>";
     //   litDeleted.Text = "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Form ID</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student ID</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student Name</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>&nbsp;</b></p></div></div>";
        for (int x = 0; x < controlCount; x++)
        {
            if (pnlReport.Controls[x] is CheckBox)
            {
                ck = (CheckBox)pnlReport.Controls[x];
                //    Response.Write("id: " + ck.ID + "; " + ck.Checked.ToString() + "; ");
                if (ck.Checked)
                {
                    // get the LateEnrollmentFormID from the checkbox id
                    string[] idPieces = ck.ID.Split('|');
                    string tableID = idPieces[1];

                    // pull the record from the db to know who to send it to
                    isInt = int.TryParse(tableID, out formID);
                    if (strSite == "SFCC")
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " + 
                        "WHERE LateEnrollmentFormID = {0}", formID);
                    } else
                    {
                        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                        "WHERE LateEnrollmentForm_SCCID = {0}", formID);
                    }
                    DataTable dtDeleteStudent = leInfo.RunGetQuery(strSQL);
                    if (dtDeleteStudent != null)
                    {
                        if (dtDeleteStudent.Rows.Count > 0)
                        {
                            if(strSite == "SFCC")
                                strBody += "<tr><td style='width:25%;'><p>" + dtDeleteStudent.Rows[0]["LateEnrollmentFormID"].ToString() + "</p></td><td style='width:25%;'><p>";
                            else
                                strBody += strBody += "<tr><td style='width:25%;'><p>" + dtDeleteStudent.Rows[0]["LateEnrollmentForm_SCCID"].ToString() + "</p></td><td style='width:25%;'><p>";
                            strBody += dtDeleteStudent.Rows[0]["ctcLinkID"].ToString() + "</p></td><td style='width:25%;'><p>" + dtDeleteStudent.Rows[0]["First_Name"].ToString() + " " + dtDeleteStudent.Rows[0]["Last_Name"].ToString() + "</p></td><td style='width:25%;'><p><b>&nbsp;</b></p></td></tr>";
                            //  litDeleted.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>" + dtDeleteStudent.Rows[0]["LateEnrollmentFormID"].ToString() + "</p></div><div class='col-md-3'><p>";
                            //  litDeleted.Text += dtDeleteStudent.Rows[0]["ctcLinkID"].ToString() + "</p></div><div class='col-md-3'><p>" + dtDeleteStudent.Rows[0]["First_Name"].ToString() + " " + dtDeleteStudent.Rows[0]["Last_Name"].ToString() + "</p></div><div class='col-md-3''><p><b>&nbsp;</b></p></div></div>";
                            // Response.Write("formID: " + dtDeleteStudent.Rows[0]["LateEnrollmentFormID"].ToString() + "; ctcLinkID: " + dtDeleteStudent.Rows[0]["ctcLinkID"].ToString() + "; Name: " + dtDeleteStudent.Rows[0]["First_Name"].ToString() + " " + dtDeleteStudent.Rows[0]["Last_Name"].ToString() + "<br />");
                            if(strSite == "SFCC") { 
                            strSQL = String.Format("DELETE FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                               "WHERE LateEnrollmentFormID = {0}", formID);
                            } else
                            {
                                strSQL = String.Format("DELETE FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                               "WHERE LateEnrollmentForm_SCCID = {0}", formID);
                            }
                            intResult = leInfo.runSPQuery(strSQL, false);
                        }
                        else
                        {
                            // no records found
                            strBody = "<tr><td colspan='4'><p>No Records Found for Form ID: " + formID.ToString() + ".</p></td></tr>";
                        }
                    }// end datatable is not null
                 }// end checked
            }// end is a check box
        }// end for all controls loop
         //  pnlDeleted.Controls.Add(litDeleted);
         //  pnlReport.Controls.Clear();
        strBody += "</tbody></table>";
        // send the email
        strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);
        if (strEmailResult != "Success")
        {
            lblReportErrMsg.Text = "<span style='color:#DD0000;'>ERROR! The List of Deleted Duplicates email was NOT sent<br />";
        }
        else
        {
            if (strSite == "SFCC")
            {
                // Response.Redirect("https://sfcc.spokane.edu/For-Our-Students/Getting-Help/Late-Enrollment/Late-Enrollment-Reports");
                Response.Redirect(Request.RawUrl);
            }
            else
                Response.Redirect("https://shared.spokane.edu/TEST/test-page2.aspx");
        }

    }// end btnDelete_Click

    public void printNoShows(DataTable dtSentIn)
    {
        DateTime dteShortDate;
        string strShortDate = "";
        string emailCCnoShows = "";
        Label litBodyNoShow = new Label();
        litBodyNoShow.Text = "";
        pnlReport.Controls.Clear();
        //  btnDelete.Visible = false;

        for (int i = 0; i < dtSentIn.Rows.Count; i++)
        {
            if (i != 0)
            {
                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-2'><br /></div><div class='col-md-4'><br /></div></div>";
            }
            if (strSite == "SFCC" || (strSite == "SCC" && dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "") || (strSite=="SCC" && (dtSentIn.Rows[i]["OnlineClass"].ToString()=="Yes" && dtSentIn.Rows[i]["DeanApproveDeny"].ToString() == "")) || (day6Plus && dtSentIn.Rows[i]["VPApproveDeny"].ToString() == ""))
            {
                dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FormInserted"].ToString());
                strShortDate = dteShortDate.ToShortDateString();
                // construct the email link
                if (dtSentIn.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") != "")
                {
                    emailCCnoShows = "&cc=" + dtSentIn.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") + ";";
                    if (dtSentIn.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") != "")
                        emailCCnoShows += dtSentIn.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") + ";";
                }
                /*
                if(emailCCnoShows.Length > 0)
                    litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Enroll Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Drop Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Faculty/Dean</b></p><p><a href='mailto:" + dtNoShow.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCnoShows + "'>Communicate</a></b></p>";
                else
                    litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Enroll Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Drop Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Faculty/Dean</b></p><p><a href='mailto:" + dtNoShow.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCnoShows + "'>Communicate</a></b></p>";           
                litBodyNoShow.Text += "</div></div>";
                */
                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Enroll Class</b></p></div><div class='col-md-2' style='background-color:#DDDDDD;'><p><b>Drop Class</b></p></div><div class='col-md-4' style='background-color:#DDDDDD;'><p><b>Faculty/Dean</b></p></div></div>";
                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>&nbsp;</p></div><div class='col-md-3'><p>&nbsp;</p></div><div class='col-md-2'><p>&nbsp;</p></div>";
                if (emailCCnoShows.Length > 0)
                {
                    litBodyNoShow.Text += "<div class='col-md-4'><p><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCnoShows + "'><b>Communicate</a></b></p>";
                    //  litBodyNoShow.Text += "<div class='col-md-3'><p><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request&body=formid is: i=" + dtSentIn.Rows[i]["LateEnrollmentFormID"].ToString() + emailCCnoShows + "'><b>Communicate</a></b></p>";
                }
                else
                    litBodyNoShow.Text += "<div class='col-md-4'><p><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request'><b>Communicate</a></b></p>";
                litBodyNoShow.Text += "</div></div>";


                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>" + dtSentIn.Rows[i]["First_Name"].ToString() + " " + dtSentIn.Rows[i]["Last_Name"].ToString() + " (" + dtSentIn.Rows[i]["ctcLinkID"].ToString() + ")" + "<br />";
                litBodyNoShow.Text += "<a href='mailto: " + dtSentIn.Rows[i]["Email_Address"].ToString() + "'>" + dtSentIn.Rows[i]["Email_Address"].ToString() + "</a><br />";
                litBodyNoShow.Text += "<b>Form Submitted</b>: <span style='color:#DD0000;'>";
                dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FormInserted"].ToString());
                strShortDate = dteShortDate.ToShortDateString();
                litBodyNoShow.Text += strShortDate + "</span><br /><br /></p></div>";

                litBodyNoShow.Text += "<div class='col-md-3'><p>" + dtSentIn.Rows[i]["CourseName"].ToString() + "<br />";
                litBodyNoShow.Text += dtSentIn.Rows[i]["CourseNumber"].ToString() + "<br />";
                litBodyNoShow.Text += dtSentIn.Rows[i]["ClassNumber"].ToString() + "<br />";
                if (strSite == "SCC")
                    litBodyNoShow.Text += "<b>Online Course?</b>&nbsp;&nbsp;" + dtSentIn.Rows[i]["OnlineClass"].ToString() + "<br />";
                if (dtSentIn.Rows[i]["FirstDateAttend"].ToString() != "")
                {
                    litBodyNoShow.Text += "<b>First attend:</b> ";
                    dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FirstDateAttend"].ToString());
                    strShortDate = dteShortDate.ToShortDateString();
                    litBodyNoShow.Text += strShortDate + "<br />";
                }
                litBodyNoShow.Text += "</p></div>";

                litBodyNoShow.Text += "<div class='col-md-2'><p>";
                if (dtSentIn.Rows[i]["DropClass"].ToString() == "Yes")
                {
                    // dropping a class
                    litBodyNoShow.Text += dtSentIn.Rows[i]["DropCourseDescription"].ToString() + "<br />";
                    litBodyNoShow.Text += dtSentIn.Rows[i]["DropCourseNumber"].ToString() + "<br />";
                    litBodyNoShow.Text += dtSentIn.Rows[i]["DropClassNumber"].ToString() + "<br />";
                }
                else
                {
                    //NOT dropping a class
                    litBodyNoShow.Text += "<p>No class to drop.";
                }
                litBodyNoShow.Text += "</p></div>";

                litBodyNoShow.Text += "<div class='col-md-4'><p><b></b>";
                if (strSite == "SFCC")
                {
                    if (dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "")
                    {
                        // faculty has not responded
                        litBodyNoShow.Text += "Faculty has yet to respond:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                    }
                    else if (dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "Approved")
                    {
                        // faculty approved, dean has not responded
                        litBodyNoShow.Text += "Faculty approved:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                        litBodyNoShow.Text += "Dean has yet to respond:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["DeanName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "'>" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "</a><br />";
                    }
                    litBodyNoShow.Text += "</p></div></div>";
                }
                else
                {
                    // SCC
                    if (dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "")
                    {
                        // faculty has not responded
                        litBodyNoShow.Text += "Faculty has yet to respond:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                    }
                    else if (dtSentIn.Rows[i]["OnlineClass"].ToString() == "Yes" && dtSentIn.Rows[i]["DeanApproveDeny"].ToString() != "Approved")
                    {
                        // faculty approved, dean has not responded
                        litBodyNoShow.Text += "Faculty approved:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                        litBodyNoShow.Text += "Dean has yet to respond:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["DeanName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "'>" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "</a><br />";
                    }
                    else if (day6Plus && dtSentIn.Rows[i]["VPApproveDeny"].ToString() == "")
                    {
                        // days 6+
                        litBodyNoShow.Text += "Faculty approved:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                        litBodyNoShow.Text += "VP has yet to respond:<br />";
                        litBodyNoShow.Text += dtSentIn.Rows[i]["VPName"].ToString() + "<br />";
                        litBodyNoShow.Text += "<a href='mailto:" + dtSentIn.Rows[i]["VPEmail"].ToString() + "'>" + dtSentIn.Rows[i]["VPEmail"].ToString() + "</a><br />";
                    }
                    litBodyNoShow.Text += "</p></div></div>";
                }
            } // end SCC tests
            pnlReport.Controls.Add(litBodyNoShow);
            ReportBody.Visible = true;
            ReportHead.Visible = true;
            processBtn.Visible = false;
        }
    }
    public void printReport(DataTable dtData)
    {
        DateTime dteShortDate;
        string strShortDate = "";
        string emailCCs = "";

        // print routine for SFCC
        // will skip SCC UNLESS days 1-5 and online class since that will display like all SFCC requests
        for (int i = 0; i < dtData.Rows.Count; i++)
        {
            if (strSite == "SFCC")
            {
                if ((strSite == "SFCC" && dtData.Rows[i]["DeanApproveDeny"].ToString() != "Denied") || (strSite == "SCC" && (dtData.Rows[i]["DeanApproveDeny"].ToString() != "" || dtData.Rows[i]["DeanApproveDeny"].ToString() != "Denied")))
                {
                    Label litBody = new Label();
                    litBody.Text = "";
                    if (i != 0)
                    {
                        litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div></div>";
                    }
                    dteShortDate = Convert.ToDateTime(dtData.Rows[i]["FormInserted"].ToString());
                    strShortDate = dteShortDate.ToShortDateString();
                    if (dtData.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") != "")
                    {
                        emailCCs = "&cc=" + dtData.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") + ";";
                        if (dtData.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") != "")
                        {
                            emailCCs += dtData.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'");
                            emailCCs = emailCCs.Replace(" ", "");
                        }
                        if (strSite == "SCC")
                        {
                            if (dtData.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'") != "")
                            {
                                emailCCs += dtData.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'");
                                emailCCs = emailCCs.Replace(" ", "");
                            }
                        }
                    }
                    if (emailCCs.Length > 0)
                        litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtData.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCs + "'>Communicate</a></b></p></div></div>";
                    else
                        litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtData.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request'>Communicate</a></b></p></div></div>";
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["ctcLinkID"].ToString() + "</p></div>";
                    litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["First_Name"].ToString().Replace("''", "'") + " " + dtData.Rows[i]["Last_Name"].ToString().Replace("''", "'") + "</p></div>";
                    litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["Email_Address"].ToString().Replace("''", "'") + "</p></div>";
                    if (dtData.Rows[i]["AgreePayTerms"].ToString() == "True")
                        litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Agree to payment Terms</p></div></div>";
                    else
                        litBody.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Does NOT agree to payment Terms</p></div></div>";
                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Class Information</b></p></div><div class='col-md-3'><p><b>Instructor Response</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Dean Response</b></p></div><div class='col-md-3'><p><b>Office</b></p></div></div>";

                    litBody.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Course to enroll:</b><br />" + dtData.Rows[i]["CourseName"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["CourseNumber"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["ClassNumber"].ToString() + "<br />";
                    if (strSite == "SCC")
                        litBody.Text += "<b>Online Course?</b>&nbsp;&nbsp;" + dtData.Rows[i]["OnlineClass"].ToString() + "<br />";
                    if (dtData.Rows[i]["FirstDateAttend"].ToString() != "")
                    {
                        litBody.Text += "First attend: ";
                        dteShortDate = Convert.ToDateTime(dtData.Rows[i]["FirstDateAttend"].ToString());
                        strShortDate = dteShortDate.ToShortDateString();
                        litBody.Text += strShortDate + "<br />";
                    }
                    if (dtData.Rows[i]["DropClass"].ToString() == "Yes")
                    {
                        litBody.Text += "<b>Course to drop:</b><br />";
                        litBody.Text += dtData.Rows[i]["DropCourseDescription"].ToString() + "<br />";
                        litBody.Text += dtData.Rows[i]["DropCourseNumber"].ToString() + "<br />";
                        litBody.Text += dtData.Rows[i]["DropClassNumber"].ToString() + "<br />";
                        litBody.Text += "</p></div>";
                    }
                    else
                    {
                        litBody.Text += "<br /></p></div>";
                    }

                    litBody.Text += "<div class='col-md-3'><p>" + dtData.Rows[i]["InstructorName"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["InstructorEmail"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["FacApproveDeny"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["WeNeedABrief"].ToString() + "</p></div>";

                    litBody.Text += "<div class='col-md-3' style='background-color:#DDDDDD;'><p>" + dtData.Rows[i]["DeanName"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["DeanEmail"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["DeanApproveDeny"].ToString() + "<br />";
                    litBody.Text += dtData.Rows[i]["DeanDenialReason"].ToString().Replace("''", "'") + "</p></div>";

                    //   litBody.Text += "<div class='col-md-1'><p>" + dtStuInfo.Rows[i]["Process"].ToString() + "</p></div>";
                    addBoxes(dtData, i, litBody, 4);
                    /*
                    string ctcID = "";
                    if (strSite == "SFCC")
                        ctcID = "chk | " + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + " | " + dtData.Rows[i]["ctcLinkID"].ToString();
                    else
                        ctcID = "chk | " + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + " | " + dtData.Rows[i]["ctcLinkID"].ToString();
                    CheckBox chk = new CheckBox();
                    if (strSite == "SFCC")
                        chk.ID = "chk|" + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
                    else
                        chk.ID = "chk|" + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
                    chk.EnableViewState = true;
                    if (dtData.Rows[i]["Processed"].ToString().Trim() == "Yes")
                        chk.Checked = true;
                    litBody.Text += "<div class='col-md-3' style='align:left;'><p>Processed:&nbsp;&nbsp;";

                    pnlReport.Controls.Add(litBody);
                    pnlReport.Controls.Add(chk);
                    TextBox txtB = new TextBox();
                    if (strSite == "SFCC")
                        txtB.ID = "txtBox|" + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
                    else
                        txtB.ID = "txtBox|" + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
                    txtB.TextMode = TextBoxMode.MultiLine;
                    txtB.Rows = 4;
                    if (dtData.Rows[i]["FinalComment"].ToString() != "")
                        txtB.Text = dtData.Rows[i]["FinalComment"].ToString().Replace("''", "'");
                    txtB.EnableViewState = true;
                    pnlReport.Controls.Add(txtB);
                    Label litBody2 = new Label();
                    litBody2.Text = "</p></div></div>";

                    pnlReport.Controls.Add(litBody2);
                    */
                }// if dean denied then skip printing
            }// SCC didn't qualify
        } // end loop through data set whether SCC or SFCC 
        if(strSite == "SCC") {    
        // SCC
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                
                    // SCC, online and 6+ days so need another column - Insturctor, Dean and VP
                    Label lit5Cols = new Label();
                    lit5Cols.Text = "";
                lit5Cols.Text = "<style>p {font-size: 1.2rem;}</style>";
                if (i != 0)
                    {
                        lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2'><br /></div><div class='col-md-3'><br /></div><div class='col-md-2'><br /></div><div class='col-md-3'><br /></div><div class='col-md-2'><br /></div></div>";
                    }
                    dteShortDate = Convert.ToDateTime(dtData.Rows[i]["FormInserted"].ToString());
                    strShortDate = dteShortDate.ToShortDateString();
                    if (dtData.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") != "")
                    {
                        emailCCs = "&cc=" + dtData.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") + ";";
                        if (dtData.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") != "")
                        {
                            emailCCs += dtData.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'");
                            emailCCs = emailCCs.Replace(" ", "");
                        }
                        if (dtData.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'") != "")
                        {
                            emailCCs += dtData.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'");
                            emailCCs = emailCCs.Replace(" ", "");
                        }
                    }
                    if (emailCCs.Length > 0)
                        lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-2' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-2' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtData.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCs + "'>Communicate</a></b></p></div></div>";
                    else
                        lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-2' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-2' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtData.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request'>Communicate</a></b></p></div></div>";
                    if ((dtData.Rows[i]["FacApproveDeny"].ToString() != "Denied") && (dtData.Rows[i]["DeanApproveDeny"].ToString() != "Denied") && (dtData.Rows[i]["VPApproveDeny"].ToString() != "Denied"))
                    {
                        // continue 5-column layout
                        lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["ctcLinkID"].ToString() + "</p></div>";
                        lit5Cols.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["First_Name"].ToString().Replace("''", "'") + " " + dtData.Rows[i]["Last_Name"].ToString().Replace("''", "'") + "</p></div>";
                        lit5Cols.Text += "<div class='col-md-2' style='background-color: #d3e8fb'><p>" + dtData.Rows[i]["Email_Address"].ToString().Replace("''", "'") + "</p></div>";       
                        if (dtData.Rows[i]["AgreePayTerms"].ToString() == "True")
                            lit5Cols.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Agrees to payment Terms</p></div>";
                        else
                            lit5Cols.Text += "<div class='col-md-3' style='background-color: #d3e8fb'><p>Does NOT agree to payment Terms</p></div>";
                    lit5Cols.Text += "<div class='col-md-2' style='background-color: #d3e8fb'><p>&nbsp;</p></div></div>";

                    lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2' style='background-color:#DDDDDD;'><p><b>Class Information</b></p></div><div class='col-md-3'><p><b>Instructor Response</b></p></div><div class='col-md-2' style='background-color:#DDDDDD;'><p><b>Dean Response</b></p></div><div class='col-md-3'><p><b>VP Response</b></p></div><div class='col-md-2' style='background-color:#DDDDDD;'><p><b>Office</b></p></div></div>";
                        lit5Cols.Text += "<div class='cs_row col-lg-12'><div class='col-md-2' style='background-color:#DDDDDD;'><p><b>Course to enroll:</b><br />" + dtData.Rows[i]["CourseName"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["CourseNumber"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["ClassNumber"].ToString() + "<br />";
                        lit5Cols.Text += "<b>Online Course?</b>&nbsp;&nbsp;" + dtData.Rows[i]["OnlineClass"].ToString() + "<br />";
                        if (dtData.Rows[i]["FirstDateAttend"].ToString() != "")
                        {
                            lit5Cols.Text += "First attend: ";
                            dteShortDate = Convert.ToDateTime(dtData.Rows[i]["FirstDateAttend"].ToString());
                            strShortDate = dteShortDate.ToShortDateString();
                            lit5Cols.Text += strShortDate + "<br />";
                        }
                        if (dtData.Rows[i]["DropClass"].ToString() == "Yes")
                        {
                            lit5Cols.Text += "<b>Course to drop:</b><br />";
                            lit5Cols.Text += dtData.Rows[i]["DropCourseDescription"].ToString() + "<br />";
                            lit5Cols.Text += dtData.Rows[i]["DropCourseNumber"].ToString() + "<br />";
                            lit5Cols.Text += dtData.Rows[i]["DropClassNumber"].ToString() + "<br />";
                            lit5Cols.Text += "</p></div>";
                        }
                        else
                        {
                            lit5Cols.Text += "<br /></p></div>";
                        }
                        lit5Cols.Text += "<div class='col-md-3'><p>" + dtData.Rows[i]["InstructorName"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["InstructorEmail"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["FacApproveDeny"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["WeNeedABrief"].ToString() + "</p></div>";

                        lit5Cols.Text += "<div class='col-md-2' style='background-color:#DDDDDD;'><p>" + dtData.Rows[i]["DeanName"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["DeanEmail"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["DeanApproveDeny"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["DeanDenialReason"].ToString().Replace("''", "'") + "</p></div>";

                    lit5Cols.Text += "<div class='col-md-3'><p>";
                    if (day6Plus)
                    {
                        lit5Cols.Text += dtData.Rows[i]["VPName"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["VPEmail"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["VPApproveDeny"].ToString() + "<br />";
                        lit5Cols.Text += dtData.Rows[i]["VPDenialReason"].ToString().Replace("''", "'") + "</p></div>";
                    } else
                    {
                        // days 1-5 so VP not applicable
                        lit5Cols.Text += "VP approval not applicable</p></div>";
                    }

                        addBoxes(dtData, i, lit5Cols, 5);
                    }
             //   } // end 5 column layout
            } // loop through the dataset
        } // end SCC print routine

        if (reportSummary)
        {
            ReportBody.Visible = true;
            ReportHead.Visible = true;
            processBtn.Visible = true;
        }
        int controlCount = pnlReport.Controls.Count;
    }


    public void exportData()
    {
       // string CCSInternalSharePath;
        String strUploadURL = @"\\ccs-kentico\FilesFromWeb\LateEnrollment\";
        DateTime currDay = DateTime.Now;
        string oneLine = "";
        string currHour = currDay.Hour.ToString();
        string currMin = currDay.Minute.ToString();
        StreamWriter sw = null;
        FileStream fs = null;

        //email fields:
        string sendTo = "";
        string sentFrom = "";
        string strSubj = "";
        string strExport = "";
        string strCCaddresses = "";
        string strEmailResult = "";
        string shortDate = "";

        string filePath = strUploadURL + currDay.Month + "_" + currDay.Day + "_" + currDay.Year + "_" + currHour + "_" + currMin + ".txt";
        if (!File.Exists(filePath))
        {
            fs = File.Open(filePath, FileMode.Create, FileAccess.ReadWrite);
            sw = new StreamWriter(fs);
            sw.Flush();
            // write column headings to file
            if (strSite == "SCC")
            {
                oneLine = "ctcLinkID|First name|Last name|Email|Pay|Reason|Course Name|Course Number|Class Number|Online|First Attend|";
                oneLine += "Drop Class|Drop Description|Drop Course Number|Drop Class Number|Instructor Name|Instructor Email|Approve/Deny|Instructor Comment|Dean Name|Dean Email|Approve/Deny|Dean Comment|";
                oneLine += "VP Name|VP Email|Approve/Deny|VP Comment|";
                oneLine += "Status|Processed|Final Comment|Form Submitted";
            } else
            {
                // SFCC
                oneLine = "ctcLinkID|First name|Last name|Email|Pay|Reason|Course Name|Course Number|Class Number|First Attend|";
                oneLine += "Drop Class|Drop Description|Drop Course Number|Drop Class Number|Instructor Name|Instructor Email|Approve/Deny|Instructor Comment|Dean Name|Dean Email|Approve/Deny|Dean Comment|";
                oneLine += "Status|Processed|Final Comment|Form Submitted";
            }
            sw.WriteLine(oneLine);
            sw.Flush();

            strExport = "<p>EXPORT FILE ATTACHED</p>";
            strExport += "<p>Please save the attached file and import into Excel.</p><br />";

            /* FOR TESTING --> show contents of file as well 
            strExport += oneLine; */
            oneLine = "";

            // get all the records no matter what
            // report all records
            if(strSite == "SFCC") { 
            strSQL = String.Format("SELECT FormInserted, ctcLinkID, First_Name, Last_Name, Email_Address, replace(replace(HowDoYouPlanToPay,char(10),''),char(13),'') AS HowPay, replace(replace(ReasonForLateEnrollment,char(10),''),char(13),'') AS ReasonLate, " +
                "replace(replace(WeNeedABrief,char(10),''),char(13),'') AS FacDeny, FacApproveDeny, DeanApproveDeny, Category, Program, CourseNumber, ClassNumber, InstructorName, InstructorEmail, AgreePayTerms, " +
                "DeanName, DeanEmail, replace(replace(DeanDenialReason,char(10),''),char(13),'') AS DeanDeny, CourseName, FirstDateAttend, Processed, Status, DropClass, DropCourseDescription, DropCourseNumber, DropClassNumber, replace(replace(FinalComment,char(10),''),char(13),'') AS FinalC " +
                "FROM [KenticoCCSNew].[dbo].[Form_SFCC_LateEnrollmentForm] " +
                "ORDER BY FormInserted DESC");
            } else
            {
                strSQL = String.Format("SELECT FormInserted, ctcLinkID, First_Name, Last_Name, Email_Address, replace(replace(HowDoYouPlanToPay,char(10),''),char(13),'') AS HowPay, replace(replace(ReasonForLateEnrollment,char(10),''),char(13),'') AS ReasonLate, " +
                "replace(replace(WeNeedABrief,char(10),''),char(13),'') AS FacDeny, FacApproveDeny, DeanApproveDeny, Category, Program, CourseNumber, ClassNumber, OnlineClass, InstructorName, InstructorEmail, AgreePayTerms, " +
                "DeanName, DeanEmail, replace(replace(DeanDenialReason,char(10),''),char(13),'') AS DeanDeny, CourseName, FirstDateAttend, Processed, Status, DropClass, DropCourseDescription, DropCourseNumber, DropClassNumber, replace(replace(FinalComment,char(10),''),char(13),'') AS FinalC, " +
                "VPApproveDeny, replace(replace(VPDenialReason,char(10),''),char(13),'') AS VPDeny, VPName, VPEmail " +
                "FROM [KenticoCCSNew].[dbo].[Form_SCC_LateEnrollmentForm] " +
                "ORDER BY FormInserted DESC");
            }
            DataTable dtAllStuInfo = leInfo.RunGetQuery(strSQL);
            if (dtAllStuInfo != null)
            {
                if (dtAllStuInfo.Rows.Count > 0)
                {
                    foreach (DataRow oneRow in dtAllStuInfo.Rows)
                    {
                        shortDate = Convert.ToDateTime(oneRow["FormInserted"].ToString()).ToShortDateString();
                        oneLine = oneRow["ctcLinkID"].ToString() + "|" + oneRow["First_Name"].ToString() + "|" + oneRow["Last_Name"].ToString() + "|" + oneRow["Email_Address"].ToString() + "|" + oneRow["AgreePayTerms"].ToString() + "|";
                        oneLine += oneRow["ReasonLate"].ToString() + "|" + oneRow["CourseName"].ToString() + "|" + oneRow["CourseNumber"].ToString() + "|" + oneRow["ClassNumber"].ToString() + "|";
                        if(strSite == "SCC")
                            oneLine += oneRow["OnlineClass"].ToString() + "|";
                        if (oneRow["FirstDateAttend"].ToString() == "")
                            oneLine += "" + "|";
                        else
                            oneLine += Convert.ToDateTime(oneRow["FirstDateAttend"].ToString()).ToShortDateString() + "|";
                        oneLine += oneRow["DropClass"].ToString() + "|" + oneRow["DropCourseDescription"].ToString() + "|" + oneRow["DropCourseNumber"].ToString() + "|" + oneRow["DropClassNumber"].ToString() + "|" + oneRow["InstructorName"].ToString() + "|";
                        oneLine += oneRow["InstructorEmail"].ToString() + "|" + oneRow["FacApproveDeny"].ToString() + "|" + oneRow["FacDeny"].ToString() + "|" + oneRow["DeanName"].ToString() + "|" + oneRow["DeanEmail"].ToString() + "|";
                        oneLine += oneRow["DeanApproveDeny"].ToString() + "|" + oneRow["DeanDeny"].ToString() + "|";
                        if (strSite == "SCC")
                            oneLine += oneRow["VPName"].ToString() + "|" + oneRow["VPEmail"].ToString() + "|" + oneRow["VPApproveDeny"].ToString() + "|" + oneRow["VPDeny"].ToString() + "|";
                        oneLine += oneRow["Status"].ToString() + "|" + oneRow["Processed"].ToString() + "|" + oneRow["FinalC"].ToString() + "|" + shortDate;

                        sw.WriteLine(oneLine);
                        sw.Flush();

                        /* FOR TESTING --> show contents of file as well 
                        strExport += oneLine; */
            
                    }
                } else
                {
                    // no data found
                    fs.Close();
                    sw = null;
                    litMessage.Text = "No data found";
                    return;
                }
                fs.Close();
                sw = null;

                Utility uEmail = new Utility();
                if (strSite == "SFCC")
                {
                    sendTo = "Geoff.Lake@sfcc.spokane.edu";
                    sentFrom = "LateEnrollment@ccs.spokane.edu";
                } else
                {
                    sendTo = "Erika.Naccarato@scc.spokane.edu";
                  //  sendTo = "laura.padden@ccs.spokane.edu";
                    sentFrom = "LateEnrollment@ccs.spokane.edu";
                }
                strSubj = "Late Enrollment Status Report";
                strEmailResult = uEmail.SendEmailAndAttachment(sendTo, sentFrom, strSubj, false, strExport, strCCaddresses, filePath);
                litMessage.Text = "<h3>return from email:  " + strEmailResult + "</h3>";
            }// end not null
            
        }// end Export
    }

    protected void cmdBtnCreateReport_Click(object sender, EventArgs e)
    {

    }

    public void printDuplicates (DataTable dtSentIn)
    {
        DateTime dteShortDate;
        string strShortDate = "";
        string emailCCnoShows = "";
        string ctcID = "";
        pnlReport.Controls.Clear();
        deleteBtn.Visible = true;

        for (int i = 0; i < dtSentIn.Rows.Count; i++)
        {
            Label litBodyNoShow = new Label();
            litBodyNoShow.Text = "";
            litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Student</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Enroll Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Drop Class</b></p></div><div class='col-md-3' style='background-color:#DDDDDD;'><p><b>Faculty/Dean</b></p></div></div>";
            if (i != 0)
            {
                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div><div class='col-md-3'><br /></div></div>";
            }
            dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FormInserted"].ToString());
            strShortDate = dteShortDate.ToShortDateString();
            // construct the email link
            if (dtSentIn.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") != "")
            {
                emailCCnoShows = "&cc=" + dtSentIn.Rows[i]["InstructorEmail"].ToString().Trim().Replace("''", "'") + ";";
                if (dtSentIn.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") != "")
                    emailCCnoShows += "," + dtSentIn.Rows[i]["DeanEmail"].ToString().Trim().Replace("''", "'") + ";";
                if (strSite == "SCC")
                {
                    if (dtSentIn.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'") != "")
                        emailCCnoShows += "," + dtSentIn.Rows[i]["VPEmail"].ToString().Trim().Replace("''", "'") + ";";
                }
            }
            
 //           if (emailCCnoShows.Length > 0)
 //               litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCnoShows + "'>Communicate</a></b></p></div></div>";
 //           else
//                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Student Information</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>Form Submitted:  " + strShortDate + "</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b>&nbsp;</b></p></div><div class='col-md-3' style='background-color: #d3e8fb'><p><b><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request'>Communicate</a></b></p></div></div>";
         //   if (reportNoResponse)
         //       litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>&nbsp;</p></div><div class='col-md-3'><p>&nbsp;</p></div><div class='col-md-3'><p>&nbsp;</p></div>";
         //   else 
         //   if (duplicateRecords)
         //   {
                litBodyNoShow.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>";
                if (strSite == "SFCC")
                    ctcID = "chk | " + dtSentIn.Rows[i]["LateEnrollmentFormID"].ToString() + " | " + dtSentIn.Rows[i]["ctcLinkID"].ToString();
                else
                    ctcID = "chk | " + dtSentIn.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + " | " + dtSentIn.Rows[i]["ctcLinkID"].ToString();
                CheckBox chkDelete = new CheckBox();
                if (strSite == "SFCC")
                    chkDelete.ID = "chk|" + dtSentIn.Rows[i]["LateEnrollmentFormID"].ToString() + "|" + dtSentIn.Rows[i]["ctcLinkID"].ToString();
                else
                    chkDelete.ID = "chk|" + dtSentIn.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + "|" + dtSentIn.Rows[i]["ctcLinkID"].ToString();
                chkDelete.EnableViewState = true;
            chkDelete.Checked = false;
                pnlReport.Controls.Add(litBodyNoShow);
                pnlReport.Controls.Add(chkDelete);
                Label litBodyEnd = new Label();
                litBodyEnd.Text = "&nbsp;&nbsp;&nbsp;Delete Record</p></div><div class='col-md-3'><p>&nbsp;</p></div><div class='col-md-3'><p>&nbsp;</p></div>";
          //      pnlReport.Controls.Add(litBodyEnd);
         //   }
            if (emailCCnoShows.Length > 0)
                litBodyEnd.Text += "<div class='col-md-3'><p><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request" + emailCCnoShows + "'><b>Communicate</a></b></p>";
            else
                litBodyEnd.Text += "<div class='col-md-3'><p><a href='mailto:" + dtSentIn.Rows[i]["Email_Address"].ToString() + "?subject=Late Enrollment Request'><b>Communicate</a></b></p>";
            litBodyEnd.Text += "</div></div>";


            litBodyEnd.Text += "<div class='cs_row col-lg-12'><div class='col-md-3'><p>" + dtSentIn.Rows[i]["First_Name"].ToString() + " " + dtSentIn.Rows[i]["Last_Name"].ToString() + " (" + dtSentIn.Rows[i]["ctcLinkID"].ToString() + ")" + "<br />";
            litBodyEnd.Text += "<a href='mailto: " + dtSentIn.Rows[i]["Email_Address"].ToString() + "'>" + dtSentIn.Rows[i]["Email_Address"].ToString() + "</a><br />";
            litBodyEnd.Text += "<b>Form Submitted</b>: ";
            dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FormInserted"].ToString());
            strShortDate = dteShortDate.ToShortDateString();

            litBodyEnd.Text += "<span style='color:#DD0000;'>" + strShortDate + "</span><br /><br /></p></div>";

            litBodyEnd.Text += "<div class='col-md-3'><p>" + dtSentIn.Rows[i]["CourseName"].ToString() + "<br />";
            litBodyEnd.Text += dtSentIn.Rows[i]["CourseNumber"].ToString() + "<br />";
            litBodyEnd.Text += dtSentIn.Rows[i]["ClassNumber"].ToString() + "<br />";
            if (strSite == "SCC")
                litBodyEnd.Text += "<b>Online Course?</b>&nbsp;&nbsp;" + dtSentIn.Rows[i]["OnlineClass"].ToString() + "<br />";
            if (dtSentIn.Rows[i]["FirstDateAttend"].ToString() != "")
            {
                litBodyEnd.Text += "<b>First attend</b>: ";
                dteShortDate = Convert.ToDateTime(dtSentIn.Rows[i]["FirstDateAttend"].ToString());
                strShortDate = dteShortDate.ToShortDateString();
                litBodyEnd.Text += strShortDate + "<br />";
            }
            litBodyEnd.Text += "</p></div>";

            litBodyEnd.Text += "<div class='col-md-3'><p>";
            if (dtSentIn.Rows[i]["DropClass"].ToString() == "Yes")
            {
                // dropping a class
                litBodyEnd.Text += dtSentIn.Rows[i]["DropCourseDescription"].ToString() + "<br />";
                litBodyEnd.Text += dtSentIn.Rows[i]["DropCourseNumber"].ToString() + "<br />";
                litBodyEnd.Text += dtSentIn.Rows[i]["DropClassNumber"].ToString() + "<br />";
            }
            else
            {
                //NOT dropping a class
                litBodyEnd.Text += "<p>No class to drop.";
            }
            litBodyEnd.Text += "</p></div>";

            litBodyEnd.Text += "<div class='col-md-3'><p><b></b>";
            if (dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "")
            {
                // faculty has not responded
                litBodyEnd.Text += "Faculty has yet to respond:<br />";
                litBodyEnd.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                litBodyEnd.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
            }
            else if (dtSentIn.Rows[i]["FacApproveDeny"].ToString() == "Approved")
            {
                // faculty approved, dean has not responded
                litBodyEnd.Text += "Faculty approved:<br />";
                litBodyEnd.Text += dtSentIn.Rows[i]["InstructorName"].ToString() + "<br />";
                litBodyEnd.Text += "<a href='mailto:" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "'>" + dtSentIn.Rows[i]["InstructorEmail"].ToString() + "</a><br />";
                litBodyEnd.Text += "Dean has yet to respond:<br />";
                litBodyEnd.Text += dtSentIn.Rows[i]["DeanName"].ToString() + "<br />";
                litBodyEnd.Text += "<a href='mailto:" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "'>" + dtSentIn.Rows[i]["DeanEmail"].ToString() + "</a><br />";
            }
            litBodyEnd.Text += "</p></div></div>";
            pnlReport.Controls.Add(litBodyEnd);
        }
        ReportBody.Visible = true;
        ReportHead.Visible = true;
        processBtn.Visible = false;
        btnDelete.Visible = true;
    }

    private void addBoxes(DataTable dtData, int i, Label litBody, int cols)
    {
        string ctcID = "";
        if (strSite == "SFCC")
            ctcID = "chk | " + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + " | " + dtData.Rows[i]["ctcLinkID"].ToString();
        else
            ctcID = "chk | " + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + " | " + dtData.Rows[i]["ctcLinkID"].ToString();
        CheckBox chk = new CheckBox();
        if (strSite == "SFCC")
            chk.ID = "chk|" + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
        else
            chk.ID = "chk|" + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
        chk.EnableViewState = true;
        if (dtData.Rows[i]["Processed"].ToString().Trim() == "Yes")
            chk.Checked = true;
        if(strSite == "SFCC")
            litBody.Text += "<div class='col-md-3' style='align:left;'><p>Processed:&nbsp;&nbsp;";
        else
            litBody.Text += "<div class='col-md-2' style='align:left; style='color:#DD0000;'><p>Processed:&nbsp;&nbsp;";

        pnlReport.Controls.Add(litBody);
        pnlReport.Controls.Add(chk);
        TextBox txtB = new TextBox();
        if (strSite == "SFCC")
            txtB.ID = "txtBox|" + dtData.Rows[i]["LateEnrollmentFormID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
        else
            txtB.ID = "txtBox|" + dtData.Rows[i]["LateEnrollmentForm_SCCID"].ToString() + "|" + dtData.Rows[i]["ctcLinkID"].ToString();
        txtB.TextMode = TextBoxMode.MultiLine;
        txtB.Rows = 4;
        if (dtData.Rows[i]["FinalComment"].ToString() != "")
            txtB.Text = dtData.Rows[i]["FinalComment"].ToString().Replace("''", "'");
        txtB.EnableViewState = true;
        pnlReport.Controls.Add(txtB);
        Label litBody2 = new Label();
        litBody2.Text = "</p></div></div>";

        pnlReport.Controls.Add(litBody2);
    }

}
 