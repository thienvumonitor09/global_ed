﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_RaveAlertsDetailBox : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string htmlAlert = "<div id=\"raveAlertBoxBlue\">[Alert details]</div>";
        bool pullFreshAlertInfo = true;
        int secondsToWait = 60;
        int hoursToLive = 24; //24 normally
        DateTime currentTime = System.DateTime.Now;

        // Check and/or set Application variable to limit hits on remote CAP alert channel
        // Check status string to determine (based on secondsToWait) whether to refresh
        try
        {
            DateTime dtLastPulled = Convert.ToDateTime(Application["RaveChannel1DataLastPulled"].ToString());
            if (dtLastPulled.AddSeconds(secondsToWait) > currentTime)
            {
                pullFreshAlertInfo = false;
            }
        }
        catch
        {
            //If any error encountered, leave flag set to pull and store alert information
            pullFreshAlertInfo = true;
        }

        string sent = "";
        string eventname = "";
        string description = "";
        string web = "";
        string data = "";

        // IF flag is set, pull and store alert information
        if (pullFreshAlertInfo)
        {
            //XML parsing attempts have not worked with this Rave-hosted source so far...switching to barebones approach - BN 2016-01-15
            string url = "http://www.getrave.com/cap/ccs/channel1";
            WebClient wc = new WebClient();
            
            try
            {
                data = wc.DownloadString(url);
                sent = "[]";  //initial population in case of error (catch)
                sent = data.Substring(data.IndexOf("<sent>") + 6, data.IndexOf("</sent>") - data.IndexOf("<sent>") - 6);
                eventname = "Error";  //initial population in case of error (catch)
                eventname = data.Substring(data.IndexOf("<event>") + 7, data.IndexOf("</event>") - data.IndexOf("<event>") - 7);
                //eventname = "Really long and verbose event name for a Rave Mobile alert sent via CAP";
                description = "Unable to access or populate alert information"; //initial population in case of error (catch)
                description = data.Substring(data.IndexOf("<description>") + 13, data.IndexOf("</description>") - data.IndexOf("<description>") - 13);
                //description = "Lengthy description with lots of detail about the alert and its surrounding circumstances appears here and goes on and on to a surprising degree even for those who are used to long descriptions.";
                web = data.Substring(data.IndexOf("<web>") + 5, data.IndexOf("</web>") - data.IndexOf("<web>") - 5);
            }
            catch
            {
                if (sent == "[]")
                {
                    sent = System.DateTime.Now.AddHours(-96).ToString(); //if error encountered with sent value, assume old alert
                }
                if (description == "Unable to access or populate alert information")
                {
                    //empty or missing description caused error in try block
                    sent = System.DateTime.Now.AddHours(-96).ToString(); //if error encountered with description value, treat as old alert
                }
                web = "http://www.ccs.spokane.edu/alert.aspx"; //default
            }

            //Update Application variables
            Application.Lock();
            Application["RaveChannel1DataLastPulled"] = System.DateTime.Now;
            Application["RaveChannel1_sent"] = sent;
            Application["RaveChannel1_eventname"] = eventname.Trim();
            Application["RaveChannel1_description"] = description.Trim();
            Application["RaveChannel1_web"] = web.Trim();
            Application.UnLock();
        }

        bool assumeAllClear = true;

        //Finalize alert data for display
        //Is it too old? [24-hour expiry based on sent date; may need more complexity if and when start and stop datetimes are included in the alert]
        try
        {
            DateTime dtSent = Convert.ToDateTime(Application["RaveChannel1_sent"].ToString());
            if (dtSent.AddHours(hoursToLive) > currentTime)
            {
                assumeAllClear = false;
            }
        }
        catch
        {
            //If any error encountered, leave assumeAllClear set to true
            assumeAllClear = true;
        }

        if (eventname.ToLower().Trim() == "remove message")
        {
            //If "Remove Message" (any case) is the event name, leave assumeAllClear set to true
            assumeAllClear = true;
        }

        htmlAlert = htmlAlert.Replace("[Alert details]", "<h2></h2><div></div>");
     //   assumeAllClear = false;
        if (assumeAllClear)
        {
            // Build All Clear message (banner display should not occur in this case)
            htmlAlert = htmlAlert.Replace("<h2>", "<h2>OPERATIONS NORMAL");
            htmlAlert = htmlAlert.Replace("<div>", "<div>No alert is in effect at this time.");
        }
        else
        {
            // Build alert message (banner display should also occur in this case - see subpage.master.cs
            string alignSpec = "";
            if (Application["RaveChannel1_description"].ToString().Length > 80)
            {
                alignSpec = " style=\"text-align:left;\"";
            }
            htmlAlert = htmlAlert.Replace("<h2>", "<h2>" + Application["RaveChannel1_eventname"]);
            htmlAlert = htmlAlert.Replace("<div>", "<div" + alignSpec + ">" + Application["RaveChannel1_description"]).Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");
          //  htmlAlert = htmlAlert.Replace("<h2>", "<h2>" + "On No!! Something bad has happened!");
          //  htmlAlert = htmlAlert.Replace("<div>", "<div" + alignSpec + ">" + "Oh No Details. Oh No Details. Oh No Details. Oh No Details. Oh No Details. Oh No Details. ");
            htmlAlert = htmlAlert.Replace("raveAlertBoxBlue", "raveAlertBoxYellow");
        }

        // Display alert information on page
        raveContent.Text = htmlAlert;
    }
}