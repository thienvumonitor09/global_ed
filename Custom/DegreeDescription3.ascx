﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DegreeDescription3.ascx.cs" Inherits="CMSWebParts_Custom_DegreeDescription3" %>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>

<div class="container">
        <div id="degree" class="brown-border-left">
            <asp:Literal ID="litMsg" runat="server"></asp:Literal>
            <section id="quarterDDsection">
                <div class="content-block">
                    <h4>Viewing Program Version for</h4>
                    <p id="quarterDescription"><asp:DropDownList ID="quarterDD" runat="server" onchange="this.form.submit()" Style="width:205px;"></asp:DropDownList></p>
                </div>
            </section>
            <section id="degreeDescription">
                <div class="content-block">
                    <p id="description"></p>
                </div>
            </section>
            <section id="courseOfStudySection">
                <div class="content-block">
                    <h2>Course of Study</h2>
                    <p id="courseOfStudy"></p>
                </div>
            </section>
            <section id="programGoals">
                <div class="content-block">
                    <h2>Learning Outcomes</h2>
                    <p id="learningOutcomes"></p>
                </div>
            </section>
            <section id="careerOpportunitiesSection">
                <div class="content-block">
                    <h2>Career Opportunities</h2>
                    <p id="careerOpportunities"></p>
                </div>
            </section>
            <section id="schedule">
                <h2>Typical Student Schedule <span style="color:#903923;">for <asp:Literal ID="litQuarter" runat="server"></asp:Literal></span></h2>
            </section>
            <section id="disclaimer">
                <div class="content-block">
                    <p>
                        <i><span class="bold"><u>Disclaimer</u>:</span> The college cannot guarantee courses will be 
                        offered in the quarters indicated. During the period this guide is in 
                        circulation, there may be curriculum revisions and program changes. Students 
                        are responsible for consulting the appropriate academic unit or adviser for 
                        more current and specific information. The information in this guide is subject 
                        to change and does not constitute an agreement between the college and the student.</i>
                    </p>
                </div>
            </section>
        </div>
    </div>

 <!-- START Workarounds BN --> 
    <script src="/CMSWebParts/Custom/DegreeDescription_files/bluebird.min.js"></script> <!--to make promises work in IE - https://stackoverflow.com/questions/36016327/how-to-make-promises-work-in-ie11 - bn 2018-05-14 -->
 <!-- END Workarounds BN -->

    <script type="text/javascript">

        $(function (                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ) {

            //add dropdownlist change events
            //$("select[id$='quarterDD']").change(getProgramDetails);
            //$("select[id$='quarterDD']").change(getProgramAttributes);

        });

        //get query string parameters by name/key
        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

        function appendHTML(html, containerID) {
            $(html).appendTo(containerID);
        }

        //get the gainful employment disclosure
        function getGainfulEmploymentDisclosure(gainfulEmploymentID) { //change to pass in container id and section id
            //disclosure URL
            var url = "http://ccs.spokane.edu/GEDT/" + gainfulEmploymentID + "/gedt.html";
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/RemoteFileExists",
                data: { URL: url },
                dataType: "jsonp",
                async: false,
                success: function (response) {
                    //if the disclosure file exists
                    if (response == true) {
                        //append the disclosure to the gainfulEmployment section
                        var disclosure = "<p><a href='" + url + "' target='_blank'>" + url + "</a></p>";
                        $(disclosure).appendTo("#gainfulEmployment");
                    } else {
                        $("#gainfulEmploymentSection").hide(); //temporary until the GE Widget comes back
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#gainfulEmploymentSection").hide(); //temporary until the GE Widget comes back
                }
            });
        }

        function getProgramDetails(programID, strm) {
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetProgramByProgramID",
                data: { ProgramID: programID, STRM: strm },
                dataType: "jsonp",
                success: function (data) {
                    $.each(data.Table, function (index, degree) {
                        console.log(degree.ProgramTitle + ": " + degree.ProgramID + "; ");
                        //display the program title
                        //change this later to include the degree title when there is a one-to-one program-to-degree relationship
                        var programTitle = degree.ProgramTitle;
                        if (programTitle != "Associate in Arts") {
                            $(".page-title").html("<h1>" + programTitle + "</h1>");
                        } else {
                            $(".page-title").html("");
                        }

                        //display the description
                        $("#description").append(degree.ProgramDescription);

                        //display when enrollment starts
                        if (degree.ProgramEnrollment != null && degree.ProgramEnrollment != "") {
                            $("#enrollmentStart").append(degree.ProgramEnrollment)
                        } else {
                            $("#enrollmentStartSection").hide();
                        }

                        //display the program website
                        if (degree.WebsiteURL != null && degree.WebsiteURL != "") {
                            $("#websiteURL").append("<a href='" + degree.WebsiteURL + "'>" + degree.WebsiteURL + "</a>");
                        } else {
                            $("#websiteURLSection").hide();
                        }

                        //display the course of study
                        if (degree.ProgramCourseOfStudy != null && degree.ProgramCourseOfStudy != "") {
                            $("#courseOfStudy").append(degree.ProgramCourseOfStudy);
                        } else {
                            $("#courseOfStudySection").hide();
                        }

                        //display the learning outcomes
                        if (degree.ProgramGoals != null && degree.ProgramGoals != "") {
                            $("#learningOutcomes").append(degree.ProgramGoals);
                        } else {
                            $("#programGoals").hide();
                        }

                        //display the career opportunities if the gainful employment id is null
                        if (degree.ProgramCareerOpportunities != null && degree.ProgramCareerOpportunities != "") {
                            $("#careerOpportunities").append(degree.ProgramCareerOpportunities);
                        } else {
                            $("#careerOpportunitiesSection").hide(); //hide this section if the gainful employment id is null or empty in the getOption function
                        }
                    });
                }
            });
        }

        function getProgramAttributes(programID, strm) {
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetProgramAttributesByProgramID",
                data: { STRM: strm, ProgramID: programID },
                dataType: "jsonp",
                success: function (data) {

                    $("#financialAidSection").hide();
                    $("#tuitionSection").hide();

                    $.each(data.Table, function (index, program) {

                        //display financial aid eligibility
                        if (program.FinancialAidEligible == "Y") {
                            $("#financialAid").append("Yes. Read more about <a href=\"/How-to-Pay-for-College\">Financial Aid</a>.");
                            $("#financialAidSection").show();
                        } else if (program.FinancialAidEligible == "N") {
                            $("#financialAid").append("No");
                            $("#financialAidSection").show();
                        }

                        //display tuition
                        /*
                        if (program.Tuition != 0) {
                            $("#tuition").append("$" + program.Tuition);
                            $("#tuitionSection").show();
                        }
                        */
                    });
                }
            });
        }



        

        //if the document is ready
        $(document).ready(function () {
            //Degrees/Certificates Offered
            //$("#sidebarContent").append("<div class='content-block'><h4>Degrees/Certificates Offered</h4><p>Please holder for links to other degrees/certificates offered in the same area of study. If none exist, this section will be hidden.</p></div>");

            //Enrollment Start
            $("#sidebarContent").append("<div id='enrollmentStartSection' class='content-block'><h4>Start</h4><p id='enrollmentStart'></p></div>");

            //Website URL
            $("#sidebarContent").append("<div id='websiteURLSection' class='content-block'><h4>Website</h4><p id='websiteURL'></p></div>");

            //Locations Offered
            $("#sidebarContent").append("<div id='campusOfferedSection' class='content-block'><h4>Locations Offered</h4><p id='campusOffered'></p></div>");

            //Financial Aid Eligible
            $("#sidebarContent").append("<div id='financialAidSection' class='content-block'><h4>Financial Aid Eligible</h4><p id='financialAid'></p></div>");

            //What to Expect to Pay
            $("#sidebarContent").append("<div id='tuitionSection' class='content-block'><h4>What to Expect to Pay</h4><p id='tuition'></p></div>");

            //Print Button
            $("#sidebarContent").append("<div id='printButton' class='button btn-group'><a href='#'onclick='window.print();return false;'>Print this page</a></div>");

            var referrer = getParameterByName('ref');

            if(referrer != null && referrer != ""){
                //add a breadcrumb
                var baseURL = window.location.protocol + "//" + window.location.host;
                var pathArray = referrer.split('/');
                var path = "", breadcrumb = "";
                for (i = 0; i < pathArray.length; i++) {
                    if(pathArray[i] != ""){
                        path += "/" + pathArray[i];
                        breadcrumb += "<a href='" + baseURL + path + "'>" + pathArray[i].replace(/-\(\d\)/g,"").replace(/-/g," ") + "</a>"; // replace calls use Regular Expressions to get rid of numbers in parentheses and then hyphens
                        if (i != (pathArray.length-1)) {
                            breadcrumb += " > ";
                        }
                    }
                }
                $(".breadcrumb").html(breadcrumb);
            }

            //get the id passed in the query string
            var programID = getParameterByName('id');

            //get the term code passed in the query string
            var strm = getParameterByName('strm');

            //create a global array to store the degree title(s)
            var degreeTitle = [];

            // get the default list of terms. Select based on strm passed in or default to current term?

            //display the program details
            getProgramDetails(programID, $("select[id$='quarterDD']").val());

            //display the program attributes
            getProgramAttributes(programID, $("select[id$='quarterDD']").val());

            //get program degree(s) - will change to return one degree per program
            $.ajax({
                //get program degrees
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetProgramDegrees",
                data: { ProgramID: programID, STRM: $("select[id$='quarterDD']").val() },
                dataType: "jsonp"
            }).then(function (programData) {
                //get degree option data
                var promises = [];
                var degreeCount = 0;
                $.each(programData.Table, function (degreeIndex, programDegree) {
                    var promise = $.ajax({
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOption",
                        data: { OptionID: programDegree.OptionID },
                        dataType: "jsonp"
                    });
                    promises.push(promise);
                    degreeTitle.push(programDegree.DegreeLongTitle);
                    degreeCount++;
                });

                //if (degreeCount == 1) {
                    //$("#title").text(degreeTitle[0] + " - " + $("#title").text());
                //}
                if ($(".page-title").text() == "") {
                    $(".page-title").html("<h1>" + degreeTitle[0] + "</h1>");
                }else{
                    $(".page-title").html("<h1>" + degreeTitle[0] + " - " + $(".page-title").text() + "</h1>");
                }
                return Promise.all(promises);
            }).then(function (promiseData) {
                $.each(promiseData, function (promiseIndex, promise) {
                    $.each(promise.Table, function (optionIndex, option) {
                        var html = "";

                        //open option div
                        html += "<div id='option" + option.OptionID + "'>";

                        //display the option title
                        if (option.OptionTitle != null && option.OptionTitle != "") {
                            if(option.OptionTitle != "Associate in Arts"){ //temporary to show Associate in Arts degree
                                html += "<h3>" + option.OptionTitle + "</h3>";
                            }
                        }

                        //display the option description
                        if (option.OptionDescription != null && option.OptionDescription != "") {
                            html += "<div class='content-block'><p>" + option.OptionDescription + "</p></div>";
                        }

                        //display the gainful employment disclosure by calling function
                        if (option.GainfulEmploymentID != null && option.GainfulEmploymentID != "") {
                            /*
                            html += "<section id='gainfulEmploymentSection" + option.GainfulEmploymentID + "'>";
                            html += "<div id='gainfulEmployment" + option.GainfulEmploymentID + "'></div>";
                            html += "</section>";
                            */
                            $("#sidebarContent").append("<section id='gainfulEmploymentSection'><div class='content-block'><h4>Gainful Employment</h4><p id='gainfulEmployment'></p></div></section>");
                        }

                        //html += "<div id='option" + option.OptionID + "Locations'></div>";
                        html += "<div id='option" + option.OptionID + "Prerequisites'></div>";
                        html += "<div id='option" + option.OptionID + "Courses'></div>";
                        html += "<div id='option" + option.OptionID + "ElectiveGroups'></div>";
                        html += "<div id='option" + option.OptionID + "Footnotes'></div>";
                        html += "</div>";
                        appendHTML(html, "#schedule");

                        if (option.GainfulEmploymentID != null && option.GainfulEmploymentID != "") {
                            getGainfulEmploymentDisclosure(option.GainfulEmploymentID);
                        }

                        $.ajax({ //get locations
                            crossDomain: true,
                            contentType: "application/json; charset=utf-8",
                            url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOptionLocations",
                            data: { OptionID: option.OptionID },
                            dataType: "jsonp",
                            success: function (locationData) {
                                var locations = "";
                                $.each(locationData.Table, function (locationIndex, location) {
                                    locations += "<p><a href='" + location.LocationWebsiteURL + "'>" + location.LocationTitle + "</a></p>"; //should this be a list?
                                });
                                if (locations != "") {
                                    $("#campusOffered").html(locations);
                                    //appendHTML(locations, "#campusOffered");
                                } else {
                                    $("#campusOfferedSection").hide();
                                }

                            }
                        }).then( //get prerequisites
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOptionPrerequisites",
                                data: { OptionID: option.OptionID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                dataType: "jsonp",
                                success: function (prerequisiteData) {
                                    var prerequisites = "";
                                    var prerequisiteCount = 0;

                                    $.each(prerequisiteData.Table, function (prerequisiteIndex, prerequisite) {
                                        var footnoteNumber = "";
                                        if (prerequisite.FootnoteNumber != null && prerequisite.FootnoteNumber != "") {
                                            footnoteNumber = prerequisite.FootnoteNumber;
                                        }

                                        prerequisites += "<li class='course'>";
                                        //prerequisites += "<div class='column'>&mdash;&nbsp;</div>";
                                        prerequisites += "<div class='column courseOffering'>" + prerequisite.CourseOffering + "</div>";
                                        prerequisites += "<div class='column courseTitle'>" + prerequisite.COURSE_TITLE_LONG + " <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                        prerequisites += "</li>";
                                        prerequisiteCount++;
                                    });

                                    if (prerequisiteCount > 0) {
                                        prerequisites = "<div id='prerequisites'><div class='content-block'><ul class='courseBlock no-bullet'><h4>Prerequisites</h4>" + prerequisites + "</ul></div></div>";
                                    }

                                    appendHTML(prerequisites, "#option" + option.OptionID + "Prerequisites");
                                }
                            })
                        ).then( //get courses and electives
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOptionCoursesAndElectives",
                                data: { OptionID: option.OptionID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                dataType: "jsonp",
                                success: function (courseData) {
                                    var totalUnits = 0, totalUnitsMinimum = 0, totalUnitsMaximum = 0, degreeUnits = 0, degreeUnitsMinimum = 0, degreeUnitsMaximum = 0;
                                    var previousQuarter = 0;
                                    var courses = "<div class='coursesAndElectives' style='width:100%'>";
                                    $.each(courseData.Table, function (courseIndex, course) {
                                        var footnoteNumber = "";
                                        if (course.FootnoteNumber != null && course.FootnoteNumber != "") {
                                            footnoteNumber = course.FootnoteNumber;
                                        }
                                        var currentQuarter = course.Quarter;
                                        //if a prevous quarter existed and a new quarter is starting
                                        if (previousQuarter != 0 && (currentQuarter != previousQuarter)) {

                                            //display the total units for the previous course block
                                            totalUnits = totalUnitsMinimum;
                                            if (totalUnitsMinimum != totalUnitsMaximum) {
                                                totalUnits = totalUnitsMinimum + "-" + totalUnitsMaximum;
                                            }
                                            courses += "<li class='course'>";
                                            courses += "<div class='column courseOffering'></div>";
                                            courses += "<div class='column courseTitle'></div>";
                                            courses += "<div class='column courseUnits totalUnits'>" + totalUnits + "</div>";
                                            courses += "</li>";
                                            //close the previous course block div
                                            courses += "</ul>";
                                            //increment the total units for the degree
                                            degreeUnitsMinimum += totalUnitsMinimum;
                                            degreeUnitsMaximum += totalUnitsMaximum;

                                            //clear the total units for the quarter/group of courses
                                            totalUnits = 0;
                                            totalUnitsMinimum = 0;
                                            totalUnitsMaximum = 0;
                                        }

                                        //if a new course block is started
                                        if (currentQuarter != "" && (currentQuarter != previousQuarter)) {

                                            //store the quarter title
                                            if (currentQuarter == 1) {
                                                courses += "<h4>First Quarter</h4>";
                                            } else if (currentQuarter == 2) {
                                                courses += "<h4>Second Quarter</h4>";
                                            } else if (currentQuarter == 3) {
                                                courses += "<h4>Third Quarter</h4>";
                                            } else if (currentQuarter == 4) {
                                                courses += "<h4>Fourth Quarter</h4>";
                                            } else if (currentQuarter == 5) {
                                                courses += "<h4>Fifth Quarter</h4>";
                                            } else if (currentQuarter == 6) {
                                                courses += "<h4>Sixth Quarter</h4>";
                                            } else if (currentQuarter == 7) {
                                                courses += "<h4>Seventh Quarter</h4>";
                                            } else if (currentQuarter == 8) {
                                                courses += "<h4>Eighth Quarter</h4>";
                                            } else if (currentQuarter == 9) {
                                                courses += "<h4>Ninth Quarter</h4>";
                                            } else if (currentQuarter == 10) {
                                                courses += "<h4>Tenth Quarter</h4>";
                                            } else {
                                                courses += "<h4>Courses</h4>";
                                            }

                                            totalUnitsMaximum = 0;
                                            totalUnitsMaximum = 0;

                                            //create the course block div
                                            courses += "<ul class='courseBlock no-bullet'>";
                                        }

                                        var unitsMinimum = 0, unitsMaximum = 0;
                                        if (course.OverwriteUnits == 1) {
                                            unitsMinimum = course.OverwriteUnitsMinimum;
                                            unitsMaximum = course.OverwriteUnitsMaximum;
                                        } else {
                                            unitsMinimum = course.UNITS_MINIMUM;
                                            unitsMaximum = course.UNITS_MAXIMUM;
                                        }

                                        //store course credits in string as one number or a range if min and max values are different
                                        var strUnits = "";
                                        if (unitsMinimum == unitsMaximum) {
                                            strUnits = unitsMinimum;
                                            totalUnitsMinimum += unitsMinimum;
                                            totalUnitsMaximum += unitsMinimum;
                                        } else {
                                            strUnits = unitsMinimum + "-" + unitsMaximum;
                                            totalUnitsMinimum += unitsMinimum;
                                            totalUnitsMaximum += unitsMaximum;
                                        }

                                        //create the course div
                                        courses += "<li class='course'>";

                                        //course offering
                                        courses += "<div class='column courseOffering'>";
                                        if (course.CourseOffering != "ZZZ") {
                                            courses += course.CourseOffering.replace(" ", "&nbsp;");
                                        }
                                        courses += "</div>";

                                        //course title

                                        courses += "<div class='column courseTitle'>" + course.COURSE_TITLE_LONG;
                                        if (footnoteNumber != null || footnoteNumber != "") {
                                            courses += "<div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div>";
                                        }
                                        courses += "</div>";

                                        //course units
                                        courses += "<div class='column courseUnits'>" + strUnits + "</div>";

                                        //end the course div
                                        courses += "</li>";

                                        previousQuarter = currentQuarter;
                                    });

                                    //display the total units for the last quarter/group of courses
                                    totalUnits = totalUnitsMinimum;
                                    if (totalUnitsMinimum != totalUnitsMaximum) {
                                        totalUnits = totalUnitsMinimum + "-" + totalUnitsMaximum;
                                    }

                                    courses += "<li class='course'>";
                                    courses += "<div class='column courseOffering'></div>";
                                    courses += "<div class='column courseTitle'></div>";
                                    courses += "<div class='column courseUnits totalUnits'>" + totalUnits + "</div>";
                                    courses += "</li>";

                                    //close the course block div
                                    courses += "</ul>";

                                    //increment the total units for the degree
                                    degreeUnitsMinimum += totalUnitsMinimum;
                                    degreeUnitsMaximum += totalUnitsMaximum;

                                    //display total units for the degree
                                    degreeUnits = degreeUnitsMinimum;
                                    if (degreeUnitsMinimum != degreeUnitsMaximum) {
                                        degreeUnits = degreeUnitsMinimum + "-" + degreeUnitsMaximum
                                    }

                                    courses += "<div><br /><p>" + degreeUnits + " credits are required for the <span class='bookcase'>" + degreeTitle[promiseIndex] + "</span></p></div>";

                                    //close the coursesAndElectives div
                                    courses += "</div>";

                                    appendHTML(courses, "#option" + option.OptionID + "Courses");
                                }
                            })
                        ).then( //get elective groups
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOptionElectiveGroups",
                                data: { OptionID: option.OptionID },
                                dataType: "jsonp",
                                success: function (electiveGroupData) {
                                    var electiveGroups = "<div id='electiveGroup" + option.OptionID + "' class='electiveGroup'>";
                                    $.each(electiveGroupData.Table, function (index, electiveGroup) {
                                        if (electiveGroup.ElectiveCount > 0) {
                                            var footnoteNumber = "";
                                            if (electiveGroup.FootnoteNumber != null && electiveGroup.FootnoteNumber != "") {
                                                footnoteNumber = electiveGroup.FootnoteNumber;
                                            }

                                            electiveGroups += "<div class='electiveGroupTitle'><h4>" + electiveGroup.ElectiveGroupTitle + "</h4> <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                            electiveGroups += "<ul id='ulElectiveGroup" + electiveGroup.OptionElectiveGroupID + "' class='courseBlock no-bullet'>";
                                            electiveGroups += "</ul>";
                                        }
                                    });
                                    electiveGroups += "</div>";
                                    appendHTML(electiveGroups, "#option" + option.OptionID + "ElectiveGroups");
                                }
                            }).then(function (electiveGroupData) { //get elective group courses
                                $.each(electiveGroupData.Table, function (index, electiveGroup) {
                                    $.ajax({
                                        crossDomain: true,
                                        contentType: "application/json; charset=utf-8",
                                        url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetElectiveGroupCourses",
                                        data: { OptionElectiveGroupID: electiveGroup.OptionElectiveGroupID, SelectedSTRM: $("select[id$='quarterDD']").val() },
                                        dataType: "jsonp",
                                        success: function (electiveData) {
                                            $.each(electiveData.Table, function (electiveIndex, elective) {
                                                var footnoteNumber = "";
                                                if (elective.FootnoteNumber != null && elective.FootnoteNumber != "") {
                                                    footnoteNumber = elective.FootnoteNumber;
                                                }

                                                var unitsMinimum = elective.UNITS_MINIMUM;
                                                var unitsMaximum = elective.UNITS_MAXIMUM;
                                                if (elective.OverwriteUnits == 1) {
                                                    unitsMinimum = elective.OverwriteUnitsMinimum;
                                                    unitsMaximum = elective.OverwriteUnitsMaximum;
                                                }

                                                var units = unitsMinimum;
                                                if (unitsMinimum != unitsMaximum) {
                                                    units = unitsMinimum + "-" + unitsMaximum;
                                                }

                                                var electives = "<li class='course'>";
                                                electives += "<div class='column courseOffering'>" + elective.CourseOffering.replace(" ", "&nbsp;") + "</div>";
                                                electives += "<div class='column courseTitle'>" + elective.COURSE_TITLE_LONG + " <div class='footnoteNumber' style='padding-left:5px;'><i>" + footnoteNumber + "</i></div></div>";
                                                electives += "<div class='column courseUnits'>" + units + "</div>";
                                                electives += "</li>";

                                                appendHTML(electives, "#ulElectiveGroup" + electiveGroup.OptionElectiveGroupID);
                                            });
                                        }
                                    })
                                });
                            })
                        ).then(
                        //get footnotes
                            $.ajax({
                                crossDomain: true,
                                contentType: "application/json; charset=utf-8",
                                url: "//external.spokane.edu/webservices/iCatalogJSON.asmx/GetOptionFootnotes",
                                data: { OptionID: option.OptionID },
                                dataType: "jsonp",
                                success: function (data) {
                                    var footnotes = "<div class='footnotes'><div class='content-block'>";
                                    var footnoteCount = 0;
                                    $.each(data.Table, function (index, footnote) {
                                        footnoteCount++;
                                        footnotes += "<div class='footnote'><div class='footnoteNumber' style='padding-right:5px'><i>" + footnote.FootnoteNumber + "</i></div><div class='footnoteText'>" + footnote.Footnote + "</div></div>";
                                    })
                                    footnotes += "</div></div>";
                                    if (footnoteCount > 0) {
                                        $(footnotes).appendTo("#option" + option.OptionID + "Footnotes");
                                    }
                                }
                            })
                        );
                    });
                });
            });
        })

    </script>
