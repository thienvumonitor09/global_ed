<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/AreaOfStudyAtoZmarketingLandingTemp.ascx.cs" Inherits="CMSWebParts_Custom_AreaOfStudyAtoZmarketingLandingTemp" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%--<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
<input type="hidden" id="hidPostBack" value="" runat="server" />
<input type="hidden" id="hidLtr" value="" runat="server" />
<input type="hidden" id="hidInstitution" value="" runat="server" />
<div class="container areaOfStudy">
    <div class="btn-group">
        <center><asp:TextBox ID="txtTextSearch" title="Search for areas of study" runat="server" style="width:160px;" placeholder="e.g. Accounting" MaxLength="150"></asp:TextBox>
        <!--<span id="searchclear" class="glyphicon glyphicon-remove"></span>-->
        <button id="btnSearch" onclick="clearAlphaSearch();getAreasOfStudy($('input[id$=\'hidInstitution\']').val(),'',$('input[id$=\'txtTextSearch\']').val());" aria-label="Search">
            <span class="glyphicon glyphicon-search"></span>
        </button>
        </center>
    </div>
    <div class="results"><div class="working searchinfo collapse"><h2>Searching...</h2></div></div>
    <center><asp:Panel ID="panAlpha" CssClass="panAlpha" runat="server">
        <ul id="alpha" class="alpha"></ul>
    </asp:Panel>
    </center>
    <asp:Panel ID="areaOfStudyList" CssClass="areaOfStudyList" runat="server"></asp:Panel>
</div>

<script type="text/javascript">
    //if the document is ready
    $(document).ready(function () {
        var institution = $("input[id$='hidInstitution']").val();

        //populate the alpha character list
        $(".alpha").append('<li><a href="#" id="link" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'\',\'\');">ALL</a></li>');
        for (i = 0; i < 26; i++) {
            var alphaCharacter = (i+10).toString(36).toUpperCase();
            $(".alpha").append('<li><a href="#" id="link' + alphaCharacter + '" onclick="clearTextSearch();getAreasOfStudy(\'' + institution + '\',\'' + alphaCharacter + '\',\'\');">' + alphaCharacter + '</a></li>');
        }

        //if not a post back
        if($("input[id$='hidPostBack']").val() != "true"){
            //populate all areas of study by default
            getAreasOfStudy(institution, "", "");
        } else {
            //populate areas of study by search parameters
            getAreasOfStudy(institution, $("input[id$='hidLtr']").val(), $("input[id$='txtTextSearch']").val());
        }
    })

    //clear search text
    $("#searchclear").click(function () {
        $('input[id$=\'txtTextSearch\']').val('');
    });

    function getAreasOfStudy(institution, alphaCharacter, keyword) {

        $(".results .searchstat").empty();
        $(".working").fadeIn();

        var workingIndicator = setInterval(function () { $(".working h2").append(".") }, 500);

        //remove previously active letter
        $("a[id^='link']").removeClass("active");

        //make the selected letter active
        $("a[id$='link" + alphaCharacter + "']").addClass("active");

        //store the alpha character in a hidden element
        if (alphaCharacter != null && alphaCharacter != "") {
            $("input[id$='hidLtr']").val(alphaCharacter);
        }

    //    alphaCharacter = "A";
        $.ajax({
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            url: "https://external.spokane.edu/WebServices/icatalogjson.asmx/GetAreasOfStudyByKeyword",
            data: { STRM: "", Institution: institution, AlphaCharacter: alphaCharacter, Keyword: keyword },
            dataType: "jsonp",
            success: function (data) {
                var oldAreaOfStudy = "", oldAlphaCharacter = "", oldWebstieURL = "", rowCount = 0, compareCount = 0, sectionCount = 0;
                var strSchool1 = "", strSchool2 = "";
                var strLine1 = "", strLine2 = "";
                var strAllSectionRecords = "";
                var parts;
                var strPart1 = "";
                var strPart2 = "";
                var halfCount = 0, halfCountRounded;
                var moduloValue = 0;
                //clear the area of study list
                $(".areaOfStudyList").empty();
                //clear how many results were found
                //$(".results").empty();

                $.each(data.Table, function (index, areaOfStudy) {
                    var newAreaOfStudy = areaOfStudy.Title;
                    var newAlphaCharacter = newAreaOfStudy.substring(0, 1);
                    var websiteURL = areaOfStudy.WebsiteURL;
                    compareCount++;

                    //if the area of study title starts with a new alpha character
                    if (newAlphaCharacter != oldAlphaCharacter) {
                        // alert("old: " + oldAlphaCharacter + "; new: " + newAlphaCharacter + "count: " + compareCount + "; line1: " + strLine1 + "; line2: " + strLine2)
                        // count = 2 and strLine2 is blank as it hasn't been populated yet
                        // print line 1 then start the new list
                        if (compareCount == 2 && strLine2 == "") {
                            // $("#row" + oldAlphaCharacter).append(strLine1 + "</a></h2>");
                            strAllSectionRecords += strLine1 + "</a></h2>";
                            strLine1 = "";
                            compareCount = 1;
                            sectionCount++;
                        }
                        
                        // see how many records in this section
                        halfCount = sectionCount / 2;
                        halfCountRounded = Math.floor(halfCount);
                        moduloValue = sectionCount % 2;
                        if (moduloValue > 0)
                            halfCount++;
                        parts = strAllSectionRecords.split("</h2>");
                        strPart1 = parts.slice(0, halfCountRounded).join("</h2>");
                        strPart2 = parts.slice(halfCountRounded++).join("</h2>");
                        $("#row" + oldAlphaCharacter).append("<div class='col-lg-6'>" + strPart1 + "</div>");
                        $("#row" + oldAlphaCharacter).append("<div class='col-lg-6'>" + strPart2 + "</div></div>");
                        sectionCount = 0;
                        strPart2 = "";
                        strPart1 = "";
                        strAllSectionRecords = "";
                        //display the new alpha character row area of study list
                        $(".areaOfStudyList").append("<div class='row' id='row" + newAlphaCharacter + "'></div>");
                        //if the alpha character is not the first in the loop
                        if (oldAlphaCharacter != "") {
                            //display an hr after the previous alpha area of study list
                            $("#row" + newAlphaCharacter).append("<div class='col-lg-12'><hr /></div>");
                        }
                        //display the new alpha character column in the area of study list
                        $("#row" + newAlphaCharacter).append("<div class='col-lg-12 alphaCharacter'><h1>" + newAlphaCharacter + "</h1></div>");
                    }
                    if (areaOfStudy.Institution == "WA171" && compareCount == 1)
                        strSchool1 = "(SCC)";
                    if (areaOfStudy.Institution == "WA171" && compareCount == 2)
                        strSchool2 = "(SCC)";
                    if (areaOfStudy.Institution == "WA172" && compareCount == 1)
                        strSchool1 = "(SFCC)";
                    if (areaOfStudy.Institution == "WA172" && compareCount == 2)
                        strSchool2 = "(SFCC)";
                    if (websiteURL == null || websiteURL == "") {
                        websiteURL = "#";
                    }
                    if (compareCount == 1) {
                        strLine1 = "<h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy;
                        rowCount++;
                        sectionCount++;
                    }
                    if (compareCount == 2) {
                        strLine2 = "<h2><a href=\"" + websiteURL + "\">" + newAreaOfStudy;
                        rowCount++;
                        sectionCount++;
                        if (newAreaOfStudy == oldAreaOfStudy) {
                            //print both lines; clear both line strings
                            //reset compareCount = 0
                           // $("#row" + newAlphaCharacter).append(strLine1 + " " + strSchool1 + "</a></h2>");
                            
                            strAllSectionRecords += strLine1 + " " + strSchool1 + "</a></h2>";
                            compareCount = 0;
                            strLine1 = "";
                            strSchool1 = "";
                            // $("#row" + newAlphaCharacter).append(strLine2 + " " + strSchool2 + "</a></h2>");
                            strAllSectionRecords += strLine2 + " " + strSchool2 + "</a></h2>";
                          //  alert(strAllSectionRecords);
                            strLine2 = "";
                            strSchool2 = "";
                            
                        } else {
                            // print one row; move row2 --> row1
                            // reset compareCount = 1
                           // $("#row" + newAlphaCharacter).append(strLine1 + "</a></h2>");
                            strAllSectionRecords += strLine1 + "</a></h2>";
                          //  alert(strAllSectionRecords);
                            strLine1 = strLine2;
                            strLine2 = "";
                            strSchool1 = strSchool2;
                            strSchool2 = "";
                            compareCount = 1;
                        }
                    }
                    oldAreaOfStudy = newAreaOfStudy;
                    //store the alpha character being looped through
                    oldAlphaCharacter = newAlphaCharacter;
                });

                if (alphaCharacter != "") {
                    //display for only one section determined by the alphaCharacter
                  //  alert("section count: " + sectionCount);
                    // see how many records in this section
                    if (compareCount == 2) {
                        strAllSectionRecords += strLine2 + " " + strSchool2 + "</a></h2>";
                    } else
                        strAllSectionRecords += strLine1 + "</a></h2>";

                    halfCount = sectionCount / 2;
                    halfCountRounded = Math.floor(halfCount);
                  //  alert("half count rounded: " + halfCountRounded);
                    moduloValue = sectionCount % 2;
                 //   alert("mod value: " + moduloValue);
                    if (moduloValue > 0)
                        halfCountRounded++;

                 //   alert("half count: " + halfCountRounded);
                    parts = strAllSectionRecords.split("</h2>");
                    strPart1 = parts.slice(0, halfCountRounded).join("</h2>");
                    strPart2 = parts.slice(halfCountRounded++).join("</h2>");
                    $("#row" + oldAlphaCharacter).append("<div class='col-lg-6'>" + strPart1 + "</div>");
                    $("#row" + oldAlphaCharacter).append("<div class='col-lg-6'>" + strPart2 + "</div></div>");
                    sectionCount = 0;
                    strPart2 = "";
                    strPart1 = "";
                    strAllSectionRecords = "";
                }

                //hide the "Working" indicator
                clearInterval(workingIndicator);
                $(".working").hide();

                //display how many results were found
                if (keyword != null && keyword != "") {
                    $(".results").append(('<div class="searchstat"><h2>Found ' + rowCount + ' areas of study containing "' + keyword + '"</h2></div>').replace("Found 1 areas", "Found 1 area").replace("Found 0 areas of study", "No areas of study found"));
                } else {
                    if (alphaCharacter != null && alphaCharacter != "") {
                        $(".results").append(('<div class="searchstat"><h2>Found ' + rowCount + ' areas of study beginning with "' + alphaCharacter + '"</h2></div>').replace("Found 1 areas", "Found 1 area").replace("Found 0 areas of study", "No areas of study found"));
                    }
                }
            },
            error: function (error) {
                $(".results .searchstat").remove();
                $(".working").hide();
                setTimeout(function () { $(".results").append('<div class="searchstat"><h2>Timeout: Service Unavailable</h2></div>'); }, 1000);
            }
        });
    }

    function clearTextSearch() {
         $("input[id$='txtTextSearch']").val("");
    }

    function clearAlphaSearch() {
        $("input[id$='hidLtr']").val("");
    }

</script>
   