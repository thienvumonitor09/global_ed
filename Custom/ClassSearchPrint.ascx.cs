﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;

public partial class CMSWebParts_Custom_ClassSearchPrint : CMSAbstractWebPart {
    protected void Page_Load(object sender, EventArgs e) {
        String classes = "", strm = "";
        try { 
            classes = Request.QueryString["classes"].ToString();
            strm = Request.QueryString["strm"].ToString();
        }catch {
            //do nothing
        }

        if (classes != null && classes != "") {
            try {
                //initialize web service
                ClassScheduleXML.ClassScheduleXML wsClassSchedule = new ClassScheduleXML.ClassScheduleXML();

                String termDesc = wsClassSchedule.GetTermDesc(strm), title = "";
                title = "<h3>Community Colleges of Spokane";
                if (termDesc != "") {
                    title += " - " + termDesc;
                }
                title += "</h3>";
                panContent.Controls.Add(new LiteralControl(title));

                String[] strClassList = classes.Substring(1).Split(',');              
                for (Int16 i = 0; i < strClassList.Length; i++) {
                    DataSet dsClass = wsClassSchedule.GetClasses(strm, "", "", Convert.ToInt16(strClassList[i]), "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, 0, 0, "", "", "");
                    String PreviousClassNbr = "", PreviousClassSubject = "", PreviousClassDays = "", PreviousClassTimes = "", PreviousClassLocation = "", PreviousClassInstructor = "", ClassNbr = "", ClassSubject = "", ClassDays = "", ClassTimes = "", ClassLocation = "", ClassInstructor = "";
                    Table tbl = new Table();
                    TableHeaderRow thr = new TableHeaderRow();
                    TableHeaderCell thc = new TableHeaderCell();
                    Int32 ClassCount = dsClass.Tables[0].Rows.Count;

                    for (Int16 j = 0; j < ClassCount; j++) {
                        String CurrentInstitution = dsClass.Tables[0].Rows[j]["INSTITUTION"].ToString();
                        String CurrentClassNbr = dsClass.Tables[0].Rows[j]["CLASS_NBR"].ToString();
                        String CurrentClassSubject = dsClass.Tables[0].Rows[j]["SUBJECT"].ToString() + " " + dsClass.Tables[0].Rows[j]["CATALOG_NBR"].ToString();
                        String CurrentClassStartTime = dsClass.Tables[0].Rows[j]["ClassTimeStart"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                        String CurrentClassEndTime = dsClass.Tables[0].Rows[j]["ClassTimeEnd"].ToString().Replace("AM", " AM").Replace("PM", " PM");
                        String CurrentClassTimes = "", CurrentClassDays = ""; //Will have repeating rows, need to concatinate to a complete string
                        String CurrentClassLocation = dsClass.Tables[0].Rows[j]["FacilityTable_DESCR"].ToString(); //Will have repeating rows, need to concatinate to a complete string
                        String CurrentClassInstructor = dsClass.Tables[0].Rows[j]["FIRST_NAME"].ToString() + " " + dsClass.Tables[0].Rows[j]["LAST_NAME"].ToString();
                        String Mon = dsClass.Tables[0].Rows[j]["MON"].ToString();
                        String Tues = dsClass.Tables[0].Rows[j]["TUES"].ToString();
                        String Wed = dsClass.Tables[0].Rows[j]["WED"].ToString();
                        String Thur = dsClass.Tables[0].Rows[j]["THURS"].ToString();
                        String Fri = dsClass.Tables[0].Rows[j]["FRI"].ToString();
                        String Sat = dsClass.Tables[0].Rows[j]["SAT"].ToString();
                        String Sun = dsClass.Tables[0].Rows[j]["SUN"].ToString();
                        String ClassDescription = dsClass.Tables[0].Rows[j]["CourseCatalog_DESCRLONG"].ToString();
                        String ClassNotes = dsClass.Tables[0].Rows[j]["ClassNotes_DESCRLONG"].ToString();

                        //store the class times
                        if (CurrentClassStartTime == null || CurrentClassStartTime.Trim() == "") {
                            CurrentClassTimes = "TBA";
                        } else {
                            CurrentClassTimes = CurrentClassStartTime;
                        }

                        if (CurrentClassEndTime == null || CurrentClassEndTime.Trim() == "") {
                            CurrentClassTimes = "TBA";
                        } else {
                            CurrentClassTimes += " - " + CurrentClassEndTime;
                        }

                        //store the class days
                        if (Mon == "Y") {
                            CurrentClassDays += " Mo";
                        }

                        if (Tues == "Y") {
                            CurrentClassDays += " Tu";
                        }

                        if (Wed == "Y") {
                            CurrentClassDays += " We";
                        }

                        if (Thur == "Y") {
                            CurrentClassDays += " Th";
                        }

                        if (Fri == "Y") {
                            CurrentClassDays += " Fr";
                        }

                        if (Sat == "Y") {
                            CurrentClassDays += " Sa";
                        }

                        if (Sun == "Y") {
                            CurrentClassDays += "Su";
                        }

                        if (CurrentClassDays == null || CurrentClassDays.Trim() == "") {
                            CurrentClassDays = dsClass.Tables[0].Rows[j]["STND_MTG_PAT"].ToString();
                            if (CurrentClassDays.Trim() == "") {
                                CurrentClassDays = "TBA";
                            }
                        } else {
                            CurrentClassDays = CurrentClassDays.Substring(1);
                        }

                        if (CurrentClassDays == "ARR" && CurrentClassTimes == "TBA") {
                            CurrentClassTimes = "ARR";
                        }

                        if (CurrentInstitution == "WA171") {
                            CurrentInstitution = "SCC";
                        } else if (CurrentInstitution == "WA172") {
                            CurrentInstitution = "SFCC";
                        }

                        //store the class location
                        if (CurrentClassLocation == null || CurrentClassLocation.Trim() == "") {
                            CurrentClassLocation = "TBA";
                        }

                        //if writing the first record of a new class
                        if (PreviousClassNbr != CurrentClassNbr) {
                            //write the class number, subject, and title
                            panContent.Controls.Add(new LiteralControl("<p><strong>" + ClassNbr + " " + CurrentClassNbr + " - " + CurrentClassSubject + " - " + dsClass.Tables[0].Rows[j]["COURSE_TITLE_LONG"].ToString() + " (" + CurrentInstitution + ")</strong></p>"));
                            if (ClassDescription != null && ClassDescription != "") {
                                panContent.Controls.Add(new LiteralControl("<p class=\"indent\">" + ClassDescription + "</p>"));
                            }
                            if (ClassNotes != null && ClassNotes != "") {
                                panContent.Controls.Add(new LiteralControl("<p class=\"indent\"><strong>Notes</strong><br />" + ClassNotes + "</p>"));
                            }

                            tbl = new Table();
                            thr = new TableHeaderRow();
                            thc = new TableHeaderCell();
                            thc.Controls.Add(new LiteralControl("Days"));
                            thr.Cells.Add(thc);
                            thc = new TableHeaderCell();
                            thc.Controls.Add(new LiteralControl("Times"));
                            thr.Cells.Add(thc);
                            thc = new TableHeaderCell();
                            thc.Controls.Add(new LiteralControl("Location"));
                            thr.Cells.Add(thc);
                            thc = new TableHeaderCell();
                            thc.Controls.Add(new LiteralControl("Instructor"));
                            thr.Cells.Add(thc);
                            tbl.Rows.Add(thr);
                        }

                        //if the class days, times, or location is different
                        if (PreviousClassDays != CurrentClassDays || PreviousClassTimes != CurrentClassTimes || PreviousClassLocation != CurrentClassLocation) {
                            TableRow tr = new TableRow();
                            TableCell td = new TableCell();
                            td.Controls.Add(new LiteralControl(CurrentClassDays));
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.Controls.Add(new LiteralControl(CurrentClassTimes));
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.Controls.Add(new LiteralControl(CurrentClassLocation));
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.Controls.Add(new LiteralControl(CurrentClassInstructor));
                            tr.Cells.Add(td);
                            tbl.Rows.Add(tr);

                        } else if (PreviousClassInstructor != CurrentClassInstructor) //if the days, times, and location are the same, but the instructor is different
                          {
                            TableRow tr = new TableRow();
                            TableCell td = new TableCell();
                            tr.Cells.Add(td);
                            td = new TableCell();
                            tr.Cells.Add(td);
                            td = new TableCell();
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.Controls.Add(new LiteralControl(CurrentClassInstructor));
                            tr.Cells.Add(td);
                            tbl.Rows.Add(tr);
                        }

                        if (j == (ClassCount - 1) || CurrentClassNbr != dsClass.Tables[0].Rows[j + 1]["CLASS_NBR"].ToString()) {
                            panContent.Controls.Add(tbl);
                        }

                        PreviousClassNbr = CurrentClassNbr;
                        PreviousClassSubject = CurrentClassSubject;
                        PreviousClassDays = CurrentClassDays;
                        PreviousClassTimes = CurrentClassTimes;
                        PreviousClassLocation = CurrentClassLocation;
                        PreviousClassInstructor = CurrentClassInstructor;
                    }
                }
            } catch {

            }
        }
    }
}