﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FormsAuthAD;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
public partial class CMSWebParts_Custom_AppsLogIn : CMSAbstractWebPart
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //litDebug.Text = "ReturnURL: " + Request.QueryString["ReturnURL"];
        if (Request.QueryString["ReturnURL"] != null)
        {
            if (Request.QueryString["ReturnURL"].ToLower().Contains("/StudentRecommendationRequest/") || Request.QueryString["ReturnURL"].ToLower().Contains("/onlinesurvey/"))
            {
                divStudentExample.Visible = true;
            }
        }
        if (Request.QueryString["LoginPosted"] != null)
        {
            litGPPMessage.Text += "<p style=\"color:red;\"><strong>Your credentials were authenticated successfully, but you are not authorized to access the requested site.";
            litGPPMessage.Text += "</strong></p>";
        }
    }

    protected void Login_Click(object sender, EventArgs e)
    {
        string strConnectionString = "";
        string strInstitution = "";
        Int32 intDelimiter = txtUsername.Text.IndexOf("@");
        string[] arrUserName;
        string strUserName = "";
        string strDomain = "";
        //string strADDomain = "";
        if (intDelimiter > 0)
        {
            arrUserName = txtUsername.Text.Split(new Char[] { '@' });
            strUserName = arrUserName[0];
            strDomain = arrUserName[1];
            Session["UserName"] = strUserName;
            //litDebug.Text += "Username: " + strUserName + "; Domain: " + strDomain;
            switch (strDomain.ToLower())
            {
                case "bigfoot.spokane.edu":
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_Bigfoot"].ConnectionString;
                    strInstitution = "Student";
                    break;
                case "scc.spokane.edu":
                    strInstitution = "SCC";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
                case "sfcc.spokane.edu":
                    strInstitution = "SFCC";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
                default:
                    strInstitution = "CCS";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
            }

            Session["Institution"] = strInstitution;
            //litDebug.Text += "Username: " + Session["UserName"] + "; Domain: " + strDomain + "; Institution: " + Session["Institution"];
            LdapAuthentication adAuth = new LdapAuthentication(strConnectionString);
            try
            {
                if (adAuth.IsAuthenticated(strDomain, strUserName, txtPassword.Text))
                {
                    String groups = adAuth.GetGroups(strUserName);

                    /* Testing to troubleshoot authentication/group membership behavior.
                     * Determined there are some system- and network-level issues with user objects
                     * having memberships in too many groups.
                     * See WO # 34794 for more information.
                    if (strUserName.Contains("rainer") || strUserName.Contains("medina"))
                    {
                        int count = 0;
                        foreach (char c in groups)
                        {
                            if (c == '|') count++;
                        }
                        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                        message.IsBodyHtml = true;
                        message.To.Add("bob.nelson@ccs.spokane.edu");
                        message.Subject = "Groups found report";
                        message.From = new System.Net.Mail.MailAddress("CCSWebApp@ccs.spokane.edu");
                        message.Body = "<strong>Groups detected for " + strUserName + " (" + count + ") </strong><pre>" + groups.Replace("|","<br />") + "</pre><hr />" + groups;
                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.spokane.edu");
                        smtp.Send(message);
                    }
                    */

                    //Create the ticket, and add the groups.
                    bool isCookiePersistent = chkPersist.Checked;
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, strDomain + "\\" + strUserName,
                        DateTime.Now, DateTime.Now.AddMinutes(20), isCookiePersistent, groups);

                    //Encrypt the ticket.
                    String encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                    //Create a cookie, and then add the encrypted ticket to the cookie as data.
                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    if (true == isCookiePersistent)
                        authCookie.Expires = authTicket.Expiration;

                    //Add the cookie to the outgoing cookies collection.
                    Response.Cookies.Add(authCookie);

                    //You can redirect now.
                    string strRedirectURL = FormsAuthentication.GetRedirectUrl(strUserName, false);
                    //Clear out any repeated items
                    strRedirectURL = strRedirectURL.Replace("&LoginPosted=yes", "");
                    strRedirectURL = strRedirectURL.Replace("&InstID=Student", "");
                    strRedirectURL = strRedirectURL.Replace("&InstID=CCS", "");
                    strRedirectURL = strRedirectURL.Replace("?InstID=Student", "");
                    strRedirectURL = strRedirectURL.Replace("?InstID=CCS", "");

                    if (strRedirectURL.IndexOf("?") > 0 && strRedirectURL.IndexOf("InstID") < 0)
                    {
                        strRedirectURL += "&InstID=" + strInstitution;
                        //strRedirectURL += "&InstID=" + strInstitution + "&SID=" + myInfo.strLoginSID;
                    }
                    else
                    {
                        strRedirectURL += "?InstID=" + strInstitution;
                        //strRedirectURL += "&InstID=" + strInstitution + "&SID=" + myInfo.strLoginSID;
                    }
                    strRedirectURL += "&LoginPosted=yes";
                    Response.Redirect(strRedirectURL);
                }
                else
                {
                    errorLabel.Text = "<p style=\"color:red;\">Authentication did not succeed. Check user name and password.</p>";
                }
            }
            catch (Exception ex)
            {
                litGPPMessage.Text = "<p style=\"color:red;\"><strong>" + ex.Message + "</strong></p>";
            }
        }
        else
        {
            litDebug.Text = "<p style=\"color:red;\"><strong>Please include your UPN Suffix after your username</strong> (e.g. @SCC, @SFCC, @Dist, @IEL, @bigfoot.spokane.edu, @ccs.spokane.edu, @scc.spokane.edu, @sfcc.spokane.edu).</p>";
        }
    }
}