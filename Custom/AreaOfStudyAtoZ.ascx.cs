﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.Data;


public partial class CMSWebParts_Custom_AreaOfStudyAtoZ : CMSAbstractWebPart {
    protected void Page_Load(object sender, EventArgs e) {
        //registers the stylesheet for use within the control
        HtmlLink styleLink = new HtmlLink();
        styleLink.Attributes.Add("rel", "stylesheet");
        styleLink.Attributes.Add("type", "text/css");
        styleLink.Href = "~/CMSWebparts/Custom/AreaOfStudyAtoZ_files/AreaOfStudyAtoZ.css";
        Page.Header.Controls.Add(styleLink);

        if (IsPostBack) {
            hidPostBack.Value = "true";
        } else {
            hidPostBack.Value = "false";
            //if not a postback, institution is grabbed from the querystring.
            String institution = Request.QueryString["inst"];
            if (institution != null && institution != "") {
                institution = institution.ToLower();
            }
            if (institution == "scc") {
                hidInstitution.Value = "WA171";
            } else if (institution == "sfcc") {
                hidInstitution.Value = "WA172";
            }
        }
    }
}

