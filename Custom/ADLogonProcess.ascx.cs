﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CMS.PortalEngine.Web.UI;
using FormsAuthAD;

public partial class CMSWebParts_Custom_ADLogonProcess : CMSAbstractWebPart
{
    string fullUser = "";
    string strUserName = "";
    string strDomain = "";
    string[] arrUserName;
    string user_firstname = "";
    string user_lastname = "";
    string user_id = "";
    string user_email = "";
    string user_institution = "";
    string user_phone = "";
    string UPN = "";
    string user_type = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        litMessage.Text = "";
        if (Request.QueryString["nm"] != null)
        {
            if (Request.QueryString["nm"] != "")
            {
                fullUser = Request.QueryString["nm"];
                arrUserName = fullUser.Split(new Char[] { '@' });
                strUserName = arrUserName[0];
                strDomain = arrUserName[1];
                user_type = arrUserName[1];
             //   Response.Write("usertype: " + user_type + "<br />");

                if (user_type == "bigfoot.spokane.edu")
                {
                 //   fullUser = "TestingT5019@bigfoot.spokane.edu";
                    GetStudentInfo studentInfo = new GetStudentInfo(fullUser, "", "", "StudentInfo");

                    user_firstname = studentInfo.strFirstName;
                    user_lastname = studentInfo.strLastName;
                    user_id = studentInfo.SystemID;
                    user_email = studentInfo.strEmail;
                    user_institution = studentInfo.strInstitution;
                 //   Response.Write("Student Instituion --  " + user_institution);

                    if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
                    {
                        user_type = "Student";
                    //    litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p>";
                    }
                }
                else
                {
                    GetEmployeeInfo employeeInfo = new GetEmployeeInfo(fullUser, "", "", "EmployeeInfo");

                    user_firstname = employeeInfo.FirstName;
                    user_lastname = employeeInfo.LastName;
                    user_id = employeeInfo.SystemID;
                    user_email = employeeInfo.EmailAddress;
                    user_institution = "Community Colleges of Spokane";
                    user_phone = employeeInfo.PhoneNumber;

                    if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
                    {
                        user_type = "employee";
                     //   litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p><p> Phone number: " + user_phone + "</p>";
                     //   Response.Write("employeeEmail = " + user_email + "<br />");
                    }
                }
                //  Response.Write("Name: " + user_firstname + " " + user_lastname + "<br />");
                hdnCtc.Value = user_id;
                hdnFName.Value = user_firstname;
                hdnLName.Value = user_lastname;
                hdnEmail.Value = user_email;
            }// querystring not blank
        }// querystring value NOT null
    }
}