<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Custom_CCSSearchSelector" CodeFile="~/CMSWebParts/Custom/CCSSearchSelector.ascx.cs" %>

<asp:Repeater runat="server" ID="rptResults">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate></ItemTemplate>
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater>


<asp:GridView ID="gridView" runat="server" CssClass="table" GridLines="None" Visible="false">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
