﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestWebParts.aspx.cs" Inherits="CMSWebParts_Custom_TestWebParts" %>
<%@ Register TagPrefix="ccs" TagName="WebPartToTest" Src="~/CMSWebParts/Custom/TransferDegreeLinks.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Here is my Web Part!</h1>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <ccs:WebPartToTest ID="ccsWebPart" runat="server" />
    </div>
    </form>
</body>
</html>
