﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Custom/GlobalEdQuarterInfo2.ascx.cs" Inherits="CMSWebParts_Custom_GlobalEdQuarterInfo2" %>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
    <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;">
        <img src='~/CMSWebparts/Custom/GlobalEdQuarterInfo_files/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
   
    <asp:label id="myLabel" runat="server" />
    <div id="main_content"></div>
   
    <input type="hidden" id="quarterListJsonID" runat="server" />
    <input type="hidden" id="quarterInfoListJsonID" runat="server" />
    <input type="hidden" id="quarterInfoListJsonID2" runat="server" />
    <input type="hidden" id="siteID" runat="server" />
    <div id="TBL"></div>
</div>

<script>
    $(function () {
        $(document).ajaxStart(function(){
            $("#wait").css("display", "block");
        });
        $(document).ajaxComplete(function(){
            $("#wait").css("display", "none");
        });
        //Get data as JSON type from REST API
        $.ajax({
            type: "GET",
            url: "//external.spokane.edu/REST/API/QuarterDates3/GetQuarterInfo?filter=globaled",
            //contentType: "application/json",
            dataType: "json"
        })
        .done(function (data) {
                //alert("success");
            console.log(data);
            displayTable(data);
        })
        .fail(function (xhr) {
            console.log('error', xhr);
        });
    });

    function displayTable(quarterInfoListJson) {
        quarterInfoListJson.forEach(function (element) {
            console.log(element);
            var quarterE = element.QuarterObj.QuarterName;
            var yearE = element.QuarterObj.Year;
            //create table
            var table = $("<table border='1px' cellpadding='1px' cellspacing='1px' class='ccs-table' />").appendTo("#TBL");
            table.append('<thead> <tr><th colspan="2" scope="col">' + quarterE + " " + yearE + '</th></tr></thead>');

            var tbody = $('<tbody>').appendTo(table);

            element.InfoList.forEach(function (value, index) {
                //console.log(value);
                var tr = $('<tr>').appendTo(tbody);
                $('<td>').text(value.Title).appendTo(tr);
                //$('<td>').text((value.Category)).appendTo(tr);
                $('<td>').text(convertDate(value.Category)).appendTo(tr);
            });

        });
    }

    function convertDate(dateStr) {
        var monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var d = new Date(Date.parse(dateStr));
        return monthName[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
    }
</script>



