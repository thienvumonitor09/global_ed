﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.FormEngine.Web.UI;
using CMS.DocumentEngine;
using HM.Funnelback;
using HM.Funnelback.KenticoPush;
using CMS.EventLog;
using CMS.SiteProvider;

public partial class CMSWebParts_Custom_HMFunnelbackClearCollectionButton : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {

        string collectionName = GetCollectionName();
        HttpStatusCode status = FunnelbackRestCalls.DeleteAllContentFromCollection(collectionName, SiteContext.CurrentSite.SiteName);
        if (status == HttpStatusCode.OK)
        {
            ltlResult.Text = "All items cleared from collection: " + collectionName;
            EventLogProvider.LogInformation("ClearAllDocumentsFromCollection", "INFORMATION", "Response = " + status.ToString());
        }
        
    }

    protected string GetCollectionName()
    {
        UIContext context = UIContext.Current;

        FunnelbackCollectionInfo editedObject = (FunnelbackCollectionInfo)context.EditedObject;
        return editedObject.FunnelbackCollectionName;
    }
}