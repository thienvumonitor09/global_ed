<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Custom_CCSCreditCourseListByDepartment"  CodeFile="~/CMSWebParts/Custom/CCSCreditCourseListByDepartment.ascx.cs" %>
<style type="text/css">
/* Style the accordion panel. Note: hidden by default */
   .crsaccordion div{
      border: 1px solid #E6E7E7;
      border-collapse:collapse;
      box-sizing:border-box;
   }
    /* Style the buttons that are used to open and close the accordion panel */
    .accordion {
        color: #444;
        padding: 6px;
        width: 100%;
        text-align: left;
        border: none;
        outline: none;
        transition: 0.4s;
    }
    /* Add a background color when you move the mouse over it (hover) */
    .accordion:hover {
        background-color: #ccc;
    }
    .panel {
      padding: 0 6px;
      background-color: #eee;
      width:100%;
   }
  .accordion-toggle {cursor: pointer;background-color:white;}
  .accordion-content {display: none;}
  .accordion-content.default {display: block;}
  .accordion-content p, #accordion div p {margin: 0px 0px;}
</style>
<div style="width:100%;margin:auto;max-width:925px;background-color:white;">
    <asp:Literal ID="litPageTitle" runat="server"></asp:Literal>
    <div class="crsaccordion">
        <asp:Literal ID="litCourseList" runat="server"></asp:Literal>
    </div>
</div> 
<script type="text/javascript">
    $(document).ready(function ($) {
            $('.crsaccordion').find('.accordion-toggle').unbind('click').click(function () { //unbind to prevent multiple click handlers with multiple widget instances
                //Hide the other panels
                $(".accordion-content").not($(this).next()).slideUp('fast');

                //console.log("toggling...");

                //Expand or collapse this panel
                $(this).next().slideToggle('fast');

            });
    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
        });
    }
</script>
