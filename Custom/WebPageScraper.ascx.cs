using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine.Web.UI;
using CMS.Helpers;

public partial class CMSWebParts_Custom_WebPageScraper : CMSAbstractWebPart
{
    #region "Properties"
    public string URL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("URL"), null);
        }
        set
        {
            SetValue("URL", value);
        }
    }


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        string strMessage = "";
        System.Net.WebClient client = new System.Net.WebClient();
        String strResult = "";
        try
        {
            //strMessage += "URL: " + URL + "<br />";
            strResult = client.DownloadString(URL);
            Int32 intStartIndex = strResult.IndexOf("<a href=\"#StartScrape\"></a>") + 27;
            Int32 intEndIndex = strResult.IndexOf("<a href=\"#EndScrape\"></a>");
            Int32 intLength = intEndIndex - intStartIndex;
            //strMessage += "Start Index: " + intStartIndex.ToString() + "<br />";
            //strMessage += "End Index: " + intEndIndex.ToString() + "<br />";
            //strMessage += "Length: " + intLength.ToString() + "<br />";
            if (intLength > 0)
            {
                String strScraped = strResult.Substring(intStartIndex, intLength);
                litScrapedText.Text = strScraped;
            }
        }
        catch (Exception ex)
        {
            strMessage += "Error: " + ex.Message;
        }
        litDebug.Text = strMessage;

    }


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



