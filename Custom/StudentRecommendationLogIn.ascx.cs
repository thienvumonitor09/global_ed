﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMSWebParts_Custom_StudentRecommendationLogIn : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the following to the User ID
        string strADName = Request.ServerVariables["LOGON_USER"];
        string[] holder = strADName.Split('\\');
        //string UPN = holder[1] + "@" + holder[0];
        //string user_type = holder[0];

        //Student testing
        //   string UPN = "CorneliusM0271@bigfoot.spokane.edu";
        //    string user_type = "bigfoot.spokane.edu";
        //employee testing
        string UPN = "laura.padden@ccs.spokane.edu";
        string user_type = "ccs.spokane.edu";

        if (user_type == "bigfoot.spokane.edu")
        {
            GetStudentInfo studentInfo = new GetStudentInfo(UPN, "", "", "StudentInfo");

            string user_firstname = studentInfo.strFirstName;
            string user_lastname = studentInfo.strLastName;
            string user_id = studentInfo.SystemID;
            string user_email = studentInfo.strEmail;
            string user_institution = studentInfo.strInstitution;
            Response.Write("User Instituion --  " + user_institution);

            if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
            {
                user_type = "Student";
                litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p>";
            }
        }
        else
        {
            GetEmployeeInfo employeeInfo = new GetEmployeeInfo(UPN, "", "", "EmployeeInfo");

            string user_firstname = employeeInfo.FirstName;
            string user_lastname = employeeInfo.LastName;
            string user_id = employeeInfo.SystemID;
            string user_email = employeeInfo.EmailAddress;
            string user_institution = "Community Colleges of Spokane";
            string user_phone = employeeInfo.PhoneNumber;

            if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
            {
                user_type = "employee";


                litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p><p> Phone number: " + user_phone + "</p>";
                //Response.Write("user_type = " + user_type);
            }
        }
    }
}