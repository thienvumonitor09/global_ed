﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.PortalEngine;
using CMS.Helpers;
using System.IO;
using System.Net;
using CMS.EventLog;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;

public partial class CMSWebParts_Custom_SearchStream : CMS.PortalEngine.Web.UI.CMSAbstractWebPart
{
    #region "Protected variables & constants"

    protected string path = String.Empty;
    protected string query = String.Empty;
    protected string facetscope = String.Empty;
    protected string funnelbackQueryString = String.Empty;
	protected string CurrentIP = String.Empty;

    #endregion

    #region "Public properties"
    /// <summary>
    /// Funnelback Search Page to send queries too
    /// </summary>
    public string SearchPage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FunnelbackSearchPage"), "");
        }
        set
        {
            SetValue("FunnelbackSearchPage", value);
        }
    }


    /// <summary>
    /// Funnelback Collection to query
    /// </summary>
    public string SearchCollection
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FunnelbackCollection"), "");
        }
        set
        {
            SetValue("FunnelbackCollection", value);
        }
    }

    /// <summary>
    /// Funnelback Specific .FTL Form to Use
    /// </summary>
    public string SearchForm
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FunnelbackForm"), "");
        }
        set
        {
            SetValue("FunnelbackForm", value);
        }
    }

    /// <summary>
    /// Funnelback Specific .FTL Form to Use
    /// </summary>
    public string SearchProfile
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FunnelbackProfile"), "_default");
        }
        set
        {
            SetValue("FunnelbackProfile", value);
        }
    }
    /// <summary>
    /// Error Message to display if there is a connection issue from Kentico to Funnelback
    /// </summary>
    public string SearchErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ErrorMessage"), "");
        }
        set
        {
            SetValue("ErrorMessage", value);
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        path = new Uri(HttpContext.Current.Request.Url.AbsoluteUri).OriginalString;
        //Response.Write("<br><br><br><br><br>" + HttpContext.Current.Request.RawUrl);
        if (Page.IsPostBack)
        {
            //build querystring and refresh page with variables in URL
            query = HttpUtility.UrlEncode(ValidationHelper.GetString(Request.Form["query"], null));
            //Response.Write("PostBack: " + HttpUtility.UrlEncode(query) + "<br>");
            facetscope = ValidationHelper.GetString(Request.Form["facetScope"], null);
            if (query != null)
            {
                funnelbackQueryString += "query=" + query;
                if (facetscope != null)
                {
                    funnelbackQueryString += "&" + facetscope;
                }
                Response.Redirect(HttpContext.Current.Request.RawUrl + "?" + funnelbackQueryString);
            }
        }
        else
        {
            query = HttpUtility.UrlEncode(ValidationHelper.GetString(Request.QueryString["query"], ""));
            //Response.Write("QueryString: " + query + "<br>");
        }

        SearchOutput.Text = Execute(path, SearchPage, query);
    }

	public static String GetIP()
    {
        string VisitorsIPAddr = string.Empty;
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        }
        return VisitorsIPAddr;
    }
	
	protected WebRequest AddIPtoHeader(WebRequest request)
    {
        CurrentIP = GetIP();
        Regex regex = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
        Match match = regex.Match(CurrentIP);
        if (match.Success)
        {
            request.Headers.Add("X-Forwarded-For", CurrentIP);
			EventLogProvider.LogEvent(EventType.INFORMATION, "Funnelback Search Local IP", "SearchResultWebPart", eventDescription: "Local IP Added: " + CurrentIP);
        }
        return request;
    }
	
    public string Execute(string CurrentURL, string funnelbackURL, string query)
    {
        return GetContent(CurrentURL, funnelbackURL, query);
    }

    protected string GetContent(string CurrentURL, string funnelbackURL, string query)
    {
        string contentString = SearchErrorMessage;
		//query = query.Replace("533-8006","Number_533-8006_Not_Found");
        try
        {
            //string searchURL = funnelbackURL + GetQuery(CurrentURL, query.Replace("533-8006","Number_533-8006_Not_Found"));
			string searchURL = funnelbackURL + GetQuery(CurrentURL, query);
            //Response.Write(query);
            WebRequest request = WebRequest.Create(searchURL);
			request = AddIPtoHeader(request);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }
			//html = html.Replace("Number_533-8006_Not_Found","533-8006");

            if (html.Length > 0)
            {
                string cleanedHTML = CleanURL(html, funnelbackURL);

                cleanedHTML = CleanHTML(cleanedHTML);

                if (Request.QueryString["mode"]!="PreBob") {
                    //cleanedHTML = cleanedHTML.Replace("<script src=\"resources-global/js/jquery/jquery-1.10.2.min.js\"></script>", "<!--script reference removed-->");
                    //cleanedHTML = cleanedHTML.Replace("<script src=\"resources-global/js/jquery/jquery-ui-1.10.3.custom.min.js\"></script>", "<!--script reference removed-->");
                    cleanedHTML = cleanedHTML.Replace("<script src=\"resources-global/thirdparty/bootstrap-3.0.0/js/bootstrap.min.js\"></script>", "<!--script reference removed-->");
                    //cleanedHTML = cleanedHTML.Replace("<script src=\"resources-global/js/jquery/jquery.tmpl.min.js\"></script>", "<!--script reference removed-->");
                    //cleanedHTML = cleanedHTML.Replace("<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\">", "<!--bootstrap.min.css reference removed-->");
                    //cleanedHTML = cleanedHTML.Replace("<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\">", "<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/CMSPages/GetCSS.aspx?stylesheetname=bootstrap-override-ccs.css\"/>");
                    cleanedHTML = cleanedHTML.Replace("<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\">", "<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/CMSPages/GetCSS.aspx?stylesheetname=bootstrap-override-fb-search.css\"/>");
                    //cleanedHTML = cleanedHTML.Replace("<link rel=\"stylesheet\" href=\"resources-global/thirdparty/bootstrap-3.0.0/css/bootstrap.min.css\">", "<link rel=\"stylesheet\" type=\"text/css\" href=\"/CMSPages/GetCSS.aspx?stylesheetname=bootstrap-override-fb-search.css\"/>");
                    cleanedHTML = cleanedHTML.Replace("\"resources-global/js/jquery.funnelback-completion-15.10.0.js", "\"/CMSPages/GetResource.ashx?scriptfile=/CMSScripts/Custom/jquery.funnelback-completion.js");
                    cleanedHTML = cleanedHTML.Replace("\"resources-global/", "\"//ccs-search.clients.us.funnelback.com/s/resources-global/");
                    cleanedHTML = cleanedHTML.Replace("\"/s/redirect", "\"//ccs-search.clients.us.funnelback.com/s/redirect");
                    cleanedHTML = cleanedHTML.Replace(": '../s/suggest.json',", ": '//ccs-search.clients.us.funnelback.com/s/suggest.json',");
                    cleanedHTML = cleanedHTML.Replace("\"open-search.xml?", "\"//ccs-search.clients.us.funnelback.com/s/open-search.xml?");
                    //cleanedHTML = cleanedHTML.Replace("\"?&amp;query=", "\"//ccs-search.clients.us.funnelback.com/s/?&amp;query=");
                    cleanedHTML = cleanedHTML.Replace("<!DOCTYPE html>", "");
                    cleanedHTML = cleanedHTML.Replace("<html lang=\"en-us\">", "");
                    cleanedHTML = cleanedHTML.Replace("<head>", "");
                    cleanedHTML = cleanedHTML.Replace("</head>", "");
                    cleanedHTML = cleanedHTML.Replace("<meta charset=\"utf-8\">", "");
                    cleanedHTML = cleanedHTML.Replace("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">", "");
                    cleanedHTML = cleanedHTML.Replace("<meta name=\"robots\" content=\"nofollow\">", "");
                    cleanedHTML = cleanedHTML.Replace("<!--[if IE]><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><![endif]-->", "");
                    cleanedHTML = cleanedHTML.Replace("<title>ccs-sfccwebsite-web, Funnelback Search</title>", "");
                    cleanedHTML = cleanedHTML.Replace("<body", "<section class=\"white-bg\"><div");
                    cleanedHTML = cleanedHTML.Replace("</body>", "</div></section>");
                    cleanedHTML = cleanedHTML.Replace("</html>", "");
                    //cleanedHTML = cleanedHTML.Replace("<p>","<!--p>").Replace("</p>","</p-->");  //Hide blurbs
                    cleanedHTML = cleanedHTML.Replace("Student Tools. Toggle navigation MENU. ", ""); //Remove undesired blurb text
					cleanedHTML = cleanedHTML.Replace("Employee Tools. Toggle navigation MENU. ", ""); //Remove undesired blurb text
                }

                contentString = cleanedHTML;

            }
        }
        catch (Exception ex)
        {
            //log exception 
            EventLogProvider.LogEvent(EventType.INFORMATION, "Funnelback Search Results Web Part", "SearchResultWebPart", eventDescription: "Error loading search results: " + ex.InnerException);
        }

        return contentString;
    }

    public string GetQuery(string CurrentURL, string query)
    {
        string queryString = string.Empty;
        string funnelbackCollection = SearchCollection;
        string funnelbackForm = SearchForm;
        string funnelbackProfile = SearchProfile;

        queryString = "?collection=" + funnelbackCollection;
        if (!String.IsNullOrEmpty(query))
        {
            queryString += "&query=" + query;
        }
        if (!String.IsNullOrEmpty(funnelbackForm))
        {
            queryString += "&form=" + funnelbackForm;
        }
        if (!String.IsNullOrEmpty(funnelbackProfile))
        {
            queryString += "&profile=" + funnelbackProfile;
        }

        queryString += CleanQuery(CurrentURL);

        return queryString;
    }

    protected string CleanQuery(string URL)
    {
        string clean = string.Empty;

        NameValueCollection qc = HttpUtility.ParseQueryString(URL);
        StringBuilder sb = new StringBuilder();
        foreach (String parameter in qc.AllKeys)
        {
            string value = qc[parameter];
            if (parameter != null)
            {
                if (parameter != "aliaspath" & parameter != "form" & parameter != "profile" & parameter != "collection" & parameter != "query")
                {
                    if (!parameter.Contains("http"))
                    {
                        value = WebUtility.UrlEncode(value);
                        sb.Append("&" + parameter + "=" + value);
                    }
                }
            }
        }
        clean = sb.ToString();

        return clean;
    }

    protected string CleanHTML(string html)
    {
        string updatedButtonHTML = html;
        updatedButtonHTML = updatedButtonHTML.Replace("<html>", "");

        return updatedButtonHTML;
    }

    protected string CleanURL(string html, string funnelbackURL)
    {
        string cleanURLHTML = html;
        int end = funnelbackURL.IndexOf("/s/");
        string baseURL = funnelbackURL.Substring(0, end);
        string collection = SearchCollection;
        cleanURLHTML = cleanURLHTML.Replace("search.html", "");
        cleanURLHTML = cleanURLHTML.Replace("collection=" + collection, "");
        string redirectURL = "/s/redirect?collection=" + collection + "&url=";
        cleanURLHTML = cleanURLHTML.Replace("/s/redirect?&amp;url=", redirectURL);
        cleanURLHTML = cleanURLHTML.Replace("/s/redirect?&url=", redirectURL);
        string funnelbackForm = SearchForm;
        if (!String.IsNullOrEmpty(funnelbackForm))
        {
            string formquery = "&form=" + funnelbackForm;
            cleanURLHTML = cleanURLHTML.Replace(formquery, "");
            formquery = "&amp;form=" + funnelbackForm;
            cleanURLHTML = cleanURLHTML.Replace(formquery, "");
        }
        string funnelbackProfile = SearchProfile;
        if (!String.IsNullOrEmpty(funnelbackProfile))
        {
            string profilequery = "&profile=" + funnelbackProfile;
            cleanURLHTML = cleanURLHTML.Replace(profilequery, "");
            profilequery = "&amp;profile=" + funnelbackProfile;
            cleanURLHTML = cleanURLHTML.Replace(profilequery, "");
        }
        cleanURLHTML = cleanURLHTML.Replace("&profile=_default", "");
        cleanURLHTML = cleanURLHTML.Replace("&amp;profile=_default", "");

        return cleanURLHTML;
    }
}