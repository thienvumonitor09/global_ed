﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Web.UI.HtmlControls;
using CMS.PortalEngine.Web.UI;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using FormsAuthAD;

public partial class CMSWebParts_Custom_StudentRecommendation : CMSAbstractWebPart
{
    int intFormID;
    string strSQL = "";
    // for Email
    string sendTo = "";
    string sentFrom = "";
    string strCCaddresses = "";
    string strSubject = "Recommendation for ";
    string strEmailResult = "";
    string strBody = "";
    string postBackControlName = "";
    string strBuildAttend = "";
    string strFileName = "";
    String strUploadURL = @"\\ccs-kentico\FilesFromWeb\StudentRecommendations\";
    LateEnrollment stuRecommend = new LateEnrollment(); // 

    public static String registrarEmail = "laura.padden@ccs.spokane.edu";
    // public static String registrarEmail = "sindi.howland@sfcc.spokane.edu";
    Boolean blEmailSent = false;

    // EVALUATION FIELDS
    string strAcadAbility = "";
    string strVerbal = "";
    string strWritten = "";
    string strDetail = "";
    string strAttendance = "";
    string strTeamwork = "";
    string strLeadership = "";
    string strAccountability = "";
    string strInitiative = "";
    string strFeedback = "";
    string strTime = "";
    string strResources = "";
    string strThinking = "";
    string strFullDuration = "";

    protected void Page_Load(object sender, EventArgs e)
    {
       if(!IsPostBack)
        {
            signIn.Visible = true;
            matchID.Visible = false;
        }
        
    }// end page load

    protected void btnCmdSubmit_Click(object sender, EventArgs e)
    {
        // make sure only files with .pdf extensions are uploaded
        if (uplRecommendFile.HasFile)
        {
            string[] pdfFile = uplRecommendFile.FileName.Split('.');
            if (pdfFile[1].ToLower() != "pdf")
            {
                lblErrorMsg.Text = "Only PDF files are allowed.";
                return;
            } else
            {
                lblErrorMsg.Text = "";
            }
        }

        //get the form id from the querystring
        intFormID = Convert.ToInt32(Request.QueryString["i"]);
        //generate FERPA waiver information

        string[] arrName = hdnFacStaffEmail.Value.Split('.');
        string facName = arrName[0].ToString() + " " + arrName[1].ToString();
        string strWaive = "";
        if (hdnFERPAwaiver.Value.Trim() == "not waive")
            strWaive = "I, " + hdnStudentName.Value + " did not waive my right to review a copy of this recommendation.";
        else
            strWaive = "I, " + hdnStudentName.Value + " did waive my right to review a copy of this recommendation.";
        String strFERPA = "";
        strFERPA = String.Format("<p>The Family Educational Rights and Privacy Act (FERPA) prohibits an educational institution from releasing confidential, " +
            "non-directory information about a student without the student’s consent. A student may waive this right for faculty and staff when written letters of recommendation are requested.</p>" +
            "<p>Completion of this form authorizes an individual to appropriately use a student’s education record to provide requested information. " +
            "This authorization to provide a recommendation is valid for (1) year from the date of the signature below.</p>" +
            "<p>Name of faculty/staff authorized to release Academic Information: {0}</p>" +
            "<p>Email address of individual authorized to release Academic Information: {1}</p>" +
            "<p>I give the faculty/staff member listed above my permission to send a letter of recommendation to: {2}</p>" +
            "<p>I understand that, under FERPA, I have a right to review a copy of my education records upon request, unless I choose to waive that right, " +
            "in which case, I understand that the recommendation will be emailed to the requested recipient.  With that understanding, " +
            "I make the following decision: {3}</p>" +
            "<p>I give my permission to include the following non-directory information in this recommendation: </p>", litFacultyStaffName.Text, hdnFacStaffEmail.Value, hdnSendInstitution.Value, strWaive);

        if (hdnRecSent.Value == "")
        {
            // generate the email to the student (if waiver NOT ok'd or want it sent to them) or institution
            Utility uEmail = new Utility();
            strCCaddresses = registrarEmail;
            if (hdnSendToEmail.Value.Trim() != "")
                sendTo = hdnSendToEmail.Value;
            else if (hdnStudentEmail.Value.Trim() != "")
                sendTo = hdnStudentEmail.Value;
            if (hdnFERPAwaiver.Value.Trim() == "not waive")
                sendTo += "," + hdnStudentEmail.Value;
            strSubject += hdnStudentName.Value;
            //prepare the body of the email
            strBody = "";
            strBody = "<p>The student, " + lblStudentName.Text + ", has requested a recommendation created and submitted by the faculty on record in the document created through SFCC. Please accept this as the official verification.</p>";

            strBody += strFERPA;

            strBody += "<p>" + lblStudentName.Text + " attended SFCC from " + litDatesAttend.Text + ".</p>";
            strBody += "<p>Student's Program of Study: " + litProgramOfStudy.Text + ".</p>";
            strBody += "<p>" + litFacultyStaffName.Text + " has known this student for " + lblLengthKnown.Text + " in the following capacities:";
            strBody += "<table><tbody><tr><td style='width:30%;display: inline-block;'>" + lblListCapacity.Text + "</td><td style='width:55%;display: inline-block;'>" + lblListDuration.Text + "</td></tr></tbody></table>";
            strBody += "<p>The student was rated on the following areas.</p>";
            strBody += "<table width='100%'><tbody><tr><th width='40%' align='left'>Area</th><th width='20%' align='left'>Rating</th></tr>";
            strBody += "<tr><td valign='top'><b>Academic Ability</b></td><td valign='top'>" + rblAcadABility.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Verbal Communication Skills</b></td><td valign='top'>" + rblVerbal.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Written Communication Skills</b></td><td valign='top'>" + rblWritten.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Attention to Detail</b></td><td valign='top'>" + rblDetail.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Attendance</b></td><td valign='top'>" + rblAttendance.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Teamwork</b></td><td valign='top'>" + rblTeamwork.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Leadership Ability</b></td><td valign='top'>" + rblLeadership.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Acountability</b></td><td valign='top'>" + rblAccountability.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Personal Initiative</b></td><td valign='top'>" + rblInitiative.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Ability to Apply Feedback to future Work</b></td><td valign='top'>" + rblFeedback.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Time Management</b></td><td valign='top'>" + rblTime.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Ability to Manage Resources / Network</b></td><td valign='top'>" + rblResources.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Critical Thinking Skills</b></td><td valign='top'>" + rblThinking.SelectedValue + "</td></tr>";
            strBody += "<tr><td valign='top'><b>Additional Comments</b></td><td valign='top'>" + txtComment.Text.Trim() + "</td></tr>";
            if (stuOption1.Text.Trim() != "")
            {
                strBody += "<tr><td colspan='2'>In addition, the student requested to be rated in the following area(s):</td></tr>";
                strBody += "<tr><td valign='top'><b>" + stuOption1.Text + "</b></td><td valign='top'>" + rblStuOption1.SelectedValue + "</td></tr>";
                if (stuOption2.Text.Trim() != "")
                    strBody += "<tr><td valign='top'><b>" + stuOption2.Text + "</b></td><td valign='top'>" + rblStuOption2.SelectedValue + "</td></tr>";
                if (stuOption3.Text.Trim() != "")
                    strBody += "<tr><td valign='top'><b>" + stuOption3.Text + "</b></td><td valign='top'>" + rblStuOption3.SelectedValue + "</td></tr>";
            }
            strBody += "</tbody></table>";
            bool blIsHTML = true;
            SmtpClient objSender = new SmtpClient();
            if (sentFrom == "") { sentFrom = "CCSWebApp@ccs.spokane.edu"; }
            MailMessage objEmail = new MailMessage(sentFrom, sendTo, strSubject, strBody);
            StringBuilder objBuilder = new StringBuilder();
            objBuilder.Clear();
            try
            {
             //   strEmailResult = uEmail.SendEmailMsg(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses);
             //   strEmailResult = uEmail.SendEmailAndAttachment(sendTo, sentFrom, strSubject, true, strBody, strCCaddresses, uplRecommendFile.FileName);
                
                
                if (sendTo != "" && strSubject != "" && strBody != "")
                {
                    if (blIsHTML == false)
                    {
                        strBody = strBody.Replace("<br />", "\n");  //Convert to a horizontal tab/carriage return
                        strBody = strBody.Replace("&quot;", "\"");
                        strBody = strBody.Replace("&rsquo;", "\'");
                        strBody = strBody.Replace("&#39;", "\'");
                        strBody = strBody.Replace("<p>", "");
                        strBody = strBody.Replace("</p>", "\n\n");
                        strBody = strBody.Replace("&nbsp;", " ");
                        strBody = strBody.Replace("<strong>", "");
                        strBody = strBody.Replace("</strong>", "");
                        strBody = strBody.Replace("<b>", "");
                        strBody = strBody.Replace("</b>", "");
                        strSubject = strSubject.Replace("&#39;", "\'");
                        strSubject = strSubject.Replace("&quot;", "\"");
                    }
                    
                    objEmail.IsBodyHtml = blIsHTML;
                    string[] arrCCList = parseList(strCCaddresses);
                    int uBound = arrCCList.GetUpperBound(0);
                    if (strCCaddresses != "")
                    {
                        for (int i = 0; i < uBound; i++)
                        {
                            if (arrCCList[i] != "")
                            {
                                objEmail.CC.Add(arrCCList[i]);
                            }
                        }
                    }
                    // add attachment
                    string FileName = uplRecommendFile.FileName;
                    objEmail.Attachments.Add(new Attachment(uplRecommendFile.PostedFile.InputStream, FileName));
                }
                try
                {
                    //TESTING MODE: UNCOMMENT NEXT LINE BEFORE GOING LIVE
                    objSender.Send(objEmail);
                }
                catch (Exception exc)
                {
                    objBuilder.Append("<br /><b>Email Delivery Failed for " + sendTo + ".</b> Error returned: " + exc + ".");
                    Response.Write("Mail error: " + objBuilder.ToString());
                }
                if (objBuilder.ToString() == "")
                {
                    objBuilder.Append("Success");
                    strEmailResult = "Success";
                }
                Response.Write("TRY email result: " + objBuilder.ToString() + "<br />");
            } catch(Exception err)
            {
                Response.Write("failed email: " + err.Message + "<br />" + err.StackTrace + "<br />");
                Response.Write("CATCH email result: " + objBuilder.ToString() + "<vr />");
                return;
            }

            int intResult;
            if (strEmailResult == "Success")
            {
                // get values and save to the database
                try
                {
                    // update emailSent, date sent field in database (form field)
                    strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                        "SET DateRecommendationSent = '{0}' " +
                        "WHERE FacultyRecommendationID = {1}", DateTime.Now, intFormID);
                    intResult = stuRecommend.runSPQuery(strSQL, false);
                    Response.Write("email sent: " + strSQL + "<br />");
                    /*update table storing form fields
                     * check each value, if nothing selected, use ""
                     */
                    if (rblAcadABility.SelectedIndex == -1)
                        strAcadAbility = "";
                    else
                        strAcadAbility = rblAcadABility.SelectedValue;
                    if (rblVerbal.SelectedIndex == -1)
                        strVerbal = "";
                    else
                        strVerbal = rblVerbal.SelectedValue;
                    if (rblWritten.SelectedIndex == -1)
                        strWritten = "";
                    else
                        strWritten = rblWritten.SelectedValue;
                    if (rblDetail.SelectedIndex == -1)
                        strDetail = "";
                    else
                        strDetail = rblDetail.SelectedValue;
                    if (rblAttendance.SelectedIndex == -1)
                        strAttendance = "";
                    else
                        strAttendance = rblAttendance.SelectedValue;
                    if (rblTeamwork.SelectedIndex == -1)
                        strTeamwork = "";
                    else
                        strTeamwork = rblTeamwork.SelectedValue;
                    if (rblLeadership.SelectedIndex == -1)
                        strLeadership = "";
                    else
                        strLeadership = rblLeadership.SelectedValue;
                    if (rblAccountability.SelectedIndex == -1)
                        strAccountability = "";
                    else
                        strAccountability = rblAccountability.SelectedValue;
                    if (rblInitiative.SelectedIndex == -1)
                        strInitiative = "";
                    else
                        strInitiative = rblInitiative.SelectedValue;
                    if (rblFeedback.SelectedIndex == -1)
                        strFeedback = "";
                    else
                        strFeedback = rblFeedback.SelectedValue;
                    if (rblTime.SelectedIndex == -1)
                        strTime = "";
                    else
                        strTime = rblTime.SelectedValue;
                    if (rblResources.SelectedIndex == -1)
                        strResources = "";
                    else
                        strResources = rblResources.SelectedValue;
                    if (rblThinking.SelectedIndex == -1)
                        strThinking = "";
                    else
                        strThinking = rblThinking.SelectedValue;

                    strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                        "SET AcademicAbility = '{0}', VerbalCommunication = '{1}', WrittenCommunication = '{2}', AttentionDetail = '{3}', Attendance = '{4}', Teamwork = '{5}', Leadership = '{6}', " +
                        "Accountability = '{7}', Initiative = '{8}', Feedback = '{9}', TimeManagement = '{10}', Resources = '{11}', Thinking = '{12}', Comments = '{13}' " +
                        "WHERE FacultyRecommendationID = {14}", strAcadAbility, strVerbal, strWritten, strDetail, strAttendance, strTeamwork, strLeadership, strAccountability,
                        strInitiative, strFeedback, strTime, strResources, strThinking, txtComment.Text.Trim().Replace("'", "''"), intFormID);
                    intResult = stuRecommend.runSPQuery(strSQL, false);
                    // update the student requested areas to rate, if any
                    if (studentOptions1.Visible == true)
                    {
                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                            "SET Other_1_Choices = '{0}' WHERE FacultyRecommendationID = {1}", rblStuOption1.SelectedValue, intFormID);
                        intResult = stuRecommend.runSPQuery(strSQL, false);
                    }
                    if (studentOptions2.Visible == true)
                    {
                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                            "SET Other_2_Choices = '{0}' WHERE FacultyRecommendationID = {1}", rblStuOption2.SelectedValue, intFormID);
                        intResult = stuRecommend.runSPQuery(strSQL, false);
                    }
                    if (studentOptions3.Visible == true)
                    {
                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                            "SET Other_3_Choices = '{0}' WHERE FacultyRecommendationID = {1}", rblStuOption3.SelectedValue, intFormID);
                        intResult = stuRecommend.runSPQuery(strSQL, false);
                    }

                    //upload the recommendation pdf file, if any
                    if (uplRecommendFile.HasFile)
                    {
                        strFileName = lblStudentName.Text.Replace("'", "") + "-recommendationLetter-" + uplRecommendFile.FileName;
                        string strFileURL = strUploadURL + strFileName;
                        uplRecommendFile.SaveAs(strFileURL);
                        //update the table
                        strSQL = String.Format("UPDATE [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
                            "SET FileUpload = '{0}' WHERE FacultyREcommendationID = {1}", strFileURL, intFormID);
                        //intResult = stuRecommend.InsertFileInfo(intFormID, strUploadURL + strFileName);
                        intResult = stuRecommend.runSPQuery(strSQL, false);
                    }
                }
                catch (Exception ex)
                {
                    lblErrorMsg.Text = ex.Message + ex.StackTrace;
                }
                Response.Redirect("http://sfcc.spokane.edu/TEST-form/Form-Confirmation");
            }
            else
            {
                lblErrorMsg.Text = strEmailResult;
            }
        } else
        {
            // record already updated and email sent - values in db 
            lblAlreadyActed.Text = "<span style='color:#DD0000>This student recommendation has already been sent on " + hdnRecSent.Value + ".</span>";
        }
    }// end submit button

    /// <summary>
    /// Used to parse any semi-colon delineated list of items
    /// </summary>
    /// <param name="strList"></param>
    /// <returns></returns>
    public string[] parseList(string strList)
    {
        Int32 intLength = strList.Length;
        if (intLength > 0)
        {
            //Add a final semicolon if missing
            string strChar = strList.Substring(intLength - 1, 1);
            if (strChar != ";")
            {
                strList += ";";
            }
        }
        string[] arrStrList;
        arrStrList = strList.Split(';');
        return arrStrList;
    }

    public string CreateDurationString(string durationYears, string durationMonths)
    {
        string durationStringYear = "";
        string durationStringMonth = "";
        if (durationYears != "")
        {
            // 1 or more years
            if (durationYears == "1")
                durationStringYear = "1 year";
            else
                durationStringYear = durationYears + " years";
        } else
        {
            // no years
            durationStringYear = "";
        }
        // get month string
        if (durationMonths != "")
        {
            // 1 or more months
            if (durationMonths == "1")
                durationStringMonth = "1 month";
            else
                durationStringMonth = durationMonths + " months";
        }
        else
        {
            // no months
            durationStringMonth = "";
        }

        if (durationStringYear == "")
            return durationStringMonth;
        else if (durationStringMonth == "")
            return durationStringYear;
        else
            return durationStringYear + " and " + durationStringMonth;
        
    }

    protected void btnLogIn_Click(object sender, EventArgs e)
    {

        string strConnectionString = "";
        string strInstitution = "";
        // Set the following to the User ID
        //      string strDomainName = "";
        //string strADName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;  //Request.ServerVariables["LOGON_USER"];
        string UPN = "";
        string user_type = "";

        //Student testing
        //   string UPN = "CorneliusM0271@bigfoot.spokane.edu";
        //    string user_type = "bigfoot.spokane.edu";
        //employee testing
        //string UPN = "laura.padden@ccs.spokane.edu";
        //string user_type = "ccs.spokane.edu";
        UPN = txtUName.Text.Trim();
        string[] holder = UPN.Split('@');
        user_type = holder[1];
     //   Response.Write("uName: " + UPN + "; usertype: " + user_type + "<br />");
      //  UPN = "laura.padden@ccs.spokane.edu";
      //  user_type = "ccs.spokane.edu";
        

        if (user_type == "bigfoot.spokane.edu")
        {
            GetStudentInfo studentInfo = new GetStudentInfo(UPN, "", "", "StudentInfo");

            string user_firstname = studentInfo.strFirstName;
            string user_lastname = studentInfo.strLastName;
            string user_id = studentInfo.SystemID;
            string user_email = studentInfo.strEmail;
            string user_institution = studentInfo.strInstitution;
        //    Response.Write("User Instituion --  " + user_institution);

            if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
            {
                user_type = "Student";
                litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p>";
            }
        }
        else
        {
            GetEmployeeInfo employeeInfo = new GetEmployeeInfo(UPN, "", "", "EmployeeInfo");

            string user_firstname = employeeInfo.FirstName;
            string user_lastname = employeeInfo.LastName;
            string user_id = employeeInfo.SystemID;
            string user_email = employeeInfo.EmailAddress;
            string user_institution = "Community Colleges of Spokane";
            string user_phone = employeeInfo.PhoneNumber;

            if (user_id != null && user_firstname != null && user_lastname != null && user_email != null)
            {
                user_type = "employee";


                litMessage.Text += "<p>User ID: " + user_id + "</p><p> First Name: " + user_firstname + "</p><p> Last Name: " + user_lastname + "</p><p> Email: " + user_email + "</p><p> Institution: " + user_institution + "</p><p> User Type: " + user_type + "</p><p> Phone number: " + user_phone + "</p>";
                //Response.Write("user_type = " + user_type);
            }
        }
        if (user_type == "employee")
        {
            /* determine if login is correct using password supplied */
            string strUserName = holder[0];
            string strDomain = holder[1];
            switch (strDomain.ToLower())
            {
                case "bigfoot.spokane.edu":
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_Bigfoot"].ConnectionString;
                    strInstitution = "Student";
                    break;
                case "scc.spokane.edu":
                    strInstitution = "SCC";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
                case "sfcc.spokane.edu":
                    strInstitution = "SFCC";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
                default:
                    strInstitution = "CCS";
                    strConnectionString = ConfigurationManager.ConnectionStrings["LDAP_CCS"].ConnectionString;
                    break;
            }
            LdapAuthentication adAuth = new LdapAuthentication(strConnectionString);
            try
            {
                if (adAuth.IsAuthenticated(strDomain, strUserName, txtPWord.Text.Trim()))
                {
                    matchID.Visible = true;
                    litMessage.Text = "";
                    signIn.Visible = false;
                } else
                {
                    litMessage.Text = "<p><span style='color:#DD0000'>Authentication did not succeed. Check user name and password</span>.</p>";
                    signIn.Visible = true;
                    matchID.Visible = false;
                }
            } catch (Exception ex)
            {
                litMessage.Text = "<p><span style='color:#DD0000'>Authentication error - the user name or password is incorrect</span>.<p>";
                    //ex.Message + "<br />" + ex.StackTrace;
            }
        }
        else
        {
            litMessage.Text = "<p><span style='color:#DD0000'> There was a problem logging in</span>.</p>";
            noMatchID.Visible = true;
            matchID.Visible = false;
            return;
        }
        intFormID = Convert.ToInt32(Request.QueryString["i"]);
        lblErrorMsg.Text = "";
        strSQL = String.Format("SELECT * FROM [KenticoCCSNew].[dbo].[Form_ccsglobal_FacultyRecommendation] " +
            "WHERE FacultyRecommendationID = {0} ", intFormID);
        DataTable dtForm = stuRecommend.RunGetQuery(strSQL);
        if (dtForm != null)
        {
            if (dtForm.Rows.Count > 0)
            {
                if (dtForm.Rows[0]["DateRecommendationSent"].ToString() != "")
                {
                    string sentDate = Convert.ToDateTime(dtForm.Rows[0]["DateRecommendationSent"].ToString()).ToShortDateString();
                    lblErrorMsg.Text = "<p><span style='color:#DD0000;'>This recommendation has already been sent on " + sentDate + ".  You will be unable to modify it</span>.</p>";
                    // disable all fields
                    btnCmdSubmit.Enabled = false;
                    // populate fields
                    if (dtForm.Rows[0]["AcademicAbility"].ToString() != "")
                        rblAcadABility.SelectedValue = dtForm.Rows[0]["AcademicAbility"].ToString();
                    rblAcadABility.Enabled = false;
                    if (dtForm.Rows[0]["VerbalCommunication"].ToString() != "")
                        rblVerbal.SelectedValue = dtForm.Rows[0]["VerbalCommunication"].ToString();
                    rblVerbal.Enabled = false;
                    if (dtForm.Rows[0]["WrittenCommunication"].ToString() != "")
                        rblWritten.SelectedValue = dtForm.Rows[0]["WrittenCommunication"].ToString();
                    rblWritten.Enabled = false;
                    if (dtForm.Rows[0]["AttentionDetail"].ToString() != "")
                        rblDetail.SelectedValue = dtForm.Rows[0]["AttentionDetail"].ToString();
                    rblDetail.Enabled = false;
                    if (dtForm.Rows[0]["Attendance"].ToString() != "")
                        rblAttendance.SelectedValue = dtForm.Rows[0]["Attendance"].ToString();
                    rblAttendance.Enabled = false;
                    if(dtForm.Rows[0]["Teamwork"].ToString() != "")
                        rblTeamwork.SelectedValue = dtForm.Rows[0]["Teamwork"].ToString();
                    rblTeamwork.Enabled = false;
                    if (dtForm.Rows[0]["Leadership"].ToString() != "")
                        rblLeadership.SelectedValue = dtForm.Rows[0]["Leadership"].ToString();
                    rblLeadership.Enabled = false;
                    if (dtForm.Rows[0]["Accountability"].ToString() != "")
                        rblAccountability.SelectedValue = dtForm.Rows[0]["Accountability"].ToString();
                    rblAccountability.Enabled = false;
                    if (dtForm.Rows[0]["Initiative"].ToString() != "")
                        rblInitiative.SelectedValue = dtForm.Rows[0]["Initiative"].ToString();
                    rblInitiative.Enabled = false;
                    if (dtForm.Rows[0]["Feedback"].ToString() != "")
                        rblFeedback.SelectedValue = dtForm.Rows[0]["Feedback"].ToString();
                    rblFeedback.Enabled = false;
                    if (dtForm.Rows[0]["TimeManagement"].ToString() != "")
                        rblTime.SelectedValue = dtForm.Rows[0]["TimeManagement"].ToString();
                    rblTime.Enabled = false;
                    if (dtForm.Rows[0]["Resources"].ToString() != "")
                        rblResources.SelectedValue = dtForm.Rows[0]["Resources"].ToString();
                    rblResources.Enabled = false;
                    if (dtForm.Rows[0]["Thinking"].ToString() != "")
                        rblThinking.SelectedValue = dtForm.Rows[0]["Thinking"].ToString();
                    rblThinking.Enabled = false;
                    if (dtForm.Rows[0]["Other_1_Choices"].ToString() != "")
                        rblStuOption1.SelectedValue = dtForm.Rows[0]["Other_1_Choices"].ToString();
                    rblStuOption1.Enabled = false;
                    if (dtForm.Rows[0]["Other_2_Choices"].ToString() != "")
                        rblStuOption2.SelectedValue = dtForm.Rows[0]["Other_2_Choices"].ToString();
                    rblStuOption2.Enabled = false;
                    if (dtForm.Rows[0]["Other_3_Choices"].ToString() != "")
                        rblStuOption3.SelectedValue = dtForm.Rows[0]["Other_3_Choices"].ToString();
                    rblStuOption3.Enabled = false;
                    txtComment.Text = dtForm.Rows[0]["Comments"].ToString().Replace("''", "'");
                    int idx = dtForm.Rows[0]["FileUpload"].ToString().LastIndexOf('\\');
                    if (idx != -1)
                    {
                        string fileName = dtForm.Rows[0]["FileUpload"].ToString().Substring(idx + 1);
                        lblFileUploaded.Text = "<b>Name of file uploaded</b>: " + fileName + "<p>&nbsp;</p>";
                    }
                    uplRecommendFile.Enabled = false;
                }
                lblSendWhere.Text = "<i>Upon form submission, this recommendation will be sent to the SFCC registrar. In addition, it will be sent to ";
                if (dtForm.Rows[0]["WhereSent"].ToString().Trim() == "Yes")  //stundent answered 'yes' send to institution
                {
                    lblSendWhere.Text += dtForm.Rows[0]["NameOfInstitution"].ToString() + " at the provided email address of:  " + dtForm.Rows[0]["SendToEmail"].ToString() + "</i>";
                }
                else
                {
                    lblSendWhere.Text += "the Student's email (" + dtForm.Rows[0]["StudentEmail"].ToString() + ") and the student is then responsible for mailing it out.</i>";
                }
                if (dtForm.Rows[0]["FERPAwaiver"].ToString().Trim() == "not waive")
                {
                    lblSendWhere.Text += "<br /><i>This student did NOT waive their right to review the recommendation so a copy of the responses will be sent to the student as well.</i>";
                }
                lblStudentName.Text = dtForm.Rows[0]["StudentName"].ToString().Replace("''", "'");
                lblCtcLinkID.Text = dtForm.Rows[0]["StudentID"].ToString();
                lblStudentEmail.Text = dtForm.Rows[0]["StudentEmail"].ToString();
                strBuildAttend = dtForm.Rows[0]["StartMonth"].ToString() + "/" + dtForm.Rows[0]["StartYear"].ToString();
                if (dtForm.Rows[0]["endYear"].ToString().Trim().Length <= 4)
                {
                    strBuildAttend += " - " + dtForm.Rows[0]["EndMonth"].ToString() + "/" + dtForm.Rows[0]["EndYear"].ToString();
                }
                else
                {
                    strBuildAttend += " - " + dtForm.Rows[0]["EndYear"].ToString();
                }
                litDatesAttend.Text = strBuildAttend;
                litProgramOfStudy.Text = dtForm.Rows[0]["ProgramOfStudy"].ToString();
                litDepartment.Text = dtForm.Rows[0]["Department"].ToString();
                litFacultyStaffName.Text = dtForm.Rows[0]["Faculty_StaffFirstName"].ToString();
                litFacultyStaffEmail.Text = dtForm.Rows[0]["Faculty_StaffEmail"].ToString();
                strFullDuration = CreateDurationString(dtForm.Rows[0]["KnownFacYears"].ToString(), dtForm.Rows[0]["KnownFacMonths"].ToString());
                // lblLengthKnown.Text = dtForm.Rows[0]["KnownFacYears"].ToString() +  " year(s) and " + dtForm.Rows[0]["KnownFacMonths"].ToString() + " month(s)";
                lblLengthKnown.Text = strFullDuration;
                lblListCapacity.Text = "";
                lblListDuration.Text = "";
                if (dtForm.Rows[0]["CapacityStudent"].ToString() == "True")
                {
                    lblListCapacity.Text += "Student<br />";
                    strFullDuration = CreateDurationString(dtForm.Rows[0]["StudentYears"].ToString(), dtForm.Rows[0]["StudentMonths"].ToString());
                    //    lblListDuration.Text += dtForm.Rows[0]["StudentYears"].ToString() + " year(s) and " + dtForm.Rows[0]["StudentMonths"].ToString() + " months<br />";
                    lblListDuration.Text += strFullDuration + "<br />";
                }
                if (dtForm.Rows[0]["CapacityWorkStudy"].ToString() == "True")
                {
                    lblListCapacity.Text += "Work Study<br />";
                    //   lblListDuration.Text += dtForm.Rows[0]["WorkStudyYears"].ToString() + " year(s) and " + dtForm.Rows[0]["WorkStudyMonths"].ToString() + " months<br />";
                    strFullDuration = CreateDurationString(dtForm.Rows[0]["WorkStudyYears"].ToString(), dtForm.Rows[0]["WorkStudyMonths"].ToString());
                    lblListDuration.Text += strFullDuration + "<br />";
                }
                if (dtForm.Rows[0]["CapacityMentor"].ToString() == "True")
                {
                    lblListCapacity.Text += "Mentor<br />";
                    //  lblListDuration.Text += dtForm.Rows[0]["MentorYears"].ToString() + " year(s) and " + dtForm.Rows[0]["MentorMonths"].ToString() + " months<br />";
                    strFullDuration = CreateDurationString(dtForm.Rows[0]["MentorYears"].ToString(), dtForm.Rows[0]["MentorMonths"].ToString());
                    lblListDuration.Text += strFullDuration + "<br />";
                }
                if (dtForm.Rows[0]["CapacityTutor"].ToString() == "True")
                {
                    lblListCapacity.Text += "Tutor<br />";
                    //  lblListDuration.Text += dtForm.Rows[0]["TutorYears"].ToString() + " year(s) and " + dtForm.Rows[0]["TutorMonths"].ToString() + " months<br />";
                    strFullDuration = CreateDurationString(dtForm.Rows[0]["TutorYears"].ToString(), dtForm.Rows[0]["TutorMonths"].ToString());
                    lblListDuration.Text += strFullDuration + "<br />";
                }
                if (dtForm.Rows[0]["CapacityOther"].ToString() == "True")
                {
                    lblListCapacity.Text += dtForm.Rows[0]["TitleOther"].ToString() + "<br />";
                    //  lblListDuration.Text += dtForm.Rows[0]["OtherYears"].ToString() + " year(s) and " + dtForm.Rows[0]["OtherMonths"].ToString() + " months<br />";
                    strFullDuration = CreateDurationString(dtForm.Rows[0]["OtherYears"].ToString(), dtForm.Rows[0]["OtherMonths"].ToString());
                    lblListDuration.Text += strFullDuration + "<br />";
                }
                if (dtForm.Rows[0]["Other_1_Skill"].ToString().Trim() != "")
                {
                    studentOptions1.Visible = true;
                    stuOption1.Text = dtForm.Rows[0]["Other_1_Skill"].ToString().Trim();
                }
                if (dtForm.Rows[0]["Other_2_Skill"].ToString().Trim() != "")
                {
                    studentOptions2.Visible = true;
                    stuOption2.Text = dtForm.Rows[0]["Other_2_Skill"].ToString().Trim();
                }

                if (dtForm.Rows[0]["Other_3_Skill"].ToString().Trim() != "")
                {
                    studentOptions3.Visible = true;
                    stuOption3.Text = dtForm.Rows[0]["Other_3_Skill"].ToString().Trim();
                }
                if (dtForm.Rows[0]["DateRecommendationSent"].ToString().Trim() != "")
                    hdnRecSent.Value = Convert.ToDateTime(dtForm.Rows[0]["DateRecommendationSent"].ToString().Trim()).ToShortDateString();
                hdnFacStaffEmail.Value = dtForm.Rows[0]["Faculty_StaffEmail"].ToString().Trim();
                hdnStudentEmail.Value = dtForm.Rows[0]["StudentEmail"].ToString().Trim();
                hdnSendToEmail.Value = dtForm.Rows[0]["SendToEmail"].ToString().Trim();
                hdnFERPAwaiver.Value = dtForm.Rows[0]["FERPAwaiver"].ToString().Trim();
                hdnStudentName.Value = dtForm.Rows[0]["StudentName"].ToString().Trim().Replace("''", "'");
                hdnSendInstitution.Value = dtForm.Rows[0]["NameOfInstitution"].ToString().Trim();
            }
        }
    }
}