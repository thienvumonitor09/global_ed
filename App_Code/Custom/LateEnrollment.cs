﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Summary description for LateEnrollment
/// </summary>
public class LateEnrollment
{
    public String strResultMsg = "";
    public Int32 intNewIdentity = 0;
    public Boolean blSuccess = false;
    String strSQL = "";
    Boolean blReturnIdentity = false;

    public LateEnrollment()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable RunGetQuery(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        //strResultMsg += "RunGetQuery: " + strQuery + "<br />";
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            return ds.Tables[0];
        }
        catch (Exception ex)
        {
            strResultMsg += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return null;
    }

    public DataTable RunGetQueryODS(string strQuery)
    {
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        //strResultMsg += "RunGetQuery: " + strQuery + "<br />";
        try
        {
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
            objConn.Open();
            objCommand.CommandText = strQuery;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;

            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds);
            return ds.Tables[0];
        }
        catch (Exception ex)
        {
            strResultMsg += "Error RunGetQuery: " + ex.Message + "<br />SQL: " + strQuery + "<br />";
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return null;
    }
    public Int32 runSPQuery(string strSQL, bool blIsInsert)
    {
        Int32 intResult = 0;
        blSuccess = true;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        //strResultMsg += "runSPQuery: " + strSQL + "<br />";
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            if (blIsInsert)
            {
                //Execute scalar
                intResult = (Int32)objCommand.ExecuteScalar();
            }
            else
            {
                //Execute NonQuery
                intResult = objCommand.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            //capture error
            strResultMsg += "Error runSPQuery: " + ex.Message + "<br />";
            strResultMsg += "SQL: " + strSQL + "<br />";
            blSuccess = false;
            return intResult;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return intResult;
    }
}