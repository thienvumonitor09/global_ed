﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections;

/// <summary>
/// Displays ctcLink continuing ed classes by institution filtered by a class ID
/// </summary>
public class ContinuingEdClasses
{
    private SqlConnection ctcLinkCon;
    public ContinuingEdClasses()
    {
        ctcLinkCon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString_CCSGenCTCLink"]);
        ctcLinkCon.Open();
    }

    public ArrayList Query(String sql)
    {
        ArrayList result = new ArrayList();
        ArrayList row;
        // sql = sql.Replace('@', ' ');
        SqlCommand cmd = new SqlCommand(sql, ctcLinkCon);
        cmd.CommandType = CommandType.Text;
        SqlDataReader reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            row = new ArrayList();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                row.Add(reader[i]);
            }
            result.Add(row);
        }
        reader.Close();
        return result;
    }

    public void NonQuery(String sql)
    {
        SqlCommand cmd = new SqlCommand(sql, ctcLinkCon);
        cmd.CommandType = CommandType.Text;
        cmd.ExecuteNonQuery();
    }

    /// <summary>
    /// Stored procedure which will be returned in an arraylist.
    /// string procedure: sp name
    /// parameters: params in an arraylist where each entry is an array containing:
    ///             0: Variable name (remember the "@" in front.)
    ///             1: Size (for strings)
    ///             2: Direction (i.e. input/output)
    ///             3: Value (for input)
    ///             4: Data type
    /// For ease of use add "input" variables first, then output.
    /// </summary>

    public ArrayList SPQueryRO(string procedure, ArrayList parameters, bool returnvalues)
    {
        SqlCommand sp = new SqlCommand(procedure, ctcLinkCon);
        sp.CommandType = CommandType.StoredProcedure;
        ArrayList result = new ArrayList();
        ArrayList outparameters = new ArrayList();

        // Set parameters
        for (int i = 0; i < parameters.Count; i++)
        {
            string df = ((ArrayList)parameters[i])[0].ToString();
            SqlDbType sd = (SqlDbType)((ArrayList)parameters[i])[4];
            int len = Convert.ToInt16(((ArrayList)parameters[i])[1]);
            string inout = ((ArrayList)parameters[i])[2].ToString();
            string val = ((ArrayList)parameters[i])[3].ToString();
            sp.Parameters.Add(df, sd, len);
            if (inout == "input")
            {
                sp.Parameters[df].Value = val;
            }
        }

        // Execute and grab data
        if ((returnvalues))
        {
            SqlDataReader reader = sp.ExecuteReader();
            while (reader.Read())
            {
                ArrayList row = new ArrayList();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row.Add(reader[i]);
                }
                result.Add(row);
            }
            reader.Close();
            reader = null;
        }
        else
        {
            ArrayList row = new ArrayList();
            row.Add(sp.ExecuteNonQuery());
            result.Add(row);
        }

        // Clean up
        sp.Dispose();
        sp = null;
        return result;
        // dbcon.Close();

    }

    public DataSet SPQueryDS(string procedure, ArrayList parameters, bool returnvalues)
    {
        SqlDataAdapter da = new SqlDataAdapter(procedure, ctcLinkCon);
        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        DataSet ds = new DataSet();

        // Set parameters
        for (int i = 0; i < parameters.Count; i++)
        {
            string df = ((ArrayList)parameters[i])[0].ToString();
            SqlDbType sd = (SqlDbType)((ArrayList)parameters[i])[4];
            int len = Convert.ToInt16(((ArrayList)parameters[i])[1]);
            string inout = ((ArrayList)parameters[i])[2].ToString();
            string val = ((ArrayList)parameters[i])[3].ToString();
            da.SelectCommand.Parameters.AddWithValue(df, val);
        }

        da.Fill(ds);

        // Clean up
        da.Dispose();

        return ds;
    }

    public void StartTrans()
    {
        NonQuery("IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRAN END");
        NonQuery("BEGIN TRAN");
    }

    public void EndTrans()
    {
        NonQuery("IF @@TRANCOUNT > 0 BEGIN COMMIT TRAN END");
    }

    public void RollbackTrans()
    {
        NonQuery("IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRAN END");
    }

    public void CloseCon()
    {
        ctcLinkCon.Close();
    }
}