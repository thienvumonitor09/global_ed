﻿using System;
using System.Text;
using System.Collections;
using System.DirectoryServices;

namespace FormsAuthAD
{
    public class LdapAuthentication
    {
        private String _path;

        public LdapAuthentication(String path)
        {
            _path = path;
        }

        public Boolean IsAuthenticated(String domain, String username, String pwd)
        {
            if (domain.ToLower() == "dist") { domain = "district17_adm"; }
            String domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);
            //System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add("Bob.Nelson@ccs.spokane.edu");
            //message.Subject = "Login troubleshooting";
            //message.From = new System.Net.Mail.MailAddress("CCSWebApp@ccs.spokane.edu");
            //message.Body = _path + ", " + domainAndUsername + ", ";
            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("134.39.170.248");
            //smtp.Send(message);
            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("memberOf");
                SearchResult result = search.FindOne();
                if (result == null)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error authenticating user. " + ex.Message);
            }

            return true;
        }

        public String GetGroups(String username)
        {
            DirectoryEntry entry = new DirectoryEntry(_path);
            DirectorySearcher search = new DirectorySearcher(entry);
            search.Filter = "(SAMAccountName=" + username + ")";
            search.PropertiesToLoad.Add("memberOf");
            StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();
                if (result != null)
                {
                    int propertyCount = result.Properties["memberOf"].Count;

                    String dn, groupName;
                    int equalsIndex, commaIndex;
                    // Sample property value:  CN=SFCCComport,OU=Staff and Faculty Roles,OU=Staff and Faculty Users,DC=sfcc,DC=spokane,DC=cc,DC=wa,DC=us
                    for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                    {
                        groupName = "";
                        dn = (String)result.Properties["memberOf"][propertyCounter];

                        dn = dn.Replace("SCC\\,", "SCC[comma]"); //Special handling for groups with names that include commas.

                        equalsIndex = dn.IndexOf("=", 1);
                        commaIndex = dn.IndexOf(",", 1);
                        if (-1 == equalsIndex)
                        {
                            return null;
                        }

                        groupName = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1);
                        // In order to limit FormsAuthenticationTicket size bloat (http://stackoverflow.com/questions/7344110/maximum-length-of-formsauthenticationticket-userdata-property),
                        // the line below excludes any groups that do not match patterns which appear in groups used to control access to apps.spokane.edu resources. ~ bn 2014-11-06
                        if (groupName.ToUpper().Contains("ADMIN") || groupName.ToUpper().Contains("ACCESS") || groupName.ToUpper().Contains("CFE-") || groupName.ToUpper().Contains("EDIT"))
                        {
                          groupNames.Append(groupName);
                          groupNames.Append("|");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error obtaining group names. " + ex.Message);
            }
            return groupNames.ToString().Replace("[comma]", ","); //Special handling for groups with names that include commas.
        }
    }
}
