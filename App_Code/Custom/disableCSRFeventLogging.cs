﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CMS;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Base;

/// <summary>
/// Summary description for disableCSRFeventLogging
/// </summary>
/// 
// Registers the custom module into the system
[assembly: RegisterModule(typeof(disableCSRFeventLogging))]
public class disableCSRFeventLogging : Module
{
    // Module class constructor, the system registers the module under the name "CustomInit"
    public disableCSRFeventLogging()
        : base("CSRFlogging")
    {
    }
    // Contains initialization code that is executed when the application starts
    protected override void OnInit()
    {
        base.OnInit();

        // Assigns a handler to the LogEvent.Before event
        EventLogEvents.LogEvent.Before += LogEvent_Before;
    }

    private void LogEvent_Before(object sender, LogEventArgs e)
    {
        // Gets an object representing the event that is being logged
        EventLogInfo eventLogRecord = e.Event;

        // Cancels logging for events with the "CREATEOBJ" or "UPDATEOBJ" event codes.
        // Disables event log records notifying about the creation or update of objects in the system,
        // but still allows events related to object deletion.
        string eventDesc = eventLogRecord.EventDescription;
        if (eventDesc.Contains("CSRF cookie was missing"))
        {
            e.Cancel();
        }
    }
}