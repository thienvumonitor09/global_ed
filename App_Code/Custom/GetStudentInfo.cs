﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GetStudentInfo
/// </summary>
public class GetStudentInfo
{
    public string SystemID;
    public string strFullName;
    public string strFirstName;
    public string strLastName;
    public string strEmail;
    public string strPhone1;
    public string strAddr1;
    public string strAddr2;
    public string strCity;
    public string strState;
    public string strZIP;
    public string strSID;
    public string strInstitution;
    
    public GetStudentInfo(string strAccountName, string strSID, string strColCode, string strLookupType)
	{

        switch(strLookupType)
        {
            case "SMS":
                //Get Legacy data
                GetStudentSMSInfo(strSID, strColCode);
                break;
            case "PS":
                //get PeopleSoft
                GetStudentPSInfo(strSID);
                break;
            case "AD":
                //Active Directory
                GetStudentADInfo(strAccountName);
                break;
            case "StudentInfo":
                //Active Directory contact information
                GetEmployeeStudentAndEmployeeInfo(strAccountName);
                break;
        }
    }

    private void GetEmployeeStudentAndEmployeeInfo(string strAccountName)
    {
        //DEPENDS UPON Sudent and Employee Info
        string strSQL = "usp_GetStudentInfo";
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_StudentAndEmployeeInfo"].ToString();
        //try
        //{
        objConn.Open();
        objCommand.CommandText = strSQL;
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Connection = objConn;

        SqlParameter objParam = new SqlParameter();
        objParam.ParameterName = "@ADLOGIN";
        objParam.Size = 200;
        objParam.SqlDbType = SqlDbType.Char;

        objParam.Value = strAccountName;
        //objParam.Value = "jeremy.anderson@ccs.spokane.edu";
        objCommand.Parameters.Add(objParam);

        SqlDataReader reader = objCommand.ExecuteReader();

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                SystemID = Convert.ToString(reader.GetString(2));
                strFirstName = Convert.ToString(reader.GetString(0));
                strLastName = Convert.ToString(reader.GetString(1));
                strEmail = Convert.ToString(reader.GetString(3));
                strInstitution = Convert.ToString(reader.GetString(4));
               
            }
        }
        else
        {
            Console.WriteLine("No rows found.");
        }
        reader.Close();

        //}

        //catch (Exception ex)
        //{
        //    ErrorMessage = ex.Message;
        //}
        //finally
        //{
        //    objConn.Close();
        //    objCommand.Dispose();
        //}

    }

    private void GetStudentSMSInfo(string strSID, string strColCode)
    {
        string strSQL = "usp_GetOneStu_D '" + strSID + "', '" + strColCode + "'";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ToString();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;

        try
        {
            objConn.ConnectionString = strConnectionString;
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    strFullName = objReader["STU_NAME"].ToString();
                    strEmail = objReader["StuEmailAddr"].ToString();
                    strPhone1 = "(" + objReader["DAY_AREA_CODE"].ToString() + ") " + objReader["DAY_PREFIX"].ToString() + "-" + objReader["DAY_SUFFIX"].ToString();
                    strAddr1 = objReader["STU_STREET"].ToString();
                    strCity = objReader["STU_CITY"].ToString();
                    strState = objReader["STU_ST"].ToString();
                    strZIP = objReader["STU_ZIP"].ToString();
                }
            }
            objReader.Dispose();
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
    }

    private void GetStudentADInfo(string strADName)
    {
        string strDirectoryEntry = "LDAP://OU=Student Accounts,DC=student,DC=spokane,DC=cc,DC=wa,DC=us";
        try
        {
            //strADName = "AUSTINB7379";
            DirectoryEntry de = new DirectoryEntry(strDirectoryEntry);
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(&(sAMAccountName=" + strADName + "))"; // search filter
            deSearch.SearchScope = SearchScope.Subtree;
            deSearch.PropertiesToLoad.Add("sn"); // last name
            deSearch.PropertiesToLoad.Add("givenName"); // first name
            deSearch.PropertiesToLoad.Add("cn"); // full name
            deSearch.PropertiesToLoad.Add("mail"); // email address
            deSearch.PropertiesToLoad.Add("company"); // Institution
            deSearch.PropertiesToLoad.Add("employeeID"); // EmplID
            //Do the search
            SearchResult objResult = deSearch.FindOne();
            DirectoryEntry dirEntry = objResult.GetDirectoryEntry();
            //Fetch the properties
            strFullName = dirEntry.Properties["cn"].Value.ToString();
            strFirstName = dirEntry.Properties["givenName"].Value.ToString();
            strLastName = dirEntry.Properties["sn"].Value.ToString();
            strEmail = dirEntry.Properties["mail"].Value.ToString();
            strSID = dirEntry.Properties["employeeID"].Value.ToString();
            strInstitution = dirEntry.Properties["company"].Value.ToString();
            //Load the array
        }
        catch
        {
            //do nothing
        }
    }

    private void GetStudentPSInfo(string strEmplID)
    {
        string strSQL = "usp_StudentInformation_ByEMPLID '" + strEmplID + "'";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;

        try
        {
            objConn.ConnectionString = strConnectionString;
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    strFirstName = objReader["FIRST_NAME"].ToString();
                    strLastName = objReader["LAST_NAME"].ToString();
                    strEmail = objReader["PREFEMAILADDR"].ToString();
                    string strTemp = objReader["PREFPHONE"].ToString();
                    if (strTemp.Contains("/"))
                    {
                        string[] arrPhone = strTemp.Split(new Char[] { '/' });
                        strPhone1 = "(" + arrPhone[0] + ") " + arrPhone[1];
                    }
                    else
                    {
                        strPhone1 = strTemp;
                    }
                    strAddr1 = objReader["ADDRESS1"].ToString();
                    strAddr2 = objReader["ADDRESS2"].ToString();
                    strCity = objReader["CITY"].ToString();
                    strState = objReader["ST"].ToString();
                    strZIP = objReader["ZIP"].ToString();
                    strTemp = objReader["INSTITUTION"].ToString();
                    strInstitution = GetInstitution(strTemp);
                    strEmail = objReader["PREFEMAILADDR"].ToString();
                }
            }
            objReader.Dispose();
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
    }

    private string GetInstitution(string strColCode)
    {
        string strCollege = "";
        if (strColCode == "WA171")
        {
            strCollege = "Spokane Community College";
        }
        else if (strColCode == "WA172")
        {
            strCollege = "Spokane Falls Community College";
        }
        return strCollege;
    }
}
