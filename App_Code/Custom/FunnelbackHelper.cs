﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Data;
using CMS.EventLog;
using System.Data.SqlTypes;
using HM.Funnelback.KenticoPush;
using CMS.SiteProvider;

namespace FunnelbackHelper
{
    public class ConnectionHelper
    {
        private string clientEndpoint;

        public string GetClientEndpoint()
        {
            return clientEndpoint;
        }

        public void SetClientEndpoint(string value)
        {
            clientEndpoint = value;
        }

        private string collection;

        public string GetCollection()
        {
            return collection;
        }

        public void SetCollection(string value)
        {
            collection = value;
        }

        private string form;

        public string GetForm()
        {
            return form;
        }

        public void SetForm(string value)
        {
            form = value;
        }

        private string query;

        public string GetQuery()
        {
            return query;
        }

        public void SetQuery(string value)
        {
            query = value;
        }

        /// <summary>
        /// Gets JSON response from Funnelback, using a query (from a textbox).
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="collection"></param>
        /// <param name="form"></param>
        /// <param name="query"></param>
        /// <returns>Deserialized JSON object</returns>
        public JObject RequestJSON(string endpoint, string collection, string form, string query)
        {
            SetClientEndpoint(endpoint);
            SetCollection(collection);
            SetForm(form);
            SetQuery(query);

            var client = new RestClient(endpoint);

            var request = new RestRequest("search.json", Method.GET);
            request.AddParameter("collection", collection);
            request.AddParameter("form", form);
            request.AddParameter("query", query);
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(SiteContext.CurrentSiteName));
            request.AddHeader("Content-Type", "text/json");

            //Execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; //Raw content as string for parsing

            JObject responseJobj = JsonConvert.DeserializeObject<JObject>(response.Content);

            return responseJobj;
        }

        /// <summary>
        /// Overload to manually provide all query string parameters as a string.
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="queryParameters"></param>
        /// <returns>Deserialized JSON object</returns>
        public JObject RequestJSON(string endpoint, string queryParameters)
        {
            SetClientEndpoint(endpoint);

            var client = new RestClient(endpoint);

            var request = new RestRequest("search.json" + queryParameters, Method.GET);
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(SiteContext.CurrentSiteName));
            request.AddHeader("Content-Type", "text/json");

            //Execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; //Raw content as string
            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpClient", "endpoint = " + endpoint + "|client = " + client.BaseUrl.ToString() + "|request = " + request.Resource);
            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpResponse", "ContentType = " + response.ContentType + "|StatusCode = " + response.StatusCode);
            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpResponseContent", response.Content);

            JObject responseJobj = JsonConvert.DeserializeObject<JObject>(response.Content);

            return responseJobj;
        }

        /// <summary>
        /// Gets JSON response from Funnelback, using a metadata field and an array of GUIDs representing items to retrieve.
        /// Used to retrieve specific items to render.
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="collection"></param>
        /// <param name="metadataField"></param>
        /// <param name="metadataOperator"></param>
        /// <param name="guidArray"></param>
        /// <returns>Deserialized JSON object</returns>
        public JObject RequestJSON(string endpoint, string collection, string metadataField, string metadataOperator, string[] guidArray)
        {
            SetClientEndpoint(endpoint);
            SetCollection(collection);

            string metadataQuery = "meta_" + metadataField + "_" + metadataOperator;
            string metadataQueryValue = "";

            if (guidArray != null)
            {
                //metadataQueryValue = "[";
                metadataQueryValue = string.Join(" ", guidArray);
                //metadataQueryValue += "]";
            }

            var client = new RestClient(endpoint);

			var request = new RestRequest("search.json", Method.GET);
            //var request = new RestRequest("search.json?timespec=" + DateTime.Now.ToString("yyyy-dd-M--HH-mm"), Method.GET); // Attempt to limit caching by adding minute-specific querystring value
            request.AddParameter("collection", collection);
            request.AddParameter(metadataQuery, metadataQueryValue);
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(SiteContext.CurrentSiteName));
            request.AddHeader("Content-Type", "text/json");
			//request.AddHeader("Cache-Control", "max-age=60"); // set cache max-age to 60 seconds (was 300 seconds previously) - BN 2018-10-31

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; //Raw content as string

            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpResponse", response.ToString());
            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpResponseContent", response.Content);
            //EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpClient", "endpoint = " + endpoint + "|client = " + client.BaseUrl.ToString() + "|request = " + request.Resource + "|collection = " + collection + "|metadataQueryValue = " + metadataQueryValue);
            EventLogProvider.LogInformation(EventType.INFORMATION, "FBHelpRespContJSON_01", response.Content);

            JObject responseJobj = JsonConvert.DeserializeObject<JObject>(response.Content);

            return responseJobj;
        }

        public DataTable ConvertResponseToTable(JObject jo)
        {
            var response = jo["response"];

            var resultPacket = response["resultPacket"];

            var results = resultPacket["results"];

            List<JToken> resultsList = new List<JToken>();

            foreach (var result in results)
            {
                resultsList.Add(result);
            }

            DataTable dt = new DataTable("fbdata");

            JToken item;
            JToken jtoken;

            for (int i = 0; i <= resultsList.Count - 1; i++)
            {

                item = resultsList[i];
                jtoken = item.First;

                while (jtoken != null)
                {
                    if (((JProperty)jtoken).Name.ToString() == "metaData")
                    {
                        Type type = typeof(JToken);
                        JToken metadataObj = jtoken.First;

                        JToken prop = metadataObj.First;

                        while (prop != null)
                        {
                            if (!dt.Columns.Contains("" + ((JProperty)prop).Name.ToString()))
                            {
                                dt.Columns.Add(new DataColumn("" + ((JProperty)prop).Name.ToString()));
                            }

                            prop = prop.Next;
                        }
                    }
                    else
                    {
                        //Add "search_" prefix to columns not located in metadata object. Fixes duplicate object property names, e.g. Funnelback system field "Title" and custom page type column "Title"
                        if (!dt.Columns.Contains("search_" + ((JProperty)jtoken).Name.ToString()))
                        {
                            dt.Columns.Add(new DataColumn("search_" + ((JProperty)jtoken).Name.ToString()));
                        }
                    }

                    jtoken = jtoken.Next;
                }
            }

            for (int i = 0; i <= resultsList.Count - 1; i++)
            {
                // Add each of the columns into a new row then put that new row into the DataTable
                item = resultsList[i];
                jtoken = item.First;

                // Create the new row, put the values into the columns then add the row to the DataTable
                DataRow dr = dt.NewRow();

                while (jtoken != null)
                {
                    if (((JProperty)jtoken).Name.ToString() == "metaData")
                    {
                        Type type = typeof(JToken);
                        JToken metadataObj = jtoken.First;

                        JToken prop = metadataObj.First;

                        while (prop != null)
                        {
                            if (dt.Columns.Contains("" + ((JProperty)prop).Name.ToString()))
                            {
                                dr["" + ((JProperty)prop).Name.ToString()] = ((JProperty)prop).Value.ToString();
                            }

                            prop = prop.Next;
                        }
                        jtoken = jtoken.Next;
                    }
                    else
                    {
                        if (dt.Columns.Contains("search_" + ((JProperty)jtoken).Name.ToString()))
                        {
                            try
                            {
                                dr["search_" + ((JProperty)jtoken).Name.ToString()] = ((JProperty)jtoken).Value.ToString();
                            }
                            catch (Exception ex)
                            {
                                EventLogProvider.LogInformation(EventType.ERROR, "FunnelbackHelper", ex.InnerException.ToString());
                            }

                        }

                        jtoken = jtoken.Next;
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }

        /// <summary>
        /// Orders datatable according to a specified field. Used to order items to match saved GUID data order. Items are not ordered when returned from Funnelback.
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="items"></param>
        /// <param name="fieldToOrderBy"></param>
        /// <returns></returns>
        public static DataTable OrderDataTable(DataTable tbl, string[] items, string fieldToOrderBy)
        {
            //Reorder datatable rows to match order of inputted guids in items array
            for (int i = items.Length - 1; i >= 0; i--)
            {
                var itm = items[i];
                DataRow[] dr = tbl.Select(fieldToOrderBy + " ='" + itm + "'");
                DataRow newRow = tbl.NewRow();
                newRow.ItemArray = dr[0].ItemArray;
                tbl.Rows.Remove(dr[0]);
                tbl.Rows.InsertAt(newRow, 0);
            }

            return tbl;
        }

        /// <summary>
        /// Removes duplicate DataTable rows using a column to sort by (Date Modified) and a column to group by (GUID).
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="dateFieldToDeDupBy"></param>
        /// <param name="guidColumnCodeName"></param>
        /// <returns></returns>
        public static DataTable RemoveDuplicateTableRowsByDate(DataTable tbl, string dateFieldToDeDupBy, string guidColumnCodeName)
        {
            if (tbl != null)
            {
                if (tbl.Columns.Contains(dateFieldToDeDupBy) && tbl.Columns.Contains(guidColumnCodeName))
                {
                    DataTable result = tbl.AsEnumerable()
                        .OrderByDescending(x => DateTime.Parse(x.Field<string>(dateFieldToDeDupBy)))
                        .GroupBy(x => x.Field<string>(guidColumnCodeName))
                        .Select(g => g.First()).CopyToDataTable();

                    return result;
                }
                else
                { 
                    return tbl;
                }
            }
            else
            { 
                return tbl;
            }
        }
    }
}
