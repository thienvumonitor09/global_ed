﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for RaveMobile
/// </summary>
public class RaveMobile
{
    public string eventname;
    public string description;
    public string web;


    public RaveMobile()
    {
        bool pullFreshAlertInfo = true;
        int secondsToWait = 60;
        int hoursToLive = 24; //24 normally
        DateTime currentTime = System.DateTime.Now;
        DateTime dtSent = System.DateTime.Now;
        string sent = "";
        eventname = "";
        description = "";
        web = "";
        string data = "";

        // Check and/or set Application variable to limit hits on remote CAP alert channel
        // Check status string to determine (based on secondsToWait) whether to refresh
        try
        {
            DateTime dtLastPulled = Convert.ToDateTime(HttpContext.Current.Application["RaveChannel1DataLastPulled"].ToString());
            if (dtLastPulled.AddSeconds(secondsToWait) > currentTime)
            {
                pullFreshAlertInfo = false;
            }
        }
        catch
        {
            //If any error encountered, leave flag set to pull and store alert information
            pullFreshAlertInfo = true;
        }


        if (pullFreshAlertInfo)
        {

            // IF flag is set, pull and store alert information
            //XML parsing attempts have not worked with this Rave-hosted source so far...switching to barebones approach - BN 2016-01-15
            string url = "http://www.getrave.com/cap/ccs/channel1";
            WebClient wc = new WebClient();
            try
            {
                data = wc.DownloadString(url);
                sent = data.Substring(data.IndexOf("<sent>") + 6, data.IndexOf("</sent>") - data.IndexOf("<sent>") - 6);
                eventname = data.Substring(data.IndexOf("<event>") + 7, data.IndexOf("</event>") - data.IndexOf("<event>") - 7);
                //eventname = "Really long and verbose event name for a Rave Mobile alert sent via CAP";
                description = data.Substring(data.IndexOf("<description>") + 13, data.IndexOf("</description>") - data.IndexOf("<description>") - 13);
                //description = "Lengthy description with lots of detail about the alert and its surrounding circumstances appears here and goes on and on to a surprising degree even for those who are used to long descriptions.";
                web = data.Substring(data.IndexOf("<web>") + 5, data.IndexOf("</web>") - data.IndexOf("<web>") - 5);
            }
            catch
            {
                //sent = "[]";
                //eventname = "Error";
                //description = "Unable to access or populate alert information";
                //web = "http://www.ccs.spokane.edu/alert.aspx"; //default
                //If any error encountered, assume All Clear
                eventname = "";
                description = "";
                web = "";
            }
            //Is it too old?
            try
            {
                dtSent = Convert.ToDateTime(sent);
            }
            catch
            {
                //If any error encountered, assume All Clear
                 dtSent = currentTime.AddHours(hoursToLive * -2); //Populate with datetime of two days ago (so any message would be assumed expired)
            }
            if (dtSent.AddHours(hoursToLive) > currentTime && eventname.ToLower().Trim() != "remove message")
            {
                //Update Application variables
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["RaveChannel1DataLastPulled"] = System.DateTime.Now;
                HttpContext.Current.Application["RaveChannel1_sent"] = sent;
                HttpContext.Current.Application["RaveChannel1_eventname"] = eventname.Trim();
                HttpContext.Current.Application["RaveChannel1_description"] = description.Trim();
                HttpContext.Current.Application["RaveChannel1_web"] = web.Trim();
                HttpContext.Current.Application.UnLock();
            }
            else
            {
                //Assume All Clear
                eventname = "";
                description = "";
                web = "";
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["RaveChannel1DataLastPulled"] = System.DateTime.Now;
                HttpContext.Current.Application["RaveChannel1_sent"] = sent;
                HttpContext.Current.Application["RaveChannel1_eventname"] = eventname;
                HttpContext.Current.Application["RaveChannel1_description"] = description;
                HttpContext.Current.Application["RaveChannel1_web"] = web;
                HttpContext.Current.Application.UnLock();
            }
        }
        else
        {
            eventname = HttpContext.Current.Application["RaveChannel1_eventname"].ToString();
            if (eventname.ToLower().Trim() == "remove message")
            {
                eventname = ""; // If there is a remove message stored in application variables, return an empty eventname
            }
            description = HttpContext.Current.Application["RaveChannel1_description"].ToString();
            web = HttpContext.Current.Application["RaveChannel1_web"].ToString();
        }
    }
}