﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Quarter
/// </summary>
public class Quarter
{
    public string QuarterName { get; set; }
    public int Year { get; set; }
    public Quarter() { }
    public Quarter(string quarterName, int year)
    {
        this.QuarterName = quarterName;
        this.Year = year;
    }
}