﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for QuarterInfo
/// </summary>
public class QuarterInfo
{
    public Quarter QuarterObj { get; set; }
    public List<Date> InfoList { get; set; }
    public QuarterInfo() { }
    public QuarterInfo(Quarter quarterObj, List<Date> infoList)
    {
        this.QuarterObj = quarterObj;
        this.InfoList = infoList;
    }
}