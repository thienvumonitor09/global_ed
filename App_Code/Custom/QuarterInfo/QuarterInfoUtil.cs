﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Linq;
/// <summary>
/// Summary description for QuarterInfoUtil
/// </summary>
public class QuarterInfoUtil
{
    public static XDocument importantDateXML;
    public static XDocument globalXML;
    public static List<XDocument> importantDateXMLList;
    public static List<XDocument> globalXMLList;
    public static string[] schoolList = { "scc", "sfcc" };
    public QuarterInfoUtil()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static List<QuarterInfo> GetQuarterInfoList2()
    {
        List<QuarterInfo> quarterInfoList = new List<QuarterInfo>();
        string[] titleList = {"International Student Application Deadline", "Academic Students Transfer Application Deadline",
                                "IELP Transfer Application Deadline", "First day" };
        foreach (Quarter q in GetQuarters())
        {
            List<Date> infoList = new List<Date>();
            foreach (string t in titleList)
            {
                //Get XElement
                List<Date> elementList = GetDate2(q, t);
                foreach (Date element in elementList)
                {
                    infoList.Add(element);
                }
            }
            quarterInfoList.Add(new QuarterInfo(q, infoList));
        }
        return quarterInfoList;
    }


    public static List<QuarterInfo> GetQuarterInfoList()
    {
        List<QuarterInfo> quarterInfoList = new List<QuarterInfo>();
        string[] titleList = { "Application Admission Deadline", "Academic Students Transfer Application Deadline",
                                "IELP Transfer Application Deadline", "First day" };
        foreach (Quarter q in GetQuarters())
        {
            List<Date> infoList = new List<Date>();
            foreach (string t in titleList)
            {
                //Get XElement
                Date element = GetDate(q, t);
                if (element != null)
                {
                    infoList.Add(element);
                }
            }
            quarterInfoList.Add(new QuarterInfo(q, infoList));
        }
        return quarterInfoList;
    }


    public static string GetSite()
    {
        //Get current address
        string urlHostStr = HttpContext.Current.Request.Url.Host;
        string result = "";
        if (urlHostStr.Contains("scc"))
        {
            result = "scc";
        }
        else if (urlHostStr.Contains("sfcc"))
        {
            result = "sfcc";
        }
        else
        {
            result = "shared";
        }
        return result;
    }


    public static string getQuarterListJson()
    {
        //int m = DateTime.Now.Month, y = DateTime.Now.Year;
        //m = 2;
        List<Quarter> quarterList = GetQuarters(DateTime.Now.Month, DateTime.Now.Year);
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        return js.Serialize(quarterList);
    }


    public static List<Quarter> GetQuarters()
    {
        int m = DateTime.Now.Month;
        int year = DateTime.Now.Year;
        int i = 0; // where to start from quarter list
        if (m == 1)
            i = 0;
        else if (m >= 2 && m <= 4)
            i = 1;
        else if (m >= 5 && m <= 7)
            i = 2;
        else if (m >= 8 && m <= 9)
            i = 3;
        else
            i = 4;
        List<Quarter> quarterList = new List<Quarter>();
        string[] quarters = { "Winter", "Spring", "Summer", "Fall" };
        int count = 1;
        int y = year;
        while (count <= 4)
        {
            if (i >= 4)
            {
                y = year + 1;
            }
            quarterList.Add(new Quarter(quarters[i % 4], y));
            count++;
            i++;
        }
        return quarterList;
    }

    public static List<Quarter> GetQuarters(int m, int year)
    {
        int i = 0; // where to start from quarter list
        if (m == 1)
            i = 0;
        else if (m >= 2 && m <= 4)
            i = 1;
        else if (m >= 5 && m <= 7)
            i = 2;
        else if (m >= 8 && m <= 9)
            i = 3;
        else
            i = 4;
        List<Quarter> quarterList = new List<Quarter>();
        string[] quarters = { "Winter", "Spring", "Summer", "Fall" };
        int count = 1;
        int y = year;
        while (count <= 4)
        {
            if (i >= 4)
            {
                y = year + 1;
            }
            quarterList.Add(new Quarter(quarters[i % 4], y));
            count++;
            i++;
        }
        return quarterList;
    }

    public static List<Date> GetDate2(Quarter q, string title)
    {
        string category = "";
        List<XDocument> xdocs = null;
        List<Date> dateList = new List<Date>();
        string titleMatch = "";
        string categoryMatch = "";
        string displayTitle = title;
        if (title.Contains("First day"))
        {
            displayTitle = q.QuarterName + " Quarter Begins";
            xdocs = CreateXDocumentList("important-dates");
            titleMatch = "first" + " " + q.QuarterName;
            categoryMatch = q.Year.ToString();
        }
        else if (title.Contains("Application Admission Deadline"))
        {
            xdocs = CreateXDocumentList("important-dates");
            titleMatch = "New Student Admission Application Deadline" + " with no prior college " + q.QuarterName + " " + q.Year.ToString();
            //categoryMatch = q.Year.ToString();
        }
        else if (title.Contains("International Student Application Deadline"))
        {
            xdocs = CreateXDocumentList("global");
            titleMatch = "International Student Application Deadline " + q.QuarterName + " " + q.Year.ToString();
            //categoryMatch = q.Year.ToString();
        }
        else if (title.Contains("IELP"))
        {
            xdocs = CreateXDocumentList("global");
            titleMatch = "ielp" + " " + q.QuarterName + " " + q.Year.ToString();
        }
        else if (title.Contains("Academic Students Transfer"))
        {
            xdocs = CreateXDocumentList("global");
            titleMatch = "Academic Students Transfer" + " " + q.QuarterName + " " + q.Year.ToString();
        }
        else if (title.Contains("Tuition Due"))
        {
            xdocs = CreateXDocumentList("important-dates");
            titleMatch = "Tuition Due " + q.QuarterName + " " + q.Year.ToString();
        }
        else if (title.Contains("Financial Aid Priority"))
        {
            xdocs = CreateXDocumentList("important-dates");
            titleMatch = "Financial Aid Priority " + q.QuarterName + " " + q.Year.ToString();
        }






        foreach (XDocument xdocE in xdocs)
        {
            foreach (XElement d in xdocE.Root.Nodes())
            {

                if (!string.IsNullOrEmpty(categoryMatch))
                {
                    if (ContainsString(d.Element("Title").Value, titleMatch) && ContainsString(d.Element("Category").Value, categoryMatch))
                    {

                        category = d.Element("Category").Value;
                        
                    }
                    
                }
                else
                {
                    if (ContainsString(d.Element("Title").Value, titleMatch))
                    {
                        category = d.Element("Category").Value;
                        /*
                        if (d.Element("Title").Value.Contains("Financial Aid Priority Funding Deadline for Spring 2019"))
                        {
                            DateTime revisedDate = DateTime.Parse(category.Substring(0, category.IndexOf(" ")));
                            revisedDate = revisedDate.AddDays(20);
                            string displayStr = revisedDate.Year + "/" + revisedDate.Month + "/" + revisedDate.Day + " (" + revisedDate.DayOfWeek + ")";
                            category = displayStr;
                        }*/
                    }else if(d.Element("Title").Value.Equals("New Student Application Deadline for Winter Quarter (for students with no prior college)")) //Just for this case
                    {
                        //category = d.Element("Category").Value;
                    }
                    else if (d.Element("Title").Value.Equals("Tuition Due for Winter Quarter")) //Just for this case
                    {
                        //category = d.Element("Category").Value;
                    }
                    
                }
            }

            //add new Date when date of SCC <> date of SFCC
            if (!dateList.Exists(element => element.Category == category))
            {
                if (!string.IsNullOrEmpty(category))
                {
                    dateList.Add(new Date() { Title = displayTitle, Category = category });
                }
            }
        }

        //if both SCC and SFCC added, must change the title to indicate which school
        if (dateList.Count > 1)
        {
            var i = 0;
            foreach (Date dateE in dateList)
            {
                dateE.Title += " " + schoolList[i++].ToUpper();
            }
        }
        return dateList;
    }
    public static Date GetDate(Quarter q, string title)
    {
        string category = "";
        XDocument xdoc = null;
        string titleMatch = "";
        string categoryMatch = "";
        if (title.Contains("First day"))
        {
            xdoc = CreateXDocument("important-dates");
            titleMatch = "first" + " " + q.QuarterName;
            categoryMatch = q.Year.ToString();
        }
        else if (title.Contains("Application Admission Deadline"))
        {
            xdoc = CreateXDocument("important-dates");
            titleMatch = "new student deadline" + " " + q.QuarterName;
        }
        else if (title.Contains("IELP"))
        {
            xdoc = CreateXDocument("global");
            titleMatch = "ielp" + " " + q.QuarterName + " " + q.Year.ToString();
        }
        else if (title.Contains("Academic Students Transfer"))
        {
            xdoc = CreateXDocument("global");
            titleMatch = "Academic Students Transfer" + " " + q.QuarterName + " " + q.Year.ToString();
        }
        foreach (XElement d in xdoc.Root.Nodes())
        {

            if (!string.IsNullOrEmpty(categoryMatch))
            {
                if (ContainsString(d.Element("Title").Value, titleMatch) && ContainsString(d.Element("Category").Value, categoryMatch))
                {
                    category = d.Element("Category").Value;
                }
            }
            else
            {
                if (ContainsString(d.Element("Title").Value, titleMatch))
                {
                    category = d.Element("Category").Value;
                }
            }
        }
        return (category == "") ? null : new Date() { Title = title, Category = category };
    }

    public static bool ContainsString(string originalS, string s)
    {

        foreach (string e in s.Split(' '))
        {
            if (!originalS.ToLower().Contains(e.ToLower()))
            {
                return false;
            }
        }
        return true;
    }
    public static XDocument CreateXDocument(string calendarType)
    {
        string startDate = (DateTime.Now.Year - 1) + "0101";
        string endDate = (DateTime.Now.Year + 1) + "1231";
        string feedURL = "";
        string siteValue = (GetSite() == "shared") ? "sfcc" : GetSite();

        //calendarType = important-dates (SCC/SFCC) || global
        if (calendarType.Equals("important-dates"))
        {
            feedURL = "http://25livepub.collegenet.com/calendars/" + siteValue + "-important-dates.rss";
            feedURL += "?startdate=" + startDate + "&enddate=" + endDate;
        }
        else if (calendarType.Equals("global"))
        {
            feedURL = "http://25livepub.collegenet.com/calendars/global-ed.rss";
            feedURL += "?startdate=" + startDate + "&enddate=" + endDate;
        }
        XDocument xdoc = XDocument.Load(feedURL);
        XDocument convertedXDocument = new XDocument(
                                 new XElement("ImpDates",
                                   from item in xdoc.Descendants("item")
                                       //where (item.Element("title").Value.ToLower().Contains("first"))
                                   orderby (string)item.Element("category").Value
                                   select new XElement("Date",
                                          new XElement("Title", item.Element("title").Value),
                                          new XElement("Category", item.Element("category").Value))));
        return convertedXDocument;
    }
    public static List<XDocument> CreateXDocumentList(string calendarType)
    {
        List<XDocument> xdocList = new List<XDocument>();

        string startDate = (DateTime.Now.Year -1) + "0101";
        string endDate = (DateTime.Now.Year + 1) + "1231";
        string feedURL = "";
        string siteValue = GetSite();
        List<string> siteList = new List<string>();
        if (siteValue == "scc" || siteValue == "sfcc")
        {
            siteList.Add(siteValue);
        }
        else
        {
            foreach (string school in schoolList)
            {
                siteList.Add(school);
            }
        }
        List<string> feedURLList = new List<String>();
        //calendarType = important-dates (SCC/SFCC) || global
        if (calendarType.Equals("global"))
        {
            feedURL = "http://25livepub.collegenet.com/calendars/global-ed.rss";
            feedURL += "?startdate=" + startDate + "&enddate=" + endDate + "&events=1000";
            feedURLList.Add(feedURL);
        }
        else
        {
            foreach (string site in siteList)
            {
                feedURL = "http://25livepub.collegenet.com/calendars/" + site + "-important-dates.rss";
                feedURL += "?startdate=" + startDate + "&enddate=" + endDate + "&events=1000";
                feedURLList.Add(feedURL);
            }
        }
        foreach (string strURL in feedURLList)
        {
            XDocument xdoc = XDocument.Load(strURL);
            XDocument convertedXDocument = new XDocument(
                                     new XElement("ImpDates",
                                       from item in xdoc.Descendants("item")
                                           //where (item.Element("title").Value.ToLower().Contains("first"))
                                           //orderby (string)item.Element("category").Value
                                       select new XElement("Date",
                                              new XElement("Title", item.Element("title").Value),
                                              new XElement("Category", item.Element("category").Value))));
            xdocList.Add(convertedXDocument);
        }
        return xdocList;
    }

}