﻿using CMS;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EventLog;
using HM.Funnelback.KenticoPush;
using System;
using System.Collections.Generic;

// Registers the custom module into the system
[assembly: RegisterModule(typeof(WorkflowEventsModule))]

public class WorkflowEventsModule : Module
{
    // Module class constructor, the system registers the module under the name "CustomInit"
    public WorkflowEventsModule()
        : base("CCSWorkflowEvents")
    {
    }

    // Contains initialization code that is executed when the application starts
    protected override void OnInit()
    {
        base.OnInit();

        // Assigns custom handlers to events
        WorkflowEvents.Archive.After += Document_Archive_After;
        DocumentEvents.Move.Before += Document_Move_Before;
        DocumentEvents.Move.After += Document_Move_After;
        DocumentEvents.Copy.After += Document_Copy_After;
    }

    /// <summary>
    /// Custom event to remove an item from Funnelback search after the Kentico item is archived.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Document_Archive_After(object sender, WorkflowEventArgs e)
    {
        // Add custom actions here
        object doc = sender;
        WorkflowEventArgs args = e;
        TreeNode node = (TreeNode)e.Document;
        if (node != null)
        {
            string className = node.ClassName;
            string collection = FunnelbackModuleHelpers.GetPushClassCollection(className);
            string siteName = node.NodeSiteName;
            string absoluteURL = node.AbsoluteURL;
            Guid itemGUID = node.DocumentGUID;

            if (collection != "")
            {
                string response = KenticoXML.DeleteXml(collection, className, siteName, itemGUID, absoluteURL);
                EventLogProvider.LogEvent(EventType.INFORMATION, "Content Archived", "ARCHIVE", eventDescription: response);
            }

        }

    }
    /// <summary>
    /// Custom event to remove an item from Funnelback search after the Kentico item is moved in the content tree (Alias Path changes).
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Document_Move_Before(object sender, DocumentEventArgs e)
    {
        object doc = sender;
        DocumentEventArgs args = e;
        TreeNode node = (TreeNode)e.Node;
        if (node != null)
        {
            string className = node.ClassName;
            string collection = FunnelbackModuleHelpers.GetPushClassCollection(className);
            string siteName = node.NodeSiteName;
            string absoluteURL = node.AbsoluteURL;
            Guid itemGUID = node.DocumentGUID;

            if (collection != "")
            {
                string response = KenticoXML.DeleteXml(collection, className, siteName, itemGUID, absoluteURL);
                EventLogProvider.LogEvent(EventType.INFORMATION, "Content Moved", "MOVEDELETE", eventDescription: response);
            }

        }
    }

    /// <summary>
    /// Custom event to add an item to Funnelback search after the Kentico item is moved (update new Alias Path). 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Document_Move_After(object sender, DocumentEventArgs e)
    {
        object doc = sender;
        DocumentEventArgs args = e;
        TreeNode node = (TreeNode)e.Node;
        if (node != null)
        {
            string className = node.ClassName;
            string collection = FunnelbackModuleHelpers.GetPushClassCollection(className);
            string siteName = node.NodeSiteName;
            string absoluteURL = node.AbsoluteURL;
            Guid itemGUID = node.DocumentGUID;

            if (collection != "")
            {
                List<string> columns = FunnelbackModuleHelpers.GetPushClassNameColumns(node.ClassName);

                if (columns.Count > 0)
                {
                    columns.AddRange(ModuleConstants.SystemColumns);
                    bool includeCategories = FunnelbackModuleHelpers.GetPushClassIncludeCategories(node.ClassName);
                    string targetCollection = ModuleConstants.GetFunnelbackCollection(node.ClassName);
                    string response = KenticoXML.SubmitKenticoXML(targetCollection, String.Join(",", columns), "", node.ClassName, node.NodeSiteName, "", includeCategories, node.DocumentGUID, node.AbsoluteURL);
                    EventLogProvider.LogEvent(EventType.INFORMATION, "Content Moved", "MOVEADD", eventDescription: response);
                }
            }

        }
    }

    /// <summary>
    /// Custom event to add an item to Funnelback search after the Kentico item is copied (new item must be added automatically, as it is published).
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Document_Copy_After(object sender, DocumentEventArgs e)
    {
        object doc = sender;
        DocumentEventArgs args = e;
        TreeNode node = (TreeNode)e.Node;
        if (node != null)
        {
            string className = node.ClassName;
            string collection = FunnelbackModuleHelpers.GetPushClassCollection(className);
            string siteName = node.NodeSiteName;
            string absoluteURL = node.AbsoluteURL;
            Guid itemGUID = node.DocumentGUID;

            if (collection != "")
            {
                List<string> columns = FunnelbackModuleHelpers.GetPushClassNameColumns(node.ClassName);

                if (columns.Count > 0)
                {
                    columns.AddRange(ModuleConstants.SystemColumns);
                    bool includeCategories = FunnelbackModuleHelpers.GetPushClassIncludeCategories(node.ClassName);
                    string targetCollection = ModuleConstants.GetFunnelbackCollection(node.ClassName);
                    string response = KenticoXML.SubmitKenticoXML(targetCollection, String.Join(",", columns), "", node.ClassName, node.NodeSiteName, "", includeCategories, node.DocumentGUID, node.AbsoluteURL);
                    EventLogProvider.LogEvent(EventType.INFORMATION, "Content Copied", "COPYADD", eventDescription: response);
                }
            }

        }
    }

}