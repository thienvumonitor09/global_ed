﻿using CMS.Helpers;
using CMS.Base;
using CMS.DocumentEngine;
using CMS.Localization;
using CMS.Membership;
using CMS.Taxonomy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;
using CMS.EventLog;

namespace HM.Funnelback.KenticoPush
{
    public class KenticoHelper
    {
        public enum KenticoFields
        {
            Default,
            Url,
            Categories,
            NodeAliasPath,
            DateTime,
            String,
            Int32,
            Bool,
            Guid
        }
        internal static string GetUrlPath(string nodeAliasPath)
        {
            return URLHelper.GetAbsoluteUrl(nodeAliasPath);
        }

        /// <summary>
        /// Returns all published documents of a page type from Kentico database. Used only for "Push All Documents" functionality in Funnelback"Push Class" configuration.
        /// </summary>
        /// <param name="cols"></param>
        /// <param name="className"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        internal static DocumentQuery GetBulkContentFromKentico(List<string> cols, string className, string siteName)
        {
            var user = UserInfoProvider.GetUserInfo("administrator");
            using (new CMSActionContext(user))
            {

                DocumentQuery documents = DocumentHelper.GetDocuments(className).Columns(cols).Published().OnSite(siteName);

                return documents;
            }
        }

        /// <summary>
        /// Gets specific content item from Kentico database using DocumentQuery.
        /// </summary>
        /// <param name="cols">List of columns to retrieve</param>
        /// <param name="className">Classname of Page Type</param>
        /// <param name="siteName">Site code name</param>
        /// <param name="docGuid">Document GUID to retrieve</param>
        /// <returns></returns>
        internal static DocumentQuery GetSingleContentFromKentico(List<string> cols, string className, string siteName, Guid docGuid)
        {
            var user = UserInfoProvider.GetUserInfo("administrator");
            using (new CMSActionContext(user))
            {
                DocumentQuery document = DocumentHelper.GetDocuments(className).WhereEquals("DocumentGUID", docGuid).Columns(cols).OnSite(siteName).TopN(1);

                return document;
            }
        }

        /// <summary>
        /// Returns pipe separated document categories as stored in database.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        internal static string GetDocumentCategories(int documentId)
        {
            if (documentId < 1)
            {
                throw new Exception("Invalid document ID");
            }

            string result = "";

            // Gets the categories of the specified document
            DataSet ds = DocumentCategoryInfoProvider.GetDocumentCategories(documentId).Columns("CMS_Category.CategoryID, CategoryDisplayName");

            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    int categoryId = ValidationHelper.GetInteger(row["CategoryID"], 0);
                    string categoryName = ValidationHelper.GetString(row["CategoryDisplayName"], null);

                    result += categoryName;
                    result += '|';
                }
            }

            return result;
        }

        /// <summary>
        /// Returns DocumentID for Node's parent.
        /// </summary>
        /// <param name="nodeID"></param>
        /// <param name="parentOfParent"></param>
        /// <returns></returns>
        internal static int GetNodeParentID(int nodeID, bool parentOfParent)
        {
            try
            {
                int returnedNodeID = 0;
                if (nodeID > 0)
                {
                    TreeProvider tp = new TreeProvider();
                    var itemNodeParentID = DocumentHelper.GetDocument(nodeID, LocalizationContext.CurrentCulture.CultureAlias, tp).NodeParentID;
                    if (itemNodeParentID > 0)
                    {
                        if (!parentOfParent)
                        {
                            return itemNodeParentID;
                        }
                        else
                        {
                            int parentNodeParentID = GetNodeParentID(itemNodeParentID, false);
                            if (parentNodeParentID >= 0)
                            {
                                return parentNodeParentID;
                            }
                            else
                            {
                                return 0;
                            }
                        }
                    }
                    else
                    {
                        returnedNodeID = 0;
                    }
                }
                else
                {
                    returnedNodeID = 0;
                }
                return returnedNodeID;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogInformation(EventType.ERROR, "KenticoHelper", ex.InnerException.ToString());
                return 0;
            }
        }

        /// <summary>
        /// Returns Node's Alias path using a NodeID.
        /// </summary>
        /// <param name="nodeID">NodeID of the node to get NodeAliasPath</param>
        /// <returns></returns>
        internal static string GetNodeAliasPathFromNodeID(int nodeID)
        {

            TreeProvider tp = new TreeProvider();
            if (nodeID > 0)
            {
                string nodeAliasPath = DocumentHelper.GetDocument(nodeID, LocalizationContext.CurrentCulture.CultureAlias, tp).NodeAliasPath;
                return nodeAliasPath;
            }
            else
            {
                return "/";
            }

        }

        /// <summary>
        /// Returns a NodeAliasPath of a document, a document's parent, or a document's parent's parent.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="returnUrlType">self, parent, parentOfParent</param>
        /// <returns></returns>
        internal static string GetSelectedNodeAliasPath(CMS.DocumentEngine.TreeNode doc, string returnUrlType)
        {
            string returnURL = "";
            switch (returnUrlType)
            {
                case ("self"):
                    returnURL = doc["NodeAliasPath"].ToString();
                    break;
                case ("parent"):
                    returnURL = GetNodeAliasPathFromNodeID(GetNodeParentID(doc.NodeID, false));
                    break;
                case ("parentOfParent"):
                    returnURL = GetNodeAliasPathFromNodeID(GetNodeParentID(doc.NodeParentID, false));
                    break;
                default:
                    returnURL = doc["NodeAliasPath"].ToString();
                    break;
            }

            return returnURL;

        }

        /// <summary>
        /// Creates XML Node. Used in writing XML to push to Funnelback.
        /// </summary>
        /// <param name="xmlWriter"></param>
        /// <param name="nodeValue"></param>
        /// <param name="xmlField"></param>
        /// <param name="cData"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        internal static XmlWriter CreateXmlNode(XmlWriter xmlWriter, string nodeValue, KenticoFields xmlField, bool cData = false, string nodeName = "")
        {
            string valueString = String.Empty;
            //<column name>
            if (nodeName == "")
            {
                xmlWriter.WriteStartElement(xmlField.ToString());
            }
            else
            {
                xmlWriter.WriteStartElement(nodeName);
            }

            switch (xmlField)
            {
                case KenticoFields.Url:
                    valueString = GetUrlPath(nodeValue);
                    break;
                case KenticoFields.NodeAliasPath:
                    valueString = CMS.Helpers.HTMLHelper.HTMLEncode(nodeValue);
                    break;
                case KenticoFields.DateTime:
                    System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                    string result = DateTime.Parse(nodeValue).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss''");
                    valueString = CMS.Helpers.HTMLHelper.HTMLEncode(result.ToString());
                    break;
                case KenticoFields.Default:
                case KenticoFields.Categories:
                default:
                    valueString = nodeValue;
                    break;
            }
            if (cData)
            {
                xmlWriter.WriteCData(valueString);
            }
            else
            {
                var output = (!string.IsNullOrEmpty(valueString) && valueString[0] == '~') ? "" + valueString.Substring(1) : valueString;
                xmlWriter.WriteString(output); //check overloads for other site options
            }
            //</column name>
            xmlWriter.WriteEndElement();

            return xmlWriter;
        }
    }
}