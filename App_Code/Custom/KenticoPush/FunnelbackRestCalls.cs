﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Net;

namespace HM.Funnelback.KenticoPush
{
    public class FunnelbackRestCalls
    {
        static string _restApiUri = ModuleConstants.GetFunnelbackRestApiUri();
        static string _apiString;

        /* This function is used for both creating and updating items in a collection */
        public static HttpStatusCode PushXmlToFunnelback(string xmlUri, string collection, string content, string siteName)
        {

            _apiString = ModuleConstants.GetFunnelbackRestApiUri() + collection + xmlUri;
            var client = new RestClient(CreateApiUriForDocument(xmlUri, collection));
            var request = new RestRequest(Method.PUT);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(siteName));
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("text/xml", content, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.StatusCode;
        }

        /// <summary>
        /// Delete Individual content item from collection
        /// </summary>
        /// <param name="xmlUri"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static HttpStatusCode DeleteXmlFromFunnelback(string xmlUri, string collection, string siteName)
        {

            var client = new RestClient(CreateApiUriForDocument(xmlUri, collection));
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(siteName));
            IRestResponse response = client.Execute(request);

            return response.StatusCode;
        }

        /// <summary>
        /// Delete all content from Index
        /// </summary>
        /// <param name="xmlUri"></param>
        /// <param name="collection">Name of Collection to delete all content from.</param>
        /// <returns></returns>
        public static HttpStatusCode DeleteAllContentFromCollection(string collection, string siteName)
        {

            var client = new RestClient(CreateApiUriForCollection(collection));
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("x-security-token", ModuleConstants.GetFunnelbackApiKey(siteName));
            IRestResponse response = client.Execute(request);

            return response.StatusCode;
        }

        public static string CreateApiUriForDocument(string xmlUri, string collection)
        {
            _apiString = _restApiUri + "/" + collection + "/documents?key=" + xmlUri;
            return _apiString;
        }

        public static string CreateApiUriForCollection(string collection)
        {
            _apiString = _restApiUri + "/" + collection;
            return _apiString;
        }
    }
}