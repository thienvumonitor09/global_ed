﻿using CMS.DocumentEngine;
using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using CMS.EventLog;

namespace HM.Funnelback.KenticoPush
{
    public class KenticoXML
    {
        /// <summary>
        /// Method to send XML to Funnelback
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="columns"></param>
        /// <param name="specialColumns"></param>
        /// <param name="className"></param>
        /// <param name="siteName"></param>
        /// <param name="returnUrlType"></param>
        /// <param name="includeCategories"></param>
        /// <param name="docGuid"></param>
        /// <param name="absoluteURL"></param>
        /// <returns></returns>
        public static string SubmitKenticoXML(string collection, string columns, string specialColumns, string className, string siteName, string returnUrlType, bool includeCategories, Guid docGuid = new Guid(), string absoluteURL = "")
        {
            string returnXML = String.Empty;
            returnXML = BuildXML(collection, columns, specialColumns, className, siteName, returnUrlType, includeCategories, docGuid, absoluteURL);
            return returnXML;
        }

        /// <summary>
        /// Builds out XML using passed settings.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="columns"></param>
        /// <param name="specialColumns"></param>
        /// <param name="className"></param>
        /// <param name="siteName"></param>
        /// <param name="returnUrlType"></param>
        /// <param name="includeCategories"></param>
        /// <param name="docGuid"></param>
        /// <param name="absoluteURL"></param>
        /// <returns></returns>
        public static string BuildXML(string collection, string columns, string specialColumns, string className, string siteName, string returnUrlType, bool includeCategories, Guid docGuid, string absoluteURL)
        {
            int specialColumnsAdded = 0;

            StringBuilder xmlReturnString = new StringBuilder();
            string[] cols = new string[] { };   //String array passed to document query method

            if (columns != null)
            {
                cols = columns.ToString().Split(',');
            }
            if (specialColumns != null && specialColumns != "")
            {
                string[] pipedColumns = specialColumns.ToString().Split(',');
                int specialColumnsLength = pipedColumns.Length;

                foreach (string col in pipedColumns)
                {
                    //Resize array and add special column to end
                    Array.Resize(ref cols, cols.Length + 1);
                    cols[cols.Length - 1] = col;
                    specialColumnsAdded++;
                }
            }
            if (cols != null && cols.Length > 0)
            {
                List<string> listOfColumns = cols.ToList<string>();
                listOfColumns = listOfColumns.Distinct().ToList();

                DocumentQuery documents;
                if (docGuid == Guid.Empty)
                {
                    documents = KenticoHelper.GetBulkContentFromKentico(listOfColumns, className, siteName);
                }
                else
                {
                    documents = KenticoHelper.GetSingleContentFromKentico(listOfColumns, className, siteName, docGuid);
                }
                if (specialColumnsAdded > 0)
                {
                    //Remove special columns from cols array, as we're adding it as a seperate property and not retrieving from same table.
                    Array.Resize(ref cols, cols.Length - specialColumnsAdded);
                }
                foreach (var doc in documents)
                {
                    //Relative URL
                    string itemUrl = ValidationHelper.GetString(KenticoHelper.GetUrlPath(doc.NodeAliasPath), "");

                    if (absoluteURL != "")
                    {
                        itemUrl = absoluteURL;
                    }

                    string itemXML = ValidationHelper.GetString(CreateXmlElement(doc, listOfColumns, returnUrlType, includeCategories, absoluteURL, siteName), "");
                    if (itemUrl != "" && itemXML != "")
                    {
                        xmlReturnString.Append(FunnelbackRestCalls.PushXmlToFunnelback(itemUrl, collection, itemXML, siteName));
                        xmlReturnString.Append(itemXML);
                    }
                }
            }

            return xmlReturnString.ToString();
        }

        /// <summary>
        /// Remove XML from index using absolute URL
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="className"></param>
        /// <param name="siteName"></param>
        /// <param name="docGuid"></param>
        /// <param name="absoluteURL"></param>
        /// <returns></returns>
        public static string DeleteXml(string collection, string className, string siteName, Guid docGuid = new Guid(), string absoluteURL = "")
        {
            StringBuilder xmlReturnString = new StringBuilder();
            string[] cols = new string[] { "NodeAliasPath" };   //String array passed to document query method
            DocumentQuery documents;
            List<string> listOfColumns = cols.ToList<string>();

            if (docGuid == Guid.Empty)
            {
                documents = KenticoHelper.GetBulkContentFromKentico(listOfColumns, className, siteName);
            }
            else
            {
                documents = KenticoHelper.GetSingleContentFromKentico(listOfColumns, className, siteName, docGuid);
            }
            foreach (var doc in documents)
            {
                string itemUrl = "";
                if (absoluteURL != "")
                {
                    itemUrl = ValidationHelper.GetString(absoluteURL, "");
                }
                else
                {
                    itemUrl = ValidationHelper.GetString(KenticoHelper.GetUrlPath(doc.NodeAliasPath), "");
                }


                xmlReturnString.Append("<br><br>" + FunnelbackRestCalls.DeleteXmlFromFunnelback(itemUrl, collection, siteName));
            }
            return xmlReturnString.ToString();
        }

        /// <summary>
        /// Create XML nodes to be submitted to Funnelback
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="cols"></param>
        /// <param name="returnUrlType"></param>
        /// <param name="includeCategories"></param>
        /// <param name="absoluteUrl"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public static string CreateXmlElement(TreeNode doc, List<string> cols, string returnUrlType, bool includeCategories, string absoluteUrl, string siteName)
        {
            //setup XML writer
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            StringBuilder sb = new StringBuilder();
            XmlWriter xmlw = XmlWriter.Create(sb, settings);
            xmlw.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
            //<classname>
            xmlw.WriteStartElement(ModuleConstants.GetKenticoXmlRootName(siteName));
            xmlw.WriteStartElement(ModuleConstants.GetKenticoXmlElementName(doc.ClassName));

            //Should be set to an absolute URL and should be set in Funnelback as the main URL for the collection item
            //Pushing to the same URL will overwrite the collection's entry for that node
            xmlw = KenticoHelper.CreateXmlNode(xmlw, absoluteUrl, KenticoHelper.KenticoFields.Url);

            //Write the search title node
            string titleColumn = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(doc.ClassName).PushTitleField;
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc[titleColumn].ToString(), KenticoHelper.KenticoFields.String, false, "searchtitle");

            //Write the search summary node
            string summaryColumn = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(doc.ClassName).PushContentField;
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc[summaryColumn].ToString(), KenticoHelper.KenticoFields.String, false, "searchsummary");

            //doc.properties for all DB columns specified
            foreach (var col in cols)
            {
                //EventLogProvider.LogInformation(EventType.INFORMATION, "Col", "column = " + col);

                try
                {
                    if (doc.ContainsColumn(col) && ValidationHelper.GetString(doc[col], "") != "") 
                    {
                        //EventLogProvider.LogInformation(EventType.INFORMATION, "doc[col]", "doc[col] = " + doc[col]);
                        string fieldType = doc[col].GetType().Name;

                        if (col.ToUpper() == KenticoHelper.KenticoFields.NodeAliasPath.ToString().ToUpper())
                        {
                            xmlw = KenticoHelper.CreateXmlNode(xmlw, KenticoHelper.GetSelectedNodeAliasPath(doc, returnUrlType), KenticoHelper.KenticoFields.NodeAliasPath);
                        }
                        else if (fieldType == KenticoHelper.KenticoFields.DateTime.ToString())
                        {
                            xmlw = KenticoHelper.CreateXmlNode(xmlw, ValidationHelper.GetString(doc[col], ""), KenticoHelper.KenticoFields.DateTime);
                        }
                        else if (fieldType == KenticoHelper.KenticoFields.Int32.ToString())
                        {
                            xmlw = KenticoHelper.CreateXmlNode(xmlw, ValidationHelper.GetString(doc[col], ""), KenticoHelper.KenticoFields.Default, false, col.ToString());
                        }
                        else if (fieldType == KenticoHelper.KenticoFields.String.ToString())
                        {
                            xmlw = KenticoHelper.CreateXmlNode(xmlw, ValidationHelper.GetString(doc[col], ""), KenticoHelper.KenticoFields.Default, false, col.ToString());
                        }
                        else if (fieldType == KenticoHelper.KenticoFields.Guid.ToString())
                        {
                            xmlw = KenticoHelper.CreateXmlNode(xmlw, ValidationHelper.GetString(doc[col], ""), KenticoHelper.KenticoFields.Default, false, col.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogProvider.LogInformation(EventType.ERROR, "PushToSearchCreateXML", ex.ToString() + "column = " + doc[col].GetType().Name);
                }
            }
            #region Handle Custom Tables
            //try
            //{
            //    if (ColumnToProcessArgs != null)
            //    {
            //        var pattern = @"\[([^]]*)\]";
            //        var query = ColumnToProcessArgs;
            //        var matches = Regex.Matches(query, pattern);

            //        foreach (Match m in matches)
            //        {
            //            string matched = m.Groups[1].ToString();

            //            string[] specialArgs = matched.Split(',');

            //            if (specialArgs.Length == 6)
            //            {
            //                string colToProcess = specialArgs[0];
            //                string colToProcessDisplayName = specialArgs[1];
            //                string className = specialArgs[2];
            //                string colID = specialArgs[3];
            //                string isGUID = specialArgs[4];
            //                string returnColumn = specialArgs[5];

            //                StringBuilder returnedResults = new StringBuilder();

            //                var pipeSeperatedData = doc[colToProcess];

            //                if (pipeSeperatedData != null)
            //                {
            //                    TreeProvider tree = new TreeProvider();
            //                    string[] splitIDs = pipeSeperatedData.ToString().Split('|');

            //                    if (splitIDs != null)
            //                    {
            //                        foreach (var id in splitIDs)
            //                        {
            //                            if (Boolean.Parse(isGUID))
            //                            {
            //                                Guid itemGUID = Guid.NewGuid();

            //                                bool isValid = Guid.TryParseExact(id, "D", out itemGUID);
            //                                if (isValid)
            //                                {

            //                                    var guidDoc = DocumentHelper.GetDocuments().WhereEquals("NodeGUID", id).FirstObject;
            //                                    if (guidDoc != null)
            //                                    {
            //                                        try
            //                                        {
            //                                            var specialDoc = DocumentHelper.GetDocuments().WhereEquals("NodeID", guidDoc.NodeID).FirstObject;
            //                                            returnedResults.Append(specialDoc[returnColumn].ToString());
            //                                            returnedResults.Append('|');
            //                                        }
            //                                        catch { }
            //                                    }

            //                                }
            //                            }
            //                            else
            //                            {
            //                                var specialDoc = DocumentHelper.GetDocuments(className).WhereEquals(colID, Int32.Parse(id)).FirstObject; //Rewrite to WHERE IN ids instead of separate DB calls
            //                                returnedResults.Append(specialDoc[returnColumn].ToString());
            //                                returnedResults.Append('|');
            //                            }

            //                        }

            //                        try
            //                        {
            //                            if (colToProcessDisplayName != null && colToProcessDisplayName != "")
            //                            {
            //                                xmlw.WriteStartElement(colToProcessDisplayName.ToString());
            //                            }
            //                            else
            //                            {
            //                                xmlw.WriteStartElement(colToProcess.ToString());
            //                            }

            //                            try
            //                            {
            //                                xmlw.WriteString(CMS.Helpers.HTMLHelper.HTMLEncode(returnedResults.ToString()));
            //                            }
            //                            catch
            //                            {

            //                            }

            //                            xmlw.WriteEndElement();
            //                        }
            //                        catch (Exception e)
            //                        {
            //                            //EventLogProvider.LogEvent(EventType.ERROR, "CustomResponseXML", "DocsFromCustomTableSettings", e.Message);
            //                        }
            //                    }

            //                }
            //            }
            //        }
            //    }

            //}
            //catch
            //{

            //}
            #endregion

            if (includeCategories)
            {
                string cats = KenticoHelper.GetDocumentCategories(Int32.Parse(doc["DocumentID"].ToString()));

                if (cats != "")
                {
                    xmlw = KenticoHelper.CreateXmlNode(xmlw, cats, KenticoHelper.KenticoFields.Categories, false, "ItemCategories");
                }
            }

            //Add classname to metadata
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc.ClassName, KenticoHelper.KenticoFields.Default, false, "ClassName");

            //Add DocumentCreatedWhen to metadata
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc.DocumentCreatedWhen.ToString(), KenticoHelper.KenticoFields.DateTime, false, "DocumentCreatedWhen");

            //Add DocumentModifiedWhen to metadata
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc.DocumentModifiedWhen.ToString(), KenticoHelper.KenticoFields.DateTime, false, "DocumentModifiedWhen");
            xmlw = KenticoHelper.CreateXmlNode(xmlw, doc.DocumentModifiedWhen.ToString(), KenticoHelper.KenticoFields.DateTime, false, "d");

            xmlw.WriteEndElement();
            xmlw.WriteEndElement();
            xmlw.Close();

            return sb.ToString().Replace("\r", "").Replace("\n", "");
        }
    }
}