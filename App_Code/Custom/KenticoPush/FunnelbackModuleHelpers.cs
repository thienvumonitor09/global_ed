﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DataEngine;
using CMS.SiteProvider;

/// <summary>
/// Summary description for ModuleHelpers
/// </summary>
namespace HM.Funnelback.KenticoPush
{
    public class FunnelbackModuleHelpers
    {
        /// <summary>
        /// Returns columns defined for ClassName in Funnelback module. Used for pushing and retrieving data.
        /// </summary>
        /// <param name="className">ClassName of Page Type</param>
        /// <returns></returns>
        public static List<string> GetPushClassNameColumns(string className)
        {
            List<string> columns = new List<string>();
            try
            {
                FunnelbackPushClassInfo classInfo = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(className);

                columns.Add(classInfo.PushUrlPathField);
                columns.Add(classInfo.PushTitleField);
                columns.Add(classInfo.PushContentField);

                List<string> otherColumns = classInfo.PushOtherFields.Split(',').ToList();
                columns.AddRange(otherColumns);
            }
            catch
            {
                return columns;
            }
            return columns;
        }

        /// <summary>
        /// Gets including categories boolean for specific classname from Funnelback module. Used for adding categories to XML (automatically) when pushing data to Funnelback.
        /// </summary>
        /// <param name="className">ClassName of Page Type</param>
        /// <returns></returns>
        public static bool GetPushClassIncludeCategories(string className)
        {
            FunnelbackPushClassInfo classInfo = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(className);

            if (classInfo != null)
            {
                return classInfo.PushHasCategories;
            }
            else
            {
                return false;
            }
            
        }

        /// <summary>
        /// Returns configured push collection from Funnelback module using a classname.
        /// </summary>
        /// <param name="className">ClassName of Page Type</param>
        /// <returns></returns>
        public static string GetPushClassCollection(string className)
        {
            FunnelbackPushClassInfo classInfo = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(className);
            if (classInfo != null)
            {
                return classInfo.PushCollectionName;
            }
            else
            {
                return "";
            }
           
        }
    }
}