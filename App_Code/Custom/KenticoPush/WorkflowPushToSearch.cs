﻿using System;
using System.Collections.Generic;
using System.Web;
using CMS.DocumentEngine;
using CMS.Base;
using CMS.WorkflowEngine;
using CMS.EventLog;
using HM.Funnelback.KenticoPush;
using CMS.SiteProvider;
using CMS;

/// <summary>
/// Push to search class
/// </summary>
[assembly: RegisterCustomClass("PushToSearch", typeof(PushToSearch))]
public class PushToSearch : DocumentWorkflowAction
{
    /// <summary>
    /// Custom workflow action: Push to Search. Before publishing, the piece of Kentico content is transformed to XML and pushed to the selected Funnelback Push Collection based on settings in HM Funnelback Search Module.
    /// </summary>
    public override void Execute()
    {
        string className = Node.ClassName;
        bool containsCoupledData = Node.IsCoupled;
        var typeInfo = Node.TypeInfo;

        var test = Node.RelatedData;
        string siteName = Node.NodeSiteName;
        try
        {
            //Get Columns settings from Module settings
            List<string> columns = FunnelbackModuleHelpers.GetPushClassNameColumns(Node.ClassName);

            if (columns.Count > 0)
            {
                columns.AddRange(ModuleConstants.SystemColumns);
                bool includeCategories = FunnelbackModuleHelpers.GetPushClassIncludeCategories(Node.ClassName);
                string targetCollection = ModuleConstants.GetFunnelbackCollection(Node.ClassName);

                //Check workflow step and push or delete from index depending on step
                if (targetCollection != "")
                {
                    string response = KenticoXML.SubmitKenticoXML(targetCollection, String.Join(",", columns), "", Node.ClassName, Node.NodeSiteName, "", includeCategories, Node.DocumentGUID, Node.AbsoluteURL);
                    EventLogProvider.LogInformation(EventType.INFORMATION, "PushToSearchWorkflowResult", "Response = " + response);
                    
                }
                else
                {
                    EventLogProvider.LogInformation(EventType.WARNING, "PushToSearchWorkflowResult", "No Funnelback Collection target is defined for the classname: " + Node.ClassName);
                }
                
            }
            else
            {
                //ClassName doesn't need to be pushed or there is no push class defined in HM Funnelback module
                EventLogProvider.LogInformation(EventType.INFORMATION, "PushToSearchWorkflowResult", "No configuration for " + Node.ClassName);
            }

        }

        catch (Exception ex)
        {
            EventLogProvider.LogInformation(EventType.ERROR, "PushToSearchWorkflowResult", ex.InnerException.ToString());
        }

    }
}