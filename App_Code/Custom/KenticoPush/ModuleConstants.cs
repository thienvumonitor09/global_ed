﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using CMS.DataEngine;
using CMS.SiteProvider;

namespace HM.Funnelback.KenticoPush
{
    public class ModuleConstants
    {
        public static string[] SystemColumns = new string[] { "DocumentID", "DocumentGUID", "NodeAliasPath", "DocumentModifiedWhen", "DocumentCreatedWhen", "NodeSiteID" };
       

        //"DocumentID,DocumentGUID,NodeAliasPath,DocumentName"

        internal static string GetFunnelbackApiKey(string siteName)
        {
            string setting = SettingsKeyInfoProvider.GetValue(siteName + ".FunnelbackSecurityToken");
            
            return setting;
        }

        //Read push collection API uri
        internal static string GetFunnelbackRestApiUri()
        {
            //string setting = GetModuleSetting("CCS", "FunnelbackPushRestAPI");
            string setting = SettingsKeyInfoProvider.GetValue(SiteContext.CurrentSiteName + ".FunnelbackPushRestAPI");

            //return "https://c3po.highmonkey.com:5555/push-api/v2/collections";
            return setting;
        }

        //Get Collection name to be pushed into
        internal static string GetFunnelbackCollection(string className)
        {

            FunnelbackPushClassInfo info = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(className);

            string pushCollectionName = info.PushCollectionName;

            return pushCollectionName;
        }

        internal static string GetKenticoXmlRootName(string siteName)
        {
            string setting = SettingsKeyInfoProvider.GetValue(siteName + ".FunnelbackXMLRoot");

            return setting;
        }

        //Get ClassName for item root node name
        internal static string GetKenticoXmlElementName(string className)
        {
            string elementName = "";
            var classInfo = FunnelbackPushClassInfoProvider.GetFunnelbackPushClassInfo(className);
            if (classInfo != null)
            {
                elementName = classInfo.PushClassName;
            }

            return elementName;
        }
    }
}