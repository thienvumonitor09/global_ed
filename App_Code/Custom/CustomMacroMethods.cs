﻿using CMS.MacroEngine;
using CMS.Helpers;
using System.Collections.Generic;
using CMS.DataEngine;
using System;

public class CustomMacroMethods : MacroMethodContainer
{
    /// <summary>
    /// Method to list all columns in Page Type. Used in Funnelback Module to build out selectable columns in drop-down lists.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    [MacroMethod(typeof(List<string>), "Returns a list of fields for the class.", 1)]
    [MacroMethodParam(0, "classcodename", typeof(string), "Class code name.")]
    public static object GetClassFields(EvaluationContext context, params object[] parameters)
    {
        {
            // Branches according to the number of the method's parameters
            switch (parameters.Length)
            {
                case 1:
                    // Overload with one parameter
                    List<string> lstFields = new List<string>();
                    lstFields = ClassStructureInfo.GetColumns(ValidationHelper.GetString(parameters[0], ""));
                    return lstFields;
                default:
                    // No other overloads are supported
                    throw new NotSupportedException();
            }
        }
    }
}