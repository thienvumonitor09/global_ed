﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.DirectoryServices;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
//using System.DirectoryServices.AccountManagement;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    // https://www.codeproject.com/Articles/22777/Email-Address-Validation-Using-Regular-Expression
    public const string MatchEmailPattern =
       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";


    public static bool IsValidEmail(string email)
    {
        if (email != null)
        {
            return Regex.IsMatch(email, MatchEmailPattern);
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Remove dangerous script tags; prepare/convert text for email in HTML or Plain Text; escape aostrophes for SQL server 
    /// </summary>
    /// <param name="strText"></param>
    /// <param name="ASCIIToHTML"></param>
    /// <param name="IsSQL"></param>
    /// <returns></returns>
    public string ConvertAndFixText(string strText, bool ASCIIToHTML, bool HTMLToASCII, bool IsSQL)
    {
        //TO DO:  ADD HTMLToASCII BOOLEAN. SEPARATE THEM OUT FROM SQL. 
        StringBuilder objBuilder = new StringBuilder();
        char strChar;
        string strTemp = "";
        Int32 intLength = strText.Length;
        for (Int32 i = 0; i < intLength; i++)
        {
            strTemp = strText.Substring(i, 1);
            strChar = Convert.ToChar(strTemp);
            if (ASCIIToHTML == true)
            {
                if (strChar.ToString() == "\n")
                { strTemp = "<br />"; }
                if (strChar.ToString() == "\t")
                { strTemp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; }
                if (strChar == (char)39)
                { strTemp = "&#39;"; }
                if (strChar == (char)34)
                { strTemp = "&#34;"; }
            }
            if (IsSQL == true)
            {
                // ESCAPE APOSTROPHES
                if (strChar == (char)39 || strTemp == "'")
                { strTemp = "''"; }
            }
            objBuilder.Append(strTemp);
        }
        if (HTMLToASCII == true)
        {
            objBuilder.Replace("<p>", "");
            objBuilder.Replace("</p>", "\n");
            objBuilder.Replace("<br />", "\n");
            // ESCAPE APOSTROPHES
            if (IsSQL == true)
            { objBuilder.Replace("&#39;", "''"); }
            else
            { objBuilder.Replace("&#39;", "'"); }
            objBuilder.Replace("&#34;", "\"");
        }
        objBuilder.Replace("<script", "");
        objBuilder.Replace("\x3cscript", "");
        objBuilder.Replace("/script", "");
        objBuilder.Replace("%3cscript", "");
        objBuilder.Replace("declare @", "");
        objBuilder.Replace("sysobjects", "");
        objBuilder.Replace("sysindexes", "");
        objBuilder.Replace("update [", "");
        objBuilder.Replace("drop [", "");
        objBuilder.Replace("set [", "");
        objBuilder.Replace("insert [", "");

        return objBuilder.ToString();
    }

    /// <summary>
    /// Calculate a date accounting for weekends
    /// </summary>
    /// <param name="intBusinessDayCount"></param>
    /// <param name="dtStartDate"></param>
    /// <returns></returns>
    public DateTime GetBusinessDate(int intBusinessDayCount, DateTime dtStartDate)
    {
        DateTime dtBusinessDate = dtStartDate;
        int intDateAdd = intBusinessDayCount;
        int intDayOfWeek = 0;
        Quarter_MetaData myQMD = new Quarter_MetaData();
        string strCurrentYRQ = myQMD.CurrentYrQ;
        string strQuarter = strCurrentYRQ.Substring(3, 1);
        for (int i = 0; i <= intBusinessDayCount; i++)
        {
            intDayOfWeek = (int)dtStartDate.AddDays(i).DayOfWeek;
            if (strQuarter == "1")
            {
                //If this is summer quarter, account for Friday no class
                if (intDayOfWeek == 6 || intDayOfWeek == 0 || intDayOfWeek == 5)
                {
                    intDateAdd += 1;
                }
                //If this is the July 4th holiday (and not already accounted for above), add another day
                else if (dtStartDate.AddDays(i).Day == 4)
                {
                    intDateAdd += 1;
                }
            }
            else
            {
                if (intDayOfWeek == 6 || intDayOfWeek == 0)
                {
                    intDateAdd += 1;
                }
            }
        }
        //If the last calculated day is Sunday, add one more day
        if ((int)dtStartDate.AddDays(intDateAdd).DayOfWeek == 0)
        {
            intDateAdd = intDateAdd + 1;
        }
        //If the last calculated day is Saturday, add two more days
        if ((int)dtStartDate.AddDays(intDateAdd).DayOfWeek == 6)
        {
            intDateAdd = intDateAdd + 2;
        }

        dtBusinessDate = dtStartDate.AddDays(intDateAdd);
        return dtBusinessDate;
    }

    /// <summary>
    /// strCCEmailAddresses is a semi-colon delineated list of email addresses to parse.  
    /// </summary>
    /// <param name="strTo"></param>
    /// <param name="strFrom"></param>
    /// <param name="strSubject"></param>
    /// <param name="blIsHTML"></param>
    /// <param name="strBody"></param>
    /// <param name="strCCEmailAddresses"></param>
    /// <returns>Returns "Success" or error message.</returns>
    public string SendEmailMsg(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses)
    {
        StringBuilder objBuilder = new StringBuilder();
        if (strFrom == "") { strFrom = "CCSWebApp@ccs.spokane.edu"; }
        if (strTo != "" && strSubject != "" && strBody != "")
        {
            if (blIsHTML == false)
            {
                strBody = strBody.Replace("<br />", "\n");  //Convert to a horizontal tab/carriage return
                strBody = strBody.Replace("&quot;", "\"");
                strBody = strBody.Replace("&rsquo;", "\'");
                strBody = strBody.Replace("&#39;", "\'");
                strBody = strBody.Replace("<p>", "");
                strBody = strBody.Replace("</p>", "\n\n");
                strBody = strBody.Replace("&nbsp;", " ");
                strBody = strBody.Replace("<strong>", "");
                strBody = strBody.Replace("</strong>", "");
                strBody = strBody.Replace("<b>", "");
                strBody = strBody.Replace("</b>", "");
                strSubject = strSubject.Replace("&#39;", "\'");
                strSubject = strSubject.Replace("&quot;", "\"");
            }
            SmtpClient objSender = new SmtpClient();
            MailMessage objEmail = new MailMessage(strFrom, strTo, strSubject, strBody);
            objEmail.IsBodyHtml = blIsHTML;
            string[] arrCCList = parseList(strCCEmailAddresses);
            int uBound = arrCCList.GetUpperBound(0);
            if (strCCEmailAddresses != "")
            {
                for (int i = 0; i < uBound; i++)
                {
                    if (arrCCList[i] != "")
                    {
                        objEmail.CC.Add(arrCCList[i]);
                    }
                }
            }
            //set host in web.config
            try
            {
                //TESTING MODE: UNCOMMENT NEXT LINE BEFORE GOING LIVE
                objSender.Send(objEmail);

                //TESTING ONLY 
                //objBuilder.Append("Email Message Parameters: " + strTo + "; " + strFrom + "; " + strSubject + "; " + strCCEmailAddresses + "<br />" + strBody + "<br />");
                //END TESTING
            }
            catch (Exception exc)
            {
                objBuilder.Append("<br /><b>Email Delivery Failed for " + strTo + ".</b> Error returned: " + exc + ".");
                return objBuilder.ToString();
            }
            if (objBuilder.ToString() == "")
            {
                objBuilder.Append("Success");
            }

        }
        return objBuilder.ToString();
    }


    public string SendEmailAndAttachment(string strTo, string strFrom, string strSubject, bool blIsHTML, string strBody, string strCCEmailAddresses, string strFileName)
    {
        StringBuilder objBuilder = new StringBuilder();
        Char delimiter = ';';
        Boolean blIsValidTo = false;
        Boolean blIsValidFrom = false;

        //validate email addresses
        if (strFrom == "") { strFrom = "CCSWebApp@ccs.spokane.edu"; }
        if (strTo != "" && IsValidEmail(strTo)) { blIsValidTo = true; }
        if (IsValidEmail(strFrom)) { blIsValidFrom = true; }

        if (blIsValidTo && blIsValidFrom && strSubject != "" && strBody != "")
        {
            if (blIsHTML == false)
            {
                strBody = strBody.Replace("<br />", "\n");  //Convert to a horizontal tab/carriage return
                strBody = strBody.Replace("&quot;", "\"");
                strBody = strBody.Replace("&rsquo;", "\'");
                strBody = strBody.Replace("&#39;", "\'");
                strBody = strBody.Replace("<p>", "");
                strBody = strBody.Replace("</p>", "\n\n");
                strBody = strBody.Replace("&nbsp;", " ");
                strBody = strBody.Replace("<strong>", "");
                strBody = strBody.Replace("</strong>", "");
                strBody = strBody.Replace("<b>", "");
                strBody = strBody.Replace("</b>", "");
                strSubject = strSubject.Replace("&#39;", "\'");
                strSubject = strSubject.Replace("&quot;", "\"");
            }
            SmtpClient objSender = new SmtpClient();
            MailMessage objEmail = new MailMessage(strFrom, strTo, strSubject, strBody);
            if (strFileName != "")
            {
                if (strFileName.Contains(";"))
                {
                    String[] substrings = strFileName.Split(delimiter);
                    foreach (var substring in substrings)
                    {
                        try
                        {
                            System.Net.Mail.Attachment attachment;
                            attachment = new System.Net.Mail.Attachment(substring);
                            objEmail.Attachments.Add(attachment);
                        }
                        catch (Exception ex)
                        {
                            HttpContext.Current.Response.Write("Error sending attachment: " + ex.Message + "<br />");
                        }
                    }
                }
                else
                {
                    try
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(strFileName);
                        objEmail.Attachments.Add(attachment);
                    }
                    catch (Exception ex)
                    {
                        HttpContext.Current.Response.Write("Error sending attachment: " + ex.Message + "<br />");
                    }
                }
            }
            objEmail.IsBodyHtml = blIsHTML;
            string[] arrCCList = parseList(strCCEmailAddresses);
            int uBound = arrCCList.GetUpperBound(0);
            if (strCCEmailAddresses != "")
            {
                for (int i = 0; i < uBound; i++)
                {
                    if (arrCCList[i] != "")
                    {
                        objEmail.CC.Add(arrCCList[i]);
                    }
                }
            }
            //TESTING ONLY 
            //With email message
            //objBuilder.Append("<br />Email Message Parameters: To: " + strTo + "; From: " + strFrom + "; Subject: " + strSubject + "; CC Addresses: " + strCCEmailAddresses + "<br />" + strBody + "<br />");
            //set host in web.config
            try
            {
                //TESTING MODE: UNCOMMENT NEXT LINE BEFORE GOING LIVE
                objSender.Send(objEmail);

                //TESTING ONLY 
                //With email message
                //objBuilder.Append("<br />Email Message Parameters: To: " + strTo + "; From: " + strFrom + "; Subject: " + strSubject + "; CC Addresses: " + strCCEmailAddresses + "<br />" + strBody + "<br />");
                //No email message
                //objBuilder.Append("<br />Email Message Parameters: To: " + strTo + "; From: " + strFrom + "; Subject: " + strSubject + "; CC Addresses: " + strCCEmailAddresses + "<br />");
                //END TESTING
            }
            catch (Exception exc)
            {
                objBuilder.Append("<br /><b>Email Delivery Failed for " + strTo + ".</b> Error returned: " + exc + "<br />");
                return objBuilder.ToString();
            }
            finally
            {
                //dispose
                objEmail.Dispose();
                objSender.Dispose();
            }
            if (objBuilder.ToString() == "")
            {
                objBuilder.Append("Success");
            }

        }
        return objBuilder.ToString();
    }


    /// <summary>
    /// Used to parse any semi-colon delineated list of items
    /// </summary>
    /// <param name="strList"></param>
    /// <returns></returns>
    public string[] parseList(string strList)
    {
        Int32 intLength = strList.Length;
        if (intLength > 0)
        {
            //Add a final semicolon if missing
            string strChar = strList.Substring(intLength - 1, 1);
            if (strChar != ";")
            {
                strList += ";";
            }
        }
        string[] arrStrList;
        arrStrList = strList.Split(';');
        return arrStrList;
    }

    /// <summary>
    /// Fetches class_D stu_D information by class ID and college code
    /// </summary>
    /// <param name="strClassID"></param>
    /// <param name="strCollegeCode"></param>
    /// <returns></returns>
    public DataTable GetClassStuInfo(string strClassID, string strCollegeCode)
    {
        SqlConnection objConn = new SqlConnection();
        DataTable _returnDataTable = new DataTable();
        SqlDataAdapter objDataAdapter;
        DataSet ds = new DataSet();
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ConnectionString;
        objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            string strSQL = "usp_GetClassDStuD_ByClassID '" + strClassID + "', '" + strCollegeCode + "'";
            SqlCommand objCommand = new SqlCommand(strSQL, objConn);
            objCommand.CommandType = CommandType.Text;
            objDataAdapter = new SqlDataAdapter(objCommand);
            objDataAdapter.Fill(ds, "ClassStuInfo");
            _returnDataTable = ds.Tables["ClassStuInfo"];
            objCommand.Dispose();
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
        }
        objConn.Dispose();
        return _returnDataTable;
    }

    /// <summary>
    /// Returns official student info from CCSsm
    /// </summary>
    /// <param name="strSID"></param>
    /// <param name="strCollegeCode"></param>
    /// <returns></returns>
    public string[] GetStudentInfo_CCSsm(string strSID, string strCollegeCode)
    {
        //Currently returns a one-dimensional array with 10 elements:
        //0 = Last Name
        //1 = First Name
        //2 = Middle Initial
        //3 = Name as it appears in the student records
        //4 = Email address
        //5 = Phone Number
        //6 = Address 
        //7 = City
        //8 = State
        //9 = ZIP Code
        string[] strReturnArray = new string[10];
        string strFullName = "";
        string strFName = "";
        string strLName = "";
        string strMInitial = "";
        string strEmail = "";
        string strPhone1 = "";
        string strAddr1 = "";
        string strCity = "";
        string strState = "";
        string strZIP = "";
        int intUpBound = 0;
        int i = 0;
        string strTemp1 = "";
        string strTemp2 = "";
        string strSQL = "usp_GetOneStu_D '" + strSID + "', '" + strCollegeCode + "'";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CcsSm"].ToString();
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCommand = new SqlCommand();
        SqlDataReader objReader;
        try
        {
            objConn.ConnectionString = strConnectionString;
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.Text;
            objCommand.Connection = objConn;
            objReader = objCommand.ExecuteReader();
            if (objReader.HasRows)
            {
                while (objReader.Read())
                {
                    strFullName = objReader["STU_NAME"].ToString();
                    strEmail = objReader["StuEmailAddr"].ToString();
                    strPhone1 = "(" + objReader["DAY_AREA_CODE"].ToString() + ") " + objReader["DAY_PREFIX"].ToString() + "-" + objReader["DAY_SUFFIX"].ToString();
                    strAddr1 = objReader["STU_STREET"].ToString();
                    strCity = objReader["STU_CITY"].ToString();
                    strState = objReader["STU_ST"].ToString();
                    strZIP = objReader["STU_ZIP"].ToString();
                }
            }
            objReader.Dispose();
            string[] strArray = strFullName.Split(new Char[] { ' ' });
            intUpBound = strArray.GetUpperBound(0);
            for (i = 0; i <= intUpBound; i++)
            {
                //strResultMsg =+ "i: " + i + "<br />";
                switch (i)
                {
                    case 0:
                        strLName = strArray[0];
                        strTemp2 = strLName.Substring(1, strLName.Length - 1).ToLower();
                        strTemp1 = strLName.Substring(0, 1);
                        strReturnArray[0] = strTemp1 + strTemp2;
                        break;
                    case 1:
                        strFName = strArray[1];
                        strTemp2 = strFName.Substring(1, strFName.Length - 1).ToLower();
                        strTemp1 = strFName.Substring(0, 1);
                        strReturnArray[1] = strTemp1 + strTemp2;
                        break;
                    case 2:
                        strMInitial = strArray[2];
                        strReturnArray[2] = strMInitial;
                        break;
                }
            }
            strReturnArray[3] = strFullName;
            strReturnArray[4] = strEmail;
            strReturnArray[5] = strPhone1;
            strReturnArray[6] = strAddr1;
            strReturnArray[7] = strCity;
            strReturnArray[8] = strState;
            strReturnArray[9] = strZIP;
        }
        catch
        {
            //do nothing
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
            objCommand.Dispose();
        }
        return strReturnArray;
    }

    /// <summary>
    /// Fetch faculty employee ID by SSN
    /// </summary>
    /// <param name="strSSN"></param>
    /// <returns></returns>
    public string LookupEmployeeSIDbySSN(string strSSN)
    {
        string strSID = "";
        SqlParameter param = new SqlParameter();
        string strSQLCmd = "";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ConnectionString;
        SqlConnection objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            strSQLCmd = "CCS_SP_EmpM_LookUpSID_bySSN";
            SqlCommand objCommand = new SqlCommand(strSQLCmd, objConn);
            objCommand.CommandType = CommandType.StoredProcedure;
            param.ParameterName = "@SSN";
            param.Value = strSSN;
            param.Direction = ParameterDirection.Input;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SID";
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            objCommand.ExecuteReader();
            strSID = (objCommand.Parameters["@SID"].Value.ToString());
        }
        catch (Exception ex)
        {
            return ex.Message; //+"; Source: " + ex.Source + "; Stack Trace: " + ex.StackTrace;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
        }
        return strSID;
    }

   

    /// <summary>
    /// Legacy system SSN lookup using CCSReplNLink
    /// Used by ISAC
    /// </summary>
    /// <param name="strSID"></param>
    /// <returns>SSN</returns>
    public string LookupEmployeeSSNbySID(string strSID)
    {
        string strSSN = "";
        SqlParameter param = new SqlParameter();
        string strSQLCmd = "";
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ConnectionString;
        SqlConnection objConn = new System.Data.SqlClient.SqlConnection();
        objConn.ConnectionString = strConnectionString;
        try
        {
            objConn.Open();
            strSQLCmd = "CCS_SP_EmpM_LookUpSSN_bySID";
            SqlCommand objCommand = new SqlCommand(strSQLCmd, objConn);
            objCommand.CommandType = CommandType.StoredProcedure;

            param.ParameterName = "@SID";
            param.Value = strSID;
            param.Direction = ParameterDirection.Input;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SSN";
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Char;
            param.Size = 9;
            objCommand.Parameters.Add(param);

            objCommand.ExecuteReader();
            strSSN = (objCommand.Parameters["@SSN"].Value.ToString());
        }
        catch (Exception ex)
        {
            return ex.Message; //+"; Source: " + ex.Source + "; Stack Trace: " + ex.StackTrace;
        }
        finally
        {
            objConn.Close();
            objConn.Dispose();
        }
        return strSSN;
    }

    /// <summary>
    /// Looks up any SSN by SID; choice of legacy or PeopleSoft systems
    /// </summary>
    /// <param name="strSystemID"></param>
    /// <param name="blUseLegacy"></param>
    /// <returns>SSN</returns>
    public string LookupSSN_by_SID(string strSystemID, Boolean blUseLegacy)
    {
        string strResult = "";
        if (blUseLegacy)
        {
            //Uses SidSsnXref table in CCSReplNLink - students and staff OK
            //District
            string strSQL = "CCS_SP_LookUpSSN_bySID";
            //SFCC
            //string strSQL = "SP_LookUpSSN_bySID";
            SqlConnection objConn = new SqlConnection();
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSReplNLink"].ToString();
            SqlCommand objCommand = new SqlCommand();
            try
            {
                objConn.Open();
                objCommand.CommandText = strSQL;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Connection = objConn;
                SqlParameter objParam = new SqlParameter();
                objParam.ParameterName = "@SID";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 9;
                objParam.Value = strSystemID;
                objCommand.Parameters.Add(objParam);
                objParam = new SqlParameter();
                objParam.ParameterName = "@SSN";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 9;
                objParam.Direction = ParameterDirection.Output;
                objCommand.Parameters.Add(objParam);
                objCommand.ExecuteReader();
                strResult = Convert.ToString(objCommand.Parameters["@SSN"].Value);
            }
            catch
            {
                strResult = "";
            }
            finally
            {
                objConn.Close();
                objCommand.Dispose();
            }
        }
        else
        {
            //get peoplesoft info
            //@ID = input ID can be valid values for SID, SSN, EMPLID   - REQUIRED varchar(9)
            //@INPUTTYPE = SID/SSN/EMPLID string to indicate what you are passing in.  - REQUIRED varchar(6)
            //@OUTPUTTYPE = SID/SSN/EMPLID string to indicate what you want returned.  - REUQIRED varchar(6)
            //@COLLEGE = 25/26/27  (DIST/SCC/SFCC)   -optional varchar(2)
            //@OUTPUTID = returned value

            string strSQL = "usp_ReturnID";
            SqlConnection objConn = new SqlConnection();
            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
            SqlCommand objCommand = new SqlCommand();
            try
            {
                objConn.Open();
                objCommand.CommandText = strSQL;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Connection = objConn;
                SqlParameter objParam = new SqlParameter();
                objParam.ParameterName = "@ID";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 9;
                objParam.Value = strSystemID;
                objCommand.Parameters.Add(objParam);
                objParam = new SqlParameter();
                objParam.ParameterName = "@INPUTTYPE";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 6;
                objParam.Value = "SID";
                objCommand.Parameters.Add(objParam);
                objParam = new SqlParameter();
                objParam.ParameterName = "@OUTPUTTYPE";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 6;
                objParam.Value = "SSN";
                objCommand.Parameters.Add(objParam);
                objParam = new SqlParameter();
                objParam.ParameterName = "@OUTPUTID";
                objParam.SqlDbType = SqlDbType.Char;
                objParam.Size = 9;
                objParam.Direction = ParameterDirection.Output;
                objCommand.Parameters.Add(objParam);
                objCommand.ExecuteReader();
                strResult = Convert.ToString(objCommand.Parameters["@OUTPUTID"].Value);
            }
            catch
            {
                strResult = "";
            }
            finally
            {
                objConn.Close();
                objCommand.Dispose();
            }
        }
        return strResult;

    }

    /// <summary>
    /// PeopleSoft data ID lookup of any input/output type using legacy SID, SSN and/or EmplID
    /// </summary>
    /// <param name="strInput"></param>
    /// <param name="strInputType"></param>
    /// <param name="strOutputType"></param>
    /// <returns>Specified ID</returns>
    public string LookupID(string strInput, string strInputType, string strOutputType)
    {
        //@ID = input ID can be valid values for SID, SSN, EMPLID   - REQUIRED varchar(9)
        //@INPUTTYPE = SID/SSN/EMPLID string to indicate what you are passing in.  - REQUIRED varchar(6)
        //@OUTPUTTYPE = SID/SSN/EMPLID string to indicate what you want returned.  - REUQIRED varchar(6)
        //@COLLEGE = 25/26/27  (DIST/SCC/SFCC)   -optional varchar(2)
        //@OUTPUTID = returned value

        string strResult = "";
        string strSQL = "usp_ReturnID";
        SqlConnection objConn = new SqlConnection();
        objConn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_CCSGen_ctcLink_ODS"].ToString();
        //objConn.ConnectionString = "Data Source=tcp:CCSSQL2\\CCSSQLInt2;Initial Catalog=CCSGen_ctcLink_ODS;Integrated Security=True";
        SqlCommand objCommand = new SqlCommand();
        try
        {
            objConn.Open();
            objCommand.CommandText = strSQL;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Connection = objConn;
            SqlParameter objParam = new SqlParameter();
            objParam.ParameterName = "@ID";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 9;
            objParam.Value = strInput;
            objCommand.Parameters.Add(objParam);
            objParam = new SqlParameter();
            objParam.ParameterName = "@INPUTTYPE";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 6;
            objParam.Value = strInputType;
            objCommand.Parameters.Add(objParam);
            objParam = new SqlParameter();
            objParam.ParameterName = "@OUTPUTTYPE";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 6;
            objParam.Value = strOutputType;
            objCommand.Parameters.Add(objParam);
            objParam = new SqlParameter();
            objParam.ParameterName = "@OUTPUTID";
            objParam.SqlDbType = SqlDbType.Char;
            objParam.Size = 9;
            objParam.Direction = ParameterDirection.Output;
            objCommand.Parameters.Add(objParam);
            objCommand.ExecuteReader();
            strResult = Convert.ToString(objCommand.Parameters["@OUTPUTID"].Value);
        }
        catch (Exception ex)
        {
            strResult = ex.Message;
        }
        finally
        {
            objConn.Close();
            objCommand.Dispose();
        }
        return strResult;
    }

    /// <summary>
    /// Detects correct characters for IEL credit Class for first character in class item number
    /// </summary>
    /// <param name="strChar"></param>
    /// <returns></returns>
    public bool IsIELCreditClass(string strChar)
    {
        bool blIsIEL = false;
        switch (strChar.ToUpper())
        {
            case "A":
            case "B":
            case "C":
            case "D":
            case "E":
            case "F":
            case "G":
            case "H":
                blIsIEL = true;
                break;
        }
        return blIsIEL;
    }

    public string GetInstitution(string strInput, bool blGetAbbreviation)
    {
        string strInstitution = "";
        switch (strInput)
        {
            case "ccs.spokane.edu":
                if (blGetAbbreviation) { strInstitution = "CCS"; } else { strInstitution = "Community Colleges of Spokane"; }
                break;
            case "scc.spokane.edu":
                if (blGetAbbreviation) { strInstitution = "SCC"; } else { strInstitution = "Spokane Community Colleges"; }
                break;
            case "sfcc.spokane.edu":
                if (blGetAbbreviation) { strInstitution = "SFCC"; } else { strInstitution = "Spokane Falls Community Colleges"; }
                break;
            case "bigfoot.spokane.edu":
                if (blGetAbbreviation) { strInstitution = "Student"; } else { strInstitution = "Student"; }
                break;
        }
        return strInstitution;
    }

    public string TitleCase(string strText)
    {
        string strChar = "";
        string strResult = "";
        int i = 0;
        int intLen = strText.Length;
        int j = 0;
        string[] strSplit = strText.Split(new Char[] { ' ' });
        int intUBound = 0;
        bool blCapNext = false;

        if (intLen != 0)
        {
            intUBound = strSplit.GetUpperBound(0);
            for (i = 0; i <= intUBound; i++)
            {
                intLen = strSplit[i].Length;
                for (j = 0; j < intLen; j++)
                {
                    strChar = strSplit[i].Substring(j, 1);
                    if (blCapNext)
                    {
                        strChar = strChar.ToUpper();
                        blCapNext = false;
                    }
                    else
                    {
                        if (j == 0)
                        {
                            strChar = strChar.ToUpper();
                        }
                        else
                        {
                            strChar = strChar.ToLower();
                        }
                    }
                    if (strChar == "/") { blCapNext = true; }
                    if (strChar == "-") { blCapNext = true; }
                    if (strChar == " ") { blCapNext = true; }
                    if (strChar == "(") { blCapNext = true; }
                    strResult += strChar;
                }
                strResult += " ";
            }
        }
        strResult = strResult.Replace("Scc", "SCC");
        strResult = strResult.Replace("Sfcc", "SFCC");
        strResult = strResult.Replace("Ccs", "CCS");
        strResult = strResult.Replace("(Cahims)", "(CAHIMS)");
        return strResult;
    }
}