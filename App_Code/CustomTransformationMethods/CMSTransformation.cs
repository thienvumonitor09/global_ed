﻿using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System;
using CMS.SiteProvider;
using CMS.EventLog;

namespace CMS.DocumentEngine.Web.UI
{
    /// <summary>
    /// Extends the CMSTransformation partial class.
    /// </summary>
    public partial class CMSTransformation
    {

        public string GetPageSelectorPageURL(string PageSelectorValue)
        {
            string[] split = PageSelectorValue.Split(';');
            Guid documentGUID = new Guid();
            string absoluteURL = "";
            string siteName = SiteContext.CurrentSiteName;
            var site = SiteInfoProvider.GetSiteInfo(siteName);

            if (split.Length > 0)
            {
                documentGUID = Guid.Parse(split[0]);
                EventLogProvider.LogInformation("GetPageURL", "INFORMATION", documentGUID.ToString());
            }

            if (split.Length > 1)
            {
                siteName = split[1];
                site = SiteInfoProvider.GetSiteInfo(siteName);
                EventLogProvider.LogInformation("GetPageURL", "INFORMATION", site.SiteName.ToString());
            }

            var page = DocumentHelper.GetDocuments().WhereEquals("DocumentGUID", documentGUID).TopN(1);
            foreach (var result in page)
            {
                absoluteURL = GetAbsoluteUrl(result.NodeAliasPath, siteName);
                EventLogProvider.LogInformation("GetPageURL", "INFORMATION", absoluteURL);
            }
            
            
            return absoluteURL;
        }
        
        public string CustomStripHTML(object txtValue)
        {
            var str = "";

            str = WebUtility.HtmlDecode(txtValue.ToString());

            str.Replace("<& lt; p>", "");
            str.Replace("<em>", "");
            str.Replace("<strong>", "");
            str.Replace("</strong>", "");
            str.Replace("</em>", "");
            str.Replace("<br />", "");
            str.Replace("</strong& gt;", "");
            str.Replace("<p>", "");
            return str;
        }

       
        public string RemoveInvalidTags(string txtValue)
        {
            var str = "";

            str = txtValue.ToString();

            str.Replace("&lt;p&gt;", "");
            str.Replace("&lt;em&gt;", "");
            str.Replace("&lt;strong&gt;", "");
            str.Replace("&lt;/strong& gt;", "");
            str.Replace("&lt;/em&gt;", "");
            str.Replace("&lt;br /&gt;", "");
            str.Replace("&lt;br /&gt;", "");
            str.Replace("<p>", "");


            var doc = new HtmlDocument();
            doc.LoadHtml(str ?? "");
            return doc.DocumentNode.InnerText;


            

          
        }

    }
}