using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using MyCustomModule;

[assembly: RegisterObjectType(typeof(SelectedItemInfo), SelectedItemInfo.OBJECT_TYPE)]

namespace MyCustomModule
{
    /// <summary>
    /// SelectedItemInfo data container class.
    /// </summary>
    [Serializable]
    public partial class SelectedItemInfo : AbstractInfo<SelectedItemInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "ccs.selecteditem";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(SelectedItemInfoProvider), OBJECT_TYPE, "CCS.SelectedItem", "SelectedItemID", "SelectedItemLastModified", "SelectedItemGuid", "SelectedItemName", null, null, null, null, null)
        {
            ModuleName = "MyCustomModule",
            TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Selected item ID
        /// </summary>
        [DatabaseField]
        public virtual int SelectedItemID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("SelectedItemID"), 0);
            }
            set
            {
                SetValue("SelectedItemID", value);
            }
        }


        /// <summary>
        /// Selected item name
        /// </summary>
        [DatabaseField]
        public virtual string SelectedItemName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("SelectedItemName"), String.Empty);
            }
            set
            {
                SetValue("SelectedItemName", value, String.Empty);
            }
        }


        /// <summary>
        /// Selected item valid
        /// </summary>
        [DatabaseField]
        public virtual bool SelectedItemValid
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("SelectedItemValid"), false);
            }
            set
            {
                SetValue("SelectedItemValid", value);
            }
        }


        /// <summary>
        /// Selected item guid
        /// </summary>
        [DatabaseField]
        public virtual Guid SelectedItemGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("SelectedItemGuid"), Guid.Empty);
            }
            set
            {
                SetValue("SelectedItemGuid", value);
            }
        }


        /// <summary>
        /// Selected item last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime SelectedItemLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("SelectedItemLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("SelectedItemLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            SelectedItemInfoProvider.DeleteSelectedItemInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            SelectedItemInfoProvider.SetSelectedItemInfo(this);
        }

        #endregion


        #region "Constructors"

        /// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected SelectedItemInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty SelectedItemInfo object.
        /// </summary>
        public SelectedItemInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new SelectedItemInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public SelectedItemInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}