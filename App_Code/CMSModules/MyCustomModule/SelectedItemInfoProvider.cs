using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace MyCustomModule
{    
    /// <summary>
    /// Class providing SelectedItemInfo management.
    /// </summary>
    public partial class SelectedItemInfoProvider : AbstractInfoProvider<SelectedItemInfo, SelectedItemInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public SelectedItemInfoProvider()
            : base(SelectedItemInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the SelectedItemInfo objects.
        /// </summary>
        public static ObjectQuery<SelectedItemInfo> GetSelectedItems()
        {
            return ProviderObject.GetSelectedItemsInternal();
        }


        /// <summary>
        /// Returns SelectedItemInfo with specified ID.
        /// </summary>
        /// <param name="id">SelectedItemInfo ID</param>
        public static SelectedItemInfo GetSelectedItemInfo(int id)
        {
            return ProviderObject.GetSelectedItemInfoInternal(id);
        }


        /// <summary>
        /// Returns SelectedItemInfo with specified name.
        /// </summary>
        /// <param name="name">SelectedItemInfo name</param>
        public static SelectedItemInfo GetSelectedItemInfo(string name)
        {
            return ProviderObject.GetSelectedItemInfoInternal(name);
        }


        /// <summary>
        /// Returns SelectedItemInfo with specified GUID.
        /// </summary>
        /// <param name="guid">SelectedItemInfo GUID</param>                
        public static SelectedItemInfo GetSelectedItemInfo(Guid guid)
        {
            return ProviderObject.GetSelectedItemInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified SelectedItemInfo.
        /// </summary>
        /// <param name="infoObj">SelectedItemInfo to be set</param>
        public static void SetSelectedItemInfo(SelectedItemInfo infoObj)
        {
            ProviderObject.SetSelectedItemInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified SelectedItemInfo.
        /// </summary>
        /// <param name="infoObj">SelectedItemInfo to be deleted</param>
        public static void DeleteSelectedItemInfo(SelectedItemInfo infoObj)
        {
            ProviderObject.DeleteSelectedItemInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes SelectedItemInfo with specified ID.
        /// </summary>
        /// <param name="id">SelectedItemInfo ID</param>
        public static void DeleteSelectedItemInfo(int id)
        {
            SelectedItemInfo infoObj = GetSelectedItemInfo(id);
            DeleteSelectedItemInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the SelectedItemInfo objects.
        /// </summary>
        protected virtual ObjectQuery<SelectedItemInfo> GetSelectedItemsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns SelectedItemInfo with specified ID.
        /// </summary>
        /// <param name="id">SelectedItemInfo ID</param>        
        protected virtual SelectedItemInfo GetSelectedItemInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns SelectedItemInfo with specified name.
        /// </summary>
        /// <param name="name">SelectedItemInfo name</param>        
        protected virtual SelectedItemInfo GetSelectedItemInfoInternal(string name)
        {
            return GetInfoByCodeName(name);
        } 


        /// <summary>
        /// Returns SelectedItemInfo with specified GUID.
        /// </summary>
        /// <param name="guid">SelectedItemInfo GUID</param>
        protected virtual SelectedItemInfo GetSelectedItemInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified SelectedItemInfo.
        /// </summary>
        /// <param name="infoObj">SelectedItemInfo to be set</param>        
        protected virtual void SetSelectedItemInfoInternal(SelectedItemInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified SelectedItemInfo.
        /// </summary>
        /// <param name="infoObj">SelectedItemInfo to be deleted</param>        
        protected virtual void DeleteSelectedItemInfoInternal(SelectedItemInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}