using System;
using System.Data;
using System.Runtime.Serialization;
using System.Collections.Generic;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using HM.Funnelback;

[assembly: RegisterObjectType(typeof(FunnelbackCollectionClassInfo), FunnelbackCollectionClassInfo.OBJECT_TYPE)]
    
namespace HM.Funnelback
{
    /// <summary>
    /// FunnelbackCollectionClassInfo data container class.
    /// </summary>
	[Serializable]
    public class FunnelbackCollectionClassInfo : AbstractInfo<FunnelbackCollectionClassInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "hm.funnelbackcollectionclass";


        /// <summary>
        /// Type information.
        /// </summary>
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(FunnelbackCollectionClassInfoProvider), OBJECT_TYPE, "HM.FunnelbackCollectionClass", "FunnelbackCollectionClassID", null, null, null, null, null, null, "FunnelbackCollectionID", "hm.funnelbackcollection")
        {
			ModuleName = "HM.Funnelback",
            IsBinding = true,
			TouchCacheDependencies = true,
            DependsOn = new List<ObjectDependency>() 
			{
			    //new ObjectDependency("FunnelbackCollectionID", "hm.funnelbackcollection", ObjectDependencyEnum.Required), 
			    new ObjectDependency("FunnelbackPushClassID", "hm.funnelbackpushclass", ObjectDependencyEnum.Binding), 
            },
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Funnelback collection class ID
        /// </summary>
        [DatabaseField]
        public virtual int FunnelbackCollectionClassID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("FunnelbackCollectionClassID"), 0);
            }
            set
            {
                SetValue("FunnelbackCollectionClassID", value);
            }
        }


        /// <summary>
        /// Funnelback collection ID
        /// </summary>
        [DatabaseField]
        public virtual int FunnelbackCollectionID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("FunnelbackCollectionID"), 0);
            }
            set
            {
                SetValue("FunnelbackCollectionID", value);
            }
        }


        /// <summary>
        /// Funnelback push class ID
        /// </summary>
        [DatabaseField]
        public virtual int FunnelbackPushClassID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("FunnelbackPushClassID"), 0);
            }
            set
            {
                SetValue("FunnelbackPushClassID", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            FunnelbackCollectionClassInfoProvider.DeleteFunnelbackCollectionClassInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            FunnelbackCollectionClassInfoProvider.SetFunnelbackCollectionClassInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public FunnelbackCollectionClassInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty FunnelbackCollectionClassInfo object.
        /// </summary>
        public FunnelbackCollectionClassInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new FunnelbackCollectionClassInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public FunnelbackCollectionClassInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}