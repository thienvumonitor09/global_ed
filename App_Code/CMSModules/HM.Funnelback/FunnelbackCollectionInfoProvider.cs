using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace HM.Funnelback
{    
    /// <summary>
    /// Class providing FunnelbackCollectionInfo management.
    /// </summary>
    public partial class FunnelbackCollectionInfoProvider : AbstractInfoProvider<FunnelbackCollectionInfo, FunnelbackCollectionInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public FunnelbackCollectionInfoProvider()
            : base(FunnelbackCollectionInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the FunnelbackCollectionInfo objects.
        /// </summary>
        public static ObjectQuery<FunnelbackCollectionInfo> GetFunnelbackCollections()
        {
            return ProviderObject.GetFunnelbackCollectionsInternal();
        }


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionInfo ID</param>
        public static FunnelbackCollectionInfo GetFunnelbackCollectionInfo(int id)
        {
            return ProviderObject.GetFunnelbackCollectionInfoInternal(id);
        }


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified name.
        /// </summary>
        /// <param name="name">FunnelbackCollectionInfo name</param>
        public static FunnelbackCollectionInfo GetFunnelbackCollectionInfo(string name)
        {
            return ProviderObject.GetFunnelbackCollectionInfoInternal(name);
        }


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified GUID.
        /// </summary>
        /// <param name="guid">FunnelbackCollectionInfo GUID</param>                
        public static FunnelbackCollectionInfo GetFunnelbackCollectionInfo(Guid guid)
        {
            return ProviderObject.GetFunnelbackCollectionInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackCollectionInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionInfo to be set</param>
        public static void SetFunnelbackCollectionInfo(FunnelbackCollectionInfo infoObj)
        {
            ProviderObject.SetFunnelbackCollectionInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackCollectionInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionInfo to be deleted</param>
        public static void DeleteFunnelbackCollectionInfo(FunnelbackCollectionInfo infoObj)
        {
            ProviderObject.DeleteFunnelbackCollectionInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes FunnelbackCollectionInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionInfo ID</param>
        public static void DeleteFunnelbackCollectionInfo(int id)
        {
            FunnelbackCollectionInfo infoObj = GetFunnelbackCollectionInfo(id);
            DeleteFunnelbackCollectionInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the FunnelbackCollectionInfo objects.
        /// </summary>
        protected virtual ObjectQuery<FunnelbackCollectionInfo> GetFunnelbackCollectionsInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionInfo ID</param>        
        protected virtual FunnelbackCollectionInfo GetFunnelbackCollectionInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified name.
        /// </summary>
        /// <param name="name">FunnelbackCollectionInfo name</param>        
        protected virtual FunnelbackCollectionInfo GetFunnelbackCollectionInfoInternal(string name)
        {
            return GetInfoByCodeName(name);
        } 


        /// <summary>
        /// Returns FunnelbackCollectionInfo with specified GUID.
        /// </summary>
        /// <param name="guid">FunnelbackCollectionInfo GUID</param>
        protected virtual FunnelbackCollectionInfo GetFunnelbackCollectionInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackCollectionInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionInfo to be set</param>        
        protected virtual void SetFunnelbackCollectionInfoInternal(FunnelbackCollectionInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackCollectionInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionInfo to be deleted</param>        
        protected virtual void DeleteFunnelbackCollectionInfoInternal(FunnelbackCollectionInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}