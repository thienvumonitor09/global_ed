using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using HM.Funnelback;

[assembly: RegisterObjectType(typeof(FunnelbackCollectionInfo), FunnelbackCollectionInfo.OBJECT_TYPE)]
    
namespace HM.Funnelback
{
    /// <summary>
    /// FunnelbackCollectionInfo data container class.
    /// </summary>
	[Serializable]
    public partial class FunnelbackCollectionInfo : AbstractInfo<FunnelbackCollectionInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "hm.funnelbackcollection";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(FunnelbackCollectionInfoProvider), OBJECT_TYPE, "HM.FunnelbackCollection", "FunnelbackCollectionID", "FunnelbackCollectionLastModified", "FunnelbackCollectionGuid", "FunnelbackCollectionName", null, null, null, null, null)
        {
			ModuleName = "HM.Funnelback",
			TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Funnelback collection ID
        /// </summary>
        [DatabaseField]
        public virtual int FunnelbackCollectionID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("FunnelbackCollectionID"), 0);
            }
            set
            {
                SetValue("FunnelbackCollectionID", value);
            }
        }


        /// <summary>
        /// Funnelback collection name
        /// </summary>
        [DatabaseField]
        public virtual string FunnelbackCollectionName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("FunnelbackCollectionName"), String.Empty);
            }
            set
            {
                SetValue("FunnelbackCollectionName", value, String.Empty);
            }
        }


        /// <summary>
        /// Funnelback collection guid
        /// </summary>
        [DatabaseField]
        public virtual Guid FunnelbackCollectionGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("FunnelbackCollectionGuid"), Guid.Empty);
            }
            set
            {
                SetValue("FunnelbackCollectionGuid", value);
            }
        }


        /// <summary>
        /// Funnelback collection last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime FunnelbackCollectionLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("FunnelbackCollectionLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("FunnelbackCollectionLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            FunnelbackCollectionInfoProvider.DeleteFunnelbackCollectionInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            FunnelbackCollectionInfoProvider.SetFunnelbackCollectionInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected FunnelbackCollectionInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty FunnelbackCollectionInfo object.
        /// </summary>
        public FunnelbackCollectionInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new FunnelbackCollectionInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public FunnelbackCollectionInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}