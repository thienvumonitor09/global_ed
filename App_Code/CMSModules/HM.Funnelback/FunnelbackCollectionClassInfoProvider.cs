using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace HM.Funnelback
{    
    /// <summary>
    /// Class providing FunnelbackCollectionClassInfo management.
    /// </summary>
    public class FunnelbackCollectionClassInfoProvider : AbstractInfoProvider<FunnelbackCollectionClassInfo, FunnelbackCollectionClassInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public FunnelbackCollectionClassInfoProvider()
            : base(FunnelbackCollectionClassInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the FunnelbackCollectionClassInfo objects.
        /// </summary>
        public static ObjectQuery<FunnelbackCollectionClassInfo> GetFunnelbackCollectionClasses()
        {
            return ProviderObject.GetFunnelbackCollectionClassesInternal();
        }


        /// <summary>
        /// Returns FunnelbackCollectionClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionClassInfo ID</param>
        public static FunnelbackCollectionClassInfo GetFunnelbackCollectionClassInfo(int id)
        {
            return ProviderObject.GetFunnelbackCollectionClassInfoInternal(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackCollectionClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionClassInfo to be set</param>
        public static void SetFunnelbackCollectionClassInfo(FunnelbackCollectionClassInfo infoObj)
        {
            ProviderObject.SetFunnelbackCollectionClassInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackCollectionClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionClassInfo to be deleted</param>
        public static void DeleteFunnelbackCollectionClassInfo(FunnelbackCollectionClassInfo infoObj)
        {
            ProviderObject.DeleteFunnelbackCollectionClassInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes FunnelbackCollectionClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionClassInfo ID</param>
        public static void DeleteFunnelbackCollectionClassInfo(int id)
        {
            FunnelbackCollectionClassInfo infoObj = GetFunnelbackCollectionClassInfo(id);
            DeleteFunnelbackCollectionClassInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the FunnelbackCollectionClassInfo objects.
        /// </summary>
        protected virtual ObjectQuery<FunnelbackCollectionClassInfo> GetFunnelbackCollectionClassesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns FunnelbackCollectionClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackCollectionClassInfo ID</param>        
        protected virtual FunnelbackCollectionClassInfo GetFunnelbackCollectionClassInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackCollectionClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionClassInfo to be set</param>        
        protected virtual void SetFunnelbackCollectionClassInfoInternal(FunnelbackCollectionClassInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackCollectionClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackCollectionClassInfo to be deleted</param>        
        protected virtual void DeleteFunnelbackCollectionClassInfoInternal(FunnelbackCollectionClassInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}