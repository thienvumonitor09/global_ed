using System;
using System.Data;
using System.Runtime.Serialization;

using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using HM.Funnelback;

[assembly: RegisterObjectType(typeof(FunnelbackPushClassInfo), FunnelbackPushClassInfo.OBJECT_TYPE)]
    
namespace HM.Funnelback
{
    /// <summary>
    /// FunnelbackPushClassInfo data container class.
    /// </summary>
	[Serializable]
    public partial class FunnelbackPushClassInfo : AbstractInfo<FunnelbackPushClassInfo>
    {
        #region "Type information"

        /// <summary>
        /// Object type
        /// </summary>
        public const string OBJECT_TYPE = "hm.funnelbackpushclass";


        /// <summary>
        /// Type information.
        /// </summary>
#warning "You will need to configure the type info."
        public static ObjectTypeInfo TYPEINFO = new ObjectTypeInfo(typeof(FunnelbackPushClassInfoProvider), OBJECT_TYPE, "HM.FunnelbackPushClass", "FunnelbackPushClassID", "FunnelbackPushClassLastModified", "FunnelbackPushClassGuid", "PushClassName", null, null, null, null, null)
        {
			ModuleName = "HM.Funnelback",
			TouchCacheDependencies = true,
        };

        #endregion


        #region "Properties"

        /// <summary>
        /// Funnelback push class ID
        /// </summary>
        [DatabaseField]
        public virtual int FunnelbackPushClassID
        {
            get
            {
                return ValidationHelper.GetInteger(GetValue("FunnelbackPushClassID"), 0);
            }
            set
            {
                SetValue("FunnelbackPushClassID", value);
            }
        }


        /// <summary>
        /// Push class name
        /// </summary>
        [DatabaseField]
        public virtual string PushClassName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushClassName"), String.Empty);
            }
            set
            {
                SetValue("PushClassName", value);
            }
        }


        /// <summary>
        /// Push collection name
        /// </summary>
        [DatabaseField]
        public virtual string PushCollectionName
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushCollectionName"), String.Empty);
            }
            set
            {
                SetValue("PushCollectionName", value);
            }
        }


        /// <summary>
        /// Push url path field
        /// </summary>
        [DatabaseField]
        public virtual string PushUrlPathField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushUrlPathField"), String.Empty);
            }
            set
            {
                SetValue("PushUrlPathField", value, String.Empty);
            }
        }


        /// <summary>
        /// Push title field
        /// </summary>
        [DatabaseField]
        public virtual string PushTitleField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushTitleField"), String.Empty);
            }
            set
            {
                SetValue("PushTitleField", value, String.Empty);
            }
        }


        /// <summary>
        /// Push content field
        /// </summary>
        [DatabaseField]
        public virtual string PushContentField
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushContentField"), String.Empty);
            }
            set
            {
                SetValue("PushContentField", value, String.Empty);
            }
        }


        /// <summary>
        /// Push other fields
        /// </summary>
        [DatabaseField]
        public virtual string PushOtherFields
        {
            get
            {
                return ValidationHelper.GetString(GetValue("PushOtherFields"), String.Empty);
            }
            set
            {
                SetValue("PushOtherFields", value, String.Empty);
            }
        }


        /// <summary>
        /// Push has categories
        /// </summary>
        [DatabaseField]
        public virtual bool PushHasCategories
        {
            get
            {
                return ValidationHelper.GetBoolean(GetValue("PushHasCategories"), false);
            }
            set
            {
                SetValue("PushHasCategories", value);
            }
        }


        /// <summary>
        /// Funnelback push class guid
        /// </summary>
        [DatabaseField]
        public virtual Guid FunnelbackPushClassGuid
        {
            get
            {
                return ValidationHelper.GetGuid(GetValue("FunnelbackPushClassGuid"), Guid.Empty);
            }
            set
            {
                SetValue("FunnelbackPushClassGuid", value);
            }
        }


        /// <summary>
        /// Funnelback push class last modified
        /// </summary>
        [DatabaseField]
        public virtual DateTime FunnelbackPushClassLastModified
        {
            get
            {
                return ValidationHelper.GetDateTime(GetValue("FunnelbackPushClassLastModified"), DateTimeHelper.ZERO_TIME);
            }
            set
            {
                SetValue("FunnelbackPushClassLastModified", value);
            }
        }

        #endregion


        #region "Type based properties and methods"

        /// <summary>
        /// Deletes the object using appropriate provider.
        /// </summary>
        protected override void DeleteObject()
        {
            FunnelbackPushClassInfoProvider.DeleteFunnelbackPushClassInfo(this);
        }


        /// <summary>
        /// Updates the object using appropriate provider.
        /// </summary>
        protected override void SetObject()
        {
            FunnelbackPushClassInfoProvider.SetFunnelbackPushClassInfo(this);
        }

        #endregion


        #region "Constructors"

		/// <summary>
        /// Constructor for de-serialization.
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected FunnelbackPushClassInfo(SerializationInfo info, StreamingContext context)
            : base(info, context, TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates an empty FunnelbackPushClassInfo object.
        /// </summary>
        public FunnelbackPushClassInfo()
            : base(TYPEINFO)
        {
        }


        /// <summary>
        /// Constructor - Creates a new FunnelbackPushClassInfo object from the given DataRow.
        /// </summary>
        /// <param name="dr">DataRow with the object data</param>
        public FunnelbackPushClassInfo(DataRow dr)
            : base(TYPEINFO, dr)
        {
        }

        #endregion
    }
}