using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace HM.Funnelback
{    
    /// <summary>
    /// Class providing FunnelbackPushClassInfo management.
    /// </summary>
    public partial class FunnelbackPushClassInfoProvider : AbstractInfoProvider<FunnelbackPushClassInfo, FunnelbackPushClassInfoProvider>
    {
        #region "Constructors"

        /// <summary>
        /// Constructor
        /// </summary>
        public FunnelbackPushClassInfoProvider()
            : base(FunnelbackPushClassInfo.TYPEINFO)
        {
        }

        #endregion


        #region "Public methods - Basic"

        /// <summary>
        /// Returns a query for all the FunnelbackPushClassInfo objects.
        /// </summary>
        public static ObjectQuery<FunnelbackPushClassInfo> GetFunnelbackPushClasses()
        {
            return ProviderObject.GetFunnelbackPushClassesInternal();
        }


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackPushClassInfo ID</param>
        public static FunnelbackPushClassInfo GetFunnelbackPushClassInfo(int id)
        {
            return ProviderObject.GetFunnelbackPushClassInfoInternal(id);
        }


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified name.
        /// </summary>
        /// <param name="name">FunnelbackPushClassInfo name</param>
        public static FunnelbackPushClassInfo GetFunnelbackPushClassInfo(string name)
        {
            return ProviderObject.GetFunnelbackPushClassInfoInternal(name);
        }


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified GUID.
        /// </summary>
        /// <param name="guid">FunnelbackPushClassInfo GUID</param>                
        public static FunnelbackPushClassInfo GetFunnelbackPushClassInfo(Guid guid)
        {
            return ProviderObject.GetFunnelbackPushClassInfoInternal(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackPushClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackPushClassInfo to be set</param>
        public static void SetFunnelbackPushClassInfo(FunnelbackPushClassInfo infoObj)
        {
            ProviderObject.SetFunnelbackPushClassInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackPushClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackPushClassInfo to be deleted</param>
        public static void DeleteFunnelbackPushClassInfo(FunnelbackPushClassInfo infoObj)
        {
            ProviderObject.DeleteFunnelbackPushClassInfoInternal(infoObj);
        }


        /// <summary>
        /// Deletes FunnelbackPushClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackPushClassInfo ID</param>
        public static void DeleteFunnelbackPushClassInfo(int id)
        {
            FunnelbackPushClassInfo infoObj = GetFunnelbackPushClassInfo(id);
            DeleteFunnelbackPushClassInfo(infoObj);
        }

        #endregion


        #region "Internal methods - Basic"
	
        /// <summary>
        /// Returns a query for all the FunnelbackPushClassInfo objects.
        /// </summary>
        protected virtual ObjectQuery<FunnelbackPushClassInfo> GetFunnelbackPushClassesInternal()
        {
            return GetObjectQuery();
        }    


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified ID.
        /// </summary>
        /// <param name="id">FunnelbackPushClassInfo ID</param>        
        protected virtual FunnelbackPushClassInfo GetFunnelbackPushClassInfoInternal(int id)
        {	
            return GetInfoById(id);
        }


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified name.
        /// </summary>
        /// <param name="name">FunnelbackPushClassInfo name</param>        
        protected virtual FunnelbackPushClassInfo GetFunnelbackPushClassInfoInternal(string name)
        {
            return GetInfoByCodeName(name);
        } 


        /// <summary>
        /// Returns FunnelbackPushClassInfo with specified GUID.
        /// </summary>
        /// <param name="guid">FunnelbackPushClassInfo GUID</param>
        protected virtual FunnelbackPushClassInfo GetFunnelbackPushClassInfoInternal(Guid guid)
        {
            return GetInfoByGuid(guid);
        }


        /// <summary>
        /// Sets (updates or inserts) specified FunnelbackPushClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackPushClassInfo to be set</param>        
        protected virtual void SetFunnelbackPushClassInfoInternal(FunnelbackPushClassInfo infoObj)
        {
            SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified FunnelbackPushClassInfo.
        /// </summary>
        /// <param name="infoObj">FunnelbackPushClassInfo to be deleted</param>        
        protected virtual void DeleteFunnelbackPushClassInfoInternal(FunnelbackPushClassInfo infoObj)
        {
            DeleteInfo(infoObj);
        }	

        #endregion
    }
}